%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Large Colored Title Article
% LaTeX Template
% Version 1.1 (25/11/12)

% This template has been downloaded from:
% http://www.LaTeXTemplates.com
%
% Original author:
% Frits Wenneker (http://www.howtotex.com)
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\def\fontpar{12pt}
\documentclass[DIV=calc, paper=a4, fontsize=\fontpar]{scrartcl}	 % A4 paper and 11pt font size
\usepackage[margin=3cm]{geometry}
\setlength{\parindent}{\fontpar}
\usepackage{indentfirst}
\usepackage{mathtools}
\DeclarePairedDelimiter\abs{\lvert}{\rvert}

\usepackage{graphicx}
%\usepackage{float}
\usepackage[english]{babel} % English language/hyphenation
\usepackage[protrusion=true,expansion=true]{microtype} % Better typography
\usepackage{amsmath,amsfonts,amsthm} % Math packages
\usepackage[svgnames]{xcolor} % Enabling colors by their 'svgnames'
\usepackage[hang, small,labelfont=bf,up,textfont=it,up]{caption} % Custom captions under/above floats in tables or figures
\usepackage{booktabs} % Horizontal rules in tables
\usepackage{fix-cm}	 % Custom font sizes - used for the initial letter in the document

\usepackage{sectsty} % Enables custom section titles
\allsectionsfont{\usefont{OT1}{phv}{b}{n}} % Change the font of all section commands

\usepackage{fancyhdr} % Needed to define custom headers/footers
\pagestyle{fancy} % Enables the custom headers/footers
\usepackage{lastpage} % Used to determine the number of pages in the document (for "Page X of Total")

% Headers - all currently empty
\lhead{}
\chead{}
\rhead{}

% Footers
\lfoot{}
\cfoot{}
\rfoot{\footnotesize Page \thepage\ of \pageref{LastPage}} % "Page 1 of 2"

\renewcommand{\headrulewidth}{0.0pt} % No header rule
\renewcommand{\footrulewidth}{0.4pt} % Thin footer rule

\usepackage{lettrine} % Package to accentuate the first letter of the text
\newcommand{\initial}[1]{ % Defines the command and style for the first letter
\lettrine[lines=3,lhang=0.3,nindent=0em]{
\color{DarkGoldenrod}
{\textsf{#1}}}{}}

%----------------------------------------------------------------------------------------
%	TITLE SECTION
%----------------------------------------------------------------------------------------

\usepackage{titling} % Allows custom title configuration

\newcommand{\HorRule}{\color{DarkGoldenrod} \rule{\linewidth}{1pt}} % Defines the gold horizontal rule around the title

\pretitle{\vspace{-3cm} \begin{flushleft} \HorRule \fontsize{42}{42} \usefont{OT1}{phv}{b}{n} \color{DarkRed} \selectfont} % Horizontal rule before the title

\title{Gaussian Processes for Terrain Mapping in Ground Robots} % Your article title

\posttitle{\par\end{flushleft}\vskip 0.5em} % Whitespace under the title

\preauthor{\begin{flushleft} \normalsize \lineskip 0.5em \usefont{OT1}{phv}{b}{sl} \color{DarkRed}} % Author font configuration
\author{Carlos Vido, } % Your name
\postauthor{\footnotesize \usefont{OT1}{phv}{m}{sl} \color{Black} % Configuration for the institution name
BSc in Molecular Sciences (University of S\~ao Paulo)% Your institution

\vspace{1mm}
\lineskip 0.5em \normalsize \usefont{OT1}{phv}{b}{sl} \color{DarkRed}
Dr. Fabio Ramos, 
\footnotesize \usefont{OT1}{phv}{m}{sl} \color{Black}
supervisor in the Learning and Reasoning Group (University of Sydney)

\vspace{1mm}
\lineskip 0.5em \normalsize \usefont{OT1}{phv}{b}{sl} \color{DarkRed}
Dr. Edwin Bonilla, 
\footnotesize \usefont{OT1}{phv}{m}{sl} \color{Black}
supervisor in the Machine Learning Research Group (NICTA)

\par\end{flushleft}\HorRule} % Horizontal rule after the title

\date{}% Add a date here if you would like one to appear underneath the title block




%----------------------------------------------------------------------------------------

\begin{document}

\maketitle % Print the title

\thispagestyle{fancy} % Enabling the custom headers/footers for the first page 

%----------------------------------------------------------------------------------------
%	ABSTRACT
%----------------------------------------------------------------------------------------

% The first character should be within \initial{}
% \initial{H}\textbf{ere is some sample text to show the initial in the introductory paragraph of this template article. The color and lineheight of the initial can be modified in the preamble of this document.}

%----------------------------------------------------------------------------------------
%	ARTICLE CONTENTS
%----------------------------------------------------------------------------------------
\vspace{-10mm}
\section*{Aims and Background}

Navigation is one of the most fundamental tasks that an autonomous robot must perform. It involves both a good understanding of the environment around the robot and the ability to discern among the regions which are safe and dangerous for its own integrity. Advances in this field will impact various others. Most notably, the mining industry may greatly benefit from streamlined automated mines, leading to more efficient extraction and larger profits.

Terrain modelling is a vital part of navigation: a precise model of the environment around leads to more efficient path planning. The aim of this project is to explore statistical machine learning techniques for non-stationary spatial modelling of unstructured environments. The main technique investigated will be Gaussian processes, which are able to generate continuous-domain non-parametric models.

The key objectives for the project are:
\begin{enumerate}
	\item{Investigation of known non-stationary kernels and elaboration of a new kernel suitable for the task at hand}
	\item{Investigation and development of variational inference methods for optimisation}
	\item{Investigation and development of informative data acquisition techniques for greater efficiency}
	\item{Application of the methods developed to robotic navigation in unstructured terrain}
\end{enumerate}

The outcome of this project will be a powerful statistical technique with the potential to revolutionise non-stationary spatial modelling. Although this research focuses on robotics, the findings achieved will certainly be applicable to a number of other fields, and potentially configure an important contribution to the machine learning community.

\subsection*{Terrain modelling}

It is essential for a robot expected to interact with its surroundings to have a reliable spatial representation of them. Possible interactions include navigating through them, object recognition and manipulation or simply determining its own position relative to known landmarks. That means spatial modelling is arguably one of the most fundamental tasks in robotics. The amount of research in this field in the past decades makes this conclusion even more evident.

The terrain mapping problem may be more simply expressed as the task of spatially modelling the robot's environment. This can be done in two main ways. The first is through a graph-like representation of important landmarks, connected via arcs that may contain information on how to get from a node to another. This is called a \emph{topological map} of terrain. This however isn't very readily applied to navigation.

Enter metric maps, which concern themselves with capturing geometric information about the environment by representing the area as a grid. An archetypical metric mapping technique is occupancy grids. They indicate occupied and free space regions in the robot surroundings. A good discussion of occupancy grids can be found in O'Callahan, 2012, in which a novel approach is also proposed.

Another metric approach would be to represent each point of the space in terms of its elevation. This is commonly called a digital elevation map (DEM), and is illustrated in Figure \ref{dem}. This is normally used to model the terrain around the robot. They are especially useful in natural spaces, which frequently lack regular and easily recognisable structures. Both DEMs and occupancy grids have the problems associated with discretisation: they may misrepresent a continuous phenomenon and the space and processing power needed to create and analyse the grid scale with the number of cells. This work will concern itself with presenting an alternative to grid techniques in terrain modelling.

\begin{figure*}[h]
\center
\includegraphics[width = \textwidth]{dem.png}
\caption{Conventional DEM. In Ishigami \textit{et al.}, 2013.\label{dem}}
\end{figure*}

\subsection*{Gaussian Processes}

A Gaussian process (GP) can be thought of as the generalisation of a Gaussian distribution over a finite vector space to a function space of infinite dimension, according to David MacKay (MacKay, 2003). Instead of deriving its properties from a mean and covariance matrix, a GP is characterised by a pair of analogous and homonymous functions. Given a function $y(\mathbf{x})$ and an input set $\mathbf{X}_{N} \equiv \{\mathbf{x}^{(n)}\}_{n=1}^{N}$, the mean is a function of $\mathbf{x}$, often taken to be the zero function, and the covariance is a function $C(\mathbf{x}_1, \mathbf{x}_2)$ expressing the expected covariance of function $y$ at points $\mathbf{x}_1$ and $\mathbf{x}_2$. The function $y(\mathbf{x})$ is assumed to be a single sample from the distribution.

The idea behind its is obtaining a prior distribution $P(y(\mathbf{x}))$ without having to parameterise $y$, by placing it directly on the infinite function space. Given a target value space $\mathbf{t}_{N} \equiv \{t_{n}\}_{n=1}^{N}$, we can then use the Bayes rule to calculate the inference of $y(\mathbf{x})$ given by $P(y(\mathbf{x})|\mathbf{t}_{N}, \mathbf{x}_{N})$. But since we prescinded from parameterising $y$, no assumptions have to be made over its properties, setting it apart from approaches that require parametrisation. Beyond that, unlike other Bayesian methods, Gaussian processes normally return functions which are analytically solvable.

Figure \ref{gp} offers a visual intuition of GPs. It represents sample functions drawn from the prior and posterior distribution of a Gaussian process which favours smooth functions. This property is specified by it's covariance function. For the prior, the mean function is taken to be the zero function, but this not mean that the mean value of each function in the distribution is zero. After data points are introduced, the mean prediction is adjusted accordingly. Notice that the standard deviation is proportional to the distance to points for which the target value is known.

\begin{figure*}[h]
\center
\includegraphics[width = 0.9\textwidth]{gp.png}
\caption{An intuitive visual representation of GPs, taken from Rasmunssen and Williams, 2006. Panel (a) shows four samples drawn from the prior distribution. Panel (b) shows the situation after two data points have been observed. The solid line represents the mean function. Dashed lines represent other functions in the distribution, randomly chosen. In both plots the shaded region denotes twice the standard deviation at each input value x.\label{gp}}
\end{figure*}

\section*{Research Project}
\subsection*{Significance and Innovation}

In the dawn of their existence, autonomous systems were confined to industrial environments, where they worked in assembly lines performing repetitive tasks. This was an early presage of what kinds of tasks they would be performing in modern society: nowadays, robots are widely used for tasks perceived as too dirty (\textit{e.g.} cleaning the inside of long pipes and ducts), dangerous (\textit{e.g.} defusing bombs or exploring hazardous environments) or dull (\textit{e.g.} the aforementioned assembly lines or domestic robots) for humans.

There is also a trend towards removing robots from industrial environments and inserting them in the outdoor world, as is the case with self-driven cars. In those situations, they must negotiate moving obstacles (such as people or vehicles), unknown or unpredictable regions and unstructured terrain. While this is obvious in the case of off-road vehicles, even urban cars must be able to avoid potholes, bumps and other irregularities on the road.

Therefore, the need for robust terrain modelling algorithms---to provide robots with a good notion of where they are and what their environment is like---is greater than it has ever been. Current popular algorithms are grid-based, which makes them very space- and processing power-demanding as map size or resolution increases.

This project proposes to create a non-stationary terrain modelling algorithm that models the terrain using a math function, resulting in unlimited resolution and allowing maps to grow without interfering as much in the computational demands. Bigger, higher resolution maps will allow for reliable localisation and navigation in large, unstructured and possibly hazardous areas, either natural---like volcanoes, deserts or other planets---or artificial---such as mines or disaster areas.

\subsection*{Approach and Methodology}

The first step is  to understand GPs and their use in this research context. Applied to terrain modelling, the task is to derive a model for the predicted distribution of target elevations $y_{new}$ for each new input location $\mathbf{x_{new}}$ starting from a set $\mathcal{D}$ of known locations and corresponding elevations. This can be expressed as $p(y_{new} | \mathbf{x_{new}}, \mathcal{D})$, for $\mathcal{D} = \{x_i, y_i\}_{i=1}^n$, and it is essentially a non-parametric regression approach.

But $\mathcal{D}$ comes from sensor readings, which presents a challenge: sensorial data is inherently noisy and unevenly distributed. This means there might be gaps in the data. They must be handled according to size: small ones can be filled with high confidence, but larger ones should result in lower confidence. In addition to that, structural elements in real terrain aren't necessarily uniform in their smoothness: a sharp edge or corner might be found in an otherwise smooth terrain. This must be preserved by the model, as it might be an important feature for localisation or path planning.

GPs are a good tool for this task, since their standard implementation already account for the data noise and uneven distribution. However, it tends to weaken the sharp features we want to preserve. Paciorek and Schervish (2004) proposed a solution for this in the form of non-stationary covariance functions.

The authors proposed a technique to build non-stationary covariance functions starting from stationary ones. Instead of taking the difference between pairs of input locations, each input location $\mathbf{x}$ is assigned a Gaussian kernel matrix $\Sigma$. The covariance function then takes the form

\begin{equation*}
C(\mathbf{x}_i, \mathbf{x}_j) = \abs*{\Sigma_i}^{\frac{1}{4}} \abs*{\Sigma_j}^{\frac{1}{4}} \abs*{\frac{\Sigma_i + \Sigma_j}{2}}^{-\frac{1}{2}} \exp{\left[-(\mathbf{x}_i - \mathbf{x}_l)^T \left(\frac{\Sigma_i + \Sigma_j}{2}\right)^{-1} (\mathbf{x}_i - \mathbf{x}_k)\right]},
\end{equation*}

\noindent and the covariance between two target elevations $y_i$ and $y_j$ is calculated from averaging between the kernels at input locations $\mathbf{x}_i$ and $\mathbf{x}_j$. This means that the characteristics of the input locations influence the covariance of the target elevations.

The kernel used by Paciorek and Schervish, however, was obtained by placing additional GP priors on the kernel matrix parameters. Their solution, while flexible and general, demanded too much processing power to be applied to the real world. Several kernels can be used instead of this one.

Lang \textit{et al.} used a kernel based on a computer vision algorithm, through a technique they called local kernel adaptation. They defined a tensor that captures the local structure of the terrain by averaging over the elevation gradient in the neighbourhood of a given input location $\mathbf{x}_i$. The tensor is represented by a $2 \times 2$ matrix. If the terrain is flat around $\mathbf{x}_i$, the elevation gradients are small, and so are the tensor's eigenvalues. Conversely, ascending terrain would cause one of the eigenvalues to be greater, and its eigenvector's orientation angle would point towards the strongest ascent. By taking the inverse of this matrix as kernel, flat areas will be populated by large, isotropic kernels, while sharp edges will give long, thin kernels oriented along the edge directions, as can be seen in figure \ref{lang}. Their model was very effective in accounting for gaps in the data in terrains containing sharp discontinuities, and yields the predicted uncertainties as well as the terrain model.

\begin{figure*}[h]
\center
\includegraphics[width = 0.9\textwidth]{kernels.png}
\caption{Visual representation of the kernels obtained by Lang \textit{et al.} (2004). To the left, an artificial terrain with a sharp edge. To the right, the shape of the kernels in the region.\label{lang}}
\end{figure*}


Theirs is not the only option to stationary kernels. In Vasudevan \textit{et al.} (2009), a neural-network (NN) kernel, a different kind of non-stationary kernel that is also very apt at modelling sharp edges, has its performance compared to the Gaussian kernel. The NN kernel is found to be powerful enough to model terrain data in large scale applications, and a local approximation technique that uses k-dimensional trees to improve the algorithm's scalability without compromising the modelling of sharp spatial features in the terrain.

For the core of this work, the candidate will analyse these and other non-stationary algorithms in order to develop a new kernel, addressing the weaknesses of previous works. A possible line of action is trying to implement the NN kernel using sparse matrices, which will considerably speed up the transforms and multiplications in the process and ultimately increase the performance of the algorithm.

The equations in the model might be intractable due to the number of terms. If that is the case, variational inference methods will be used to deal with this kind of problem. Variational inference is a method that has its origins in the calculus of variations, and it is used to reduce the range of functions the model comprises by setting restrictions on their properties---\textit{e.g.} only allowing for linear combinations of a fixed set of basis functions.

Another problem that may arise is that GP complexity grows with the number of data points. This is an obvious concern because point cloud data sets for large areas may comprise hundreds of thousands of points. Informative data acquisition techniques can be utilised to make sure data points will only be added to the model when they can make an important contribution. Thus we can avoid adding redundant points to well-known regions, which'd increase the complexity of the algorithm without improving much the precision of the model.

The field tests will be conducted within the University and its partner institutions, using simple ground vehicles equipped with laser rangefinders tilted to aim at the ground in front of them. The algorithm will be implemented assuring compatibility with ROS (Robot Operating System), a widespread framework for robotics. They should be general enough to be used by any autonomous ground vehicle.

The test areas can be any regions with unstructured terrains, and it would be interesting to use regions with different topological properties. The Mars Yard, a small simulated Mars environment in the Powerhouse Museum in Sydney, will be used for small-scale tests in unstructured environment with a few rocks and boulders. Medium-scale tests in a relatively smooth surface can be performed with data acquired from the Victoria Park, due to its convenient location near the University of Sydney. For large scale tests, the candidate can use the elevation datasets from several mines in Australia used by Vasudevan et al. in his research. That would allow the candidate to compare his algorithm to the ones already in existence.

\subsection*{Proposed Timeline}

\textbf{First year}
\begin{itemize} \itemsep 5pt \parskip 0pt 
	\item Theoretical background acquisition: introduction to statistic machine learning, basic robotics, Gaussian processes and terrain mapping
	\item Technical background acquisition: introduction to ROS and other such resources in robotics.
\end{itemize}

\textbf{Second year}
\begin{itemize} \itemsep 5pt \parskip 0pt 
	\item Algorithm development
	\item Field testing in smaller environments (\textit{e.g.} Mars Yard) and subsequent corrections and optimisations in the algorithm
\end{itemize}

\textbf{Third year}
\begin{itemize} \itemsep 5pt \parskip 0pt 
	\item Field testing in larger environments (\textit{e.g.} Victoria Park) and subsequent corrections and optimisations in the algorithm
	\item Journal paper and thesis submission
\end{itemize}

\subsection*{Feasibility and Risk}

The project can be divided in two major components: theoretical and practical.

In the theoretical part, existing statistical machine learning approaches will be studied. In special, Gaussian process kernels will be investigated and new ones will be proposed. During this, the candidate will have support and assistance from his supervisors and colleagues in the Learning and Reasoning Group. He will also have access to University of Sydney's physical and online resources, such as lectures and seminars, libraries etc.

During the application of the knowledge thus acquired, he will have access to the robots of the group as well as those of ACFR. Besides the data collection the candidate will perform in suitable venues, the group also has datasets with point clouds acquired in much larger areas, including several real mines in Australia.

Most risks are those associated with experimental research: equipment unavailability due to breakdown (or other unpredictable inconveniences) and technical impossibility of applying the proposed techniques (\textit{e.g.}, the algorithm ends up being too slow to be used in real time). Equipment unavailability can't be readily addressed due to its unpredictability, but in order to avoid it, the student will have access to more than one robot during his research. To address technical risks, frequent small-scale trials can be run to assure the complexity of the algorithm is within manageable ranges.

\subsection*{National Benefit}

Humanity is marching into the era of robotics. Autonomous and semi-autonomous systems are becoming more and more widespread, and performing all sorts of tasks, from the Roomba that vacuums our houses to the Spirit and Opportunity rovers which probe the surface of Mars.

This project will allow robots to improve their navigation capabilities in unstructured environments through the use of statistical models. A better modelling of the environment that surrounds them will allow to decrease the risk of their deployment in field applications, such as planetary exploration, defence-related recognition and mapping tasks and, most notably, mine automation.

It is worth mentioning that Brazil has 72 different mineral products. The mining industry's GDP is in the tens of millions of dollars (DNPM, 2011). As of 2013, the sector accounts for $27\%$ of the country's GDP, and it's growth is expected to continue (Mining IQ, 2013). Automated mines may play a central role in improving mining efficiency in the future, and that would impact Brazilian products' competitiveness in the international market.

\section*{Research Environment}
This project will be developed in conjunction with Doctor Fabio Ramos's Learning and Reasoning research group in the School of Information Technology (SIT) of the University of Sydney and the Machine Learning Research Group (MLRG) in NICTA (National Information and Communication Technology Australia), under supervision of Doctor Edwin Bonilla.

\subsection*{University of Sydney}
The Learning and Reasoning Group focuses in Machine Learning techniques, and its members have numerous publications in the areas of robotic navigation, multi-modal data integration and environmental monitoring. Doctor Ramos himself counts among his interests Bayesian statistics and stochastic processes for spatial modelling, and his experience in these fields shall be useful for this project.

In addition to the facilities offered by the SIT---which include a research laboratory equipped with a few small robots---, the group also has access to the Australian Centre for Field Robotics (ACFR). The latter is part of Australian Research Council Centre of Excellence for Autonomous Systems, and undertakes research in numerous areas related to autonomous perception, control and learning capabilities in land, air and sea-based systems.

\subsection*{NICTA}
NICTA's MLRG researches in various topics in machine learning; graphical models, large-scale machine learning, structured prediction and topic modelling are a few. It is Australia's ICT Research Centre of Excellence, and thus gathers several highly competent researchers in the field.

Doctor Bonilla also has several publications in Bayesian learning techniques and Gaussian Processes in particular, and his expertise will no doubt be of great value for the development of this project.

\subsection*{Results Communication}
The expectation is for the candidate to publish his intermediary results in top-quality conferences, such as the ICRA (International Conference on Robotics and Automation) and the IJCAI (International Joint Conference on Artificial Intelligence). Once all the most relevant experiments are concluded, a journal paper should also be submitted to a respected journal within the field, such as the IJRR (International Journal of Robotics Research).

\section*{Resources}
For the development of this program, the only resources needed are a ground robot equipped with a laser rangefinder, of which the Learning and Reasoning Group already owns a few of different sizes and capabilities. Initially, the main robot used will be the Wombot, a small four-wheeled, skid-steered autonomous vehicle developed by Roman Marchant, a PhD candidate in the group. The group also has access to a customised Husky, from Clearpath Robotics, and Mantis and Shrimp (figure \ref{segs}), two robots developed by the Australian Centre of Field Robotics (ACFR).

\begin{figure*}[h!]
\center
\includegraphics[width = 0.88\textwidth]{husky.png}\\
(a), Husky\\
\includegraphics[width = 0.88\textwidth]{segs.png}\\
(b), Mantis (left) and Shrimp (right)
\caption{The robots available in ACFR. Figure (a) shows a standard Clearpath Robotics Husky with no sensors attached (taken from ROS website). Figure (b) shows the Mantis and the Shrimp, two ground robots carrying a cutting-edge payload of sensors (cameras, lasers, radar) and able to log huge amounts of data (~500 GB/hour or 150MB/sec) (taken from ACFR website).\label{segs}}
\end{figure*}

A reasonably sized stretch of unstructured terrain is necessary to test the algorithms developed. Any of Sydney's parks would be suitable for this. Victoria's Park offers a $2.6km^2$ area close to the University's buildings, where the robots are located. Also available for this project is the Mars Yard(figure \ref{mars}), a small-scale replication of the Martian surface within Sydney's Powerhouse Museum. The facility is a research collaboration between the University of Sydney's ACFR and the University of New South Wales's Australian Centre for Astrobiology.

\begin{figure*}[h!]
\center
\includegraphics[width = 0.9\textwidth]{mars.jpg}\\
\caption{Picture of the Mars Yard and one of the ACFR rovers used therein by students for robotics research (taken from the Mars Yard website). \label{mars}}
\end{figure*}


%----------------------------------------------------------------------------------------
%	REFERENCE LIST
%----------------------------------------------------------------------------------------
%\newpage
%
\begin{thebibliography}{99} % Bibliography - this is intentionally simple in this template
\fontsize{10}{12}\selectfont

\bibitem[\textbf{MacKay, 2003}]{book1}
David MacKay:
\newblock Information Theory, Inference, and Learning Algorithms.
\newblock {\em Cambridge University Press}, 535--548 (2003)

\bibitem[\textbf{Paciorek and Schervish, 2004}]{paper0}
Chris Paciorek and Mark Schervish:
\newblock Nonstationary covariance functions for Gaussian process regression.
\newblock {\em Procedings of Neural Information Processing Systems Conference (NIPS)} (2004)

\bibitem[\textbf{Rasmunssen and Williams, 2006}]{book2}
Carl Edward Rasmussen and Christopher Williams:
\newblock Gaussian Processes for Machine Learning.
\newblock {\em The MIT Press,}, 3-5 (2006)

\bibitem[\textbf{Lang \textit{et al.}, 2007}]{paper1}
Tobias Lang, Christian Plagemann and Wolfram Burgard:
\newblock Adaptive Non-Stationary Kernel Regression for Terrain Modeling.
\newblock {\em Proceedings of the Robotics: Science and Systems Conference (RSS)} (2007)

\bibitem[\textbf{Vasudevan \textit{et al.}, 2009}]{paper2}
Shrihari Vasudevan, Fabio Ramos, Eric Nettleton, and Hugh Durrant-Whyte:
\newblock Gaussian process modelling of large-scale terrain.
\newblock {\em  Journal of Field Robotics}, 26(10): 812--840 (2009)

\bibitem[\textbf{DNPM, 2011}]{report}
Jo\~ao C\'esar de Freitas Pinheiro:
\newblock A Import\^ancia Econ\^omica da Minera\c{c}\~ao no Brasil.
\newblock {\em Apresenta\c{c}\~ao RENAI} (2011)

\bibitem[\textbf{O'Callahan, 2012}]{thesis}
Simon O'Callahan:
\newblock Continuous Occupancy Maps for the Representation of Unstructured Environments.
\newblock {\em PhD thesis, University of Sydney} (2012)

\bibitem[\textbf{Mining IQ, 2013}]{magazine}
Mining IQ Editorial:
\newblock Brazilian Mining Industry Overview: A Reflection on the Current Marketplace.
\newblock {\em Mining IQ} (2013)

\bibitem[\textbf{Ishigami \textit{et al.}, 2013}]{paper4}
Genya Ishigami, Masatsugu Otsuki and Takashi Kubota:
\newblock Range-dependent Terrain Mapping and Multipath Planning using Cylindrical Coordinates for a Planetary Exploration Rover.
\newblock {\em Journal of Field Robotics}, 30(4): 536–551 (2013)

\end{thebibliography}

%----------------------------------------------------------------------------------------

\end{document}