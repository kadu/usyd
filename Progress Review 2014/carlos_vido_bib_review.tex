%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Large Colored Title Article
% LaTeX Template
% Version 1.1 (25/11/12)

% This template has been downloaded from:
% http://www.LaTeXTemplates.com
%
% Original author:
% Frits Wenneker (http://www.howtotex.com)
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\def\fontpar{12pt}
\documentclass[DIV=calc, paper=a4, fontsize=\fontpar, twocolumn]{scrartcl}	 % A4 paper and 11pt font size
%\usepackage[margin=3cm]{geometry}
\setlength{\parindent}{\fontpar}
\usepackage{indentfirst}
\usepackage{mathtools}
\DeclarePairedDelimiter\abs{\lvert}{\rvert}

\usepackage{graphicx}
\usepackage{float}
\usepackage[english]{babel} % English language/hyphenation
\usepackage[protrusion=true,expansion=true]{microtype} % Better typography
\usepackage{amsmath,amsfonts,amsthm} % Math packages
\usepackage[svgnames]{xcolor} % Enabling colors by their 'svgnames'
\usepackage[hang, small,labelfont=bf,up,textfont=it,up]{caption} % Custom captions under/above floats in tables or figures
\usepackage{booktabs} % Horizontal rules in tables
\usepackage{fix-cm}	 % Custom font sizes - used for the initial letter in the document

\usepackage{sectsty} % Enables custom section titles
\allsectionsfont{\usefont{OT1}{phv}{b}{n}} % Change the font of all section commands

\usepackage{fancyhdr} % Needed to define custom headers/footers
\pagestyle{fancy} % Enables the custom headers/footers
\usepackage{lastpage} % Used to determine the number of pages in the document (for "Page X of Total")

% Headers - all currently empty
\lhead{}
\chead{}
\rhead{}

% Footers
\lfoot{}
\cfoot{}
\rfoot{\footnotesize Page \thepage\ of \pageref{LastPage}} % "Page 1 of 2"

\renewcommand{\headrulewidth}{0.0pt} % No header rule
\renewcommand{\footrulewidth}{0.4pt} % Thin footer rule

\usepackage{lettrine} % Package to accentuate the first letter of the text
\newcommand{\initial}[1]{ % Defines the command and style for the first letter
\lettrine[lines=3,lhang=0.3,nindent=0em]{
\color{DarkGoldenrod}
{\textsf{#1}}}{}}

%----------------------------------------------------------------------------------------
%	TITLE SECTION
%----------------------------------------------------------------------------------------

\usepackage{titling} % Allows custom title configuration

\newcommand{\HorRule}{\color{Black} \rule{\linewidth}{1pt}} % Defines the gold horizontal rule around the title

\pretitle{\vspace{-3cm} \begin{flushleft} \HorRule \fontsize{40}{40} \usefont{OT1}{phv}{b}{n} \color{Black} \selectfont} % Horizontal rule before the title

\title{Bibliography Review} % Your article title

\posttitle{\par\end{flushleft}} % Whitespace under the title

\preauthor{\begin{flushleft} \normalsize \usefont{OT1}{phv}{b}{sl} \color{Black}} % Author font configuration
\author{Carlos Vido, } % Your name
\postauthor{\footnotesize \usefont{OT1}{phv}{m}{sl} \color{Black} % Configuration for the institution name
SID 430063976, PhD candidate (University of Sydney) % Your institution

\flushleft{\small \usefont{OT1}{phv}{n}{sl}{May 2014}}
\par\end{flushleft}\HorRule} % Horizontal rule after the title

\date{}% Add a date here if you would like one to appear underneath the title block




%----------------------------------------------------------------------------------------

\begin{document}

\maketitle % Print the title

\thispagestyle{fancy} % Enabling the custom headers/footers for the first page 

%----------------------------------------------------------------------------------------
%	ABSTRACT
%----------------------------------------------------------------------------------------

% The first character should be within \initial{}
% \initial{H}\textbf{ere is some sample text to show the initial in the introductory paragraph of this template article. The color and lineheight of the initial can be modified in the preamble of this document.}

%----------------------------------------------------------------------------------------
%	ARTICLE CONTENTS
%----------------------------------------------------------------------------------------
\vspace{-10mm}
\section*{Introduction}

In outdoor environments, robots and autonomous ground vehicles must be able to navigate through unknown areas on unstructured terrain. To accomplish that, it is essential that they gather sensory information from their surroundings to determine the traversability of a stretch of terrain that crosses their path, enabling them to decide whether or not to change direction. Creating a map of the terrain in the process allows for more efficient path planning when revisiting known areas in the future.
Robots are frequently used in tasks too dangerous or inaccessible for human beings; exploration or search and rescue missions in hazardous areas and extra-terrestrial excursions are a few examples. Given that frequently a robot in these situations will have to negotiate obstacles and rough areas, terrain mapping, traversability analysis and path planning are important parts of autonomous navigation.
The aim of this work is to propose a novel terrain mapping routine using Gaussian processes to model the environment. It should enable a robot to create a map of a region while navigating through it. The map should then be used to determine where is safe to go next, either in order to expand the map or aiming to reach a particular destination.

\section*{Theoretical Background}

\subsection*{Mapping}

It is essential for a robot expected to interact with it's surroundings---be it navigating through them, interacting with external elements or simply determining it's own position---to have a reliable spatial representation of them. Therefore, mapping is arguably one of the most fundamental tasks in robotics, which is made even more evident by the amount of research in this field in the past decades.
The mapping problem may be more simply expressed as the task of spatially modelling the robot's environment, which can be done in two main ways. The first is through a graph-like representation of important landmarks, connected via arcs that may contain information on how to get from a node to another. This is called a topological map. This however isn't very readily applied to navigation.
Enter metric maps, which concern themselves with capturing geometric information about the environment by representing the area as a grid. The prototypical metric mapping technique are occupancy grids, which indicate occupied and free space regions in the robot surroundings. A good discussion of occupancy grids can be found in O'Callahan, 2012, in which a novel approach is also proposed.
Another metric approach would be to represent each point of the space in terms of it's elevation. This is commonly called a digital elevation map (as illustrated in figure \ref{dem}), and normally used to model the terrain around the robot, especially in natural spaces, that frequently lack regular and easily recognisable structures. This kind of modelling is going to be the main concern of this work, and it'll be further discussed in the "related work" section.

\begin{figure*}[htp]
\center
\includegraphics[width = \textwidth]{dem.png}
\caption{Conventional DEM. In Ishigami \textit{et al.}, 2013.\label{dem}}
\end{figure*}

\subsection*{Traversability}

Traversability is the name given to the ability to access, cross and leave a certain area without suffering any damage or undesirable consequence, such as hitting a wall, being toppled over or getting stuck. In a recent survey, Panagiotis Papadakis classified the mainstream existing methodologies into two main branches: proprioceptive and exteroceptive. (Papadakis, 2013)
Proprioceptive analysis comprises strategies to classify a given region as the vehicle traverses it. By taking into consideration inputs such as wheel slippage, impacts, vibrations etc, the terrain is classified according to the difficulty it presents to traversal. The obvious drawback of these techniques, when used by themselves, is that the robot has to test each part of the terrain, which in extreme situations might lead to it's destruction, impairment or entrapping.
Exteroceptive analysis, on the other hand, relies on the use of sensory information, and Papadakis further divides it into geometry- or appearance-based. The first category, the most widely used, relies predominantly on the analysis of elevation maps, especially powerful when used in conjunction with dense point clouds such as the ones obtained by laser rangefinders. This type of sensor, however, has high energy consumption, which may be a problem in applications that limit or deny the robot's access to energy. The second one utilises images of the path ahead of the robot to reduce the path-planning to an image classification task, relying on the fact that frequently different types of terrains have different appearances. The downside of this approach is that image processing requires a lot of processing, which may be detrimental in situations that require fast decisions by the robot. Moreover, it may be rendered useless or severely impaired in situations of low visibility.
There is an evident complementarity between both exteroceptive analysis methods: appearance-based methods may be more effective in determining slippage by analysing texture, whereas geometry-basedmethods are more reliable in situations of poor visibility. Both methods can be therefore combined to obtain more complete traversability maps, which can then be further enriched by proprioceptive methods once the robot's path is restricted to relatively safe areas.

\subsection*{Gaussian Processes}

A Gaussian process (GP) can be thought of as the generalisation of a Gaussian distribution over a finite vector space to a function space of infinite dimension, according to David MacKay (MacKay, 2003). Instead of deriving its properties from a mean and covariance matrix, a GP is characterised by a pair of analogous and homonymous functions. Given a function $y(\mathbf{x})$ and an input set $\mathbf{X}_{N} \equiv \{\mathbf{x}^{(n)}\}_{n=1}^{N}$, the mean is a function of $\mathbf{x}$, often taken to be the zero function, and the covariance is a function $C(\mathbf{x}_1, \mathbf{x}_2)$ expressing the expected covariance of function $y$ at points $\mathbf{x}_1$ and $\mathbf{x}_2$. The function $y(\mathbf{x})$ is assumed to be a single sample from the distribution.

The idea behind its use is obtaining a prior distribution $P(y(\mathbf{x}))$ without having to parameterise $y$, by placing it directly on the infinite function space. Given a target value space $\mathbf{t}_{N} \equiv \{t_{n}\}_{n=1}^{N}$, we can then use the Bayes rule to calculate the inference of $y(\mathbf{x})$ given by $P(y(\mathbf{x})|\mathbf{t}_{N}, \mathbf{x}_{N})$. But since we prescinded from parameterising $y$, no assumptions have to be made over its properties, setting it apart from approaches that require parametrisation. Beyond that, unlike other Bayesian methods, Gaussian processes normally return functions which are analytically solvable.

%Figure \ref{gp} offers a visual intuition of GPs. It represents sample functions drawn from the prior and posterior distribution of a Gaussian process which favours smooth functions. This property is specified by it's covariance function. For the prior, the mean function is taken to be the zero function, but this not mean that the mean value of each function in the distribution is zero. After data points are introduced, the mean prediction is adjusted accordingly. Notice that the standard deviation is proportional to the distance to points for which the target value is known.

%\begin{figure*}[htp]
%\center
%\includegraphics[width = 0.9\textwidth]{gp.png}
%\caption{An intuitive visual representation of GPs, taken from Rasmunssen and Williams, 2006. Panel (a) shows four samples drawn from the prior distribution. Panel (b) shows the situation after two data points have been observed. The solid line represents the mean function. Dashed lines represent other functions in the distribution, randomly chosen. In both plots the shaded region denotes twice the standard deviation at each input value x.\label{gp}}
%\end{figure*}

\section*{Related Work}
\subsection*{Kernel regression for sharp edge preservation in smooth surfaces}

One of the challenges in terrain mapping is to map smooth surfaces containing sharp strong edges (see figure \ref{lang}, left panel, for an example), because these are important features for path planning, localisation and object recognition. This has been solved in the past by using an approach based on Gaussian processes (Lang \textit{et al.}, 2007).


\begin{figure}[h]
\center
\includegraphics[width = 0.45\textwidth]{kernels.png}
\caption{Visual representation of the kernels obtained by Lang \textit{et al.} (2004). To the left, an artificial terrain with a sharp edge. To the right, the shape of the kernels in the region.\label{lang}}
\end{figure}

In their paper, Lang \textit{et al.} propose a method to create an elevation map from a set of data points, which could be obtained by a laser rangefinder. They start by using an algorithm based on conjugate gradients to learn in the Gaussian process framework. Although this approach tackles three terrain modelling challenges---namely sensor noise, non-uniformity in data point density and uncertainties arising from gaps in data---, it smooths the sharp edges we want to preserve.
To deal with this non-trivial problem, rather than using the then-predominant stationary covariance functions in the Gaussian process, they use a nonstationary covariance approach previously proposed: by calculating an individual Gaussian kernel matrix for each input location, they take the covariance between two targets to be the average between the kernels in the corresponding locations. This demands a Markov-chain Monte Carlo approach. This however is too computationally demanding to be applied to real-world problems.
In order to succesfully apply this to terrain mapping, the authors utilise findings from the computer vision field, which has extensively studied the problem of adapting smoothing kernels to local structures. The technique they use consists on defining a tensor that calculates the locally average of the outer product of the local elevation derivative. The tensor is representable by a 2�2 matrix that describes how the terrain changes in the local neighborhood of a given location. The practical result is that flat, smooth areas have large isotropic kernels, while discontinuities have thin elongated kernels oriented along their edges, as illustrated in the right panel of figure \ref{lang}.
Through experiments using both artificially generated data and field observations, the proposed algorithm was tested and proven to be successful. They compared the results obtained by their technique to state-of-the-art algorithms at the time of the publishing, and achieved up to 70 percent smaller prediction errors. Figure \ref{lang2} demonstrates the obtained results in artificial data.

\begin{figure*}[htp]
\center
\includegraphics[width = 0.9\textwidth]{artdata.png}
\caption{Demonstration of the local kernel adaptation process on artificially generated terrain data. The data set, shown in the first row, was designed to contain several local features hard to adapt to. Noise was added to make the surfaces rougher and more similar to natural environments, and is illustrated in the second column of this row. The second row depicts the results of standard GP learning and regression The two following rows show the results after one and three iterations of kernel adaptation. The leftmost column, the kernel dimensions and orientations after each corresponding iteration are shown. The second column represents the predicted means of the regression. The third and fourth column respectively allow us to visualise the absolute errors to the known ground truth elevations and the resulting learning rates for the next adaptation step, resulting from the estimated data likelihoods. Lang \textit{et al.} (2004)\label{lang2}}
\end{figure*}


\subsection*{Path planning in digital elevation maps using cylindrical coordinates}

\begin{figure*}[htp]
\center
\includegraphics[width = \textwidth]{cdem.png}
\caption{Cylindrical coordinate DEM. In Ishigami \textit{et al.}, 2013.\label{cdem}}
\end{figure*}


Robot navigation in unstructured terrain relies heavily on consistent and reliable terrain mapping and path-planning techniques. A common approach, as previously discussed, is to use digital elevation maps (DEMs) created from point clouds acquired by laser rangefinders. The DEM is defined by a series of elevations along with discrete nodes, and conventionally utilizes a square-shaped reference grid. Given the way rangefinders work, the average number of data points used to represent a single node varies along with the distance between this cell and the robot, resulting in the omission of larges amounts of data in near nodes.
This has been considered by Ishigami \textit{et al.} (2013), who propose a sector-shaped reference grid, as opposed to more traditional square grids illustrated in figure \ref{dem} on page 2. They termed this approach cylindrical coordinate DEM, or C2DEM, and it is represented on figure \ref{cdem}. A cylindrical grid is more dense near the robot and gets sparser as distance grows, in a fashion that less data points are wasted in this approach, as illustrated by figure \ref{ishi}. Another advantage is that, when tracing a path, a square grid limits the possible headings of the robot to 45deg increments, while the cylindrical one provides a wider variety of possibilities.


\begin{figure}[h]
\center
\includegraphics[width = 0.45\textwidth]{ishi.png}
\caption{Sampling ratio from point cloud to node in DEM and C2DEM conversion. Ishigami \textit{et al.} (2007).\label{ishi}}
\end{figure}
Using the elevation map proposed, the team then created a path-planning method consisting of three indices. The terrain inclination index, divided in pitch and roll, accounts for the inclination of the robot within a particular region; the roughness index is calculated as the standard deviation of the elevations of each point within a region (after normalising inclination according to the previous index); and the path length index, which favours shorter paths. Each is then multiplied by a weighting factor and normalised to be used in the calculation the cost ofRobot navigation in unstructured terrain relies heavily on consistent and reliable terrain mapping and path-planning techniques. A common approach, as previously discussed, is to use digital elevation maps (DEMs) created from point clouds acquired by laser rangefinders. The DEM is defined by a series of elevations along with discrete nodes, and conventionally utilizes a square-shaped reference grid. Given the way rangefinders work, the average number of data points used to represent a single node varies along with the distance between this cell and the robot, resulting in the omission of larges amounts of data in near nodes.
The authors go further to propose a multipath planning strategy, by attributing different sets of weights in the cost-calculation step. A evaluation method is then established that analyses the cost to move between nodes, the path length, the rollover risk and the stall risk. The minimum value for this metric indicates the most feasible path, which is indicated as a series of waypoints. A comparison of the performance of both DEM techniques in various paths is available on figure \ref{ishi2}.

\begin{figure}[h]
\center
\includegraphics[width = 0.45\textwidth]{ishi2.png}
\caption{Table showing performance comparison between conventional DEM and proposed C2DEM. Ishigami \textit{et al.} (2007).\label{ishi2}}
\end{figure}

\subsection*{Large scale terrain modelling}

Some robotics applications, such as mining or agriculture, require that the robot operates in a terrain that is not only unstructured but also very large in area. The scale of these applications magnify the uncertainty and incompleteness in the sensory data, enhancing the challenge posed. However, state-of-the-art mapping methods are based on interpolation do not have a statistically sound way of incorporating and managing uncertainty. Vasudevan \textit{et al.} (2009) proposed a method using Gaussian Processes to tackle this challenge.

The authors used a GP model with a neural network (NN) non-stationary kernel, which represents the covariance function of a neural network with infinitely many hidden nodes contained in a single hidden layer and using a sigmoid transfer function. The more widely used square exponential (SQEXP) kernel was also used for comparison. It is a stationary kernel which doesn't cope as well with sharp discontinuities.

The model was applied to the data to generate a DEM by performing GP regression at a set of query points, given the data sets and the GP kernel with the learned hyperparameters. It should be noted that GPs require to perform matrix inversions, meaning their complexity grows cubically with the number of data points. Therefore, sampling approaches were employed to avoid using whole datasets. A white noise model is incorporated to handle uncertainty.

The group presents experiments using data from real mining sites with different topologies and reading densities. The experiments serve to demonstrate GP terrain modelling, to compare the performance of the kernel chosen to the SQEXP kernel, as well as to parametric and non-parametric interpolation methods, and to analyse the effects of different sampling and training strategies over their efficiency.

\begin{figure}[htp]
\center
\includegraphics[width = 0.45\textwidth]{shrihari.png}
\caption{Shrihari \textit{et al.} (2009).\label{shrihari}}
\end{figure}

The results show that the GP approach is very competitive. With dense data, all methods had similar performance, \textit{i.e.} worst performer's mean square error was within one standard deviation of best performer's. In modelling sparse data, however, NN far outperformed all other methods, as shown in figure \ref{shrihari}. GPs however com with the ability to output the uncertainty, which makes them a more prominent choice for terrain modelling.

\subsection*{Continuous occupancy mapping}

\begin{figure}[htp]
\center
\includegraphics[width = 0.45\textwidth]{simon.png}
\caption{2-D Simulated Dataset.(a) Plan view of room. Robot poses are shown as red diamonds. Observed occupied points and free-space line segments are represented by green crosses and blue lines, respectively. (b) Probability of occupancy versus location. (c) Predictive variance map.(d) ROC curve comparing the performance of the analysed methods on a simulated dataset with known ground truth. O'Callaghan \textit{et al.} (2011).\label{simon}}
\end{figure}


\begin{figure}[h]
\center
\includegraphics[width = 0.45\textwidth]{simon3.png}
\caption{ Quantitative comparison of experimental results. O'Callaghan \textit{et al.} (2011).\label{simon2}}
\end{figure}

\begin{figure}[h]
\center
\includegraphics[width = 0.45\textwidth]{simon2.png}
\caption{Time comparison between the GPOMIK and the GPOM on the 3D simulated dataset. GPOMIK from scratch indicates the method where the kernel is recalculated in each iteration, while "stored K" represents optimised update. O'Callaghan \textit{et al.} (2011).\label{simon3}}
\end{figure}

Discrete representations of terrain bring with them a number of shortcomings. Discretisation requires the resolution of the map to be fixed beforehand, usually to a constant value throughout the map. Their efficiency also relies on a very strong assumption about the environment: that cells are independent and identically distributed. This disregards spatial correlations that might be useful to infer occupation in unobserved regions. O'Callaghan \textit{et al.} (2011) proposed a mapping approach that represents the environment as a continuous spatial classification problem.

To this end, a method called Gaussian Process Occupancy Mapping (GPOM), also developed by the authors, was employed. GPOMs are GP classifiers that take rangefinder sensor data and uses it to classify regions of space as occupied or free. This approach has a few drawbacks: due to the manner free space is represented, extracting points near the robot creates unique training data that requires the kernel matrices to be recalculated, which is a very costly procedure. On top of that, a sensor beam is approximated as a collection of free-space points, which causes the training set to be inflated very fast. Given that the complexity of the algorithm grows cubically with the number of training examples, this is an obvious problem.
To circumvent this, they extend the GPOM framework using something the authors call integral kernels (IK), a method that allows to model the rangefinder beam as a continuous line segment. Through this method, the covariance function is effectively redefined to handle line segments instead of points.
A main benefit of this approach is that the covariance matrix can be stored and applied to multiple query points. Therefore, matrix inversion needs not to be performed more than once, which removes the $O(n^{3})$ cost from further iterations. Instead, it can be updated through a $O(n^{2})$ operation, which takes much longer to become too large to handle. A comparison of performance with and without this optimisation is shown on figure \ref{simon3}. To allow online learning, this is aided by an active sampling technique and by splitting the covariance matrix once it grows too large.

Experiments with real and simulated data in two and three dimensions compare the GPOMs with and without IKs, as well as regular OGs. The results are shown in figure \ref{simon} and analysed on figure \ref{simon2}. GPOMIKs have been shown to perform better: they run faster then GPOMs and generate less false positives than both competing methods.


%----------------------------------------------------------------------------------------
%	REFERENCE LIST
%----------------------------------------------------------------------------------------
\newpage
%
\begin{thebibliography}{99} % Bibliography - this is intentionally simple in this template
\fontsize{10}{12}\selectfont

\bibitem[\textbf{MacKay, 2003}]{book1}
David MacKay:
\newblock Information Theory, Inference, and Learning Algorithms.
\newblock {\em Cambridge University Press}, 535--548 (2003)

\bibitem[\textbf{Lang \textit{et al.}, 2007}]{paper1}
Tobias Lang, Christian Plagemann and Wolfram Burgard:
\newblock Adaptive Non-Stationary Kernel Regression for Terrain Modeling.
\newblock {\em Proceedings of the Robotics: Science and Systems Conference (RSS)} (2007)

\bibitem[\textbf{Vasudevan \textit{et al.}, 2009}]{paper2}
Shrihari Vasudevan, Fabio Ramos, Eric Nettleton, and Hugh Durrant-Whyte:
\newblock Gaussian process modeling of large-scale terrain.
\newblock {\em  Journal of Field Robotics}, 26(10): 812--840 (2009)

\bibitem[\textbf{O'Callaghan, 2011}]{paper}
Simon O'Callaghan:
\newblock Continuous Occupancy Mapping with Integral Kernels
\newblock {\em Proceedings of the Twenty-Fifth AAAI Conference on Artificial Intelligence}, 1494-1500 (2011)

\bibitem[\textbf{O'Callaghan, 2012}]{thesis}
Simon O'Callaghan:
\newblock Continuous Occupancy Maps for the Representation of Unstructured Environments.
\newblock {\em PhD thesis, University of Sydney} (2012)

\bibitem[\textbf{Ishigami \textit{et al.}, 2013}]{paper4}
Genya Ishigami, Masatsugu Otsuki and Takashi Kubota:
\newblock Range-dependent Terrain Mapping and Multipath Planning using Cylindrical Coordinates for a Planetary Exploration Rover.
\newblock {\em Journal of Field Robotics}, 30(4): 536–551 (2013)

\bibitem[\textbf{Papadakis, 2013}]{survey}
Panagiotis Papadakis:
\newblock Terrain traversability analysis methods for unmanned ground vehicles: A survey.
\newblock {\em Engineering Applications of Artificial Intelligence}, 26: 1373--1385 (2013)


\end{thebibliography}


%@Article{survey,
%author = {Panagiotis Papadakis},
%title = {Terrain traversability analysis methods for unmanned ground vehicles: A survey},
%journal = {Engineering Applications of Artificial Intelligence},
%year = {2013},
%OPTkey = {},
%OPTvolume = {26},
%OPTnumber = {},
%OPTpages = {1373-1385},
%OPTmonth = {},
%OPTnote = {},
%OPTannote = {}
%}

%----------------------------------------------------------------------------------------

\end{document}