\select@language {american}
\vspace {-\cftbeforepartskip }
\select@language {american}
\contentsline {chapter}{Declaration}{iii}{chapter*.1}
\contentsline {chapter}{Abstract}{v}{chapter*.2}
\contentsline {chapter}{Acknowledgements}{ix}{chapter*.3}
\contentsline {chapter}{Figures}{xiii}{dummy.4}
\contentsline {chapter}{Tables}{xiv}{dummy.5}
\contentsline {chapter}{Algorithms}{xiv}{dummy.6}
\contentsline {chapter}{Symbols and Notation}{xv}{dummy.7}
\contentsline {part}{1\hspace {1em}\spacedlowsmallcaps {Introduction}}{1}{part.1}
\contentsline {chapter}{\numberline {1.1}\spacedlowsmallcaps {Motivation}}{2}{chapter.1.1}
\contentsline {chapter}{\numberline {1.2}\spacedlowsmallcaps {Problem Description}}{3}{chapter.1.2}
\contentsline {chapter}{\numberline {1.3}\spacedlowsmallcaps {Challenges and Contributions}}{5}{chapter.1.3}
\contentsline {chapter}{\numberline {1.4}\spacedlowsmallcaps {Thesis Overview}}{6}{chapter.1.4}
\contentsline {part}{2\hspace {1em}\spacedlowsmallcaps {Background}}{7}{part.2}
\contentsline {chapter}{\numberline {2.1}\spacedlowsmallcaps {Robotic Mapping}}{8}{chapter.2.1}
\contentsline {section}{\numberline {2.1.1}Occupancy Grid Mapping}{10}{section.2.1.1}
\contentsline {section}{\numberline {2.1.2}Shortcomings of Occupancy Grid Mapping}{15}{section.2.1.2}
\contentsline {chapter}{\numberline {2.2}\spacedlowsmallcaps {Gaussian Processes}}{17}{chapter.2.2}
\contentsline {section}{\numberline {2.2.1}Gaussian Process Regression}{20}{section.2.2.1}
\contentsline {section}{\numberline {2.2.2}Covariance Functions}{23}{section.2.2.2}
\contentsline {subsection}{\numberline {a.}Stationary covariance functions}{25}{subsection.2.2.2.1}
\contentsline {subsection}{\numberline {b.}Non-stationary Covariance Functions}{26}{subsection.2.2.2.2}
\contentsline {section}{\numberline {2.2.3}Gaussian Process Learning}{29}{section.2.2.3}
\contentsline {section}{\numberline {2.2.4}Shortcomings of Gaussian Processes}{30}{section.2.2.4}
\contentsline {chapter}{\numberline {2.3}\spacedlowsmallcaps {Summary}}{31}{chapter.2.3}
\contentsline {part}{3\hspace {1em}\spacedlowsmallcaps {Mapping with Multi-Support Kernels}}{33}{part.3}
\contentsline {chapter}{\numberline {3.1}\spacedlowsmallcaps {Introduction}}{34}{chapter.3.1}
\contentsline {chapter}{\numberline {3.2}\spacedlowsmallcaps {Related Work}}{35}{chapter.3.2}
\contentsline {section}{\numberline {3.2.1}Change of Support in Gaussian Processes}{35}{section.3.2.1}
\contentsline {section}{\numberline {3.2.2}Gaussian Process Occupancy Mapping}{40}{section.3.2.2}
\contentsline {chapter}{\numberline {3.3}\spacedlowsmallcaps {Multi-support Kernels}}{44}{chapter.3.3}
\contentsline {section}{\numberline {3.3.1}Proposed Kernel}{44}{section.3.3.1}
\contentsline {chapter}{\numberline {3.4}\spacedlowsmallcaps {Mapping with multi-support kernels}}{47}{chapter.3.4}
\contentsline {section}{\numberline {3.4.1}Mapping with a Synthetic Dataset}{48}{section.3.4.1}
\contentsline {chapter}{\numberline {3.5}\spacedlowsmallcaps {Summary}}{55}{chapter.3.5}
\contentsline {part}{4\hspace {1em}\spacedlowsmallcaps {Experiments}}{57}{part.4}
\contentsline {chapter}{\numberline {4.1}\spacedlowsmallcaps {Introduction}}{58}{chapter.4.1}
\contentsline {chapter}{\numberline {4.2}\spacedlowsmallcaps {Parameters of the Model}}{59}{chapter.4.2}
\contentsline {subsection}{\numberline {a.}Element Generation}{59}{subsection.4.2.0.1}
\contentsline {subsection}{\numberline {b.}Covariance Function}{60}{subsection.4.2.0.2}
\contentsline {subsection}{\numberline {c.}Optimiser}{60}{subsection.4.2.0.3}
\contentsline {chapter}{\numberline {4.3}\spacedlowsmallcaps {Benchmarks}}{66}{chapter.4.3}
\contentsline {section}{\numberline {4.3.1}Synthetic Data}{66}{section.4.3.1}
\contentsline {section}{\numberline {4.3.2}Real Data}{71}{section.4.3.2}
\contentsline {chapter}{\numberline {4.4}\spacedlowsmallcaps {Summary}}{76}{chapter.4.4}
\contentsline {part}{5\hspace {1em}\spacedlowsmallcaps {Conclusions}}{77}{part.5}
\contentsline {chapter}{\numberline {5.1}\spacedlowsmallcaps {Contributions}}{78}{chapter.5.1}
\contentsline {chapter}{\numberline {5.2}\spacedlowsmallcaps {Future Work}}{79}{chapter.5.2}
\vspace {\beforebibskip }
\contentsline {part}{\spacedlowsmallcaps {Bibliography}}{83}{dummy.8}
