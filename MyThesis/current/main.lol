\deactivateaddvspace 
\addvspace {10\p@ }
\addvspace {10\p@ }
\addvspace {10\p@ }
\addvspace {10\p@ }
\addvspace {10\p@ }
\contentsline {lstlisting}{\numberline {2.1}Occupancy grid map update algorithm.}{15}{lstlisting.2.1}
\addvspace {10\p@ }
\contentsline {lstlisting}{\numberline {2.2}Gaussian process regression algorithm.}{23}{lstlisting.2.2}
\addvspace {10\p@ }
\addvspace {10\p@ }
\addvspace {10\p@ }
\addvspace {10\p@ }
\contentsline {lstlisting}{\numberline {3.1}Element-to-element kernel algorithm.}{46}{lstlisting.3.1}
\contentsline {lstlisting}{\numberline {3.2}Element-to-point kernel algorithm.}{46}{lstlisting.3.2}
\contentsline {lstlisting}{\numberline {3.3}Multi-support kernel algorithm.}{47}{lstlisting.3.3}
\addvspace {10\p@ }
\contentsline {lstlisting}{\numberline {3.4}Pre-processing algorithm.}{52}{lstlisting.3.4}
\contentsline {lstlisting}{\numberline {3.5}Recursive algorithm to find valid elements.}{52}{lstlisting.3.5}
\contentsline {lstlisting}{\numberline {3.6}MSK-GPOM algorithm.}{55}{lstlisting.3.6}
\addvspace {10\p@ }
\addvspace {10\p@ }
\addvspace {10\p@ }
\addvspace {10\p@ }
\addvspace {10\p@ }
\addvspace {10\p@ }
\addvspace {10\p@ }
