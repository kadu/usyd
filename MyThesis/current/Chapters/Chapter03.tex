% 3. Mapping with n-Dimensional Kernels
%   ? Integral kernels background (Allistair/Simon/Bernhardt S.)
%   ? Proposed kernel
%   ? Complexity analysis
%   ? Examples
%   ? Mapping example
%   
%----------------------------------------------------------------------------------------
%
\begin{flushright}
\onehalfspacing
{\slshape
Now I have a headache\dots} \\ \smallskip
--- Marvin
\end{flushright}
%
%----------------------------------------------------------------------------------------

\label{theory}

\mychapter{Introduction}
%
In \autoref{background}, we have reviewed the \ac{OGM} algorithm, and established that its shortcomings stem mainly from the discretisation of  a continuous domain. \acp{GP} have been proposed as a way to address this, given their ability to infer contextual information from input data. This has been done before, and a previous mapping algorithm based on \acp{GP} will be reviewed in this chapter.\pg
%
The greatest drawback of \acp{GP} is their complexity, that comes from the need of a matrix inversion. Although this cannot be prevented, there are other techniques to make the algorithm run faster. \autoref{GPR} incorporates one such technique, a Cholesky decomposition of the matrix. This is not enough, though: robotic rangefinders take thousands of readings for each minute of activity, and since inverting the covariance matrix scales at ${\mathcal{O}(N^{3})}$ with the number of inputs, the operation becomes unmanageable fast.\pg
%
Discarding information would be one way to deal with this. The data collected from sensors could be filtered, and information about regions already mapped with high certainty can be discarded with minimal losses. But even then, as the environment being mapped grows, the covariance matrix inevitably grows with it.\pg
%
An alternative we will explore in this chapter is changing the support of the kernel functions. Traditional kernels use as support $D$-dimensional points. If they could be grouped in more complex geometric structures (like lines or areas), this could reduce drastically the size of the covariance matrix. We will then review previous work done using integral kernels for robotic mapping.\\
%
\mychapter{Related Work}\label{rltwrk}

\section{Change of Support in Gaussian Processes}\label{intkern}
%
Gaussian processes, as introduced in \autoref{gapr}, are very versatile tools able to infer underlying functions from the data. Normally, they deal with functions calculated over points. However, this is not the best approach for every situation. In some applications, data can come from higher dimensional elements of the domain, such as lines, areas and volumes. In particular, this is true in robotic mapping, where it is more interesting to determine if a particular area or volume is occupied, rather than a single point.\pg
%
Inference made using higher dimensional geometrical elements is what \citet*{support} call the \emph{change of support} problem. They exemplify this concept with an application that requires combining geographic data about ozone levels---which comes from fixed monitoring sites that can be considered as points in a map---to the proportion of hospital visits due to asthma---which is bundled by zip code, and therefore must be considered as areas.\pg
%
Without using complex support kernels (in this particular case, area kernels), the problem can be solved with traditional \acp{GP} by using the ozone data to fit an ozone level surface over the map, then using the estimation at the centroid of a particular zip code to represent the readings at the whole area \citep{carlin}. \citeauthor{support} argue that this approximation fails to capture spatial association and variability by treating a GP estimate as an observed value.\pg
%
Instead, they propose assuming that the variable observed is continuous and comes from an underlying spatial process. We can denote this process by $Y(\pp)$, for points $\pp \in D$ (where $D$ is our region of interest). For data observed in discrete locations, as is the case of ozone levels in the example they present, this is the process to be inferred. However, patient information comes from averages over regions of space. That is, for each region $A \in D$, 
%
\begin{equation}
Y(A) = |A|^{-1}\int_{A}{Y(\pp)}{d\pp},
\label{areaint}
\end{equation}
where $|A|$ represents the area of region A. This integral is an average of random variables, therefore the assumption of an underlying spatial process is only appropriate if the area data can be seen as averaging over point data. Valid examples are continuous environmental measurements, such as temperature, pressure or elevation. Conversely, it is inappropriate for discrete measurements, like population, as a particular point of space cannot hold any population. Proportions are also usually inappropriate, because even though they take values from a continuous range, the corresponding point data could be discrete (for instance: the proportion of unemployed people is continuous over the population of an area, but binary for each person).\pg
%
Reid, in his doctoral thesis \citeyearpar{reidthesis}, formulates a way of using geometrical elements as support for a \ac{GP} kernel, illustrated on \autoref{agp}.\pg
%
 \begin{figure}[!htp]
\center
    \subfloat[ Colored rectangles and points are the input data, grey surface is the prediction.\label{alla}]{%
      \includegraphics[width=\textwidth]{gfx/area_gp}}
    \\
    \subfloat[Ground truth\label{allb}]{%
      \includegraphics[width=0.485\textwidth]{gfx/area_gp_true}}
    \hfill
    \subfloat[Estimate\label{allc}]{%
      \includegraphics[width=0.485\textwidth]{gfx/area_gp_est}}
\caption[Demonstration of GP using areas as support]{Example of Gaussian process using kernels supported on areas and points. Taken from \citet*[section 3.3]{reidthesis}.\label{agp}}\end{figure}
%
Besides points $\x$, the higher dimensional elements he uses are lines $L$ (when the domain is one-dimensional) or areas $A$ (when two-dimensional), such that \autoref{areaint} becomes:
%
\begin{equation}
\begin{split}
Y(L) &= |L|^{-1}\int_{\x \in L}{Y(\x)}{d\x}\mbox{ and}\\
Y(A) &= |A|^{-1}\iint_{\x \in A}{Y(\x)}{d\x}.
\end{split}
\label{linearea}
\end{equation} 
%
\begin{figure}[ht]
\center
\includegraphics[width = \textwidth]{gfx/element_kernel}
\caption[Schematic representation of integral kernels]{Schematic representation of kernels using points, lines and areas. \citet*[section 3.3]{reidthesis}.\label{elker}}
\end{figure}
%
Having established the relationships between points and lines or areas, Reid proceeds to define covariance functions that accept these elements as input. The equations for these kernels, illustrated in \autoref{elker}, are:
%
\begin{equation}
\begin{split}
K_{\x, \x'} &= \kxxp,\pg
%
K_{L, \x'} &= k(L, \x') = \frac{1}{|L|}\int_{\x \in L}{\kxxp}{d\x},\\
K_{L, L'} &= k(L, L') = \frac{1}{|L||L'|}\int_{\x \in L}\int_{\x' \in L'}{\kxxp}{d\x}{d\x'},\pg
%
K_{A, \x'} &= k(A, \x') = \frac{1}{|A|}\iint_{\x \in A}{\kxxp}{d\x},\\
K_{A, A'} &= k(A, A') = \frac{1}{|A||A'|}\iint_{\x \in A}\iint_{\x' \in A'}{\kxxp}{d\x}{d\x'}.
\end{split}
\label{palK}
\end{equation}
%
Analysing the above integrals makes it easy to see that, due to being inversely proportional to the size of the higher-dimension element, the complex support kernels tend to zero as $|L|$ or $|A|$ are increased---that is, as the resolution becomes coarser.
%
A quick look through the covariance functions shown in \autoref{scovfun} shows that several can be challenging to integrate analytically. Reid chooses the squared exponential function to demonstrate the technique, for two reasons. The first is that it is fairly easy to integrate to obtain the line-to-point and line-to-line integral kernels. If $L$ is taken to be a line segment between points $\xl$ and $\xr$, we obtain:
%
\begin{equation}
\begin{split}
K_{\x, \x'} &= \sigma_{f}^{2}\exp\left(-\frac{|\x - \x'|}{2\ls^{2}}\right),\pg
%
K_{L, \x'}
&= \frac{\sigma_{f}^{2}}{\xr - \xl}\int_{\xr}^{\xl}K_{\x, \x'}d\x\\
&= \sqrt{\frac{\pi}{2}}\left(\frac{\sigma_{f}^{2}\ls}{\xr - \xl}\right)
       \left[\erf\left(\frac{\x - \xr}{\sqrt{2}\ls}\right) -
       	      \erf\left(\frac{\x - \xl}{\sqrt{2}\ls}\right)\right],\pg
%
K_{L, L'}
&= \frac{\sigma_{f}^{2}}{(\xr - \xl)(\xr' - \xl')}\int_{\xr'}^{\xl'}K_{L, \x'}d\x'\\
&= \sqrt{\frac{\pi}{2}}\left(\frac{\sigma_{f}^{2}\ls}{(\xr - \xl)(\xr' - \xl')}\right)
       \Biggl[(\xr - \xl')\erf\left(\frac{\xr - \xl'}{\sqrt{2}\ls}\right) - 
                (\xl - \xl')\erf\left(\frac{\xl - \xl'}{\sqrt{2}\ls}\right)\\
         &+ (\xl - \xr')\erf\left(\frac{\xl - \xr'}{\sqrt{2}\ls}\right) - 
                (\xr - \xr')\erf\left(\frac{\xr - \xr'}{\sqrt{2}\ls}\right)\Biggr]\\
&+ \frac{\ls^{2}}{2(\xr - \xl)(\xr' - \xl')}
       \Biggl[\exp\left(-\frac{|\xr - \xl'|}{2\ls^{2}}\right) -
       \exp\left(-\frac{|\xl - \xl'|}{2\ls^{2}}\right)\\
&+ \exp\left(-\frac{|\xl - \xr'|}{2\ls^{2}}\right) -
       \exp\left(-\frac{|\xr - \xr'|}{2\ls^{2}}\right)\Biggr].
\end{split}
\label{iksqexp}
\end{equation}
%
The second is that the function is separable in a two-dimensional Euclidean space, that is,
\begin{equation}
\begin{split}
K_{\x, \x'} &= \exp(|\x - \x'|^{2})\\
                     &= \exp((x_{1} - x'_{1})^2)\exp((x_{2} - x'_{2})^2).
%
\end{split}
\label{separable}
\end{equation}
%
This means that the area-to-point and area-to-area functions for a rectangular region can be represented as a cross product between its diagonals. If we take $\x$ an input vector containing both rectangles $A$ (with diagonals $D_1$ and $D_2$) and points $\pp$, we obtain:
%
\begin{equation}
\begin{split}
\x &= 
\begin{bmatrix}
A\\
\pp
\end{bmatrix},\pg
%
\kxxp &= 
\begin{bmatrix}
k(D_1, D'_1) & k(D_1, \pp')\\
k(\pp, D'_1) & k(\pp, \pp')
\end{bmatrix} \times
\begin{bmatrix}
k(D_2, D'_2) & k(D_2, \pp')\\
k(\pp, D'_2) & k(\pp, \pp')
\end{bmatrix}
\end{split}
\label{sepkern}
\end{equation}
%
We have discussed, however, that the computation of covariance functions is the most computationally demanding part of the \ac{GP} algorithm. Reid lessens this problem by using a piecewise approximation to the isotropic squared exponential kernel. However, an isotropic kernel is not a good option for robotic mapping.\pg
%
The method outlined above requires extra calculations, since we need to calculate two full covariance matrices, and this represents an increase in computational cost. Moreover, the mathematical equations involved in one- and two-dimensional kernels are long and complicated (see \autorefs{iksqexp,sepkern}), representing a challenge to implementation.. Moreover, were the work on this thesis to be expanded to three-dimensional data (as it is intended), the equations would become even larger, making this approach impractical.\\
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%    %%%%%     %%%%%%          %%%%        %%%                 %%%
% %%         %%      %%      %%     %%     %%         %%%          %%%
% %%                      %%      %%   %%         %%       %%%%   %%%%
% %%   %%%%      %%%%%     %%         %%       %%   %%%   %%
% %%         %%       %%                %%       %%        %%      %       %%
%   %%%%%       %%%%                %%%%        %%%%          %%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%
\section{Gaussian Process Occupancy Mapping}\label{GPOM}
%
As discussed in \autoref{roma},  discrete representations of space bring a number of shortcomings with them. Their efficiency relies on a very strong assumption about the environment: that cells are \ac{iid}. This disregards spatial correlations that might be useful to infer occupation in unobserved regions. In order to avoid this problem, \citet*{simonthesis} proposed a mapping approach using \acp{GP}, transforming the task in a continuous spatial classification problem. The method developed is called \ac{GPOM}.\pg
%
 \begin{figure}[!hb]
    \subfloat[Ground truth\label{sima}]{%
      \includegraphics[width=0.485\textwidth]{gfx/simon-a}}
    \hfill
    \subfloat[Laser returns and robot pose\label{simb}]{%
      \includegraphics[width=0.485\textwidth]{gfx/simon-b}}
      \\
    \subfloat[Traditional \ac{OGM}\label{simc}]{%
      \includegraphics[width=0.485\textwidth]{gfx/simon-c}}
    \hfill
    \subfloat[Predictive variance of Gaussian process\label{simd}]{%
      \includegraphics[width=0.485\textwidth]{gfx/simon-d}}
      \\
\subfloat[Probability of occupancy versus location\label{sime}]{%
      \includegraphics[width=0.485\textwidth]{gfx/simon-e}}
    \hfill
    \subfloat[Classified \ac{GPOM}. Black = occupied ($p(Occupied) \geq 0.65$), white = free ($p(Occupied) \leq 0.35$), grey = unsure. \label{simf}]{%
      \includegraphics[width=0.485\textwidth]{gfx/simon-f}}
    \caption[GPOM demonstration with simulated 2D dataset]{\ac{GPOM} demonstration with simulated 2D dataset. \citet*{simonthesis}.\label{sim}}
\end{figure}\\ % <- DO NOT REMOVE this linebreak, or text breaks awfully =[
%
 \acp{GPOM} are \ac{GP} classifiers that take rangefinder sensor data and uses it to classify regions of space as occupied or free. \autoref{sim} illustrates the method with a simple synthetic dataset. It is important to notice that the \ac{GP} outputs a continuous surface of probability (\autoref{sime}), which is then filtered to obtain a map (\autoref{simf}) that can be compared to a traditional \ac{OGM} (\autoref{simc}).\pg
%
The input points, though, cannot be limited to the laser hits---as those only indicate obstacles and give no information about the free areas. In order to indicate free areas, a sensor beam can be, for instance, represented as a collection of free-space points. However, that would cause the training set to be inflated very fast, and make the algorithm run even slower, since its complexity grows cubically with the number of input points. \pg
%
In order to represent the free areas without inflating the input set, \citeauthor*{simonthesis} uses Gaussian processes with covariance functions supported on lines instead of points, as discussed in the previous chapter. This allows to model the rangefinder beam as a continuous line segment between the robot's pose and the point where each beam taken from this location hit an obstacle.\pg
%
\begin{table}[h]
%\small
\center
\caption[GPOMIK result analysis]{Quantitative comparison of experimental results. False positive rates are calculated for a $90\%$ true positive rate. \citet*{simonaaai}.\label{simtab}}
\vspace{5mm}
\begin{tabular}{r | >{\centering\arraybackslash}p{2.15cm} | >{\centering\arraybackslash}p{2.5cm}}
                   & Area under the curve & False positive rate \\\hline
GPOMIK             & 0.9441                & 10.1\% \\
Previous GP Method & 0.9162                & 79.57\% \\
Occupancy Grid     & 0.8938                & 21.9\% \\
No Discrimination  & 0.5                   & 90\%
\end{tabular}
\end{table}
%
\begin{figure}[h]
\center
\subfloat[]{
\includegraphics[width = 0.425\textwidth]{gfx/simon2.png}}
\hfill
\subfloat[]{
\includegraphics[width = 0.49\textwidth]{gfx/roc_gpom.png}}
\caption[Benchmarks for GPOM variations]{Time (a) and accuracy (b) comparison between the GPOMIK and the GPOM on the 3D simulated dataset. GPOMIK "from scratch" indicates the method where the kernel is recalculated in each iteration, while "stored K" represents optimised update. \citet*{simonaaai}.\label{simik}}
\end{figure}
%
In order to allow for online learning, the covariance matrix can be stored and applied to multiple query points. Therefore, matrix inversion needs not to be performed more than once, which removes the $O(n^{3})$ cost from further iterations. Instead, the covariance matrix is updated through a $O(n^{2})$ operation. A comparison of performance with and without this optimisation is shown on \autoref{simik}. To allow online learning, this is aided by an active sampling technique and by splitting the covariance matrix once it grows too large.\pg
%
Experiments with real and simulated data in two and three dimensions compare the \acp{GPOM} with and without integral kernels, as well as regular \acp{OGM}. The results are displayed in \autoref{simik} and \autoref{simtab}. \acp{GPOMIK} were shown to perform better: they run faster then \acp{GPOM} and generate less false positives than both competing methods.\\
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% %%%                 %%%       %%%%      %%%%    %%%
%     %%%          %%%       %%                   %%       %%
%     %%%%   %%%%         %%%%%        %%   %%
%     %%   %%%   %%                      %%     %%%%%
%     %%      %       %%      %          %%       %%      %%
% %%%%          %%%%    %%%%%      %%%%     %%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%
\mychapter{Multi-support Kernels}\label{MSK}
%
The objective of this work is to propose a kernel that can use higher dimensional support elements---lines, rectangles and hyperrectangles---for robotic mapping. Instead of integrating over the laser beams, however, the objective is to integrate over elements of the same dimension of the map. In other words, we'll use areas (when building 2D maps) and volumes (in 3D maps) as support to the covariance functions. It is also desirable that it is able to handle arrays containing these geometrical features as well as points, or any combination between them---hence the chosen name, \acfi{MSK}.\pg
%
The kernels obtained by the method described in \autoref{intkern} would, theoretically, fulfil these requirements. However, analytically integrating a more complicated kernel, such as the \ac{NN} or those of the Mat\'ern class, can be exceedingly difficult. Numerical methods, on the other hand, can be computationally demanding and unstable. An alternative must be presented that is both easy to implement and allows to draw upon the richness and abundance of existing kernel functions.\pg
%
In the next section, we propose a novel method to allow kernels to be calculated using data about complex geometrical shapes. From here onwards, these will be referred to as \emph{elements of support} or \emph{elements}, to differentiate them from data points.\\
%
\section{Proposed Kernel}
%
To fulfil all the criteria priorly discussed, instead of integrating the traditional kernels as the methods discussed in \autoref{rltwrk}, the element kernels are approximated by sampling the data points inside the element and calculating the mean of the kernels between all of them. Following that, for generic geometrical elements, \autoref{palK} can be restated as:
%
\begin{equation}
\begin{split}
K_{\x, \x'} &= \kxxp,\pg
%
K_{E, \x'} &= k(E, \x') = \frac{1}{|E|}\int_{\x \in E}{\kxxp}{d\x},\\
K_{E, E'} &= k(E, E') = \frac{1}{|E||E'|}\int_{\x \in E}\int_{\x' \in E'}{\kxxp}{d\x}{d\x'},
\end{split}
\label{mski}
\end{equation}
%
which, given a finite number of measurements $\x \in E$, can be approximated as:
%
\begin{equation}
\begin{split}
K_{\x, \x'} &= \kxxp,\pg
K_{E, \x'} &= k(E, \x') = \frac{1}{n_{E}}\sum_{\x \in E}{\kxxp},\pg
K_{E, E'} &= k(E, E') = \frac{1}{n_{E}n_{E'}}\sum_{\x \in E}\sum_{\x' \in E'}{\kxxp},
\end{split}
\label{MSK}
\end{equation}
%
where $E$ is a geometrical element of any dimension and $n_{E}$ indicates the number of data points sampled from E. Being constructed by direct sum, it is a valid \ac{PSD} kernel \citep[section 4.2.4]{hastie, gpml}. A similar and more rigorous derivation of can also be found in \citet*[section 2]{support}, and an analogous approach was also used by \citet*[section 3.2]{scholk2013} to calculate kernels over probability distributions.\pg
%
We call the kernel described in \autoref{MSK} a \ac{MSK}, because it allows us to calculate element-to-element and element-to-point covariances, and their algorithmic forms are rather simple\footnote{Also worthy of mention is that, although the way these algorithms are written here suggests a calculation between a single element and a single point, both of them can be vectorised, allowing the input of arrays of elements and points.}:
%
\begin{center}
\begin{minipage}{0.9\textwidth}
\singlespacing
\begin{lstlisting}[caption=Element-to-element kernel algorithm\label{kee}.]
input : E1, E2, k(), par|<\\>|# element, point, covariance function and parameters|<\vspace{3mm}>|
Kpp := []
Kep := [] # empty arrays|<\vspace{3mm}>|
for all p1 in E1:
   for all p2 in E2:
      Kpp.append(k(p1, p2))
   Kep.append(mean(Kpp))|<\vspace{3mm}>|
K := mean(Kep)|<\vspace{3mm}>|
output : K|<\\>|# covariance between E1 and E2
\end{lstlisting}
\end{minipage}
\vspace{3mm}
\end{center}
%
\begin{center}
\begin{minipage}{0.9\textwidth}
\singlespacing
\begin{lstlisting}[caption=Element-to-point kernel algorithm\label{kep}.]
input : E1, p2, k(), par|<\\>|# element, point, covariance function and parameters
Kpp := [] # empty array|<\vspace{3mm}>|
for all p1 in E1:
   Kpp.append(k(p1, p2))|<\vspace{3mm}>|
K := mean(Kpp)|<\vspace{3mm}>|
output : K|<\\>|# covariance between E1 and p2
\end{lstlisting}
\end{minipage}
\vspace{3mm}
\end{center}
%
The above algorithms clearly use elements for support, however each alone is constrained to a single support. In order to enable them to calculate a full covariance matrix between two sets of observations containing any combination of supports (and thus making it truly multi-support), we return to \autorefs{sepkern}, which now takes a much simpler form:
%
\begin{equation}
\begin{split}
\x &= 
\begin{bmatrix}
E\\
\pp
\end{bmatrix},\ \  \x' = 
\begin{bmatrix}
E'\\
\pp'
\end{bmatrix},\pg
%
\kxxp &= 
\begin{bmatrix}
k(E, E') & k(E, \pp')\\
k(\pp, E') & k(\pp, \pp')
\end{bmatrix}.
%
\end{split}
\label{multikern}
\end{equation}
%
We can use this result to build, using Algorithms \ref{kep} and \ref{kee}, a novel multi-support covariance function able to handle inputs containing both elements and points:
%
\begin{center}
\begin{minipage}{0.9\textwidth}
\singlespacing
\begin{lstlisting}[caption=Multi-support kernel algorithm\label{mskalg}.]
input : x1, x2, k(), par|<\\>|# array, array, covariance function and parameters.|<\\>|# arrays are assumed to be of the format x := [E, p]|<$^{\top}$\,\,>||<\vspace{3mm}>|
Kee := kee(x1.E, x2.E, k(), par)   # element-to-element covariance
Kep := kep(x1.E, x2.p, k(), par)   # element-to-point covariance
Kpe := kep(x1.p, x2.E, k(), par)|<$^{\top}$\,\,>| # point-to-element covariance
Kpp := k(x1.p, x2.p, par)          # point-to-point covariance|<\vspace{3mm}>|
K := [[Kee, Kep],|<\\>|  [Kpe, Kpp]]|<\vspace{3mm}>|
output : K|<\\>|# covariance between x1 and x2
\end{lstlisting}
\end{minipage}
\vspace{3mm}
\end{center}
%
The greatest advantage of the proposed kernel is that we can now aggregate several input points into a single element. When we use it as a covariance function for a \ac{GP}, it can significantly reduce the size of the covariance matrix, which directly impacts the cost of its inversion.\\
%
\mychapter{Mapping with multi-support kernels}
%
A covariance function able to produce reduced matrices from the input dataset is a valuable tool. As discussed in \autoref{background}, the \acf{GP} algorithm requires a matrix inversion, which has a complexity of $O(N^{3})$ with the size of the matrix, and using it for mapping is a challenge because laser rangefinders can generate thousands of data points per minute.\pg
%
By using a \acf{MSK}, points can be grouped together in spatial elements which count as a single input in the array. The effective reduction in computation time will depend on how many points can be grouped into a single element, but if we group a dataset containing $N$ entries into elements containing an average of $n$ points each, the input size becomes roughly $N/n$. Since its dependency with the number of entries is cubical, the complexity of the inversion is $n^{-3}$ of the original.\pg
%
It should then become obvious that the way in which the elements are defined hugely impacts the efficiency of \ac{MSK}. The more points there are inside each element, the smaller the covariance matrix will be and the faster the result will be. However, as the size of the elements grow, the resolution becomes coarser and the map loses its predictive ability.\pg
%
Once the elements are generated, they are passed as input to the \ac{MSK-GPOM} algorithm, outputs the probability of occupancy in the form of a three-dimensional surface. The occupancy map is then obtained by generating a point grid and categorising each point, according to its probability of occupancy, into one of three states: \emph{free}, \emph{occupied} or \emph{unknown}. The whole process is summarised in \autoref{flow}\pg
%
\begin{figure}[ht]
\center
\includegraphics[width = 0.9\textwidth]{gfx/flow.jpg}
\caption[Flowchart for MSK-GPOM]{Flowchart for \ac{MSK-GPOM}.\label{flow}}
\end{figure}
%
Several variables can affect the performance of the algorithm, not all of which can be efficiently optimised through the learning step described above. This will be discussed in greater detail further ahead in \autoref{experiments}. However, for instructional purposes, each step of the algorithm outlined in \autoref{flow} will first be illustrated in the next section.\\
%
\section{Mapping with a Synthetic Dataset}
%
In this section, we use a simulation to illustrate the main aspects of the proposed algorithm. It models a robot equipped with a rangefinder moving within a 2 dimensional space. At each time step, the robot performs a number of rangefinder readings, then updates its pose (\ie position and orientation) based on its previous pose and speed. A sample dataset is shown in \autoref{data}.\pg
%
 \begin{figure}[!htp]
\center
    \subfloat[Ground truth\label{gt}]{%
      \includegraphics[trim=40mm 95mm 40mm 97.5mm,clip,width=0.5\textwidth]{gfx/maps/truth.pdf}}
    \hfill
    \subfloat[Rangefinder data: free beams in red and hits in blue.\label{rf}]{%
      \includegraphics[trim=40mm 95mm 40mm 97.5mm,clip,width=0.5\textwidth]{gfx/maps/data.pdf}}
    \\
    \subfloat[Free points in green, occupied in red.\label{fh}]{%
      \includegraphics[trim=40mm 95mm 40mm 97mm,clip,width=0.5\textwidth]{gfx/maps/points.pdf}}
      \hfill
          \subfloat[Dataset partitioned into elements.\label{fh}]{%
      \includegraphics[trim=40mm 95mm 40mm 97mm,clip,width=0.5\textwidth]{gfx/maps/el_1p0.pdf}}
\caption[Sample dataset]{Sample synthetic dataset and data preprocessing. A robot moves around in a room (a), equipped with a laser rangefinder with 180� field of view, generating a series of readings (b). These are then sampled and divided in free and occupied points (c). Lastly, these points are used to create the elements to be used as input by the \ac{MSK-GP} algorithm (d). All units displayed are arbitrary.\label{data}}
\end{figure}\\
%
The first step is the pre-processing: from each beam (that is, each robot-hit coordinate pair), a set of equally spaced free points is generated. The hits that are at the edge of the perceptual range (that is, the ones where it is unclear whether or not an obstacle exists) are discarded at this stage. The closest free point to each hit can be discarded to make the element creation easier, avoiding elements containing both free and occupied points---however, it also results in wider uncertain boundaries between free and occupied areas, a trade-off that must be kept in mind.\pg
%
After the free points, the elements must be created. An initial element is defined by a polygon set around the data points. For the sake of simplicity, this work used exclusively rectangles with sides parallel to the cartesian axes, although this restriction is unnecessary. The initial element is divided following a logic similar to a quad-tree, and two things are taken into consideration before each division: the size of the element and the type of points inside it. If an element contains only one type of points inside (hits or free points) it is used as an input. If it has both free points and hits, it is divided in half perpendicular to its longest dimension, except if it is smaller than the smallest allowed size, a threshold defined by the user. In this example, we used a threshold of $1.0$ (see \autoref{data})\footnote{It is important to stress that the threshold does not represent the smallest element dimensions possible. Rather, it means that elements with dimensions larger than those would still be divided---therefore, a threshold of $1.0$ means that the smallest dimension found in an element in the input dataset can be as small as $0.5+\epsilon$.}. The process is illustrated in \autoref{elements} and Algorithms \ref{ppalg} and \ref{findel}.\pg
%
 \begin{figure}[!htp]
\center
    \subfloat[]{%
      \includegraphics[width=27mm, height=30mm]{gfx/example.png}}
    \hfill
    \subfloat[]{%
      \includegraphics[width=27mm, height=30mm]{gfx/div1.png}}
    \hfill
    \subfloat[]{%
      \includegraphics[width=27mm, height=30mm]{gfx/div2.png}}
    \hfill
    \subfloat[]{%
      \includegraphics[width=27mm, height=30mm]{gfx/div3.png}}
    \hfill
    \subfloat[]{%
      \includegraphics[width=27mm, height=30mm]{gfx/div4.png}}
\caption[Element generation process demonstration]{Element generation process demonstration. For this example, we use an initial dataset (a) spanning a $0.9\times1.0$ area and a threshold of $0.25$. Since it contains both free (green) and occupied (red) points, it is divided along the longest axis (b). The lower element only contains free points, so it is marked free and not divided any further; upper element is divided again (c). The upper right element can be divided one more time (d), yielding one free and one occupied element. The upper left element, however, after an additional division (e) yields an empty element (which is discarded) and an element containing one free and one occupied point that can't be divided any further (because it's longer axis is smaller than the threshold). Therefore, it is ambiguous, with $50\%$ chance of occupancy (yellow), as determined by the proportion of occupied and free points.\label{elements}}
\end{figure}\\
%
\begin{center}
\begin{minipage}{0.9\textwidth}
\singlespacing
\begin{lstlisting}[caption=Pre-processing algorithm\label{ppalg}.]
input : B, thresh|<\\>|# beams, size threshold |<\vspace{3mm}>|
[Xf, Xo] = split_beams(B, thresh)|<\\>|# samples the beams at regular distances and returns one|<\\>|# vector of free and one of occupied coordinates|<\vspace{3mm}>|
E = []
E0 = [Xf; Xo]|<$^{\top}$\,\,>| # initial element containing all the data|<\vspace{3mm}>|
E = find_elements(E0, thresh) # |<\texttt{\autoref{findel}}\vspace{3mm}>|
output : E|<\\>|# list of elements created from the data
\end{lstlisting}
\end{minipage}
\vspace{3mm}
\end{center}
%
\begin{center}
\begin{minipage}{0.9\textwidth}
\singlespacing
\begin{lstlisting}[caption=Recursive algorithm to find valid elements\label{findel}.]
input : E, thresh|<\\>|# list of elements, size threshold|<\pg>|# this algorithm takes as input elements that are too large or|<\\>|# contain mixed entries and ultimately returns a list of|<\\>|# elements that obey all constraints imposed|<\vspace{3mm}>|
[Xf; Xo] = E
if Xf = empty and Xo = empty:
   E = [] # empty elements are discarded
   return|<\vspace{3mm}>|
if E |<$\leq$>| thresh or Xf = empty or Xo = empty:|<\\>|# elements that contain only one type of entry or are smaller|<\\>|# than the size threshold do not get divided any further
   return|<\vspace{3mm}>|
[E1, E2] := divide(E)|<\\>|# divides E in half by the longer axis and creates two new|<\\>|# elements, splitting the free and occupied points accordingly|<\vspace{3mm}>|
E := [find_elements(E1, thresh), find_elements(E2, thresh)|<\vspace{3mm}>|
output : E|<\\>|# list of valid elements
\end{lstlisting}
\end{minipage}
\vspace{3mm}
\end{center}
%
This method of division has a desirable property. If we look at a single time step, since the beams in a rangefinder have diverging trajectories, the free points are abundant and grouped more densely near the robot's location, while the hits are at the edges and further apart. Therefore, the elements generated near the robot will be larger, so they group more points together and ultimately make a larger dent in the size of the covariance matrix. At the same time, closer to the hit, the size of the elements will decrease, which allows for extra resolution where it matters most.\pg
%
After the elements are prepared, they are ready to be used to train a GP. In this example, a squared exponential covariance function is used, trained using annealing \citep{ann1, ann2} to minimise the log marginal likelihood.  In \autoref{experiments}, we compare these options to the neural network and Mat\'ern 3 covariance functions, and the \ac{BFGS} \citep{bfgs1, bfgs2, bfgs3, bfgs4} is tested as an optional optimiser.\pg
%
After the \ac{GP} is trained, a test dataset composed by uniformly distributed points over the entire region spanned by the input points can be used to set a grid, with spacing of $0.5$ between them. The outputs of the \ac{GP} regression are the prediction and the variance. A sigmoid function then flattens the prediction to confine it within the $[0;1]$ interval, reflecting the probability of occupancy of each point. The function used for this purpose in the experiments in this work takes the form:
%
\begin{equation}
\varsigma(\mu, v) = \Phi\left(\frac{(\alpha*\mu + \beta)}{\sqrt{1+\alpha^{2} * v}}\right),
\label{sigmoid}
\end{equation}
%
where $\mu$ is the prediction, $v$ is the variance, $\Phi$ is the normal cumulative distribution function and $\alpha$ and $\beta$ are parameters that can be experimentally chosen. A pair of thresholds can subsequently be used to classify the grid between the three occupancy states traditionally used for \acp{OGM}. For all maps presented in this thesis, unless otherwise stated, a point was considered free if its predicted occupancy probability was below $33\%$, and occupied if above $66\%$. Results can be seen in \autoref{map1}, and the whole algorithm in \autoref{mskgpomalg}.\pg
%
 \begin{figure}[!htp]
\center
    \subfloat[Probability of occupancy\label{gt}]{%
      \includegraphics[trim=40mm 95mm 40mm 97.5mm,clip,width=0.6\textwidth]{gfx/maps/pred_mat3_1p0_ann.pdf}}
    \\
    \subfloat[Variance]{%
      \includegraphics[trim=40mm 95mm 40mm 97.5mm,clip,width=0.6\textwidth]{gfx/maps/var_mat3_1p0_ann.pdf}}
    \\
    \subfloat[Occupancy map.\label{fh}]{%
      \includegraphics[trim=40mm 95mm 40mm 97mm,clip,width=0.6\textwidth]{gfx/maps/map_mat3_1p0_ann.pdf}}
\caption[Sample output]{Sample output. For the occupancy map (c), if a cell is less than $33\%$ likely to be occupied, it is marked as "free" (white), and if it's more than $66\%$ likely, it is marked as "occupied" (black). All remaining cells are "unknown" (grey).\label{map1}}\end{figure}\\
%
\begin{center}
\begin{minipage}{0.9\textwidth}
\singlespacing
\begin{lstlisting}[caption=MSK-GPOM algorithm\label{mskgpomalg}.]
input : B, par0, x|<\\>|# beams, parameters, test points|<\vspace{3mm}>|
[thresh, par, alpha, beta] := par0|<\vspace{3mm}>|
E := pre_process(B, thresh) # |<\texttt{\autoref{ppalg}}\vspace{3mm}>| 
m, var, lml := gp(E, msk(), par, x)|<\\>|# where gp is |<\texttt{\autoref{GPR}}>|, msk is |<\texttt{\autoref{mskalg}}>| and|<\\>|# par contains sig and the kernel's hyperparameters|<\vspace{3mm}>|
map := sigmoid(m, var, alpha, beta) # |<\texttt{\autoref{sigmoid}}>|
for all i in map:
   if map[i] < 0.33:
      map[i] := 0   # free
   else if map[i] < 0.66:
      map[i] := 0.5 # unknown
   else:
      map[i] := 1   # occupied|<\vspace{3mm}>|
output : map, var, lml|<\\>|# prediction at x, variances and log-marginal likelihood
\end{lstlisting}
\end{minipage}
\end{center}
%
\mychapter{Summary}
%
In this chapter, we have overviewed some previous work on change of support for kernel methods. \citeauthor{reidthesis}'s work on area kernels for image analysis and \citeauthor{simonthesis} work on line kernels for robotic mapping laid the path for this work.\pg
%
Multi-support kernels have been introduced. They offer an easy implementation that allows any kernel to be supported on multi-dimensional geometrical elements rather than points. They do not rely on integration and are able to generate reduced covariance matrices, making them suitable for applications that rely on large amounts of spatially correlated data.\pg
%
These qualities make \acp{MSK} well suited for robotic mapping. A procedure for creating geometrical elements from rangefinder data is described, and the algorithm for mapping with \acp{MSK-GP} is outlined and demonstrated for simulated data.\pg
%
In the next chapter, we further explore the \ac{MSK-GPOM} algorithm. Some choices of parameters and their effects on its performance are demonstrated. It is also benchmarked against \ac{GPOMIK}.
%