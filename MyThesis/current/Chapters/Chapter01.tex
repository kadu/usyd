%1. INTRO
% - Motivation
% - Problem Description
% - Contributions
% - Overview
%
%----------------------------------------------------------------------------------------
%
\begin{flushright}
\onehalfspacing
{\slshape
 Excuse me sir, but might I inquire as to what's going on? } \\ \smallskip
--- C3PO
\end{flushright}
%
%----------------------------------------------------------------------------------------

\label{ch:introduction}
\mychapter{Motivation}
In the dawn of their existence, autonomous systems were confined to industrial environments where they worked in assembly lines performing repetitive tasks. This was an early presage of what kinds of tasks they would be performing in modern society: nowadays, robots are widely used for tasks perceived as too dirty (\eg sewage pipe maintenance), dangerous (\eg defusing bombs or exploring hazardous environments) or dull (\eg domestic robots) for humans.\pg
%
In modern times, there is a growing trend to remove robots from controlled industrial environments and bring them to the outside world. Such is the case not only with self-driven cars, but also vacuum robots and security robots, for example. In all of those situations, the robots are in unknown environment, where they must be able to localise themselves while negotiating moving obstacles (such as living beings or other vehicles) and obscured regions.\pg
%
In most of these applications, it is impossible to pre-program robots with the maps they need. Rather, they should be equipped to build this maps themselves, based on available sensory information. The need for robust and reliable mapping algorithms to provide robots with a good notion of what their surroundings are like is greater than it has ever been. With mapping being a very fundamental task in robotics, improvements in this field can impact several applications.\\
%
\mychapter{Problem Description}
%
It is essential for a robot expected to interact with its surroundings to have a reliable spatial representation of them. Possible interactions include navigation, object recognition and manipulation or simply determining its own position relative to known landmarks. That means spatial modelling is arguably one of the most fundamental tasks in robotics. The amount of research in this field in the past decades makes this conclusion even more evident.\pg
%
The terrain mapping problem may be more simply expressed as the task of spatially modelling the robot's environment. This can be done in two main ways. The first is through a graph-like representation of important landmarks, connected via arcs that may contain information on how to get from a node to another. This is called a topological map of terrain. This however isn't very readily applied to navigation.\pg
%
The second approach are metric maps, which aim at capturing geometric information about the environment. To achieve this, the area to be mapped is generally represented as a grid. In natural or otherwise unstructured spaces, which frequently lack regular and easily recognisable structures, \acp{DEM} \citep{DEM} are a popular approach. In this method, the mapped terrain is represented by a 2D grid where each cell contains the approximate elevation at the corresponding coordinates.\pg
%
This work is concerned with structured terrain, such as inside a building or other manmade structure. An archetypical technique is the \ac{OGM} \citep*[\citeyear{moravek, elfes}]{moravekelfes}. In this method, the grid that overlays the terrain, rather than storing elevation, contains information relative to whether or not that part of space is occupied.\pg
%
Both DEMs and occupancy grids have the problems associated with discretisation: they may misrepresent a continuous phenomenon and the space and processing power needed to create and analyse the grid scale with the number of cells.\\
%
\begin{figure}[!htp]
\center
    \subfloat[Input data and ground truth.]{%
      \includegraphics[width=0.5\textwidth]{gfx/dataset0}}
    \hfill
    \subfloat[Occupancy grid map.]{%
      \includegraphics[width=0.5\textwidth]{gfx/OGM0}}
    \\
    \subfloat[Continuous occupancy map.]{%
      \includegraphics[width=0.5\textwidth]{gfx/com}}
      \hfill
    \subfloat[Associated variance.]{%
      \includegraphics[width=0.5\textwidth]{gfx/variance}}
\caption[Comparison: grid and continuous occupancy maps]{Comparison: occupancy grid map (b) and continuous occupancy map (c) with associated variance plot (d) generated from the same dataset (a). Continuous maps were sampled at the same resolution as the OGM. Taken from \citet*{simonthesis} \label{ogmcom}}
\end{figure}\\
%
A continuous occupancy map is a non-discretised representation of the environment. It can mend the difficulties of \acp{OGM} that stem from discretisation. The technique developed in this work falls within this category.\pg
%
To perform continuous mapping, we use a Bayesian learning technique known as \ac{GP}. It can be thought of as a generalisation of a Gaussian distribution. Much like the latter characterises a distribution of values through a mean and a covariance, the \ac{GP} uses a pair of a mean and a covariance functions to characterise a distribution over a space of possible functions. It learns these properties from the dataset.\\
%
\mychapter{Challenges and Contributions}
%
There are two main challenges to the technique proposed:
%
\begin{description}
\item[Input uncertainty] As is the case with any technique that depends on sensor readings, noise is inevitable. If the model fails to take input uncertainty into consideration, the maps generated will suffer in quality and potentially endanger the robot that uses them.
%
\item[large datasets] Another consequence of using sensors is the sheer volume of data provided by them. Modern laser rangefinders can generate thousands of readings per minute, which is a challenge to \acp{GP}, that scale cubically with the number of inputs.\\
\end{description}
%
In addressing the aforementioned challenges, the proposed algorithm offers the following contributions to the field:
%
\begin{description}
\item[Novel multi-support kernel] The kernel developed in this thesis is able to aggregate spatial data into generic geometric elements, reducing the number of inputs generated. Thanks to this, the algorithm can handle even the large amount of data produced by modern sensors with greater ease than state-of-the-art techniques.
%
\item[Novel mapping technique]  We propose a continuous mapping technique that uses a \ac{GP} equipped with the kernel described above. This allows the method to deal at once with the size of the datasets and the inaccuracy of sensors.
\end{description}
%
\mychapter{Thesis Overview}
%
This thesis is divided as follows:
%
\begin{description}
\item[\autoref{background}] presents the theoretical background that lays the foundation for this work. Robotic mapping is introduced and occupancy grid mapping, arguably the most popular metric mapping technique in use, explained in detail. Gaussian processes are introduced as a candidate to deal with the shortcomings in grid-based techniques, and their own drawbacks are presented.
%
\item[\autoref{theory}] is where the main contribution of this thesis is laid out. A brief review of related works is offered. Multi-support kernels are introduced and their use for continuous occupancy mapping demonstrated.
%
\item[\autoref{experiments}] conveys the result of the experiments made. The effect of each parameter on the output is analysed. The proposed algorithm is also benchmarked against the \acs{GPOMIK} \citep{simonthesis} algorithm with synthetic and real datasets.
%
\item[\autoref{conclusion}] concludes this work. The contributions made are restated and summarised. Possibilities of future research on this topic are outlined.
\end{description}






















