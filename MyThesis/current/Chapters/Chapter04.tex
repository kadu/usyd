% 4. Experiments
%   ? Benchmark for synthetic 2D data
%   ? Benchmark for real 2D data
%   ? Benchmarks for 3D data?
%   ? Demo: Continuous mapping from OGMs
%----------------------------------------------------------------------------------------
%
\begin{flushright}
\onehalfspacing
{\slshape
Bold, persistent experimentation is the hallmark of good science.} \\ \smallskip
--- GLaDOS
\end{flushright}
%
%----------------------------------------------------------------------------------------
\label{experiments}
\mychapter{Introduction}

In \autoref{theory}, we introduced the concept of \emph{change of support} for kernel methods, reviewing an example from the field of image analysis. A novel approach was proposed for \acfp{MSK}, that prescinds from integrating kernel function and can be used with any existing kernel. We also reviewed a method for continuous environment mapping using \acp{GP}, and proposed an approach using \acp{MSK} to generate \acp{GPOM}.\pg
%
In this chapter, we test the performance of the proposed \ac{MSK-GPOM} algorithm under different conditions to analyse the influence of certain parameters in its predictive ability. The algorithm's time performance is also compared to the \ac{GPOMIK} algorithm both for synthetic and real datasets.\\
%
\section*{System Specifications and experimental setup}
%
All the experiments described in this chapter are performed in a computer equipped with a $3.2$GHz Intel Core{\scriptsize\texttrademark} i5-3470 and 8GB RAM, running Ubuntu 14.04.2 LTS ("Trusty Tahr") 64 bits on a 3.13.0-46 generic GNU/Linux kernel and MATLAB$^{\tiny\textregistered}$ version 8.3.0.532 (R2014a).\pg
%
For all the experiments, accuracy is measured in terms of the area under the \ac{ROC} curve. The \ac{FPR} for a fixed \ac{TPR} of $95\%$ is offered as an additional performance metric.\\
% All the code is available in an online repository, hosted at *INSERT REPOSITORY ADDRESS*.\\
%
\mychapter{Parameters of the Model}\label{parmo}
%
There are some user-specified parameters that affect the performance of the model, interfering in its time efficiency and in the results obtained. We will demonstrate them in this section, using the same dataset as used in the example in \autoref{theory}.  For all the training benchmarks, to avoid the effects that random initialisation may have on training times, the initial hyperparameters were set to $\vec{1}$ (\ie, all-ones vectors of the appropriate dimension).
\\
%
\subsection{Element Generation}
%
The key contribution of this work is the addition of predictions supported on higher-dimension geometrical elements within the dataset. The input points are divided between these elements, and this allows to reduce, sometimes drastically, the number of entries in the covariance matrix. However, there is a tradeoff between the size of the covariance matrix and the predictive accuracy of the method.\pg
%
We discussed in the previous chapter how, as elements become larger, the number of points contained in each increases, creating fewer entries in the matrix. However, they can occasionally contain regions that have been sparsely probed, and their occupancy cannot always be reliably inferred by the occupancy of the entire element. As  the size of the elements becomes smaller, this problem is alleviated, but at the price of bigger covariance matrices, until the limit where each element contains a single point (amounting to a traditional \ac{GP}).\pg
%
The values used in the experiments described in this section were chosen by experimentation. \autoref{input} shows the difference in input size due to the threshold.\\
%
 \begin{figure}[!htp]
\center
    \subfloat[Ground truth\label{gt}]{%
      \includegraphics[trim=40mm 95mm 40mm 97.5mm,clip,width=0.5\textwidth]{gfx/maps/truth.pdf}}
    \hfill
    \subfloat[Dataset: free (green) and occupied (red) points]{%
      \includegraphics[trim=40mm 95mm 40mm 97.5mm,clip,width=0.5\textwidth]{gfx/maps/points.pdf}}
    \\
    \subfloat[Elements, threshold=0.5]{%
      \includegraphics[trim=40mm 95mm 40mm 97mm,clip,width=0.5\textwidth]{gfx/maps/el_0p5.pdf}}
      \hfill
          \subfloat[Elements, threshold=1.0]{%
      \includegraphics[trim=40mm 95mm 40mm 97mm,clip,width=0.5\textwidth]{gfx/maps/el_1p0.pdf}}
    \\
    \subfloat[Elements, threshold=2.0]{%
      \includegraphics[trim=40mm 95mm 40mm 97mm,clip,width=0.5\textwidth]{gfx/maps/el_2p0.pdf}}
      \hfill
          \subfloat[Elements, threshold=4.0]{%
      \includegraphics[trim=40mm 95mm 40mm 97mm,clip,width=0.5\textwidth]{gfx/maps/el_4p0.pdf}}
\caption[Element size threshold impact on element generation.]{Element size threshold impact on geometric element generation.\label{input}}
\end{figure}
%
\subsection{Covariance Function}
%
As discussed in \autoref{scovfun}, different covariance functions determine different properties for the functions in the model. They also differ in number of hyperparameters and computational cost. Therefore, it should come at no surprise that the choice of covariance function also affects the training time and outcomes of the algorithm.\\
%
\subsection{Optimiser}
%
To train a \ac{GP}, there is no standard choice of optimiser, and several can be used. Since optimisation is not the focus of this work, the optimisers chosen were the \ac{BFGS} cubic line search built into MATLAB's \texttt{fminunc} minimisation function and annealing, which is the option made by \citet{simonthesis}.\\
%
\section*{Results}
%
The influence of the above parameters in the training time of the algorithm can be seen in \autoref{traintime}. As expected, training time decreases as the element size threshold increases. BFGS optimisation runs considerably faster than annealing, and the Mat\'ern 3 kernel has a small advantage over the squared exponential. The choice of optimiser did not significantly influence the performance of the algorithms, and was omitted from the remaining analyses.\pg
%
\begin{table}[h]
%\small
\center
\caption[Influence of parameters on MSK-GPOM learning time]{Influence of the choice of covariance function, optimiser and element size threshold on the time it takes to learn the hyperparameters from the datasets presented in \autoref{input}. \label{traintime}}
\vspace{5mm}
\begin{tabular}{c|c|c|c}
\multirow{2}{*}{Kernel}                                       & \multirow{2}{*}{\begin{tabular}[c]{@{}c@{}}Element size\\ threshold\end{tabular}} & \multicolumn{2}{c}{Optimiser}                                                                                                                                                                 \\ \cline{3-4} 
                                                              &                                                                                   & BFGS                                                                                          & Annealing                                                                                      \\ \hline
\begin{tabular}[c]{@{}c@{}}Squared\\ Exponential\end{tabular} & \begin{tabular}[c]{@{}c@{}}0.5\\ 1\\ 2\\ 4\end{tabular}                           & \begin{tabular}[c]{@{}c@{}}\hspace{3mm}$2.80*10^{3}$s\hspace{3mm}\\ $686.35$s\\ $72.19$s\\ $28.43$s\end{tabular}      & \begin{tabular}[c]{@{}c@{}}$1.97*10^{4}$s\\ $6.41*10^{3}$s\\ $166.81$s\\ $39.40$s\end{tabular} \\ \hline
Mat�rn 3                                                      & \begin{tabular}[c]{@{}c@{}}0.5\\ 1\\ 2\\ 4\end{tabular}                           & \begin{tabular}[c]{@{}c@{}}$1.92*10^{3}$s\\ $1.80*10^{3}$s\\ $81.48$s\\ $20.98$s\end{tabular} & \begin{tabular}[c]{@{}c@{}}$2.24*10^{4}$s\\ $6.64*10^{3}$s\\ $157.03$s\\ $35.06$s\end{tabular}
\end{tabular}
\end{table}\\
%
\autoref{runtime} has the performance analyses for different choices of covariance function and element size threshold. We can see that the Mat\'ern 3 slightly outperforms the squared exponential kernel. Contrary to the expected, smaller element size thresholds lead to longer running times and better accuracy only to an extent. A drop in performance can be noted once the smallest allowed elements become smaller than the smallest dimension of the obstacles, which provides a tentative way to set the element size threshold.\pg
%
\begin{table}[h]
%\small
\center
\caption[Influence of parameters on MSK-GPOM performance]{Influence of the choice of covariance function and element size threshold on the time to create a map using parameters previously learned and accuracy of the resulting map. \label{runtime}}
\vspace{5mm}
\begin{tabular}{c|c|c|c|c}
Kernel                                                        & \begin{tabular}[c]{@{}c@{}}Element size\\ threshold\end{tabular} & Running time                                                           & Accuracy                                                                      & \begin{tabular}[c]{@{}c@{}}FPR when \\ TPR = 95\%\end{tabular}               \\ \hline
\begin{tabular}[c]{@{}c@{}}Squared\\ Exponential\end{tabular} & \begin{tabular}[c]{@{}c@{}}0.5\\ 1\\ 2\\ 4\end{tabular}          & \begin{tabular}[c]{@{}c@{}}11.91s\\ 5.33s\\ 3.69s\\ 3.30s\end{tabular} & \begin{tabular}[c]{@{}c@{}}0.9210\\ 0.9554\\ 0.8545\\ 0.6996\end{tabular} & \begin{tabular}[c]{@{}c@{}}11.50\%\\ 8.25\%\\ 32.49\%\\ 75.33\%\end{tabular} \\ \hline
Mat�rn 3                                                      & \begin{tabular}[c]{@{}c@{}}0.5\\ 1\\ 2\\ 4\end{tabular}          & \begin{tabular}[c]{@{}c@{}}11.34s\\ 5.63s\\ 3.77s\\ 3.31s\end{tabular} & \begin{tabular}[c]{@{}c@{}}0.9291\\ 0.9767\\ 0.8947\\ 0.7049\end{tabular} & \begin{tabular}[c]{@{}c@{}}12.75\%\\ 8.43\%\\ 25.58\%\\ 74.48\%\end{tabular}                   
\end{tabular}
\end{table}\\
%
A step-by-step illustration of the map generation process , from the dataset to the final map, is displayed for two different size thresholds from the table above in Figures \ref{sqexp} and \ref{mat3}. They shed some insight into the reasons why the performance drops under thresholds of 1.0: comparing the input elements  (subplot (c)) of each shows that, once the smallest elements become smaller than the obstacle, the peaks they generate in the prediction (subplot (d)) become less sharp. This  \pg
%
 \begin{figure}[!htp]
\center
    \subfloat[Initial Dataset]{%
      \includegraphics[trim=40mm 95mm 40mm 97.5mm,clip,width=0.5\textwidth]{gfx/maps/data.pdf}}
    \hfill
    \subfloat[Free and occupied points]{%
      \includegraphics[trim=40mm 95mm 40mm 97mm,clip,width=0.5\textwidth]{gfx/maps/points.pdf}}
      \\
          \subfloat[Input Elements\label{elsp}]{%
      \includegraphics[trim=40mm 95mm 40mm 97mm,clip,width=0.5\textwidth]{gfx/maps/el_0p5.pdf}}
      \hfill
    \subfloat[Prob. Occupancy]{%
      \includegraphics[trim=40mm 95mm 40mm 97.5mm,clip,width=0.5\textwidth]{gfx/maps/pred_mat3_0p5_ann.pdf}}
    \hfill
    \subfloat[Predictive Variance]{%
      \includegraphics[trim=40mm 95mm 40mm 97mm,clip,width=0.5\textwidth]{gfx/maps/var_mat3_0p5_ann.pdf}}
      \hfill
          \subfloat[Occupancy Map]{%
      \includegraphics[trim=40mm 95mm 40mm 97mm,clip,width=0.5\textwidth]{gfx/maps/map_mat3_0p5_ann.pdf}}
%
\caption[Map generation process]{Step-by-step map generation for a MSK-GPOM with Mat\'ern 3 kernel. Element size threshold is 0.5, training method was annealing.\label{sqexp}}
\end{figure}
%
\begin{figure}[!htp]
\center
    \subfloat[Initial Dataset]{%
      \includegraphics[trim=40mm 95mm 40mm 97.5mm,clip,width=0.5\textwidth]{gfx/maps/data.pdf}}
    \hfill
    \subfloat[Free and occupied points]{%
      \includegraphics[trim=40mm 95mm 40mm 97mm,clip,width=0.5\textwidth]{gfx/maps/points.pdf}}
      \\
          \subfloat[Input Elements]{%
      \includegraphics[trim=40mm 95mm 40mm 97mm,clip,width=0.5\textwidth]{gfx/maps/el_1p0.pdf}}
      \hfill
    \subfloat[Prob. Occupancy]{%
      \includegraphics[trim=40mm 95mm 40mm 97.5mm,clip,width=0.5\textwidth]{gfx/maps/pred_mat3_1p0_ann.pdf}}
    \hfill
    \subfloat[Predictive Variance]{%
      \includegraphics[trim=40mm 95mm 40mm 97mm,clip,width=0.5\textwidth]{gfx/maps/var_mat3_1p0_ann.pdf}}
      \hfill
          \subfloat[Occupancy Map]{%
      \includegraphics[trim=40mm 95mm 40mm 97mm,clip,width=0.5\textwidth]{gfx/maps/map_mat3_1p0_ann.pdf}}
%
\caption[Map generation process]{Step-by-step map generation for a MSK-GPOM with Mat\'ern 3 kernel. Element size threshold is 1.0, training method was annealing.\label{mat3}}
\end{figure}
%
\newpage
%
\mychapter{Benchmarks}\label{bench}
%
In all the benchmarks described in this section, the proposed algorithm was compared solely to \ac{GPOMIK} \citep*{simonthesis}. The reason behind this choice is that \ac{GPOMIK} was the most efficient \ac{GPOM} algorithm available at the time of the elaboration of this thesis, and it was shown in \citet*{simonaaai} to outperform all the competing algorithms analysed (see \autoref{simtab} and \autoref{simik}). The following experiments will show that \ac{MSK-GPOM} was able to achieve comparable performance, so the comparison with previous algorithms was deemed unnecessary.\pg
%
All benchmarks are made using identical test datasets for both methods. Input datasets differ only in the way each algorithm preprocesses the raw inputs.\\
%
\section{Synthetic Data}
%
For this test, the dataset used was larger than the one used in \autoref{parmo}, but generated by the same method. It is similar to the one used in \citet*[chapter 4]{simonthesis}. For the \ac{MSK-GPOM}, an element size threshold of $1.0$ was chosen. For the \ac{GPOMIK}, the active input sampling method described in \citet*{simonaaai} was used. The dataset and inputs for each can be seen in \autoref{inputsies}.\pg
%
Both algorithms were tested with the Mat\'ern 3 and the squared exponential kernel, each trained through annealing. The runtimes and performance analyses for the methods are shown on \autoref{benchmark} and \autoref{roc}. The predictions made by each are shown in figures \autoref{gpomik} and \autoref{mskgpom}.\pg
%
\begin{figure}[!htp]
\center
    \subfloat[Ground truth]{%
      \includegraphics[trim=40mm 95mm 40mm 97.5mm,clip,width=0.5\textwidth]{gfx/benchmark/gt.pdf}}
    \hfill
    \subfloat[Rangefinder data: free beams and hits.]{%
      \includegraphics[trim=40mm 95mm 40mm 97.5mm,clip,width=0.5\textwidth]{gfx/benchmark/beamhits.pdf}}
    \\
    \subfloat[Free points in green, occupied in red.]{%
      \includegraphics[trim=40mm 95mm 40mm 97mm,clip,width=0.5\textwidth]{gfx/benchmark/fph.pdf}}
      \hfill
          \subfloat[Dataset partitioned into elements.]{%
      \includegraphics[trim=40mm 95mm 40mm 97mm,clip,width=0.5\textwidth]{gfx/benchmark/input.pdf}}
    \\
    \subfloat[Beams and hits chosen by active sampling for the squared exponential kernel.]{%
      \includegraphics[trim=40mm 95mm 40mm 97mm,clip,width=0.5\textwidth]{gfx/benchmark/simoninput.pdf}}
      \hfill
    \subfloat[Beams and hits chosen by active sampling for the Mat\'ern 3 kernel.]{%
      \includegraphics[trim=40mm 95mm 40mm 97mm,clip,width=0.5\textwidth]{gfx/benchmark/simoninput.pdf}}
\caption{Benchmark dataset and inputs.\label{inputsies}}
\end{figure}
%
\newpage
%
\begin{table}[h]
%\small
\center
\caption[MSK-GPOM versus GPOMIK benchmark]{Benchmark times for GPOMIK and MSK-GPOM, both trained with annealing. \label{benchmark}}
\vspace{5mm}
\begin{tabular}{c|c|c|c|c}
Method   & Kernel                                                                 & Running time                                            & Accuracy                                                  & \begin{tabular}[c]{@{}c@{}}FPR when \\ TPR = 95\%\end{tabular} \\ \hline
GPOMIK   & \begin{tabular}[c]{@{}c@{}}Mat�rn 3\\ Squared Exponential\end{tabular} & \begin{tabular}[c]{@{}c@{}}79.46s\\ 86.58s\end{tabular} & \begin{tabular}[c]{@{}c@{}}0.9447\\ 0.9415\end{tabular} & \begin{tabular}[c]{@{}c@{}}9.76\%\\ 12.25\%\end{tabular}       \\ \hline
MSK-GPOM & \begin{tabular}[c]{@{}c@{}}Mat�rn 3\\ Squared Exponential\end{tabular} & \begin{tabular}[c]{@{}c@{}}23.76s\\ 19.33s\end{tabular} & \begin{tabular}[c]{@{}c@{}}0.9416\\ 0.9266\end{tabular} & \begin{tabular}[c]{@{}c@{}}3.30\%\\ 2.00\%\end{tabular}       
\end{tabular}
\end{table}
%
 \begin{figure}[!htp]
\center
    \subfloat[Squared exponential kernel]{%
      \includegraphics[trim=25mm 73.25mm 30mm 73.5mm,clip,width=0.5\textwidth]{gfx/benchmark/rocsqexp.pdf}}
    \hfill
    \subfloat[Mat\'ern 3 kernel]{%
      \includegraphics[trim=25mm 73.5mm 30mm 73mm,clip,width=0.5\textwidth]{gfx/benchmark/rocmat3.pdf}}
\caption[ROC curves for the synthetic dataset]{ROC curves for the synthetic dataset. The curve for MSK-GPOM is represented in blue, GPOMIK in red.\label{roc}}
\end{figure}
%
\newpage
%
 \begin{figure}[!htp]
\center
    \subfloat[Probability of Occupancy]{%
      \includegraphics[trim=40mm 95mm 40mm 97.5mm,clip,width=0.5\textwidth]{gfx/benchmark/simonprobsqexp.pdf}}
    \hfill
    \subfloat[Probability of Occupancy]{%
      \includegraphics[trim=40mm 95mm 40mm 97.5mm,clip,width=0.5\textwidth]{gfx/benchmark/simonprobmat3.pdf}}
    \\
    \subfloat[Predictive Variance]{%
      \includegraphics[trim=40mm 95mm 40mm 97mm,clip,width=0.5\textwidth]{gfx/benchmark/simonvarsqexp.pdf}}
      \hfill
          \subfloat[Predictive Variance]{%
      \includegraphics[trim=40mm 95mm 40mm 97mm,clip,width=0.5\textwidth]{gfx/benchmark/simonvarmat3.pdf}}
    \\
    \subfloat[Occupancy Map]{%
      \includegraphics[trim=40mm 95mm 40mm 97mm,clip,width=0.5\textwidth]{gfx/benchmark/simonmapsqexp.pdf}}
      \hfill
          \subfloat[Occupancy Map]{%
      \includegraphics[trim=40mm 95mm 40mm 97mm,clip,width=0.5\textwidth]{gfx/benchmark/simonmapmat3.pdf}}
\caption[GPOMIK outputs for synthetic dataset]{GPOMIK outputs for synthetic dataset. Left column: squared exponential kernel. Right column: Mat\'ern 3 kernel.\label{gpomik}}
\end{figure}
%
\newpage
%
 \begin{figure}[!htp]
\center
    \subfloat[Probability of Occupancy]{%
      \includegraphics[trim=40mm 95mm 40mm 97.5mm,clip,width=0.5\textwidth]{gfx/benchmark/probsqexp.pdf}}
    \hfill
    \subfloat[Probability of Occupancy]{%
      \includegraphics[trim=40mm 95mm 40mm 97.5mm,clip,width=0.5\textwidth]{gfx/benchmark/probmat3.pdf}}
    \\
    \subfloat[Predictive Variance]{%
      \includegraphics[trim=40mm 95mm 40mm 97mm,clip,width=0.5\textwidth]{gfx/benchmark/varsqexp.pdf}}
      \hfill
          \subfloat[Predictive Variance]{%
      \includegraphics[trim=40mm 95mm 40mm 97mm,clip,width=0.5\textwidth]{gfx/benchmark/varmat3.pdf}}
    \\
    \subfloat[Occupancy Map]{%
      \includegraphics[trim=40mm 95mm 40mm 97mm,clip,width=0.5\textwidth]{gfx/benchmark/mapsqexp.pdf}}
      \hfill
          \subfloat[Occupancy Map]{%
      \includegraphics[trim=40mm 95mm 40mm 97mm,clip,width=0.5\textwidth]{gfx/benchmark/mapmat3.pdf}}
\caption[MSK-GPOM outputs for synthetic dataset]{MSK-GPOM outputs for synthetic dataset. Left column: squared exponential kernel. Right column: Mat\'ern 3 kernel.\label{mskgpom}}
\end{figure}
%
\newpage
%
\section{Real Data}
%
Although the benchmarks in the last section suggest the algorithm does indeed present an advantage over \ac{GPOMIK}, synthetic data offers a lot of conveniences that in a real setting wouldn't exist, such as noiseless data. To be able to truly assess the usability of the \ac{MSK-GPOM} algorithm, we must test it using datasets taken by real robots.\pg
%
The raw data was provided by Dirk Haehnel, and contains 395 poses with 361 equally spaced laser readings spanning 180 degrees taken from each. The data was collected at the Belgioioso Castle, located in Milan. It is made available on the Robotics Datasets webpage, maintained by Cyrill \citet{stachniss}\footnote{http://www2.informatik.uni-freiburg.de/~stachnis/datasets/}. The data is presented both in raw form and after loop closure, the latter of which was used in this benchmark.\pg
%
To test the algorithm, a subset of 52 poses was used, from which 37 equally spaced laser beams were taken into consideration. This data refers to two adjacent rooms in the castle, and can be seen in \autoref{realinput}.\pg
%
 \begin{figure}[!htp]
\center
    \subfloat[Rangefinder data]{%
      \includegraphics[trim=40mm 95mm 40mm 97.5mm,clip,width=0.45\textwidth]{gfx/realdata/data.pdf}}
    \hfill
    \subfloat[Elements, threshold=1]{%
      \includegraphics[trim=40mm 95mm 40mm 97.5mm,clip,width=0.45\textwidth]{gfx/realdata/input.pdf}}
    \\
    \subfloat[Active set (squared exponential kernel)]{%
      \includegraphics[trim=40mm 95mm 40mm 97mm,clip,width=0.45\textwidth]{gfx/realdata/simoninput.pdf}}
      \hfill
          \subfloat[Active set (Mat\'ern 3 kernel)]{%
      \includegraphics[trim=40mm 95mm 40mm 97mm,clip,width=0.45\textwidth]{gfx/realdata/simonmat3input.pdf}}
\caption{Rangefinder data and inputs for each method.\label{realinput}}
\end{figure}\\
%
The proposed algorithm was trained using \ac{BFGS}, with an element size threshold of 1.0 on both the squared exponential and the Mat\'ern 3 kernels. For the contending algorithm, the parameters used were those presented in \cite{simonaaai}, presumably reflecting the algorithm's best configuration.\pg
%
Accuracy was measured by using the trained \acp{GP} to predict the occupancy of two thousand points (divided evenly between free and occupied), randomly sampled from the beams that weren't used to generate the predictions above. The sample used was the same for all algorithms. Running times and performance analyses displayed in \autoref{realbenchmark} and \autoref{realroc}. The predictions can be seen in \autoref{realgpomik} and \autoref{realmskgpom}.\pg
%
\begin{table}[h]
%\small
\center
\caption[MSK-GPOM versus GPOMIK benchmark]{Benchmark times for GPOMIK and MSK-GPOM, both trained with annealing. \label{realbenchmark}}
\vspace{5mm}
\begin{tabular}{c|c|c|c|c}
Method   & Kernel                                                                 & Running time                                              & Accuracy                                                  & \begin{tabular}[c]{@{}c@{}}FPR when \\ TPR = 95\%\end{tabular} \\ \hline
GPOMIK   & \begin{tabular}[c]{@{}c@{}}Mat�rn 3\\ Squared Exponential\end{tabular} & \begin{tabular}[c]{@{}c@{}}445.42s\\ 607.93s\end{tabular} & \begin{tabular}[c]{@{}c@{}}0.9751\\ 0.9665\end{tabular}   & \begin{tabular}[c]{@{}c@{}}9.60\%\\ 8.20\%\end{tabular}         \\ \hline
MSK-GPOM & \begin{tabular}[c]{@{}c@{}}Mat�rn 3\\ Squared Exponential\end{tabular} & \begin{tabular}[c]{@{}c@{}}52.07s\\ 37.29s\end{tabular}   & \begin{tabular}[c]{@{}c@{}}0.9918\\ 0.9947\end{tabular} & \begin{tabular}[c]{@{}c@{}}3.30\%\\ 2.00\%\end{tabular}       
\end{tabular}
\end{table}
%
 \begin{figure}[!htp]
\center
    \subfloat[Squared exponential kernel]{%
      \includegraphics[trim=25mm 73.5mm 30mm 73.4mm,clip,width=0.5\textwidth]{gfx/realdata/rocsqexp.pdf}}
    \hfill
    \subfloat[Mat\'ern 3 kernel]{%
      \includegraphics[trim=25mm 74.25mm 30mm 74.5mm,clip,width=0.5\textwidth]{gfx/realdata/rocmat3.pdf}}
\caption[ROC curves for the Belgioioso Castle dataset]{ROC curves for the Belgioioso Castle dataset. MSK-GPOM in blue, GPOMIK in red.\label{realroc}}
\end{figure}
%
 \begin{figure}[!htp]
\center
    \subfloat[Probability of Occupancy]{%
      \includegraphics[trim=40mm 95mm 40mm 97.5mm,clip,width=0.5\textwidth]{gfx/realdata/simonpred.pdf}}
    \hfill
    \subfloat[Probability of Occupancy]{%
      \includegraphics[trim=40mm 95mm 40mm 97.5mm,clip,width=0.5\textwidth]{gfx/realdata/simonmat3pred.pdf}}
    \\
    \subfloat[Predictive Variance]{%
      \includegraphics[trim=40mm 95mm 40mm 97mm,clip,width=0.5\textwidth]{gfx/realdata/simonvar.pdf}}
      \hfill
          \subfloat[Predictive Variance]{%
      \includegraphics[trim=40mm 95mm 40mm 97mm,clip,width=0.5\textwidth]{gfx/realdata/simonmat3var.pdf}}
    \\
    \subfloat[Occupancy Map]{%
      \includegraphics[trim=40mm 95mm 40mm 97mm,clip,width=0.5\textwidth]{gfx/realdata/simonmap.pdf}}
      \hfill
          \subfloat[Occupancy Map]{%
      \includegraphics[trim=40mm 95mm 40mm 97mm,clip,width=0.5\textwidth]{gfx/realdata/simonmat3map.pdf}}
\caption[GPOMIK outputs for real dataset]{GPOMIK outputs for real dataset. Left column: squared exponential kernel. Right column: Mat\'ern 3 kernel.\label{realgpomik}}
\end{figure}\\
%
 \begin{figure}[!htp]
\center
    \subfloat[Probability of Occupancy]{%
      \includegraphics[trim=40mm 95mm 40mm 97.5mm,clip,width=0.5\textwidth]{gfx/realdata/sqexppred.pdf}}
    \hfill
    \subfloat[Probability of Occupancy]{%
      \includegraphics[trim=40mm 95mm 40mm 97.5mm,clip,width=0.5\textwidth]{gfx/realdata/mat3pred.pdf}}
    \\
    \subfloat[Predictive Variance]{%
      \includegraphics[trim=40mm 95mm 40mm 97mm,clip,width=0.5\textwidth]{gfx/realdata/sqexpvar.pdf}}
      \hfill
          \subfloat[Predictive Variance]{%
      \includegraphics[trim=40mm 95mm 40mm 97mm,clip,width=0.5\textwidth]{gfx/realdata/mat3var.pdf}}
    \\
    \subfloat[Occupancy Map]{%
      \includegraphics[trim=40mm 95mm 40mm 97mm,clip,width=0.5\textwidth]{gfx/realdata/sqexpmap.pdf}}
      \hfill
          \subfloat[Occupancy Map]{%
      \includegraphics[trim=40mm 95mm 40mm 97mm,clip,width=0.5\textwidth]{gfx/realdata/mat3map.pdf}}
\caption[MSK-GPOM outputs for real dataset]{MSK-GPOM outputs for real dataset. Left column: squared exponential kernel. Right column: Mat\'ern 3 kernel.\label{realmskgpom}}
\end{figure}\newpage
%
\mychapter{Summary}
%
In this chapter, we have analysed the performance of the \ac{MSK-GPOM} in several different conditions. A series of experiments were conduced to analyse how different parameters affect the output of the algorithm. It was also compared to a previous technique that performs a similar function.\pg
%
It is safe to conclude that the method developed has performance comparable to the \ac{GPOMIK} algorithm, both in simulated and real scenarios. It was also shown to be much faster than the latter. This however must be interpreted carefully, as the \ac{GPOMIK} is tailored to do online predictions, while the \ac{MSK-GPOM} excels at batch mapping, and the experiments in this chapter just aim to provide a proof-of-concept for the viability of the technique presented in this thesis.
%
In the next chapter, we conclude this work by summarising the contributions made and outlining the future work that must be made on this algorithm.

















