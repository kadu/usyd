
function  [Hits] = findLaserHits3d(Corners,beamx, beamy, beamz)


NumberOfBeams = size(beamx,2);
NumberOfPlanes = size(Corners,2);

Hits = [];

for BeamNum = 1:NumberOfBeams
    
    PlanesHitByBeam = [];
    
    for PlaneNum = 1:NumberOfPlanes
        
        PlaneCorner1 = Corners(:,PlaneNum,1)';
        PlaneCorner2 = Corners(:,PlaneNum,2)';
        PlaneCorner3 = Corners(:,PlaneNum,3)';
        PlaneCorner4 = Corners(:,PlaneNum,4)';
        
        normalofPlane =cross(PlaneCorner1-PlaneCorner2, PlaneCorner1-PlaneCorner3);
        V0 = PlaneCorner1;
        P0 = [0 0 0];
        P1 = [beamx(BeamNum), beamy(BeamNum), beamz(BeamNum)];
        
        [I,check]=plane_line_intersect(normalofPlane,V0,P0,P1);
        
        if check == 1
            OnPlaneSeg = isHitOnPlaneSegment(PlaneCorner1,PlaneCorner2,PlaneCorner3,PlaneCorner4, I);
            if OnPlaneSeg == 1
                Hit = I;
            else
                Hit = P1;
            end
        else
            Hit = P1;
        end
        
        PlanesHitByBeam = [PlanesHitByBeam Hit'];
    end
    [theta,phi,r] = cart2sph(PlanesHitByBeam(1,:),PlanesHitByBeam(2,:),PlanesHitByBeam(3,:));
    
    [minr minindex] = min(r);
    
    Hit = PlanesHitByBeam(:,minindex);
    
    Hits = [Hits Hit];
    
end


