
function [WorldX WorldY WorldZ] = orientEnvironToRobot(WorldX, WorldY, WorldZ,Time,Direction,RobotLocation,RobotOrientation)

 %Translate
        if Time == 0
           CurGloAngHor=0;     %Current Global Angles
           WorldX = WorldX - RobotLocation(1,:);
           WorldY = WorldY - RobotLocation(2,:);
           WorldZ = WorldZ - RobotLocation(3,:);
        else
           CurGloAngHor =  (sum(Direction(1:Time-1))+pi/2);
           RotMat = [cos(CurGloAngHor) -sin(CurGloAngHor);sin(CurGloAngHor) cos(CurGloAngHor)];
           WorldX = WorldX - RobotLocation(1,:);
           WorldY = WorldY - RobotLocation(2,:);
           WorldZ = WorldZ - RobotLocation(3,:);
        end

        %Rotate the map
        if Time == 0
           Angle = (pi/2-RobotOrientation);
           PhiAngle = 0    %For the time being, we do not consider pitch
        else
            Angle = (pi/2-RobotOrientation);  
            PhiAngle = 0;    %For the time being, we do not consider pitch
        end

        %Convert to map to spherical
        [Theta, Phi, R] = cart2sph(WorldX,WorldY,WorldZ);

        %Rotate
        Theta = Theta + Angle;
        Phi = Phi + PhiAngle;

        %Convert back to cartesian
        [WorldX,WorldY,WorldZ] = sph2cart(Theta, Phi, R);