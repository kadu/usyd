
% Script that generates the laser data obtained by a robot as it navigates 
% a user-created environment moving along a predetermined path. It also
% outputs the pose of the robot during each of the scans so successive
% scans can be merged together precisely.


%INCLUDES CHANGES IN HEIGHT - WORK IN PROGRESS!

%Set up to handle multiple robots

clc
close all
clear all

disp(' ')
disp('*********LaserDataGenerator 3D***********')
disp(' ')
clear all
addpath Scenarios

%% Load Scenario File (World Map and Laser settings and Initial Conditions)
% Room3D;
% Environment3D
% BoxOnFloor
% RobotRoom;
% RobotRoomFullHeight;
RobotRoomComplex

%% Display World Map


%Find Corners
Corners = findCorners(WorldMap);


% Evaluate Planes
[PlaneEq] = evaluatePlanes(Corners);

%Plot the ground truth
scrsz = get(0,'ScreenSize');
figure('OuterPosition',scrsz)
view(40,45)
plotPlanes(Corners, DisplayObject)
pause

%Transform Corners Matrix into X Y Zs
[WorldX WorldY WorldZ NumberOfPlanes] = cornerMat2Vector(Corners);

%% Initialise Settings
SimData = [];
disp('Environment Loaded')
disp('Robot Path Loaded')
disp(' ')




%% Begin simulating data
disp('Running Simulation...')
disp(' ')

NumberOfRobots = size(SpeedsOfRobots,2);



% Generate data for each robot separately
for Robot = 1:NumberOfRobots
    Corners = findCorners(WorldMap);
    [WorldX WorldY WorldZ NumberOfPlanes] = cornerMat2Vector(Corners);
%    if Robot==2
%       pause 
%    end
    RangeFinderData = [];
    Pose = [];
    LaserOrientations = [];

    %load in robot parameters
    InitialLocation = RobotFixedParameters.InitialLocation(:,Robot);
    InitialOrientation = RobotFixedParameters.InitialOrientation(Robot)*pi/180;
    DegreesOfSweepHoriz = RobotFixedParameters.DegreesOfSweepHoriz(Robot)*pi/180;
    DegreesOfSweepVert = RobotFixedParameters.DegreesOfSweepVert(Robot)*pi/180;
    NumberOfBeamsHoriz = RobotFixedParameters.NumberOfBeamsHoriz(Robot);
    NumberOfBeamsVert = RobotFixedParameters.NumberOfBeamsVert(Robot);
    MaxRange = RobotFixedParameters.MaxRange(Robot);
    LaserAngVarHoriz = RobotFixedParameters.LaserAngVarHoriz(Robot)*pi/180;
    LaserAngVarVert = RobotFixedParameters.LaserAngVarVert(Robot)*pi/180;
    LaserRangVar = RobotFixedParameters.LaserRangVar(Robot);
    SpeedVar = RobotFixedParameters.SpeedVar(Robot);
    DirectionVarHoriz = RobotFixedParameters.DirectionVarHoriz(Robot);
    
    Direction = DirectionsOfRobots{Robot}*pi/180;
    RobotSpeed = SpeedsOfRobots{Robot};
    HeightChange = HeightChangeOfRobots{Robot};



    for Time = 0:size(RobotSpeed,2)

        if Time == 0
            RobotLocation = InitialLocation;
            RobotOrientation = InitialOrientation(1);
            PoseInfo = [RobotLocation;RobotOrientation];
        else
            RobotLocation = [RobotSpeed(Time)*cos(Direction(Time)+pi/2);...
                RobotSpeed(Time)*sin(Direction(Time)+pi/2);...
                HeightChange(Time)];
            RobotOrientation = Direction(Time)+pi/2;
            PoseInfo = [PoseInfo,PoseInfo(:,end)+...
                [RobotSpeed(Time)*cos(Direction(Time)+PoseInfo(4,end));...
                RobotSpeed(Time)*sin(Direction(Time)+PoseInfo(4,end));...
                HeightChange(Time);...
                Direction(Time)]];
        end

        %=== Orient Environment To Robot Starting Point========
       
        [WorldX WorldY WorldZ] = orientEnvironToRobot(WorldX, WorldY, WorldZ,Time,Direction,RobotLocation,RobotOrientation);
  
        %===================================================
        
        %Transform X Y Z back into corners
        Corners = vector2CornerMat(WorldX,WorldY,WorldZ,NumberOfPlanes);
        
        
        if Time ==0
            figure('OuterPosition',scrsz) %Generate figure of current robot pose [10 scrsz(2)/1.1 scrsz(3)/1.1 scrsz(4)/1.1]
        end
            hold off
            plot3(0,0,0,'ro')
            plotPlanes(Corners, DisplayObject)
            hold on
            
            title('New Map')
            axis equal


            
            
            
        %% Create Laser Beam Segments

        phi = repmat(linspace(0-DegreesOfSweepVert/2,DegreesOfSweepVert/2,NumberOfBeamsVert),NumberOfBeamsHoriz,1);
        theta = repmat(linspace(pi/2 - DegreesOfSweepHoriz/2,pi/2 + DegreesOfSweepHoriz/2,NumberOfBeamsHoriz)',1,NumberOfBeamsVert);
        [beamx beamy beamz] = sph2cart(theta,phi,MaxRange*ones(NumberOfBeamsHoriz,NumberOfBeamsVert));
        beamx = reshape(beamx,1,numel(beamx));
        beamy = reshape(beamy,1,numel(beamx));
        beamz = reshape(beamz,1,numel(beamx));
        
        

        %% Find Points of intersection
        
        Hits = findLaserHits3d(Corners,beamx, beamy, beamz);
        
        %Plot all returned signals
        [thetret phiret rret] = cart2sph(Hits(1,:),Hits(2,:),Hits(3,:));
        retIndex = find(rret<MaxRange-0.1);
        plot3(Hits(1,retIndex),Hits(2,retIndex),Hits(3,retIndex),'b+');
        plot3([Hits(1, retIndex); zeros(1,numel(retIndex))],[Hits(2,retIndex); zeros(1,numel(retIndex))],[Hits(3,retIndex); zeros(1,numel(retIndex))],'g')
        plot3([0;0],[0;0.5],[0;0],'r')
        hold off
        view(0,90)
         pause(0.1)

%         [PolarHitsTheta,PolarHitsR] = cart2pol(Hits(1,:),Hits(2,:));
%         PolarHits = [PolarHitsTheta;PolarHitsR];

        RangeFinderData = [RangeFinderData; rret];
        LaserOrientations = cat(3, LaserOrientations, [thetret; phiret]);
        

    end
 
  %=====================================================================

    %Creates stucture containing only laser info
    %--------Evaluates Pose Relative to Vehicles Origin for each scan
    [TempThet,TempPhi,TempR]= cart2sph(PoseInfo(1,:)-PoseInfo(1,1),PoseInfo(2,:)-PoseInfo(2,1),PoseInfo(3,:)-PoseInfo(3,1));
    TempThet= TempThet - PoseInfo(4,1);
    [Pose(1,:),Pose(2,:), Pose(3,:)] = sph2cart(TempThet,TempPhi,TempR);

    % Pose = [PoseInfo(1,:)-PoseInfo(1,1);PoseInfo(2,:)-PoseInfo(2,1);PoseInfo(3,:)-PoseInfo(3,1)];
    Pose(4,:) = (PoseInfo(4,:)-PoseInfo(4,1));

    %---------------------------------------------------------
    %% Structure of ranges orientations and robot pose

    RobotDataKnown = struct('RangeFinderData',RangeFinderData,'Pose',Pose,'LaserOrientations',LaserOrientations,...
                     'BeamsPerScan',[NumberOfBeamsHoriz;NumberOfBeamsHoriz],'InitialLocation',InitialLocation,'InitialOrientation',InitialOrientation,...
                     'MaxRange', MaxRange);

    SimData = [SimData RobotDataKnown];

end


disp('Simulation Complete.')
disp(' ')


save SimData SimData
disp('--Laser Data and Pose Data saved as SimData')
disp(' ')
pause(0.5)



%==========================================================================

%% Create Laser Struct
clear all
load SimData


    StructRange = [];
    StructPoseNumber = [];
    StructRobotPose = [];
    StructLaserOrien = [];
    StructRobotNumber = [];
    StructMaxRange = [];
   

for Robot=1:size(SimData,2)
    
    LasersPerScan = size(SimData(Robot).RangeFinderData,2);
    NumberOfPoses = size(SimData(Robot).Pose,2);
    NumberOfLasers = LasersPerScan*NumberOfPoses;
    LaserOrien = [];
     
     
    clear GlobalPose
    clear GlobalLaserHits
   
    RobotData = SimData(Robot);
    Pose = RobotData.Pose;
    RangeFinderData = RobotData.RangeFinderData;
    InitialLocation = RobotData.InitialLocation;
    InitialOrientation = RobotData.InitialOrientation;
    LaserOrientations = RobotData.LaserOrientations;
    MaxRange = RobotData.MaxRange;
    
    %Determine Global Pose
    %Rotate
    [TempThet1 TempPhi TempR1] = cart2sph(Pose(1,:),Pose(2,:),Pose(3,:));
    TempThet1 = TempThet1 + InitialOrientation;
    [GlobalPose(1,:),GlobalPose(2,:),GlobalPose(3,:)] = sph2cart(TempThet1, TempPhi, TempR1);
    %Translate
    GlobalPose = [GlobalPose(1:3,:)+repmat(InitialLocation,1,size(Pose,2));Pose(4,:)+InitialOrientation];

    
    Range = reshape(SimData(Robot).RangeFinderData',1,numel(SimData(Robot).RangeFinderData));
    PoseNumber = reshape(repmat(linspace(1,NumberOfPoses,NumberOfPoses)',1,LasersPerScan)',1,NumberOfLasers);
    RobotPose = GlobalPose(:,PoseNumber);
%     LaserOrien = repmat(LaserOrientations,1,NumberOfPoses);
    RobotNumber = Robot*ones(1,NumberOfLasers);
    MaxRange = MaxRange*ones(1,NumberOfLasers);
    for i = 1:size(LaserOrientations,3)
        LaserOrien = [LaserOrien LaserOrientations(:,:,i)];
    end
    
    StructRange = [StructRange Range];
    StructPoseNumber = [StructPoseNumber PoseNumber];
    StructRobotPose = [StructRobotPose, RobotPose];
    StructLaserOrien = [StructLaserOrien LaserOrien];
    StructRobotNumber = [StructRobotNumber RobotNumber];
    StructMaxRange = [StructMaxRange MaxRange];
 
end
    
    LaserStruct = struct('Range',StructRange,'Pose',StructRobotPose,'PoseNumber',...
        StructPoseNumber,'LaserOrient',StructLaserOrien,'RobotNumber',StructRobotNumber,...
        'MaxRange',StructMaxRange);
    
    save LaserStruct LaserStruct

disp('LaserStruct saved as LaserStruct')
disp('LineSegExtractor uses the LaserStruct file as the input.')





%% Evaluate Pose To Allow Truthful Merging of Scans

clear all
load LaserStruct
%-------------Rebuilds the map based on known pose and scan data-----------
disp('Merging scans using known pose... ')
disp(' ')
pause(0.5)
figure(98)


ReturnsIndex = find(LaserStruct.Range <LaserStruct.MaxRange - 0.1);

Ranges = LaserStruct.Range(ReturnsIndex);
Pose = LaserStruct.Pose(:,ReturnsIndex);
PoseNumber = LaserStruct.PoseNumber(:,ReturnsIndex);
LaserOrient = LaserStruct.LaserOrient(:,ReturnsIndex)+[Pose(4,:)-pi/2;zeros(1,numel(ReturnsIndex))];



[X Y Z] = sph2cart(LaserOrient(1,:),LaserOrient(2,:),Ranges);
X = X+Pose(1,:);
Y = Y+Pose(2,:);
Z = Z+Pose(3,:);

%Removegroundplane
X(Z<0.1)=[];
Y(Z<0.1)=[];
Z(Z<0.1)=[];


plot3(X,Y,Z,'+');
hold on
% plot3([X;Pose(1,:)],[Y;Pose(2,:)],[Z;Pose(3,:)],'r')
view(0,90)
title('Reconstructed Map including Non-returns')
hold off

disp('Merge Complete.')
disp(' ')





    