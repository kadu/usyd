
function [Result] = isInTriangle(p,a,b,c)
% p is the point
% a b c are the vertices of the triangle
 
% Compute vectors        
v0 = c - a;
v1 = b - a;
v2 = p - a;

%Compute dot products
dot00 = dot(v0, v0);
dot01 = dot(v0, v1);
dot02 = dot(v0, v2);
dot11 = dot(v1, v1);
dot12 = dot(v1, v2);

% Compute barycentric coordinates
invDenom = 1 / (dot00 * dot11 - dot01 * dot01);
u = (dot11 * dot02 - dot01 * dot12) * invDenom;
v = (dot00 * dot12 - dot01 * dot02) * invDenom;

% Check if point is in triangle
if (u > 0) && (v > 0) && (u + v < 1)
    Result = 1 ;
    
else
    Result = 0;
end


