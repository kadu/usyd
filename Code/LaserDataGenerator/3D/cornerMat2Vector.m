

function [X Y Z NumberOfPlanes] = cornerMat2Vector(Corners)

NumberOfPlanes = size(Corners,2)

           X = reshape(Corners(1,:,:),1,numel(Corners(1,:,:)));
           Y = reshape(Corners(2,:,:),1,numel(Corners(1,:,:))); 
           Z = reshape(Corners(3,:,:),1,numel(Corners(1,:,:)));