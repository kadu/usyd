
function [Corners] = vector2CornerMat(WorldX,WorldY,WorldZ, NumberOfPlanes)


for i=1:4   
    Pointer=NumberOfPlanes*i;
    Corners(1,:,i)=WorldX((Pointer-NumberOfPlanes+1):Pointer);
    Corners(2,:,i)=WorldY((Pointer-NumberOfPlanes+1):Pointer);
    Corners(3,:,i)=WorldZ((Pointer-NumberOfPlanes+1):Pointer);
end
