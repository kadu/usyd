

function [Result] = isHitOnPlaneSegment(a,b,c,d,P)

P = P+[0.0000001*randn(1,2) 0];  % A fudge to reduce chance of point being on the triangles lines
%Divide plane into two triangles and check
Triangle1Result = isInTriangle(P,a,b,c);
Triangle2Result = isInTriangle(P,c,d,a);

if Triangle1Result || Triangle2Result
    Result = 1;
else
    Result = 0;
end

