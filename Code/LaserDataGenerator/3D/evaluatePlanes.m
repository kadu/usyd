

function [PlaneEquations] = evaluatePlanes(PlaneCorners)

% NumberOfObjects = size(WorldMap,2);
% PlaneCorners = [];
% for i = 1:NumberOfObjects 
%    
%    PlaneCorners = cat(2,PlaneCorners,WorldMap{i});
%     
% end

NumberOfPlanes = size(PlaneCorners,2);
% PlaneEquation = zeros(4,NumberOfPlanes);
for PlaneNumber = 1:NumberOfPlanes
    x1 = PlaneCorners(1,PlaneNumber,1);
    y1 = PlaneCorners(2,PlaneNumber,1);
    z1 = PlaneCorners(3,PlaneNumber,1);
    x2 = PlaneCorners(1,PlaneNumber,2);
    y2 = PlaneCorners(2,PlaneNumber,2);
    z2 = PlaneCorners(3,PlaneNumber,2);
    x3 = PlaneCorners(1,PlaneNumber,3);
    y3 = PlaneCorners(2,PlaneNumber,3);
    z3 = PlaneCorners(3,PlaneNumber,3);
    
    PlaneEquations(1,PlaneNumber) =  y1*(z2-z3) + y2*(z3-z1) + y3*(z1-z2); 
    PlaneEquations(2,PlaneNumber) =  z1*(x2-x3) + z2*(x3-x1) + z3*(x1-x2); 
    PlaneEquations(3,PlaneNumber) =  x1*(y2-y3) + x2*(y3-y1) + x3*(y1-y2); 
    PlaneEquations(4,PlaneNumber) =  -x1*(y2*z3- y3*z2) - x2*(y3*z1-y1*z3) - x3*(y1*z2-y2*z1); 
    

end


