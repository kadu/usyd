
function plotPlanes(Corners, DisplayPlane)

NumberOfPlanes = size(Corners,2);


for i = 1:NumberOfPlanes


xCorners = reshape(Corners(1,i,:),numel(Corners(1,i,:)),1);
yCorners = reshape(Corners(2,i,:),numel(Corners(1,i,:)),1);
zCorners = reshape(Corners(3,i,:),numel(Corners(1,i,:)),1);
tcolor = [0.7 0.7 0.7];
if DisplayPlane(i) == 1
    patch(xCorners,yCorners,zCorners,tcolor);
    hold on
end

end

axis equal
title('Ground Truth');
hold off
