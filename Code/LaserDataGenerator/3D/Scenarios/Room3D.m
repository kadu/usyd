% 3D Environment file.
% Creates the world and the robots actions
% Set up to handle multiple robots.

%% Create World Map

%Initialise
WorldMap = {};


%% Create Object Boundaries

% OBJECT VERTICES MUST BE IN SEQUENTIAL ORDER!
% Planes must contain 4 and only 4 vertices each!
% Also planes must be planes so they can be represented by an equation.
%This puts restrictions on the 4th corner.

% Initialise Corner = [] at the start of each object and
% Always end each object with "WorldMap = IncludeObjectInWorld(Corner,
% WorldMap);"

%Object 1 --- Outer boundary of area
    %Plane Segment 1
    Object(:,1,1) = [1,1,1];
    Object(:,1,2) = [1 1 4];
    Object(:,1,3) = [1 5 4];
    Object(:,1,4) = [1 5 1];
    %Plane 2
    Object(:,2,1) = [-3 5 7];
    Object(:,2,2) = [-3 5 5];
    Object(:,2,3) = [1 5 1];
    Object(:,2,4) = [1 5 4];
    %Plane 3
    Object(:,3,1) = [1 1 4];
    Object(:,3,2) = [1 5 4];
    Object(:,3,3) = [-3 5 7];
    Object(:,3,4) = [-3 1 7];
    
    if numel(WorldMap) == 0 
        WorldMap = {Object};
        Object = [];
    else
        WorldMap{end+1} = Object;
        Object = [];
    end

    %Object 1 --- Outer boundary of area
    %Plane 1
    Object(:,1,1) = [6,6,4];
    Object(:,1,2) = [6 6 1];
    Object(:,1,3) = [6 10 1];
    Object(:,1,4) = [6 10 4];
    %Plane 2
    Object(:,2,1) = [2 10 7];
    Object(:,2,2) = [2 10 5];
    Object(:,2,3) = [6 10 1];
    Object(:,2,4) = [6 10 4];
    
    if numel(WorldMap) == 0 
        WorldMap = {Object};
        Object = [];
    else
        WorldMap{end+1} = Object;
        Object = [];
    end

    
    
    
    
    
    clear Object
    
        


%% Initialise the robot parameters
RobotFixedParameters = struct('InitialLocation', [],...
                  'InitialOrientation',[],...
                  'NumberOfBeamsHoriz',[],...
                  'DegreesOfSweepHoriz',[],...
                  'NumberOfBeamsVert',[],...
                  'DegreesOfSweepVert',[],...
                  'MaxRange',[],...
                  'LaserAngVarHoriz',[],...
                  'LaserAngVarVert',[],...
                  'LaserRangVar',[],...
                  'SpeedVar',[],...
                  'DirectionVarHoriz',[]);

% 
%% -----------Robot 1-------------------------------------
RobotNumber = 1;

% InitialLocation = [2;4;0];
InitialLocation = [4;3;5];
% InitialOrientation = [10,0];     %Relative to X axis in degrees
InitialOrientation = [90,0];     %Relative to X axis in degrees
NumberOfBeamsHoriz = 17;         %37;
DegreesOfSweepHoriz = 180;
NumberOfBeamsVert = 9;
DegreesOfSweepVert = 80;
MaxRange = 8;
LaserAngVarHoriz = 0;            %Measured in degrees
LaserAngVarVert = 0;
LaserRangVar = 0;           %Measured in metres
SpeedVar = 0;    %Variance of vehicle's speed. Measured as a fraction of vehicles speed. i.e. 0.1 m var for 1 m/s, 0.2 m var for 2 m/s
DirectionVarHoriz = 0; %Variance of vehicles direction. Measured as a fraction of vehicles turn angle

RobotSpeed = [1,1];
Direction = [0,-10];        %0 is straight ahead. +degrees to left, -degrees to right

%-------------------------------------------------------

% Update RobotFixedParameters

RobotFixedParameters = struct('InitialLocation', [RobotFixedParameters.InitialLocation InitialLocation],...
                  'InitialOrientation',[RobotFixedParameters.InitialOrientation InitialOrientation],...
                  'NumberOfBeamsHoriz',[RobotFixedParameters.NumberOfBeamsHoriz NumberOfBeamsHoriz],...
                  'DegreesOfSweepHoriz',[RobotFixedParameters.DegreesOfSweepHoriz DegreesOfSweepHoriz],...
                  'NumberOfBeamsVert',[RobotFixedParameters.NumberOfBeamsVert NumberOfBeamsVert],...
                  'DegreesOfSweepVert',[RobotFixedParameters.DegreesOfSweepVert DegreesOfSweepVert],...
                  'MaxRange',[RobotFixedParameters.MaxRange MaxRange],...
                  'LaserAngVarHoriz',[RobotFixedParameters.LaserAngVarHoriz LaserAngVarHoriz],...
                  'LaserAngVarVert',[RobotFixedParameters.LaserAngVarVert LaserAngVarVert],...
                  'LaserRangVar',[RobotFixedParameters.LaserRangVar LaserRangVar],...
                  'SpeedVar',[RobotFixedParameters.SpeedVar SpeedVar],...
                  'DirectionVarHoriz',[RobotFixedParameters.DirectionVarHoriz DirectionVarHoriz]);
              
SpeedsOfRobots(RobotNumber) = {RobotSpeed};       
DirectionsOfRobots(RobotNumber) = {Direction};
              

% 
% %% Clean Up
clear DegreesOfSweepHoriz
clear DegreesOfSweepVert
clear Direction
clear InitialLocation
clear InitialOrientation
clear MaxRange
clear NumberOfBeamsHoriz
clear NumberOfBeamsVert
clear RobotNumber
clear RobotSpeed
clear LaserAngVarHoriz
clear LaserAngVarVert
clear LaserRangVar
clear SpeedVar
clear DirectionVarHoriz




