% 3D Environment file.
% Creates the world and the robots actions
% Set up to handle multiple robots.

%% Create World Map

%Initialise
WorldMap = {};
DisplayObject = [];


%% Create Object Boundaries

% OBJECT VERTICES MUST BE IN SEQUENTIAL ORDER!
% Planes must contain 4 and only 4 vertices each!
% Also planes must be planes so they can be represented by an equation.
%This puts restrictions on the 4th corner.

% Initialise Corner = [] at the start of each object and
% Always end each object with "WorldMap = IncludeObjectInWorld(Corner,
% WorldMap);"

%Object 1 --- Ground Plane
    %Plane Segment 1
    Object(:,1,1) = [-8 -10 0];
    Object(:,1,2) = [-8 6 0];
    Object(:,1,3) = [12 6 0];
    Object(:,1,4) = [12 -10 0];

    DisplayObject = [DisplayObject 1*ones(1,size(Object,2))];
    
    if numel(WorldMap) == 0 
        WorldMap = {Object};
        Object = [];
    else
        WorldMap{end+1} = Object;
        Object = [];
    end

    %Object 2 ---  Straight Wall jutting out
    %Plane 1
    Object(:,1,1) = [-8 -6 6];
    Object(:,1,2) = [-2 -6 6];
    Object(:,1,3) = [-2 -6 8];
    Object(:,1,4) = [-8 -6 8];
    %Plane 2
    Object(:,2,1) = [-2 -6 6];
    Object(:,2,2) = [-2 -6 8];
    Object(:,2,3) = [-2 -4 8];
    Object(:,2,4) = [-2 -4 6];
    %Plane 3
    Object(:,3,1) = [-8 -6 6];
    Object(:,3,2) = [-8 -4 6];
    Object(:,3,3) = [-2 -4 6];
    Object(:,3,4) = [-2 -6 6];
    %Plane 4
    Object(:,4,1) = [-8 -6 8];
    Object(:,4,2) = [-8 -4 8];
    Object(:,4,3) = [-2 -4 8];
    Object(:,4,4) = [-2 -6 8];
    %Plane 5
    Object(:,5,1) = [-8 -4 6];
    Object(:,5,2) = [-8 -4 8];
    Object(:,5,3) = [-2 -4 8];
    Object(:,5,4) = [-2 -4 6];
   
    
    DisplayObject = [DisplayObject 1*ones(1,size(Object,2))];
    
    if numel(WorldMap) == 0 
        WorldMap = {Object};
        Object = [];
    else
        WorldMap{end+1} = Object;
        Object = [];
    end

    
        %Object 3 --- Box in floor
    %Plane 1
    Object(:,1,1) = [5 -6 3];
    Object(:,1,2) = [5 -4 3];
    Object(:,1,3) = [8 -4 3];
    Object(:,1,4) = [8 -6 3];
    %Plane 2
    Object(:,2,1) = [5 -6 0];
    Object(:,2,2) = [5 -6 3];
    Object(:,2,3) = [8 -6 3];
    Object(:,2,4) = [8 -6 0];
    %Plane 3
    Object(:,3,1) = [8 -6 0];
    Object(:,3,2) = [8 -6 3];
    Object(:,3,3) = [8 -4 3];
    Object(:,3,4) = [8 -4 0];
    %Plane 4
    Object(:,4,1) = [8 -4 0];
    Object(:,4,2) = [8 -4 3];
    Object(:,4,3) = [5 -4 3];
    Object(:,4,4) = [5 -4 0];
    %Plane 4
    Object(:,5,1) = [5 -4 0];
    Object(:,5,2) = [5 -4 3];
    Object(:,5,3) = [5 -6 3];
    Object(:,5,4) = [5 -6 0];
   
   
    
    DisplayObject = [DisplayObject 1*ones(1,size(Object,2))];
    
    if numel(WorldMap) == 0 
        WorldMap = {Object};
        Object = [];
    else
        WorldMap{end+1} = Object;
        Object = [];
    end
    
    
    
    

    
    %Object 4 --- L Shaped Wall
    %Plane 1
    Object(:,1,1) = [2 6 3];
    Object(:,1,2) = [2 1 3];
    Object(:,1,3) = [3 1 3];
    Object(:,1,4) = [3 6 3];
    %Plane 2
    Object(:,2,1) = [2 6 5];
    Object(:,2,2) = [2 1 5];
    Object(:,2,3) = [3 1 5];
    Object(:,2,4) = [3 6 5];
    %Plane 3
    Object(:,3,1) = [0 1 3];
    Object(:,3,2) = [0 2 3];
    Object(:,3,3) = [2 2 3];
    Object(:,3,4) = [2 1 3];
    %Plane 4
    Object(:,4,1) = [0 1 5];
    Object(:,4,2) = [0 2 5];
    Object(:,4,3) = [2 2 5];
    Object(:,4,4) = [2 1 5];
    %Plane 5
    Object(:,5,1) = [0 1 3];
    Object(:,5,2) = [0 1 5];
    Object(:,5,3) = [0 2 5];
    Object(:,5,4) = [0 2 3];
    %Plane 6
    Object(:,6,1) = [0 2 3];
    Object(:,6,2) = [0 2 5];
    Object(:,6,3) = [2 2 5];
    Object(:,6,4) = [2 2 3];
    %Plane 7
    Object(:,7,1) = [0 1 3];
    Object(:,7,2) = [0 1 5];
    Object(:,7,3) = [3 1 5];
    Object(:,7,4) = [3 1 3];
    %Plane 8
    Object(:,8,1) = [3 1 3];
    Object(:,8,2) = [3 1 5];
    Object(:,8,3) = [3 6 5];
    Object(:,8,4) = [3 6 3];
    %Plane 9
    Object(:,9,1) = [2 6 3];
    Object(:,9,2) = [2 6 5];
    Object(:,9,3) = [2 2 5];
    Object(:,9,4) = [2 2 3];
   
    
    DisplayObject = [DisplayObject 1*ones(1,size(Object,2))];
    
    if numel(WorldMap) == 0 
        WorldMap = {Object};
        Object = [];
    else
        WorldMap{end+1} = Object;
        Object = [];
    end
    
    
    
           %Object 5 ---Walls
    %Plane 1
    Object(:,1,1) = [-8 -10 0];
    Object(:,1,2) = [-8 -10 10];
    Object(:,1,3) = [-8 6 10];
    Object(:,1,4) = [-8 6 0];
    %Plane 2
    Object(:,2,1) = [-8 6 0];
    Object(:,2,2) = [-8 6 10];
    Object(:,2,3) = [12 6 10];
    Object(:,2,4) = [12 6 0];
    
    
    DisplayObject = [DisplayObject 1*ones(1,size(Object,2))];
    
    if numel(WorldMap) == 0 
        WorldMap = {Object};
        Object = [];
    else
        WorldMap{end+1} = Object;
        Object = [];
    end
     
    
    
    
    
    
    
    clear Object
    
        


%% Initialise the robot parameters
RobotFixedParameters = struct('InitialLocation', [],...
                  'InitialOrientation',[],...
                  'NumberOfBeamsHoriz',[],...
                  'DegreesOfSweepHoriz',[],...
                  'NumberOfBeamsVert',[],...
                  'DegreesOfSweepVert',[],...
                  'MaxRange',[],...
                  'LaserAngVarHoriz',[],...
                  'LaserAngVarVert',[],...
                  'LaserRangVar',[],...
                  'SpeedVar',[],...
                  'DirectionVarHoriz',[]);

% 
%% -----------Robot 1-------------------------------------
RobotNumber = 1;

% InitialLocation = [2;4;0];
InitialLocation = [-6;5;3.5];
% InitialOrientation = [10,0];     %Relative to X axis in degrees
InitialOrientation = [-20,-20];     %Relative to X axis in degrees
NumberOfBeamsHoriz = 10;         %37;
DegreesOfSweepHoriz = 180;
NumberOfBeamsVert = 10;
DegreesOfSweepVert = 140;
MaxRange = 8;
LaserAngVarHoriz = 0;            %Measured in degrees
LaserAngVarVert = 0;
LaserRangVar = 0;           %Measured in metres
SpeedVar = 0;    %Variance of vehicle's speed. Measured as a fraction of vehicles speed. i.e. 0.1 m var for 1 m/s, 0.2 m var for 2 m/s
DirectionVarHoriz = 0; %Variance of vehicles direction. Measured as a fraction of vehicles turn angle

% RobotSpeed = [1 2,1,1 1 2 2 2 1 1 1 ];
% Direction = [0 45,0 0,-45, -45 0 0 0 -45 -70 ];        %0 is straight ahead. +degrees to left, -degrees to right

RobotSpeed = [1,1,1,1,1,1,1,1,1,0.5,0.5,1,1,1,1,1,1,1,1,1,1];
Direction = [-10,-20,-20,-20,0,20,20,20,20,0,0,0,0,20,0,20,-20,-20,0,-20,-20];        %0 is straight ahead. +degrees to left, -degrees to right
HeightChange = [0 0 1 0 0 1 0 0 -1 0 0 0 -1 0 0 -1 0 0 -1 0 0];
%-------------------------------------------------------

% Update RobotFixedParameters

RobotFixedParameters = struct('InitialLocation', [RobotFixedParameters.InitialLocation InitialLocation],...
                  'InitialOrientation',[RobotFixedParameters.InitialOrientation InitialOrientation],...
                  'NumberOfBeamsHoriz',[RobotFixedParameters.NumberOfBeamsHoriz NumberOfBeamsHoriz],...
                  'DegreesOfSweepHoriz',[RobotFixedParameters.DegreesOfSweepHoriz DegreesOfSweepHoriz],...
                  'NumberOfBeamsVert',[RobotFixedParameters.NumberOfBeamsVert NumberOfBeamsVert],...
                  'DegreesOfSweepVert',[RobotFixedParameters.DegreesOfSweepVert DegreesOfSweepVert],...
                  'MaxRange',[RobotFixedParameters.MaxRange MaxRange],...
                  'LaserAngVarHoriz',[RobotFixedParameters.LaserAngVarHoriz LaserAngVarHoriz],...
                  'LaserAngVarVert',[RobotFixedParameters.LaserAngVarVert LaserAngVarVert],...
                  'LaserRangVar',[RobotFixedParameters.LaserRangVar LaserRangVar],...
                  'SpeedVar',[RobotFixedParameters.SpeedVar SpeedVar],...
                  'DirectionVarHoriz',[RobotFixedParameters.DirectionVarHoriz DirectionVarHoriz]);
              
SpeedsOfRobots(RobotNumber) = {RobotSpeed};       
DirectionsOfRobots(RobotNumber) = {Direction};
HeightChangeOfRobots(RobotNumber) = {HeightChange};       

              

% 
% %% Clean Up
clear DegreesOfSweepHoriz
clear DegreesOfSweepVert
clear Direction
clear InitialLocation
clear InitialOrientation
clear MaxRange
clear NumberOfBeamsHoriz
clear NumberOfBeamsVert
clear RobotNumber
clear RobotSpeed
clear LaserAngVarHoriz
clear LaserAngVarVert
clear LaserRangVar
clear SpeedVar
clear DirectionVarHoriz




