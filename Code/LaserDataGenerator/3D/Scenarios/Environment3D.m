% 3D Environment file.
% Creates the world and the robots actions
% Set up to handle multiple robots.

%% Create World Map

%Initialise
WorldMap = {};
DisplayObject = [];


%% Create Object Boundaries

% OBJECT VERTICES MUST BE IN SEQUENTIAL ORDER!
% Planes must contain 4 and only 4 vertices each!
% Also planes must be planes so they can be represented by an equation.
%This puts restrictions on the 4th corner.

% Initialise Corner = [] at the start of each object and
% Always end each object with "WorldMap = IncludeObjectInWorld(Corner,
% WorldMap);"

%Object 1 --- Ground Plane
    %Plane Segment 1
    Object(:,1,1) = [0 0 0];
    Object(:,1,2) = [0 15 0];
    Object(:,1,3) = [15 15 0];
    Object(:,1,4) = [15 0 0];

    %Plane Segment 2
    Object(:,2,1) = [25 15 0];
    Object(:,2,2) = [25 5 0];
    Object(:,2,3) = [15 5 0];
    Object(:,2,4) = [15 15 0];
    
    DisplayObject = [DisplayObject 1*ones(1,size(Object,2))];
    
    if numel(WorldMap) == 0 
        WorldMap = {Object};
        Object = [];
    else
        WorldMap{end+1} = Object;
        Object = [];
    end

    %Object 2 --- Outer boundary of area
    %Plane 1
    Object(:,1,1) = [0 0 0];
    Object(:,1,2) = [0 15 0];
    Object(:,1,3) = [0 15 10];
    Object(:,1,4) = [0 0 10];
    %Plane 2
    Object(:,2,1) = [0 15 0];
    Object(:,2,2) = [0 15 10];
    Object(:,2,3) = [25 15 10];
    Object(:,2,4) = [25 15 0];
    %Plane 3
    Object(:,3,1) = [25 15 0];
    Object(:,3,2) = [25 15 10];
    Object(:,3,3) = [25 5 10];
    Object(:,3,4) = [25 5 0];
    %Plane 4
    Object(:,4,1) = [25 5 0];
    Object(:,4,2) = [25 5 10];
    Object(:,4,3) = [15 5 10];
    Object(:,4,4) = [15 5 0];
    %Plane 5
    Object(:,5,1) = [15 5 0];
    Object(:,5,2) = [15 5 10];
    Object(:,5,3) = [15 0 10];
    Object(:,5,4) = [15 0 0];
    %Plane 6
    Object(:,6,1) = [15 0 0];
    Object(:,6,2) = [15 0 10];
    Object(:,6,3) = [0 0 10];
    Object(:,6,4) = [0 0 0];
    
    DisplayObject = [DisplayObject 0*ones(1,size(Object,2))];
    
    if numel(WorldMap) == 0 
        WorldMap = {Object};
        Object = [];
    else
        WorldMap{end+1} = Object;
        Object = [];
    end

    
        %Object 3 --- Bridge
    %Plane 1
    Object(:,1,1) = [6 0 7];
    Object(:,1,2) = [6 15 7];
    Object(:,1,3) = [10 15 7];
    Object(:,1,4) = [10 0 7];
    %Plane 2
    Object(:,2,1) = [6 0 7];
    Object(:,2,2) = [6 15 7];
    Object(:,2,3) = [6 15 10];
    Object(:,2,4) = [6 0 10];
    %Plane 3
    Object(:,3,1) = [6 0 10];
    Object(:,3,2) = [6 15 10];
    Object(:,3,3) = [10 15 10];
    Object(:,3,4) = [10 0 10];
    %Plane 4
    Object(:,4,1) = [10 0 7];
    Object(:,4,2) = [10 15 7];
    Object(:,4,3) = [10 15 10];
    Object(:,4,4) = [10 0 10];
    
    DisplayObject = [DisplayObject 1*ones(1,size(Object,2))];
    
    if numel(WorldMap) == 0 
        WorldMap = {Object};
        Object = [];
    else
        WorldMap{end+1} = Object;
        Object = [];
    end

    
    
    
    clear Object
    
        


%% Initialise the robot parameters
RobotFixedParameters = struct('InitialLocation', [],...
                  'InitialOrientation',[],...
                  'NumberOfBeamsHoriz',[],...
                  'DegreesOfSweepHoriz',[],...
                  'NumberOfBeamsVert',[],...
                  'DegreesOfSweepVert',[],...
                  'MaxRange',[],...
                  'LaserAngVarHoriz',[],...
                  'LaserAngVarVert',[],...
                  'LaserRangVar',[],...
                  'SpeedVar',[],...
                  'DirectionVarHoriz',[]);

% 
%% -----------Robot 1-------------------------------------
RobotNumber = 1;

% InitialLocation = [2;4;0];
InitialLocation = [4;3;5];
% InitialOrientation = [10,0];     %Relative to X axis in degrees
InitialOrientation = [90,0];     %Relative to X axis in degrees
NumberOfBeamsHoriz = 17;         %37;
DegreesOfSweepHoriz = 180;
NumberOfBeamsVert = 9;
DegreesOfSweepVert = 80;
MaxRange = 8;
LaserAngVarHoriz = 0;            %Measured in degrees
LaserAngVarVert = 0;
LaserRangVar = 0;           %Measured in metres
SpeedVar = 0;    %Variance of vehicle's speed. Measured as a fraction of vehicles speed. i.e. 0.1 m var for 1 m/s, 0.2 m var for 2 m/s
DirectionVarHoriz = 0; %Variance of vehicles direction. Measured as a fraction of vehicles turn angle

RobotSpeed = [1,1];
Direction = [0,-10];        %0 is straight ahead. +degrees to left, -degrees to right

%-------------------------------------------------------

% Update RobotFixedParameters

RobotFixedParameters = struct('InitialLocation', [RobotFixedParameters.InitialLocation InitialLocation],...
                  'InitialOrientation',[RobotFixedParameters.InitialOrientation InitialOrientation],...
                  'NumberOfBeamsHoriz',[RobotFixedParameters.NumberOfBeamsHoriz NumberOfBeamsHoriz],...
                  'DegreesOfSweepHoriz',[RobotFixedParameters.DegreesOfSweepHoriz DegreesOfSweepHoriz],...
                  'NumberOfBeamsVert',[RobotFixedParameters.NumberOfBeamsVert NumberOfBeamsVert],...
                  'DegreesOfSweepVert',[RobotFixedParameters.DegreesOfSweepVert DegreesOfSweepVert],...
                  'MaxRange',[RobotFixedParameters.MaxRange MaxRange],...
                  'LaserAngVarHoriz',[RobotFixedParameters.LaserAngVarHoriz LaserAngVarHoriz],...
                  'LaserAngVarVert',[RobotFixedParameters.LaserAngVarVert LaserAngVarVert],...
                  'LaserRangVar',[RobotFixedParameters.LaserRangVar LaserRangVar],...
                  'SpeedVar',[RobotFixedParameters.SpeedVar SpeedVar],...
                  'DirectionVarHoriz',[RobotFixedParameters.DirectionVarHoriz DirectionVarHoriz]);
              
SpeedsOfRobots(RobotNumber) = {RobotSpeed};       
DirectionsOfRobots(RobotNumber) = {Direction};
              

% 
% %% Clean Up
clear DegreesOfSweepHoriz
clear DegreesOfSweepVert
clear Direction
clear InitialLocation
clear InitialOrientation
clear MaxRange
clear NumberOfBeamsHoriz
clear NumberOfBeamsVert
clear RobotNumber
clear RobotSpeed
clear LaserAngVarHoriz
clear LaserAngVarVert
clear LaserRangVar
clear SpeedVar
clear DirectionVarHoriz




