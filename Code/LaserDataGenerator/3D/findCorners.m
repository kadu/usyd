
function [PlaneCorners] = findCorners(WorldMap)

NumberOfObjects = size(WorldMap,2);
PlaneCorners = [];
for i = 1:NumberOfObjects 
   
   PlaneCorners = cat(2,PlaneCorners,WorldMap{i});
    
end