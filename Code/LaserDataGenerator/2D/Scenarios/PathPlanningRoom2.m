% Environment file.
% Creates the world and the robots actions
% Set up to handle multiple robots.

%% Create World Map

%Initialise
WorldMap = struct('WallStartPointsX',[],'WallStartPointsY',[],'WallEndPointsX',[],'WallEndPointsY',[])


%% Create Object Boundaries
% Initialise Corner = [] at the start of each object and
% Always end each object with "WorldMap = IncludeObjectInWorld(Corner,
% WorldMap);"

%Object 1 --- Outer boundary of area
Corner = [];
Corner(1,:) = [-10 -10];
Corner(2,:) = [-10 30];
Corner(3,:) = [20 30];
Corner(4,:) = [20 -10];
WorldMap = IncludeObjectInWorld(Corner, WorldMap);

%Object 2 --Obstacle
Corner = [];
Corner(1,:) = [0 0];
Corner(2,:) = [-2 0];
Corner(3,:) = [5 22];
Corner(4,:) = [12 0];
Corner(5,:) = [10 0];
Corner(6,:) = [5 20];
% Corner(7,:) = [-5 -5];
WorldMap = IncludeObjectInWorld(Corner, WorldMap);

%Object 3 --- Goal
Corner = [];
Corner(1,:) = [5 25];
Corner(2,:) = [5.1 25 ];
Corner(3,:) = [5.1 25.1];
Corner(4,:) = [5 25.1];
WorldMap = IncludeObjectInWorld(Corner, WorldMap);

%Object 4 --- Start
Corner = [];
Corner(1,:) = [5 -5];
Corner(2,:) = [5 -5.1];
Corner(3,:) = [5.1 -5.1];
Corner(4,:) = [5.1 -5];
WorldMap = IncludeObjectInWorld(Corner, WorldMap);

%% Initialise the robot parameters
RobotFixedParameters = struct('InitialLocation', [],...
                  'InitialOrientation',[],...
                  'NumberOfBeams',[],...
                  'DegreesOfSweep',[],...
                  'MaxRange',[],...
                  'LaserAngVar',[],...
                  'LaserRangVar',[],...
                  'SpeedVar',[],...
                  'DirectionVar',[]);


%% -----------Robot 1-------------------------------------
RobotNumber = 1;

InitialLocation = [5;-4.9];
InitialOrientation = 90;     %Relative to X axis in degrees
NumberOfBeams = 10;         %37;
DegreesOfSweep = 180;
MaxRange = 20;
LaserAngVar = 0;            %Measured in degrees
LaserRangVar = 0;           %Measured in metres
SpeedVar = 0;    %Variance of vehicle's speed. Measured as a fraction of vehicles speed. i.e. 0.1 m var for 1 m/s, 0.2 m var for 2 m/s
DirectionVar = 0; %Variance of vehicles direction. Measured as a fraction of vehicles turn angle

RobotSpeed = 2*ones(1,5);
Direction = [zeros(1,5)];        %0 is straight ahead. +degrees to left, -degrees to right

%-------------------------------------------------------

%Update RobotFixedParameters

RobotFixedParameters = struct('InitialLocation', [RobotFixedParameters.InitialLocation InitialLocation],...
                  'InitialOrientation',[RobotFixedParameters.InitialOrientation InitialOrientation],...
                  'NumberOfBeams',[RobotFixedParameters.NumberOfBeams NumberOfBeams],...
                  'DegreesOfSweep',[RobotFixedParameters.DegreesOfSweep DegreesOfSweep],...
                  'MaxRange',[RobotFixedParameters.MaxRange MaxRange],...
                  'LaserAngVar',[RobotFixedParameters.LaserAngVar LaserAngVar],...
                  'LaserRangVar',[RobotFixedParameters.LaserRangVar LaserRangVar],...
                  'SpeedVar',[RobotFixedParameters.SpeedVar SpeedVar],...
                  'DirectionVar',[RobotFixedParameters.DirectionVar DirectionVar]);
              
SpeedsOfRobots(RobotNumber) = {RobotSpeed};       
DirectionsOfRobots(RobotNumber) = {Direction};
              

% 
% 
% %% --------------Robot 2---------------------------------
% RobotNumber = 2;
% 
% InitialLocation = [5;-8];
% InitialOrientation = 135;   %Relative to X axis in degrees
% NumberOfBeams = 17;         %37;
% DegreesOfSweep = 180;
% MaxRange = 20;
% LaserAngVar = 0;            %Measured in degrees
% LaserRangVar = 0;           %Measured in metres
% SpeedVar = 0;    %Variance of vehicle's speed. Measured as a fraction of vehicles speed. i.e. 0.1 m var for 1 m/s, 0.2 m var for 2 m/s
% DirectionVar = 0; %Variance of vehicles direction. Measured as a fraction of vehicles turn angle
% 
% RobotSpeed = [1,1,1];
% Direction = [0,0,0]; 
% % %-------------------------------------------------------------
% 
% %Update RobotFixedParameters
% 
% RobotFixedParameters = struct('InitialLocation', [RobotFixedParameters.InitialLocation InitialLocation],...
%                   'InitialOrientation',[RobotFixedParameters.InitialOrientation InitialOrientation],...
%                   'NumberOfBeams',[RobotFixedParameters.NumberOfBeams NumberOfBeams],...
%                   'DegreesOfSweep',[RobotFixedParameters.DegreesOfSweep DegreesOfSweep],...
%                   'MaxRange',[RobotFixedParameters.MaxRange MaxRange],...
%                   'LaserAngVar',[RobotFixedParameters.LaserAngVar LaserAngVar],...
%                   'LaserRangVar',[RobotFixedParameters.LaserRangVar LaserRangVar],...
%                   'SpeedVar',[RobotFixedParameters.SpeedVar SpeedVar],...
%                   'DirectionVar',[RobotFixedParameters.DirectionVar DirectionVar]);
%               
% SpeedsOfRobots(RobotNumber) = { RobotSpeed};
% DirectionsOfRobots(RobotNumber) = { Direction};


% VehicleNoiseSwitch = 0;             %This switch should not be in this file

%% Clean Up
clear DegreesOfSweep
clear Direction
clear InitialLocation
clear InitialOrientation
clear MaxRange
clear NumberOfBeams
clear RobotNumber
clear RobotSpeed
clear LaserAngVar
clear LaserRangVar
clear SpeedVar
clear DirectionVar
clear Corner




