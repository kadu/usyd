% Environment file.
% Creates the world and the robots actions
% Set up to handle multiple robots.

%% Create World Map

%Initialise
WorldMap = struct('WallStartPointsX',[],'WallStartPointsY',[],'WallEndPointsX',[],'WallEndPointsY',[])


%% Create Object Boundaries
% Initialise Corner = [] at the start of each object and
% Always end each object with "WorldMap = IncludeObjectInWorld(Corner,
% WorldMap);"

%Object 1 --- Outer boundary of area
Corner = [];
Corner(1,:) = [-8 -10];
Corner(2,:) = [-8 -6];
Corner(3,:) = [-2 -6];
Corner(4,:) = [-2 -4];
Corner(5,:) = [-8 -4];
Corner(6,:) = [-8 6];
Corner(7,:) = [2 6];
Corner(8,:) = [2 2];
Corner(9,:) = [-1 2];
Corner(10,:) = [-1 1];
Corner(11,:) = [3 1];
Corner(12,:) = [3 6];
Corner(13,:) = [12 6];
Corner(14,:) = [12 -10];
WorldMap = IncludeObjectInWorld(Corner, WorldMap);

%Object 2 -- Box in the centre of the room
Corner = [];
Corner(1,:) = [5 -6];
Corner(2,:) = [5 -4];
Corner(3,:) = [8 -4];
Corner(4,:) = [8 -6];
WorldMap = IncludeObjectInWorld(Corner, WorldMap);

%% Initialise the robot parameters
RobotFixedParameters = struct('InitialLocation', [],...
                  'InitialOrientation',[],...
                  'NumberOfBeams',[],...
                  'DegreesOfSweep',[],...
                  'MaxRange',[],...
                  'LaserAngVar',[],...
                  'LaserRangVar',[],...
                  'SpeedVar',[],...
                  'DirectionVar',[]);


%% -----------Robot 1-------------------------------------
RobotNumber = 1;

InitialLocation = [-6;5];
InitialOrientation = -20;     %Relative to X axis in degrees
NumberOfBeams = 17;         %37;
DegreesOfSweep = 180;
MaxRange = 8;
LaserAngVar = 0;            %Measured in degrees
LaserRangVar = 0;           %Measured in metres
SpeedVar = 0;    %Variance of vehicle's speed. Measured as a fraction of vehicles speed. i.e. 0.1 m var for 1 m/s, 0.2 m var for 2 m/s
DirectionVar = 0; %Variance of vehicles direction. Measured as a fraction of vehicles turn angle

RobotSpeed = [1,1,1,1,1,1,1,1,1,0.5,0.5,1,1,1,1,1,1,1,1,1,1];
Direction = [-10,-20,-20,-20,0,20,20,20,20,0,0,0,0,20,0,20,-20,-20,0,-20,-20];        %0 is straight ahead. +degrees to left, -degrees to right

%-------------------------------------------------------

%Update RobotFixedParameters

RobotFixedParameters = struct('InitialLocation', [RobotFixedParameters.InitialLocation InitialLocation],...
                  'InitialOrientation',[RobotFixedParameters.InitialOrientation InitialOrientation],...
                  'NumberOfBeams',[RobotFixedParameters.NumberOfBeams NumberOfBeams],...
                  'DegreesOfSweep',[RobotFixedParameters.DegreesOfSweep DegreesOfSweep],...
                  'MaxRange',[RobotFixedParameters.MaxRange MaxRange],...
                  'LaserAngVar',[RobotFixedParameters.LaserAngVar LaserAngVar],...
                  'LaserRangVar',[RobotFixedParameters.LaserRangVar LaserRangVar],...
                  'SpeedVar',[RobotFixedParameters.SpeedVar SpeedVar],...
                  'DirectionVar',[RobotFixedParameters.DirectionVar DirectionVar]);
              
SpeedsOfRobots(RobotNumber) = {RobotSpeed};       
DirectionsOfRobots(RobotNumber) = {Direction};
              



%% --------------Robot 2---------------------------------
RobotNumber = 2;

InitialLocation = [5;-8];
InitialOrientation = 135;   %Relative to X axis in degrees
NumberOfBeams = 17;         %37;
DegreesOfSweep = 180;
MaxRange = 20;
LaserAngVar = 0;            %Measured in degrees
LaserRangVar = 0;           %Measured in metres
SpeedVar = 0;    %Variance of vehicle's speed. Measured as a fraction of vehicles speed. i.e. 0.1 m var for 1 m/s, 0.2 m var for 2 m/s
DirectionVar = 0; %Variance of vehicles direction. Measured as a fraction of vehicles turn angle

RobotSpeed = [1,1,1];
Direction = [0,0,0]; 
% %-------------------------------------------------------------

%Update RobotFixedParameters

RobotFixedParameters = struct('InitialLocation', [RobotFixedParameters.InitialLocation InitialLocation],...
                  'InitialOrientation',[RobotFixedParameters.InitialOrientation InitialOrientation],...
                  'NumberOfBeams',[RobotFixedParameters.NumberOfBeams NumberOfBeams],...
                  'DegreesOfSweep',[RobotFixedParameters.DegreesOfSweep DegreesOfSweep],...
                  'MaxRange',[RobotFixedParameters.MaxRange MaxRange],...
                  'LaserAngVar',[RobotFixedParameters.LaserAngVar LaserAngVar],...
                  'LaserRangVar',[RobotFixedParameters.LaserRangVar LaserRangVar],...
                  'SpeedVar',[RobotFixedParameters.SpeedVar SpeedVar],...
                  'DirectionVar',[RobotFixedParameters.DirectionVar DirectionVar]);
              
SpeedsOfRobots(RobotNumber) = { RobotSpeed};
DirectionsOfRobots(RobotNumber) = { Direction};


% VehicleNoiseSwitch = 0;             %This switch should not be in this file

%% Clean Up
clear DegreesOfSweep
clear Direction
clear InitialLocation
clear InitialOrientation
clear MaxRange
clear NumberOfBeams
clear RobotNumber
clear RobotSpeed
clear LaserAngVar
clear LaserRangVar
clear SpeedVar
clear DirectionVar
clear Corner




