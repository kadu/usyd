% Receives the means [x;y] and covariances (3-d matrix) of the Gaussian and
% plots the ellipses

function  plotEllipse(Means,Cov)

% figure
plot(Means(1,:),Means(2,:),'b+')
hold on

for i = 1:size(Means,2)
   
    ellipse = ellipse_sigma(Means(:,i),Cov(:,:,i),1);
    plot(ellipse(1,:),ellipse(2,:),'r')
    
end


hold off