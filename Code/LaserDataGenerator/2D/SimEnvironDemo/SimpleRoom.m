% Scenario file.
% Creates the world and the robots
% Set up to handle multiple robots.

%% Create World Map

% Add in Line Segments of the world

WallStartPointsX = [-8,-8,12,12,2,2,-1,-1,3,8,5,5,8,-8,-2,-2];
WallStartPointsY = [-10,6,6,-10,6,2,2,1,1,-3,-3,-6,-6,-4,-4,-6];

WallEndPointsX = [-8, 12,12,-8,2,-1,-1,3,3,5,5,8,8,-2,-2,-8];
WallEndPointsY = [6,6,-10,-10,2,2,1,1,6,-3,-6,-6,-3,-4,-6,-6];



%% Initialise the robot parameters
RobotFixedParameters = struct('InitialLocation', [],...
                  'InitialOrientation',[],...
                  'NumberOfBeams',[],...
                  'DegreesOfSweep',[],...
                  'MaxRange',[],...
                  'LaserAngVar',[],...
                  'LaserRangVar',[],...
                  'VarianceOfVehNoise',[]);


%% -----------Robot 1-------------------------------------
RobotNumber = 1;

InitialLocation = [-6;4];
InitialOrientation = -20;     %Relative to X axis in degrees
NumberOfBeams = 17;         %37;
DegreesOfSweep = 180;
MaxRange = 8;
LaserAngVar = 0;            %Measured in degrees
LaserRangVar = 0;           %Measured in metres
VarianceOfVehNoise = [0;0;0];    %Variance of vehicle in x and y (metres) and orientation(degress)... Grows with each move. Assumed to be isotropic Gaussian -> [VarianceX;VarianceY]

RobotSpeed = [1,1,1,1,1,1,1,1,1,0.5,0.5,1,1,1,1,1,1,1];
Direction = [-10,-20,-20,-20,0,20,20,20,20,0,0,0,0,20,20,20,20,20];        %0 is straight ahead. +degrees to left, -degrees to right

%-------------------------------------------------------

%Update RobotFixedParameters

RobotFixedParameters = struct('InitialLocation', [RobotFixedParameters.InitialLocation InitialLocation],...
                  'InitialOrientation',[RobotFixedParameters.InitialOrientation InitialOrientation],...
                  'NumberOfBeams',[RobotFixedParameters.NumberOfBeams NumberOfBeams],...
                  'DegreesOfSweep',[RobotFixedParameters.DegreesOfSweep DegreesOfSweep],...
                  'MaxRange',[RobotFixedParameters.MaxRange MaxRange],...
                  'LaserAngVar',[RobotFixedParameters.LaserAngVar LaserAngVar],...
                  'LaserRangVar',[RobotFixedParameters.LaserRangVar LaserRangVar],...
                  'VarianceOfVehNoise',[RobotFixedParameters.VarianceOfVehNoise VarianceOfVehNoise]);
              
SpeedsOfRobots(RobotNumber) = {RobotSpeed};       
DirectionsOfRobots(RobotNumber) = {Direction};
              



%% --------------Robot 2---------------------------------
RobotNumber = 2;

InitialLocation = [5;-8];
InitialOrientation = 135;   %Relative to X axis in degrees
NumberOfBeams = 17;         %37;
DegreesOfSweep = 180;
MaxRange = 20;
LaserAngVar = 0;            %Measured in degrees
LaserRangVar = 0;           %Measured in metres
VarianceOfVehNoise = [0;0;0];     %Variance of vehicle in x and y (metres) and orientation(degress)... Grows with each move. Assumed to be isotropic Gaussian -> [VarianceX;VarianceY]

RobotSpeed = [1,1,1];
Direction = [0,0,0]; 
% %-------------------------------------------------------------

%Update RobotFixedParameters

RobotFixedParameters = struct('InitialLocation', [RobotFixedParameters.InitialLocation InitialLocation],...
                  'InitialOrientation',[RobotFixedParameters.InitialOrientation InitialOrientation],...
                  'NumberOfBeams',[RobotFixedParameters.NumberOfBeams NumberOfBeams],...
                  'DegreesOfSweep',[RobotFixedParameters.DegreesOfSweep DegreesOfSweep],...
                  'MaxRange',[RobotFixedParameters.MaxRange MaxRange],...
                  'LaserAngVar',[RobotFixedParameters.LaserAngVar LaserAngVar],...
                  'LaserRangVar',[RobotFixedParameters.LaserRangVar LaserRangVar],...
                  'VarianceOfVehNoise',[RobotFixedParameters.VarianceOfVehNoise VarianceOfVehNoise]);

SpeedsOfRobots(RobotNumber) = { RobotSpeed};
DirectionsOfRobots(RobotNumber) = { Direction};


% VehicleNoiseSwitch = 0;             %This switch should not be in this file

%% Clean Up
clear DegreesOfSweep
clear Direction
clear InitialLocation
clear InitialOrientation
clear MaxRange
clear NumberOfBeams
clear RobotNumber
clear RobotSpeed
clear LaserAngVar
clear LaserRangVar
clear VarianceOfVehNoise
