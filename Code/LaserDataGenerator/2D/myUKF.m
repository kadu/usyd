% Unscented Transform....
function[MuDash CovDash OriginalMean] = myUKF(InitUKFMean,InitUKFVar,alpha,beta,kappa,NumPose)

%% Generate input sigmas for each laser point...
NumberOfHits = size(InitUKFMean,2);


InitUKFVar = InitUKFVar + 0.000001*eye(5);        %To ensure the matrix is pos semi def.          

size(InitUKFMean,2)
% pause
for i = 1:size(InitUKFMean,2)
 
 size(InitUKFMean,2) - i
 %Increase variance in Pose for each pose from the start point
    InitUKFVarFinal = InitUKFVar;
 if nargin >5
    InitUKFVarFinal(1,1) = InitUKFVarFinal(1,1)*(NumPose(i)-1)+0.00001*rand(1);
    InitUKFVarFinal(1,2) = InitUKFVarFinal(1,2)*(NumPose(i)-1)+0.000000001*rand(1);
    InitUKFVarFinal(2,1) = InitUKFVarFinal(2,1)*(NumPose(i)-1)+0.000000001*rand(1);
    InitUKFVarFinal(2,2) = InitUKFVarFinal(2,2)*(NumPose(i)-1)+0.00001*rand(1);  
 end
    
 [xPts, wPts, nPts] = scaledSymmetricSigmaPoints(InitUKFMean(:,i),InitUKFVarFinal,alpha,beta,kappa);

 SigmaInputPoints(:,:,i) = xPts;        %3-d matrix of each of the sigma points points
 SigmaInputWeights(:,i) = wPts;         %2-d matrix of their weights
 SigmaInputnPts(i) = nPts;

end

%======================================================================


%% Project sigma points through the non-linear range function
for i = 1:NumberOfHits  
    for j = 1:nPts
        SigmaOutputPoints(:,j,i) = Sensor2World(SigmaInputPoints(1:2,j,i),...
            SigmaInputPoints(3,j,i),SigmaInputPoints(4,j,i),SigmaInputPoints(5,j,i)); 
    end
%      plot(SigmaOutputPoints(1,1,i),SigmaOutputPoints(2,1,i),'r+');
end
%==========================================================================



%% Find estimated mean and covariance of hits using UKF algorithm


for LaserHit = 1:NumberOfHits

   WeightedSigmas = repmat(SigmaInputWeights(1:end-1,LaserHit)-SigmaInputWeights(end,LaserHit),1,2)'.*SigmaOutputPoints(:,:,LaserHit);
   MuDash(:,LaserHit) = sum(WeightedSigmas,2);
   MuDashRep=repmat(MuDash(:,LaserHit),1,nPts);
   CovDash(:,:,LaserHit) = repmat([SigmaInputWeights(end,LaserHit)...
       (SigmaInputWeights(2:end-1,LaserHit)-SigmaInputWeights(end,LaserHit))'],2,1)...
       .*(SigmaOutputPoints(:,:,LaserHit)-MuDashRep)*(SigmaOutputPoints(:,:,LaserHit)-MuDashRep)';

end

if nargout >2

    OriginalMean = SigmaOutputPoints(:,1,:);
    
end
