% Unscented Transform....
function[MuDash CovDash OriginalMean] = UKF(InitUKFMean,InitUKFVar,alpha,beta,kappa)

NumberOfPoses = size(InitUKFMean,2);

InitUKFVar(1,1,:) = InitUKFVar(1,1,:)  + 0.000001;       %To ensure the matrix is pos semi def.          
InitUKFVar(2,2,:) = InitUKFVar(1,1,:)  + 0.000001;      %To ensure the matrix is pos semi def.          

for i = 1:NumberOfPoses
 [xPts, wPts, nPts] = scaledSymmetricSigmaPoints(InitUKFMean(:,i),InitUKFVar(:,:,i),alpha,beta,kappa);

 SigmaInputPoints(:,:,i) = xPts;        %3-d matrix of each of the sigma points points
 SigmaInputWeights(i,:) = wPts;         %2-d matrix of their weights
 SigmaInputnPts(i) = nPts;
end

%======================================================================


%% Project sigma points through the non-linear range function
for i = 1:NumberOfPoses
    for j = 1:nPts

                RobotLocation(:,j,i) = [SigmaInputPoints(1,j,i)*cosd(SigmaInputPoints(2,j,i)+90);SigmaInputPoints(1,j,i)*sind(SigmaInputPoints(2,j,i)+90)]
                RobotOrientation(j,i) = SigmaInputPoints(2,j,i)
                

    end
end

% for i = 1:NumberOfHits  
%     for j = 1:nPts
%         SigmaOutputPoints(:,j,i) = Sensor2World(SigmaInputPoints(1:2,j,i),...
%             SigmaInputPoints(3,j,i),SigmaInputPoints(4,j,i),SigmaInputPoints(5,j,i)); 
%     end
% %      plot(SigmaOutputPoints(1,1,i),SigmaOutputPoints(2,1,i),'r+');
% end
%==========================================================================



%% Find estimated mean and covariance of hits using UKF algorithm


for Pose = 1:NumberOfPoses

   WeightedSigmas =  repmat(SigmaInputWeights(Pose,1:end-1)-SigmaInputWeights(Pose,end),3,1).*[RobotLocation(:,:,Pose); RobotOrientation(:,Pose)']
    

   MuDash(:,Pose) = sum(WeightedSigmas,2);
   MuDashRep=repmat(MuDash(:,Pose),1,nPts);
   CovDash(:,:,Pose) = repmat([SigmaInputWeights(Pose,end)...
       (SigmaInputWeights(Pose,2:end-1)-SigmaInputWeights(Pose,end))],3,1)...
       .*([RobotLocation(:,:,Pose); RobotOrientation(:,Pose)']-MuDashRep)*([RobotLocation(:,:,Pose); RobotOrientation(:,Pose)']-MuDashRep)';

end

if nargout >2

    OriginalMean = [reshape(RobotLocation(:,1,:),2,numel(RobotLocation(:,1,:))/2); RobotOrientation(1,:)];
    
end
