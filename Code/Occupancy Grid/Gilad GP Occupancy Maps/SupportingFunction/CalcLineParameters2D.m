function Trig=CalcLineParameters2D(StartLine, EndLine, LineLength)

%StartLine,EndLine =[x;y]
%LineLength is optional parameter to save time

if nargin<3
   LineLength=sqrt(sum((StartLine-EndLine).^2,1));
end

CosTetha=(EndLine(1,:)-StartLine(1,:))./(LineLength+eps);
SinTetha=(EndLine(2,:)-StartLine(2,:))./(LineLength+eps);


Trig.CosTetha=CosTetha;
Trig.SinTetha=SinTetha;
Trig.LineLength=LineLength;
