%function [PredMean,PredVar,LogMarg]=CalcPredication(gp, L, X_query,y_Targets,LRF_in_RobotCoord)
function [PredMean,PredVar,LogMarg]=CalcPredication(gp, L, K, X_query,y_Targets,LRF_in_RobotCoord)

%Following GPO regression by Rasmussen

% if (~exist('L','var')) || (isempty(L))
%     warning('L does not exist or empty');
%     return; 
% end


Hits=LRF_in_RobotCoord.Hits;
HitsRobotIndex=LRF_in_RobotCoord.HitsRobotIndex;
FreeBeam.Start=LRF_in_RobotCoord.FreeBeam.Start;
FreeBeam.End=LRF_in_RobotCoord.FreeBeam.End;
FreeBeamIndex=LRF_in_RobotCoord.FreeBeamIndex;


K_Xstar_X_PP=feval(gp.Point2PointFunc, X_query, Hits, gp.Point2PointParamVec);
K_Xstar_X_PL=feval(gp.Point2LineFunc, X_query, FreeBeam, gp.Point2LineParamVec);

if (size(K_Xstar_X_PP,1)~=size(K_Xstar_X_PL,1))
    warning('WTF?')
end

K_Xstar_X=zeros(size(X_query,2),size(K_Xstar_X_PP,2)+size(K_Xstar_X_PL,2));
if ~isempty(HitsRobotIndex)
    K_Xstar_X(:,HitsRobotIndex)=K_Xstar_X_PP;
end
if ~isempty(FreeBeamIndex)
    K_Xstar_X(:,FreeBeamIndex)=K_Xstar_X_PL;
end


Alpha=L\(L'\y_Targets);%Our L is actaully a upper-trig matrix and not lower-trig matrix like the one in Rasmussen GP
%Alpha=L'\(L\y_Targets);


PredMean=K_Xstar_X*Alpha;
%PredMean=K_Xstar_X*inv(K)*y_Targets;


v=L'\K_Xstar_X'; %%Is L' or L ??
K_Xstar_XStar=feval(gp.Point2PointFunc, X_query, X_query, gp.Point2PointParamVec);
PredVar=diag(K_Xstar_XStar-v'*v);

%length(y_Targets)=n
LogMarg=-0.5*y_Targets'*Alpha-sum(log(diag(L,0)))-(length(y_Targets)/2)*log(2*pi);
