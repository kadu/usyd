function [Nodes, Weights]=Rescale_CC_Nodes_Weights(Nodes, Weights, Int_A, Int_B)

%Clenshaw-Curtis quadrature is normaly between [-1 1],
%This function rescale Nodes and Weights to suit the interval [Int_A Int_B]


N=size(Nodes,2);

Nodes = 0.5 * ( ( Nodes + 1.0 ) .* Int_B(:,ones(1,N)) - ( Nodes - 1.0 ) .* Int_A(:,ones(1,N)) );
Weights = 0.5 * ( Int_B - Int_A ) .* Weights;