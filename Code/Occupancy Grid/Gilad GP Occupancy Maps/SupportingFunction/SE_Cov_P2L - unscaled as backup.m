function KPL=SE_Cov_P2L(P1,L2, Param)
%Squared exponential  - Point to Line based on ClenShaw-Curtis quadrature

%%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
%!!!!Need to decide how to tackle error estimation of quadarture - how many
%node and weights to use
%%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


%P1 is a point with dimension D.
%L2 is a struct that contain start and end nodes. Ech with dimension D.
%LengthScale can be different for each dimension. LengthScale must be
%either 1 element (identical to all dimension) or D long.
%Param - to standratize covariance function. Input is identical R1,R2 and
%Param that are suitable for each specific covaraince function


LengthScale=Param(1:end-1)';
SigmaF=Param(end);

P1_N=size(P1,2);
L2_N=size(L2.Start,2);


P1Dim=size(P1,1);
L2Dim=size(L2.Start,1);


%check that dimension are right
KPL=[];

if isempty(P1) || isempty(L2.Start)
    return;
end


if P1Dim~=L2Dim
    error('Dimension error between input vectors');
end

if (length(LengthScale)~=1) && (length(LengthScale)~=P1Dim)
    error('Incorrect LengthScale input - Dimension mismatch');
elseif (length(LengthScale)==1)
     LengthScale= LengthScale*ones(P1Dim,1);  
end

%Calc Line angle
Trig2=CalcLineParameters2D(L2.Start, L2.End); %Line length is rescaled by LengthScale 

%Rescale LengthScale - Rescale length should not affect trigomnometric calc
% P1=P1./(LengthScale(:,ones(1,P1_N)));
% L2_Start=L2.Start./(LengthScale(:,ones(1,L2_N)));
% L2_End=L2.End./(LengthScale(:,ones(1,L2_N)));

 L2_Start=L2.Start;



%replicating coordinates of points to parallelize calculations


KPL=zeros(P1_N,L2_N);

for j=1:L2_N %running on laser beam
       
    X_s_2=L2_Start(:,j);
    
    Cos2=Trig2.CosTetha(j);
    Sin2=Trig2.SinTetha(j);
    L2=Trig2.LineLength(j);
    
    %%Quadrature
    %Clenshaw-Curtis quadrature
    %k=sigma(j=1:M, W2j*cos(j*pi/M)*sigma(1:N, W1i*cos(i*pi/N)))
    %Calc Nodes/Weights inside loop as aprovision for a different number of
    %nodes for different line
    NodeNum=100; %%!!! need to decide on a trategy to define number of nodes

    %L2 Chebyshev nodes and weights
    [Nodes, Weights]=Calc_ClenShawCurtis_NodesWeights(NodeNum);
    [Nodes, Weights]=Rescale_CC_Nodes_Weights(Nodes, Weights, 0, 1);
    % reshaping in order to reduce number of for loops
    
    %generating integral function estimation at the Chebyshev nodes
%   
    Nodes=[L2*Cos2*Nodes;L2*Sin2*Nodes]+X_s_2(:, ones(1,NodeNum+1));
    Weights=Weights';
    P=P1;
    
    XX1=sum(Nodes.*Nodes, 1); %sums element in X and Y for the CC nodes
    XX2=sum(P.*P, 1); %sums element in X and Y for the CC nodes
    X1X2 = (Nodes)'*P;
    XX1T = XX1';
    z = XX1T(:,ones(1,P1_N)) + XX2(ones(1,NodeNum+1),:) - 2*X1X2;
    f_x1=Weights(:,ones(1,P1_N)).*exp(-0.5*z);
    
    KPL(:,j)=L2*(SigmaF)^2*(sum(f_x1,1))';
    
        

end
    
