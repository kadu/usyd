function [Nodes, Weights]=Calc_ClenShawCurtis_NodesWeights(NodeNum);

%Calculate nodes and weights for the Cleanshaw-Curtis Quad

%Based on Fast Construction of the Fejer and Clenshaw-Curtis Quadrature Rules
%Jorg Waldvogel, Seminar for Applied Mathematics,
% BIT Numerical Mathematics 2003, Vol. 43, No. 1, pp. 001-018


Nodes=cos((0:1:NodeNum)*pi/NodeNum);
%Correct Numerical problems in case of NodeNum+1 odd
if mod(NodeNum,2)%NodeNum+1 is odd number
    Nodes(NodeNum/2+1)=0;
end

Bj=2*ones(NodeNum+1,floor(NodeNum/2));
Bj(:,end)=Bj(:,end)-(1-mod(NodeNum,2)); %A_n/2 has half the magnitude to prevent double counting of the edges

[X_indices,Y_indices] = meshgrid(1:floor(NodeNum/2),0:NodeNum);
W_TempMat=cos(2*X_indices.*Y_indices*pi/NodeNum)./(4*X_indices.^2-1).*Bj;
W=sum(W_TempMat,2)';

Ck=2*ones(1,NodeNum+1);
Ck(1)=1;
Ck(end)=1;

Weights=Ck/NodeNum.*(1-W);


