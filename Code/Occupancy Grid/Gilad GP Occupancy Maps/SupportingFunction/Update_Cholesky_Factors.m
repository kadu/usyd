function [Updated_L Updated_K]=Update_Cholesky_Factors(gp, L, Old_K, K_P_NewP, K_L_NewL, K_NewP_L, K_P_NewL, HitsRobotIndex, FreeBeamIndex)

%Updating Cholesky factor s
%Following procedure from "Online Self-Supervised Segmentation of Dynamic
%Objects"; V. Guizilini and F. Ramos; ICRA 2013




%Incermental update
%following notation from the paper above

%since mix of free beam and hits (no particular order). Update L requires
%some handling

try
    
    if isempty(L) %first update
        Updated_L=[K_P_NewP,K_P_NewL;K_NewP_L',K_L_NewL];
        Updated_L=Updated_L+gp.Sigma_n^2*eye(size(Updated_L));
        Updated_K=Updated_L;
        Updated_L=chol(Updated_L);
    else
        
        NewL_Size=max([FreeBeamIndex HitsRobotIndex]);
        
        NewHitsIndex=HitsRobotIndex(HitsRobotIndex>size(L,1));
        NewFreeBeamIndex=FreeBeamIndex(FreeBeamIndex>size(L,1));
        
        Updated_L=zeros(NewL_Size, NewL_Size);
        
        Updated_L(1:size(L,1),1:size(L,2))=L;
        
        %following Vitor's notations
        
        
        
        Knew=zeros(NewL_Size,NewL_Size);
        Knew(HitsRobotIndex,NewHitsIndex)=K_P_NewP;
        Knew(HitsRobotIndex,NewFreeBeamIndex)=K_P_NewL;
        Knew(FreeBeamIndex,NewHitsIndex)=K_NewP_L';
        Knew(FreeBeamIndex,NewFreeBeamIndex)=K_L_NewL;
        
        K_1_2=Knew(1:size(L,1),size(L,2)+1:end);%only new measurements
        K_3_3=Knew(size(L,1)+1:end,size(L,2)+1:end);%only new measurements
        
        c3=L'\K_1_2;
        
        Updated_K=[Old_K,K_1_2;K_1_2',K_3_3+gp.Sigma_n^2*eye(size(K_3_3))];
        

        Updated_L=[L, c3; zeros(size(K_3_3,1),size(L,2)), chol(K_3_3-c3'*c3+gp.Sigma_n^2*eye(size(K_3_3)))];
        
        %Updated_L=[L, c3; zeros(size(K_3_3,1),size(L,2)), chol(K_3_3-c3'*c3)];
    end
    
    
catch
    disp('oops')
end

