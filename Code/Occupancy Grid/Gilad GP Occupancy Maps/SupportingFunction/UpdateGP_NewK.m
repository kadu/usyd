%function [L,y_Targets,LRF_in_RobotCoord]=UpdateGP(gp, L, RobotLocationVar, LRF_in_RobotCoord, RobotFixedParameters)
function [L ,K, y_Targets,LRF_in_RobotCoord]=UpdateGP_NewK(gp, L, K, RobotLocationVar, LRF_in_RobotCoord, RobotFixedParameters)

%This function is only for DEBUGGING!!!!!


%This function generates the Cholesky decomp for the covraunce matrix
% the covaraince matrix is made up of several sub-matrices P2P, P2L, L2L.
% However, since each update there is a mix of line and points, there
% matrix itself does not have  a distinct L2L or P2P subsets
% Defintion: Occupied y_Target=1, Free y_Target=-1;

%Use_LRF_and_Pose_Noise - based on Simon paper on using GP with noise on
%laser/pose estimates. Note: this is not Sigma_n which is the observation
%noise which is a gp param (why does we have this difference - check Simon paper again????)

% Output:
% L: the Cholesky factorization of the covariance matrix.
% y_Targets: a vector of occupation of the training data : 1 occupied -1: not occupied
%LRF_in_RobotCoord si return with the BeamUsed and LaserHitUsed results

   

%%
%Preparation
    
BeamsAngleRobotMat=LRF_in_RobotCoord.BeamsAngleRobotMat;
RobotLaserRangeMat=LRF_in_RobotCoord.RobotLaserRangeMat;
LaserHitUsed=LRF_in_RobotCoord.LaserHitsMat;
RobotLocation=RobotLocationVar.RobotLocationVec;

MaxRange=RobotFixedParameters.MaxRange;

%prepare preivously used points




%%
%Prepare points
if isfield(LRF_in_RobotCoord, 'HitsRobotIndex') && (~isempty(LRF_in_RobotCoord.HitsRobotIndex))
    HitsRobotIndex=LRF_in_RobotCoord.HitsRobotIndex;
    %HitsRobotIndex is a matrix with the same size as the laserRange
    %matrix and it got the covariance matrix index for each laser hit
    %It assumes that each iteration of measurement is a new column (!). if
    %that not the case  then the indexing won't work
else
    HitsRobotIndex=[];
end

% if isfield(LRF_in_RobotCoord, 'FreeBeamIndex') && (~isempty(LRF_in_RobotCoord.FreeBeamIndex))
%     FreeBeamIndex=LRF_in_RobotCoord.FreeBeamIndex;
%     %FreeBeamIndex is a matrix with the same size as the laserRange
%     %matrix and it got the covariance matrix index for each beam
%     %It assumes that each iteration of measurement is a new column (!). if
%     %that not the case  then the indexing won't work
% else
%     FreeBeamIndex=[];
% end

if isfield(LRF_in_RobotCoord, 'Hits') && (~isempty(LRF_in_RobotCoord.Hits))
    Hits=LRF_in_RobotCoord.Hits;
    HitsRobotIndex=LRF_in_RobotCoord.HitsRobotIndex;
else
    Hits=[];
    
end

if isfield(LRF_in_RobotCoord, 'FreeBeamIndex') && (~isempty(LRF_in_RobotCoord.FreeBeamIndex))
    FreeBeam.Start=LRF_in_RobotCoord.FreeBeam.Start;
    FreeBeam.End=LRF_in_RobotCoord.FreeBeam.End;
    FreeBeamIndex=LRF_in_RobotCoord.FreeBeamIndex;
else
    FreeBeam.Start=[];
    
    FreeBeam.End=[];
end

FreeBeamIndex=[];
HitsRobotIndex=[];


if isempty(HitsRobotIndex) & isempty(FreeBeamIndex)
    NewUpdateIndex=0;
elseif isempty(HitsRobotIndex)
    NewUpdateIndex=max(FreeBeamIndex);
elseif isempty(FreeBeamIndex)
    NewUpdateIndex=max(HitsRobotIndex);
else
    NewUpdateIndex=max([FreeBeamIndex,HitsRobotIndex]);
end
%Arrange old and New points
%new beams/hits - always in the last column
NewBeamsAngle=BeamsAngleRobotMat(:, end);
NewRobotLaserRange=RobotLaserRangeMat(:, end);
NewPose=[RobotLocation(1,end);RobotLocation(2,end)];
[NewX,NewY]=pol2cart(NewBeamsAngle,NewRobotLaserRange);%return x and y for all range finder beams

%New hits
NewHits=logical(LaserHitUsed(:, end));
P2_NewHits=[NewX(NewHits)+NewPose(1),NewY(NewHits)+NewPose(2)]';

%new Free beams
NewBeam.Start=[NewPose(1,ones(1,size(NewBeamsAngle,1)));NewPose(2,ones(1,size(NewBeamsAngle,1)))];
[FreeBeamX,FreeBeamY]=pol2cart(NewBeamsAngle,NewRobotLaserRange-0.1);%return x and y for all range finder beams
NewBeam.End=[FreeBeamX';FreeBeamY']+NewBeam.Start;


%%
%Decide which points need to be sent to calculation of matrix
%!
%!
%!
%!Need to complete
%at the momemt all points and lines are included

%Hits and NewBeam was updated on the normal code before dubg!!
NewUpdateIndex=0;
%Hits=[Hits,P2_NewHits];
NewHitsIndex=NewUpdateIndex+1:NewUpdateIndex+size(Hits,2);
HitsRobotIndex=[HitsRobotIndex,NewHitsIndex ]; 

NewUpdateIndex=NewUpdateIndex+size(Hits,2);

FreeBeam.Start=[FreeBeam.Start,NewBeam.Start];
FreeBeam.End=[FreeBeam.End,NewBeam.End];
NewFreeBeamIndex=NewUpdateIndex+1:NewUpdateIndex+size(FreeBeam.Start,2);
FreeBeamIndex=[FreeBeamIndex, NewFreeBeamIndex];



%%


%Generates P2P,P2L, L2L for required points. These matrices will be the
%base to update the entire K matrix


    %% to test K update
    K=[];
    L=[];
    K_P_NewP2=feval(gp.Point2PointFunc, Hits, Hits, gp.Point2PointParamVec);
    K_L_NewL2=feval(gp.Line2LineFunc, FreeBeam, FreeBeam, gp.Line2LineParamVec); 
    K_NewP_L2=feval(gp.Point2LineFunc, Hits, FreeBeam, gp.Point2LineParamVec);
    K_P_NewL2=feval(gp.Point2LineFunc, Hits, FreeBeam, gp.Point2LineParamVec); 
    [L,K] =Update_Cholesky_Factors(gp,  L, K, K_P_NewP2, K_L_NewL2, K_NewP_L2, K_P_NewL2, HitsRobotIndex, FreeBeamIndex);

%%

%Update LRF object
y_Targets=zeros(max([HitsRobotIndex,FreeBeamIndex]),1);
y_Targets(HitsRobotIndex)=1;
y_Targets(FreeBeamIndex)=-sqrt(sum((FreeBeam.End-FreeBeam.Start).^2,1));


LRF_in_RobotCoord.HitsRobotIndex=HitsRobotIndex;
LRF_in_RobotCoord.FreeBeamIndex=FreeBeamIndex;
LRF_in_RobotCoord.Hits=Hits;
LRF_in_RobotCoord.FreeBeam.Start=FreeBeam.Start;
LRF_in_RobotCoord.FreeBeam.End=FreeBeam.End;

