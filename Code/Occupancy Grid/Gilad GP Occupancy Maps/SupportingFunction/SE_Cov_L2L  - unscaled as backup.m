function KLL=SE_Cov_L2L(L1,L2, Param)
%Squared exponential  - Line to Line based on ClenShaw-Curtis quadrature

%%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
%!!!!Need to decide how to tackle error estimation of quadarture - how many
%node and weights to use
%%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


%L1 and L2 is a D dimension vector in cartesian coordiantes
%L1 and L2 are a struct that contain Start and end nodes
%LengthScale can be different for each dimension. LengthScale must be
%either 1 element (identical to all dimension) or D long.
%Param - to standratize covariance function. Input is identical R1,R2 and
%Param that are suitable for each specific covaraince function


LengthScale=Param(1:end-1)';
SigmaF=Param(end);

L1_N=size(L1.Start,2);
L2_N=size(L2.Start,2);



L1Dim=size(L1.Start,1);
L2Dim=size(L2.Start,1);

KLL=[];
if isempty(L1.Start) || isempty(L2.Start)
    return;
end


%check that dimension are right
if L1Dim~=L2Dim
    error('Dimension error between input vectors');
end

if (length(LengthScale)~=1) && (length(LengthScale)~=L1Dim)
    error('Incorrect LengthScale input - Dimension mismatch');
elseif (length(LengthScale)==1)
     LengthScale= LengthScale*ones(L1Dim,1);  
end

%Calc Line angle - must be done before rescale using lengthscale
Trig1=CalcLineParameters2D(L1.Start, L1.End);
Trig2=CalcLineParameters2D(L2.Start, L2.End);
L1_Start=L1.Start;
L2_Start=L2.Start;

%Rescale with LengthScale
% L1_Start=L1.Start./(LengthScale(:,ones(1,L1_N)));
% L2_Start=L2.Start./(LengthScale(:,ones(1,L2_N)));
% L1_End=L1.End./(LengthScale(:,ones(1,L1_N)));
% L2_End=L2.End./(LengthScale(:,ones(1,L2_N)));





%Clenshaw-Curtis quadrature
%k=sigma(j=1:M, W2j*cos(j*pi/M)*sigma(1:N, W1i*cos(i*pi/N)))






KLL=zeros(L1_N,L2_N);

for i=1:L1_N 
    for j=i:L2_N
       
        X_s_1=L1_Start(:,i);
        X_s_2=L2_Start(:,j);
        
        

        %L1 Chebyshev nodes and weights - need to calculate each time since
        % it might be different for each integral
        NodeNum=100; %%!!! need to decide on a trategy to define number of nodes

        [Nodes1, Weights1]=Calc_ClenShawCurtis_NodesWeights(NodeNum);
        [Nodes1, Weights1]=Rescale_CC_Nodes_Weights(Nodes1, Weights1, 0, 1);
        %L1 Chebyshev nodes and weights
        [Nodes2, Weights2]=Calc_ClenShawCurtis_NodesWeights(NodeNum);
        [Nodes2, Weights2]=Rescale_CC_Nodes_Weights(Nodes2, Weights2, 0, 1);
        [X1i,X2j]=meshgrid(Nodes1,Nodes2);
        [W1i,W2j]=meshgrid(Weights1,Weights2);
        
        Cos1=Trig1.CosTetha(i);
        Cos2=Trig2.CosTetha(j);
        Sin1=Trig1.SinTetha(i);
        Sin2=Trig2.SinTetha(j);
        L1=Trig1.LineLength(i);
        L2=Trig2.LineLength(j);

        
        %generating integral function estimation at the Chebyshev nodes
        
        f_X1_X2=exp(-0.5*((L1*Cos1*X1i+X_s_1(1)-L2*Cos2*X2j-X_s_2(1)).^2+...
        (L1*Sin1*X1i+X_s_1(2)-L2*Sin2*X2j-X_s_2(2)).^2));
        CC_temp=sum(W1i.*f_X1_X2,2);
        CC_temp=sum(Weights2'.*CC_temp);
        KLL(i,j)=L1*L2*(SigmaF)^2*CC_temp;
        
        
%         f_X1_X2=(-0.5*((L1*Cos1*X1i-L2*Cos2*X2j+(X_s_1(1)-X_s_2(1))).^2+(L1*Sin1*X1i-L2*Sin2*X2j+(X_s_1(2)-X_s_2(2))).^2));
%         %Multiply with constants
%         f_X1_X2=exp(log(L1*L2*(SigmaF)^2)-f_X1_X2);
%                
%         
%         KLL(i,j)=sum(sum(W1i.*W2j.*f_X1_X2));
        
        
    end
end


%update lower part of matrix
KLL=KLL+triu(KLL,1)';
