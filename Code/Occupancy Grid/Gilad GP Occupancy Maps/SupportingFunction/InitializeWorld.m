function WorldMap=InitiallizeWorld(filename)

%function loads a map according to input.
%Otherwise ot either loads a default map or prompts the user for input


%World struct follows Simon's convention of keeping the coordinates of the corners
if nargin>0
    %Try to load user's request
    try
      S=load(filename);
 
    catch
       warning(ME.message) 
       promptMessage = sprintf('Cannot load request file. Try to load default');
       button = questdlg(promptMessage, 'Continue', 'Continue', 'Cancel', 'Continue');
        if strcmpi(button, 'Cancel')
             rethrow(ERR); % Or break or continue
        end
        %else continue to load default
    end
end

%load defaults or ask user to define request
if ~exist('S','var') %error in loading required file, try to load default
    currentFolder = pwd;
    if exist([currentFolder,'\SupportingFiles\Simple_2D_World.mat'],'file')
    %load existing file
        S=load([currentFolder,'\SupportingFiles\Simple_2D_World.mat']);
    else
        S = uiimport;   
    end
end

%load Map
WorldMap=S.WorldMap;
clear S;

MapXs = [WorldMap.WallStartPointsX;WorldMap.WallEndPointsX];
MapYs = [WorldMap.WallStartPointsY;WorldMap.WallEndPointsY];
GroundTruth_Fig_Handle=figure;
plot(MapXs, MapYs, 'b');
axis equal
title('Ground Truth');
