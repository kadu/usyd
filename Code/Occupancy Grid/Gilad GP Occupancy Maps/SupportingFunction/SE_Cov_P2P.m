function KPP=SE_Cov_P2P(R1,R2, Param)
%Squared exponential  - point to point

%R1 and R2 is a D dimension vector in cartesian coordiantes
%LengthScale can be different for each dimension. LengthScale must be
%either 1 element (identical to all dimension) or D long.



%Based on Fabio's code

%Param - to standratize covariance function. Input is identical R1,R2 and
%Param that are suitable for each specific covaraince function
LengthScale=Param(1:end-1);
SigmaF=Param(end);

R1Dim=size(R1,1);
R2Dim=size(R2,1);

R1_N=size(R1,2);
R2_N=size(R2,2);

KPP=[];
if isempty(R1) || isempty(R2)
    return;
end

%check that dimension are right
if R1Dim~=R2Dim
    error('Dimension error between input vectors');
end

if (length(LengthScale)~=1) && (length(LengthScale)~=R1Dim)
    error('Incorrect LengthScale input - Dimension mismatch');
elseif (length(LengthScale)==1)
     LengthScale= LengthScale*ones(1, R1Dim);  
end




%K=sigma^2*exp(-|R1-R2]^2/(2*l^2)
%|R1-R2|^2=R1^2+R2^2+R1'*R2

w=LengthScale'.^-2;
XX1 = sum(w(:,ones(1,R1_N)).*R1.*R1,1); %sum on X,Y,Z with the appropriate length scale
XX2 = sum(w(:,ones(1,R2_N)).*R2.*R2,1);
X1X2 = (w(:,ones(1,R1_N)).*R1)'*R2;
XX1T = XX1';
z = XX1T(:,ones(1,R2_N)) + XX2(ones(1,R1_N),:) - 2*X1X2;

KPP = exp(log((SigmaF)^2)-0.5*z); %put the sf2 inside the exponential to avoid numerical problems.

