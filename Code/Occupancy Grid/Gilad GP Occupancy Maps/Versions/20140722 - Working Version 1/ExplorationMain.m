%%%%%Issues 4/7/2014
%1. Do I need to implement a simulatiomn for false alarm detection -
%meaning the LRF report a distance although no actual obstacles exist - NOT
%IMPLMENTED at the moment
%2. Quadrature - need to implment a dynamic weights/nodes to define the
%right error estimate

%3. SE is not a suitable kernel for laser beam since it generates an elippse around the beam.  A more suitable kerenl will depends on the diverging angle
%4. Not clear why the Ys for the line observations should be the length of the line (BTW these are not the rescaled line length but the correct line length) 



clc
clearvars
% close all


mFileName = mfilename('fullpath');
[currentFolder,NAME,EXT] = fileparts(mFileName);
cd(currentFolder)
if ~exist('Results','dir')
    mkdir(currentFolder, 'Results')
end

if ~exist('Results','dir')
    error('Cannot find Results directory')
end

SaveStrPreFix=[sprintf('%04d',year(now)),sprintf('%02d',month(now)),sprintf('%02d',day(now))];
SimulationCond_SaveFileName=[currentFolder,'\Results\',SaveStrPreFix,'_SimulationCondition.mat'];
Pose_and_LRF_SaveFileName=[currentFolder,'\Results\',SaveStrPreFix,'_Pose_and_LRF.mat'];



%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%***************Experiminetal**********************************************
%Exprimental Parameters

LaserRangeNoise=0;
LaserAngularNoise=0;
VehiclePoseNoise=0;

%Use default GP parameter
disp('Using default GP parameters - should convert to trained parameters')





%%
%%%%%%Initialize world%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
WorldMap=InitializeWorld('H:\MATLAB\GP\Gilad Exploration\SupportingFiles\Simple_2D_World.mat');


%%
%%%%%%Initialize robot limit%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
RobotFixedParameters = struct('InitialLocation', [],...
    'InitialOrientation',[],...
    'NumberOfBeams',[],...
    'DegreesOfSweep',[],...
    'MaxRange',[],...
    'LaserAngVar',[],...
    'LaserRangVar',[],...
    'SpeedVar',[],...
    'DirectionVar',[]);


%here we can implement loading parameter of a different experiment

InitialLocation = [-6;5];
InitialOrientation = -20;     %Relative to X axis in degrees
NumberOfBeams = 17;         %37;
DegreesOfSweep = 180;
MaxRange = 8;
LaserAngVar = 0;%0.5;            %Measured in degrees
LaserRangVar = 0;%0.8;           %Measured in metres
SpeedVar = 0;    %Variance of vehicle's speed. Measured as a fraction of vehicles speed. i.e. 0.1 m var for 1 m/s, 0.2 m var for 2 m/s
DirectionVar = 0; %Variance of vehicles direction. Measured as a fraction of vehicles turn angle

RobotFixedParameters = struct('InitialLocation', [RobotFixedParameters.InitialLocation InitialLocation],...
    'InitialOrientation',[RobotFixedParameters.InitialOrientation InitialOrientation],...
    'NumberOfBeams',[RobotFixedParameters.NumberOfBeams NumberOfBeams],...
    'DegreesOfSweep',[RobotFixedParameters.DegreesOfSweep DegreesOfSweep],...
    'MaxRange',[RobotFixedParameters.MaxRange MaxRange],...
    'LaserAngVar',[RobotFixedParameters.LaserAngVar LaserAngVar],...
    'LaserRangVar',[RobotFixedParameters.LaserRangVar LaserRangVar],...
    'SpeedVar',[RobotFixedParameters.SpeedVar SpeedVar],...
    'DirectionVar',[RobotFixedParameters.DirectionVar DirectionVar]);

%%
%robot spead/direction is temporary until exploration works
RobotSpeed = [1,1,1,1,1,1,1,1,1,0.5,0.5,1,1,1,1,1,1,1,1,1,1,2,2,1,2];
Direction = [-10,-20,-20,-20,0,20,20,20,20,0,0,0,0,20,0,20,-20,-20,0,-20,-20,-20,-20,-40,-40];        %0 is straight ahead. +degrees to left, -degrees to right
ActualRobotSpeed = RobotSpeed + RobotSpeed.*(sqrt(SpeedVar)*randn(1,size(RobotSpeed,2)));
ActualDirection = Direction + Direction.*(sqrt(DirectionVar)*randn(1,size(Direction,2)));




%%
%Rotation of axis is minus the angle in the rotation matrix
MapXs = [WorldMap.WallStartPointsX;WorldMap.WallEndPointsX];
MapYs = [WorldMap.WallStartPointsY;WorldMap.WallEndPointsY];
R = [cosd(-InitialOrientation) -sind(-InitialOrientation);sind(-InitialOrientation) cosd(-InitialOrientation)];
MapXsR=MapXs-InitialLocation(1);
MapYsR=MapYs-InitialLocation(2);
T1=R*[MapXsR(1,:);MapYsR(1,:)];
T2=R*[MapXsR(2,:);MapYsR(2,:)];
MapXsR=[T1(1,:);T2(1,:)];
MapYsR=[T1(2,:);T2(2,:)];
RobotWorld.WallStartPointsX=MapXsR(1,:);
RobotWorld.WallEndPointsX=MapXsR(2,:);
RobotWorld.WallStartPointsY=MapYsR(1,:);
RobotWorld.WallEndPointsY=MapYsR(2,:);


save(SimulationCond_SaveFileName,'RobotFixedParameters','WorldMap','RobotWorld')




%%
%init variables for simulation
GlobalLocationVec=[];
RobotLocationVec=[];
RobotDirectionInRadVec=[];
GlobalDirectionVec=[];
BeamsAngleRobotMat=[];
BeamsAngleGlobalMat=[];
LaserHitsMat=[];
LaserRangeMat=[];
RobotLaserRangeMat=[];

L=[];
K=[];
LRF_in_RobotCoord.BeamUsed=[];
LRF_in_RobotCoord.LaserHitUsed=[];
%%
for Time=0:length(Direction)
    
    %Calc global position (to calculate laser returns)
    if Time==0
        GlobalLocation=InitialLocation;
        RobotLocation=[0;0];
        RobotDirectionInRad=0;
        GlobalDirection=InitialOrientation*pi/180;
    else
        GlobalDirection=GlobalDirection+ActualDirection(Time)*pi/180;
        RobotDirectionInRad=RobotDirectionInRad+Direction(Time)*pi/180;
        
        GlobalLocation=GlobalLocation+[ActualRobotSpeed(Time)*cos(GlobalDirection);ActualRobotSpeed(Time)*sin(GlobalDirection)];
        RobotLocation=RobotLocation+[RobotSpeed(Time)*cos(RobotDirectionInRad);RobotSpeed(Time)*sin(RobotDirectionInRad)];
        
    end
    
    
    %Generate beam
    BeamsAngle=(linspace(-DegreesOfSweep/2, DegreesOfSweep/2, NumberOfBeams))*pi/180; %robot assumes this is the right angle
    ActualBeamAngle=BeamsAngle+sqrt(LaserAngVar)*randn(1,NumberOfBeams)*pi/180; %actual leaserbeam angle including noise
    BeamsAngleGlobal=ActualBeamAngle+GlobalDirection;%Beam in global coordinates
    BeamsAngleRobot=BeamsAngle+RobotDirectionInRad;%Beam in global coordinates
    %Convert far end of beam to [x,y] coordinates
    BeamRange = MaxRange*ones(1,NumberOfBeams);
    [BeamGlobalX,BeamGlobalY] = pol2cart(BeamsAngleGlobal,BeamRange);
    BeamGlobalX=BeamGlobalX+GlobalLocation(1);
    BeamGlobalY=BeamGlobalY+GlobalLocation(2);
    [BeamRobotX,BeamRobotY] = pol2cart(BeamsAngleRobot,BeamRange);
    BeamRobotX=BeamRobotX+RobotLocation(1);
    BeamRobotY=BeamRobotY+RobotLocation(2);
    
    
    
    %%
    %Find hits
    %Translte beam according to range to robot beleif
    
    [LaserHits, LaserRange]=Find_LaserHits_InGlobalCoord(MapXs,MapYs, GlobalLocation, BeamGlobalX, BeamGlobalY,MaxRange);
    %add noise to laser range only for actual measurement and nothing to no
    %hits
    RobotLaserRange=LaserRange;
    RobotLaserRange(LaserHits)=RobotLaserRange(LaserHits)+sqrt(LaserRangVar)*ones(size(LaserRange(LaserHits)));
    RobotLaserRange(RobotLaserRange>MaxRange)=MaxRange;
    
    [HitsGlobalX,HitsGlobalY] = pol2cart(BeamsAngleGlobal(LaserHits),LaserRange(LaserHits));
    HitsGlobalX=HitsGlobalX+GlobalLocation(1);
    HitsGlobalY=HitsGlobalY+GlobalLocation(2);
    [HitsRobotX,HitsRobotY] = pol2cart(BeamsAngleRobot(LaserHits),RobotLaserRange(LaserHits));
    HitsRobotX=HitsRobotX+RobotLocation(1);
    HitsRobotY=HitsRobotY+RobotLocation(2);
    
    
%     %%
%     %%plot iteration
%     figure;
%     title(['Time = ',Time])
%     
%     subplot(1,2,1) %Ground truth
%     hold on
%     plot(MapXs, MapYs, 'k'); %Walls
%     plot(GlobalLocation(1),GlobalLocation(2),'b*'); % robot location
%     plot([GlobalLocation(1)*ones(1, length(BeamsAngleGlobal));BeamGlobalX],[GlobalLocation(2)*ones(1, length(BeamsAngleGlobal));BeamGlobalY],'r'); %plot laser beams
%     plot(HitsGlobalX,HitsGlobalY,'mx'); % laser hits in global coordinates
%     axis equal
%     title('Ground Truth - Global Coordinates')
%     
%     
%     
%     subplot(1,2,2)
%     hold on
%     plot(MapXsR, MapYsR, 'k');
%     plot(RobotLocation(1),RobotLocation(2),'b*'); % robot location
%     plot([RobotLocation(1)*ones(1, length(BeamsAngleGlobal));BeamRobotX],[RobotLocation(2)*ones(1, length(BeamsAngleGlobal));BeamRobotY],'r'); %beam
%     plot(HitsRobotX,HitsRobotY,'mx'); % laser hits in robot coordinates
%     axis equal
%     title('Robot Coordinates - include noise')
    
    
    
    %%
    %%Saving input data for playback later - as simulation progresses
    GlobalLocationVec=[GlobalLocationVec,GlobalLocation'];
    RobotLocationVec=[RobotLocationVec,RobotLocation];
    RobotDirectionInRadVec=[RobotDirectionInRadVec,RobotDirectionInRad'];
    GlobalDirectionVec=[GlobalDirectionVec,GlobalDirection'];
    BeamsAngleGlobalMat=[BeamsAngleGlobalMat,BeamsAngleGlobal'];
    BeamsAngleRobotMat=[BeamsAngleRobotMat,BeamsAngleRobot'];
    LaserHitsMat=[LaserHitsMat,LaserHits'];
    LaserRangeMat=[LaserRangeMat,LaserRange'];
    RobotLaserRangeMat=[RobotLaserRangeMat,RobotLaserRange'];
    
    GlobalLocationVar.GlobalLocationVec=GlobalLocationVec;
    GlobalLocationVar.GlobalDirectionVec=GlobalDirectionVec;
    RobotLocationVar.RobotLocationVec=RobotLocationVec;
    RobotLocationVar.RobotDirectionInRadVec=RobotDirectionInRadVec;
    LRF_in_GlobalCoord.BeamsAngleGlobalMat=BeamsAngleGlobalMat;
    LRF_in_GlobalCoord.LaserRangeMat=LaserRangeMat;
    LRF_in_RobotCoord.LaserHitsMat=LaserHitsMat;
    LRF_in_RobotCoord.BeamsAngleRobotMat=BeamsAngleRobotMat;
    LRF_in_RobotCoord.RobotLaserRangeMat=RobotLaserRangeMat;
    
    
    save(Pose_and_LRF_SaveFileName,'GlobalLocationVar','RobotLocationVar','LRF_in_GlobalCoord','LRF_in_RobotCoord')
    
    
   
    
    
    %%
    %Update GP
    %In general, GP is updated only if neccesary in order to prevent
    %covariance matrix to be come to big to handle.
    %Need to think of possible strategy to handle large data sets. Most
    %likely to manage memory by long term and short term memory
    
    %gp is a struct that contains the gp kernel, parameters - each gp might
    %had different type of inoput parameters though. GP should always
    %include separate handling for beam and laser hits
    lengthscale=1;
    Sigma_f=1;
    gp.Point2PointFunc=@SE_Cov_P2P;
    gp.Point2PointParamVec=[lengthscale,Sigma_f];
    gp.Point2LineFunc=@SE_Cov_P2L;
    gp.Point2LineParamVec=[lengthscale,Sigma_f];
    gp.Line2LineFunc=@SE_Cov_L2L;
    gp.Line2LineParamVec=[lengthscale,Sigma_f];
    gp.Sigma_n=0.05;
   
    %[L,y_Targets,LRF_in_RobotCoord]=UpdateGP(gp, L, RobotLocationVar, LRF_in_RobotCoord, RobotFixedParameters);
    [L,K,y_Targets,LRF_in_RobotCoord]=UpdateGP(gp, L, K, RobotLocationVar, LRF_in_RobotCoord, RobotFixedParameters);
    
    
    
    %%
    %Update occupancy map
    %Occupancy map should be updated according to intrest

    %Defining the world seen + extra 20%
    [LRF_RobotX,LRF_RobotY] = pol2cart(BeamsAngleRobot,RobotLaserRange);%same as laser hits, but all ranges (including MaxRange)
    LRFRobotX=LRF_RobotX+RobotLocation(1);
    LRFRobotY=LRF_RobotY+RobotLocation(2);
    
    Extra_Map_Stretch=0.10;%in %
        
    GridRes=0.2;%!!!eventualy grid should not be uniform, but finer next to point of instrest and less in other place
    
    if ~exist('GP_MapX_Min','var')
        %firts map building - based on measurements in robot coordinates
        GP_MapX_Min=min([LRFRobotX,RobotLocation(1)]);
        GP_MapX_Max=max([LRFRobotX,RobotLocation(1)]);
        GP_MapY_Min=min([LRFRobotY,RobotLocation(2)]);
        GP_MapY_Max=max([LRFRobotY,RobotLocation(2)]);
            %Add some buffer
        GP_MapX_Min=GP_MapX_Min-Extra_Map_Stretch*abs(GP_MapX_Min);
        GP_MapX_Max=GP_MapX_Max+Extra_Map_Stretch*abs(GP_MapX_Max);
        GP_MapY_Min=GP_MapY_Min-Extra_Map_Stretch*abs(GP_MapY_Min);
        GP_MapY_Max=GP_MapY_Max+Extra_Map_Stretch*abs(GP_MapY_Max);
    
             
    else
        TestNewBorder_X_Min=min([LRFRobotX RobotLocation(1)]);
        TestNewBorder_X_Min=TestNewBorder_X_Min-Extra_Map_Stretch*abs(TestNewBorder_X_Min);
        TestNewBorder_X_Max=max([LRFRobotX RobotLocation(1)]);
        TestNewBorder_X_Max=TestNewBorder_X_Max+Extra_Map_Stretch*abs(TestNewBorder_X_Max);
        TestNewBorder_Y_Min=min([LRFRobotY RobotLocation(2)]);
        TestNewBorder_Y_Min=TestNewBorder_Y_Min-Extra_Map_Stretch*abs(TestNewBorder_Y_Min);
        TestNewBorder_Y_Max=max([LRFRobotY RobotLocation(2)]);
        TestNewBorder_Y_Max=TestNewBorder_Y_Max+Extra_Map_Stretch*abs(TestNewBorder_Y_Max);
        
        
        GP_MapX_Min=min([GP_MapX_Min TestNewBorder_X_Min]);
        GP_MapX_Max=max([GP_MapX_Max TestNewBorder_X_Max]);
        GP_MapY_Min=min([GP_MapY_Min TestNewBorder_Y_Min]);
        GP_MapY_Max=max([GP_MapY_Max TestNewBorder_Y_Max]);
    end
   

    
    Xinput=GP_MapX_Min:GridRes:GP_MapX_Max;
    Yinput=GP_MapY_Min:GridRes:GP_MapY_Max;
%       Xinput=-10:0.5:10;
%    Yinput=-10:0.5:10;
   [GridX,GridY]=meshgrid(Xinput,Yinput);
    
    %calculate k_xquery_k_xquery
    X_query=[GridX(:)';GridY(:)'];
    
 %   [PredMean,PredVar,LogMarg]=CalcPredication(gp, L, X_query,y_Targets,LRF_in_RobotCoord);
    [PredMean,PredVar,LogMarg]=CalcPredication(gp, L, K, X_query,y_Targets,LRF_in_RobotCoord);
    PredMean= reshape(PredMean,size(GridX,1),size(GridY,2));
    
    figure;
    plot(HitsRobotX, HitsRobotY, 'b*');
    hold on
    imagesc(Xinput,Yinput,PredMean,[-1.5 1.5])
    plot(HitsRobotX, HitsRobotY, 'b*');
    title('mu map')
    % figure;imagesc(Xinput,Yinput,sigmaMap)
    % title('sigma map - only with laser hits')
    hold on
     plot([RobotLocation(1)*ones(1, length(BeamsAngleGlobal));BeamRobotX],[RobotLocation(2)*ones(1, length(BeamsAngleGlobal));BeamRobotY],'r'); %beam
   plot(MapXsR, MapYsR, 'k');
    disp('kuku')
    %%
    %Resolve next step
    %Expected imporevement ??
    %%
    %learn?? - adptive learning of noise and 
    
    %%
    %Localize?
    
    %%
    %Stop criteria?
    
    
    
    
    
    
end

