clc
clearvars
close all

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%***************Experiminetal**********************************************
%Exprimental Parameters

LaserRangeNoise=0;
LaserAngularNoise=0;
VehiclePoseNoise=0;

%Use default GP parameter
disp('Using default GP parameters - should convert to trained parameters')



%%
%%%%%%Initialize world%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Following Simon's convention of keeping the coordinates of the corners
currentFolder = pwd;
if exist([currentFolder,'\SupportingFiles\Simple_2D_World.mat'],'file')
    %load existing file
    SimWorld=load([currentFolder,'\SupportingFiles\Simple_2D_World.mat']);
else
    S = uiimport;
    SimWorld=S.WorldMap;
    clear S;
end

MapXs = [WorldMap.WallStartPointsX;WorldMap.WallEndPointsX];
MapYs = [WorldMap.WallStartPointsY;WorldMap.WallEndPointsY];
GroundTruth_Fig_Handle=figure;
plot(MapXs, MapYs, 'b');
axis equal
title('Ground Truth');


%%
%