function SimWorld=InitializeWorld(filename)

%function loads a map according to input.
%Otherwise ot either loads a default map or prompts the user for input


%World struct follows Simon's convention of keeping the coordinates of the corners
if nargin>0
    %Try to load user's request
    try
      SimWorld=load(filename);
      return;
    catch
       warning(ME.message) 
       promptMessage = sprintf('Cannot load request file. Try to load default');
       button = questdlg(promptMessage, 'Continue', 'Continue', 'Cancel', 'Continue');
        if strcmpi(button, 'Cancel')
             rethrow(ERR); % Or break or continue
        end
        %else continue to load default
    end
end

%load defaults or ask user to define request
currentFolder = pwd;
if exist([currentFolder,'\SupportingFiles\Simple_2D_World.mat'],'file')
    %load existing file
    SimWorld=load([currentFolder,'\SupportingFiles\Simple_2D_World.mat']);
else
    S = uiimport;
    SimWorld=S.WorldMap;
    clear S;
end

MapXs = [WorldMap.WallStartPointsX;WorldMap.WallEndPointsX];
MapYs = [WorldMap.WallStartPointsY;WorldMap.WallEndPointsY];
GroundTruth_Fig_Handle=figure;
plot(MapXs, MapYs, 'b');
axis equal
title('Ground Truth');
