function [LRF_in_RobotCoord, RobotFixedParameters,  RobotLocationVar]= Decide_on_DataArrangement(LRF_in_RobotCoord, RobotFixedParameters)

%At the moment - all data is used
%LRF_in_RobotCoord contain all data required to decide and all preivously
%devided data



%%
%Basically, the K matrix elements could be either line or point and they can be mix. 
%There are no division into subset of Line only or Point2Point only
%HitsMatIndex  - size of the 
if (isfield(LRF_in_RobotCoord, 'HitsMatIndex')) && (~isempty(LRF_in_RobotCoord.HitsMatIndex))
    HitsMatIndex=LRF_in_RobotCoord.HitsMatIndex;
else
    HitsMatIndex=[];
end
if (isfield(LRF_in_RobotCoord, 'FreeBeamMatIndex')) && (~isempty(LRF_in_RobotCoord.FreeBeamMatIndex))
    FreeBeamMatIndex=LRF_in_RobotCoord.FreeBeamMatIndex;
else
    FreeBeamMatIndex=[];
end

