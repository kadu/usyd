function [LaserHits, LaserRange]=Find_LaserHits_InGlobalCoord(MapXs,MapYs, GlobalLocation, BeamGlobalX, BeamGlobalY,MaxRange);

%
%LaserHits - is a bolean stating whether the laser hit an obstcle
%LaserRange - value of Range. MaxRange if no hit to simulate real laser
%range results

%MapsXs(1,:) is the wall starting Xs
%MapsXs(2,:) is the wall ending Xs
%MapsYs(1,:) is the wall starting Ys
%MapsYs(2,:) is the wall ending Ys

%BeamGlobalX and BeamGlobalY is the laser beam X,Y assuming maximum laser
%range
%GlobalLocation is the robot REAL location (does not match the robot assumed location)

%To avoid ininvertible matrix, code will not use the slope of the wall !


delX_beam=(BeamGlobalX-GlobalLocation(1));
delY_beam=(BeamGlobalY-GlobalLocation(2));

delX_wall=(MapXs(2,:)-MapXs(1,:));
delY_wall=(MapYs(2,:)-MapYs(1,:));


LaserHits=logical(zeros(size(delX_beam)));
LaserRange=MaxRange*ones(size(delX_beam));

for i=1:length(delX_beam)
    
    
    for j=1:length(delX_wall)
    
        
        
        %Should be simple inversion of a matrix.
        %However in some cases the matrix is ill conditioned
        
        if (abs(delY_beam(i))<eps(10) & abs(delY_wall(j))<eps(10)) %parallel along X
            xx=1;
        elseif (abs(delX_beam(i))<eps(10) & abs(delX_wall(j))<eps(10)) %parallel along Y
            xx=1;
        else
        
        %if ~((abs(delX_beam(i)-delX_wall(j))<eps)&&(abs(delY_beam(i)-delY_wall(j))<eps)) %beam and wall are not paralleled
            %beam not parallel to wall
            A=[[-delY_beam(i), delX_beam(i)];[-delY_wall(j),delX_wall(j)]];
          
            [InterSec]=A\[delX_beam(i)*GlobalLocation(2)-delY_beam(i)*GlobalLocation(1);delX_wall(j)*MapYs(1,j)-delY_wall(j)*MapXs(1,j)]; 
            InterSecX=InterSec(1);
            InterSecY=InterSec(2);
            
            %Check Intersection is in physical beam limits - by check thge
            %sign of the slope of the original beam with the sign of the
            %slope of the intersection and hits are in the defined wall
            %limits
            Hit_Confined_in_Limits = logical((InterSecX<=max(MapXs(:,j)))*(InterSecX>=min(MapXs(:,j)))...
                *(InterSecY<=max(MapYs(:,j)))*(InterSecY>=min(MapYs(:,j))));
            Hit_InRightDireection=logical((InterSecX<=max(GlobalLocation(1),BeamGlobalX(i)))*(InterSecX>=min(GlobalLocation(1),BeamGlobalX(i)))...
                *(InterSecY<=max(GlobalLocation(2),BeamGlobalY(i)))*(InterSecY>=min(GlobalLocation(2),BeamGlobalY(i))));

            
            %Hit_InRightDireection= logical(sign(InterSecY-GlobalLocation(2))== sign(BeamGlobalY(i)-GlobalLocation(2))& sign(InterSecX-GlobalLocation(1))== sign(BeamGlobalX(i)-GlobalLocation(1)));
            HitOK=Hit_Confined_in_Limits && Hit_InRightDireection;
            
            
            if HitOK 
                %the beam might intersect several walls
                %Take only minimum
            
                
                BeamRangeToWall=sqrt((InterSecY-GlobalLocation(2)).^2+(InterSecX-GlobalLocation(1)).^2);
                
                LaserHits(i)=logical((LaserHits(i)|| BeamRangeToWall<MaxRange));
                
            
                %Need to update closesest wall hit
                LaserRange(i)= min(LaserRange(i),BeamRangeToWall);
            end
        end
        
    end
end



