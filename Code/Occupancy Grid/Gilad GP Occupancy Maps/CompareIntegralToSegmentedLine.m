clc
clearvars

NewPose=[0;0];
NewBeam.Start=[0;0];
NewBeam.End=[4;0];

TestPoint=[2;1];

lengthscale=1;
Sigma_f=1;
gp.Point2PointFunc=@SE_Cov_P2P;
gp.Point2PointParamVec=[lengthscale,Sigma_f];
gp.Point2LineFunc=@SE_Cov_P2L;
gp.Point2LineParamVec=[lengthscale,Sigma_f];
gp.Line2LineFunc=@SE_Cov_L2L;
gp.Line2LineParamVec=[lengthscale,Sigma_f];
gp.Sigma_n=0.05;

NumOfIntervals=[1:1:15];

for i=1:length(NumOfIntervals)
    dx=
    disp(num2str(i))
    Trig=CalcLineParameters2D(NewBeam.Start, NewBeam.End);
    L_interval=linspace(0,Trig.LineLength,NumOfIntervals(i));
    
    NoHits=[L_interval*Trig.CosTetha+NewPose(1);L_interval*Trig.SinTetha+NewPose(2)];
    
    
    K=feval(gp.Point2PointFunc, NoHits, NoHits, gp.Point2PointParamVec);
    
    L=chol(K);
    

    
    y_Targets=-ones(size(NoHits,2),1);

    
    K_Xstar_X_PP=feval(gp.Point2PointFunc, TestPoint, NoHits, gp.Point2PointParamVec);


    Alpha=L'\(L\y_Targets);

    PredMean(i)=K_Xstar_X_PP*Alpha;

    v=L\K_Xstar_X_PP';
    K_Xstar_XStar=feval(gp.Point2PointFunc, TestPoint, TestPoint, gp.Point2PointParamVec);
    PredVar(i)=K_Xstar_XStar-v'*v;
    
end


K_L_NewL=feval(gp.Line2LineFunc, NewBeam, NewBeam, gp.Line2LineParamVec);

L=chol(K_L_NewL);


K_Xstar_X_PL=feval(gp.Point2LineFunc, TestPoint, NewBeam, gp.Point2LineParamVec);

y_Targets=-1;

Alpha=L'\(L\y_Targets);


PredMeanLine=K_Xstar_X_PL*Alpha;

v=L\K_Xstar_X_PL';
K_Xstar_XStar=feval(gp.Point2PointFunc, TestPoint, TestPoint, gp.Point2PointParamVec);
PredVarLine=K_Xstar_XStar-v'*v;



figure;
semilogy(NumOfIntervals, PredMean, 'b-+');
hold on
semilogy(NumOfIntervals, PredMeanLine(1,ones(size(NumOfIntervals))), 'r')
title('mu')
legend('segmeneted','Integral')

% 
% figure;
% plot(NumOfIntervals, PredVar, 'b-+');
% hold on
% plot(NumOfIntervals, PredVarLine(1,ones(size(NumOfIntervals))), 'r')
% title('var')
% legend('segmeneted','Integral')


