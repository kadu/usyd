clc
clearvars
 lengthscale=[0.5,0.5];
%lengthscale=[1,1];
NewPose=[0;0];
NewBeam.Start=NewPose;
NewBeam.End=[4;0];
TrigNew=CalcLineParameters2D(NewBeam.Start, NewBeam.End);

OldPose=[2;2];
OldBeam.Start=OldPose;
OldBeam.End=[3;4];
TrigOld=CalcLineParameters2D(OldBeam.Start, OldBeam.End);


TestPoint=[2;1];




Sigma_f=1;
gp.Point2PointFunc=@SE_Cov_P2P;
gp.Point2PointParamVec=[lengthscale,Sigma_f];
gp.Point2LineFunc=@SE_Cov_P2L;
gp.Point2LineParamVec=[lengthscale,Sigma_f];
gp.Line2LineFunc=@SE_Cov_L2L;
gp.Line2LineParamVec=[lengthscale,Sigma_f];
gp.Sigma_n=0.05;

NumOfIntervals=1:1:100;
w=lengthscale'.^-2;
for i=1:length(NumOfIntervals)
    
    %%
    dx1=TrigNew.LineLength/NumOfIntervals(i);
    disp(num2str(i))
    
    L1_interval=linspace(0,TrigNew.LineLength,NumOfIntervals(i));
    NoHits1=[L1_interval*TrigNew.CosTetha+NewPose(1);L1_interval*TrigNew.SinTetha+NewPose(2)];
    L1=NoHits1;
    R2=TestPoint;
    L1_N=size(L1,2);
    R2_N=size(R2,2);
    XX1 = sum(w(:,ones(1,L1_N)).*L1.*L1,1); %sum on X,Y,Z with the appropriate length scale
    XX2 = sum(w(:,ones(1,R2_N)).*R2.*R2,1);
    X1X2 = (w(:,ones(1,L1_N)).*L1)'*R2;
%     XX1 = sum(L1.*L1,1); %sum on X,Y,Z with the appropriate length scale
%     XX2 = sum(R2.*R2,1);
%     X1X2 = (L1)'*R2;
    XX1T = XX1';
    z = XX1T(:,ones(1,R2_N)) + XX2(ones(1,L1_N),:) - 2*X1X2;
            
    Exp_Point2Line1=exp(log((Sigma_f)^2)-0.5*z);
    
    K_P2L_1(i)=sum(dx1*Exp_Point2Line1);

    %%
    dx2=TrigOld.LineLength/NumOfIntervals(i);
    disp(num2str(i))
    
    L2_interval=linspace(0,TrigOld.LineLength,NumOfIntervals(i));
    NoHits2=[L2_interval*TrigOld.CosTetha+OldPose(1);L2_interval*TrigOld.SinTetha+OldPose(2)];
    L2=NoHits2;
    R2=TestPoint;
    L2_N=size(L2,2);
    R2_N=size(R2,2);
    XX1 = sum(w(:,ones(1,L2_N)).*L2.*L2,1); %sum on X,Y,Z with the appropriate length scale
    XX2 = sum(w(:,ones(1,R2_N)).*R2.*R2,1);
    X1X2 = (w(:,ones(1,L2_N)).*L2)'*R2;
%     XX1 = sum(L2.*L2,1); %sum on X,Y,Z with the appropriate length scale
%     XX2 = sum(R2.*R2,1);
%     X1X2 = (L2)'*R2;

    XX1T = XX1';
    z = XX1T(:,ones(1,R2_N)) + XX2(ones(1,L2_N),:) - 2*X1X2;
            
    Exp_Point2Line2=exp(log((Sigma_f)^2)-0.5*z);
    
    K_P2L_2(i)=sum(dx2*Exp_Point2Line2);
    
    %% integral on L2L
    for j=1:size(L2,2)
       
        R2=L2(:,j);
        
        R2_N=size(R2,2);
        
        
        XX1 = sum(w(:,ones(1,L1_N)).*L1.*L1,1); %sum on X,Y,Z with the appropriate length scale
        XX2 = sum(w(:,ones(1,R2_N)).*R2.*R2,1);
        X1X2 = (w(:,ones(1,L1_N)).*L1)'*R2;
%         XX1 = sum(L1.*L1,1); %sum on X,Y,Z with the appropriate length scale
%         XX2 = sum(R2.*R2,1);
%         X1X2 = (L1)'*R2;
        XX1T = XX1';
        z = XX1T(:,ones(1,R2_N)) + XX2(ones(1,L1_N),:) - 2*X1X2;
            
        Exp_Line2Line_j=exp(log((Sigma_f)^2)-0.5*z);
    
        K_L2L_j(j)=sum(dx1*Exp_Line2Line_j);
        
        
    end
    K_L2L(i)=sum(dx2*K_L2L_j);
    
end


K_L_NewL=feval(gp.Line2LineFunc, OldBeam, NewBeam, gp.Line2LineParamVec);


K_Xstar_X_PL_1=feval(gp.Point2LineFunc, TestPoint, NewBeam, gp.Point2LineParamVec);
K_Xstar_X_PL_2=feval(gp.Point2LineFunc, TestPoint, OldBeam, gp.Point2LineParamVec);



figure;
semilogy(NumOfIntervals, K_P2L_1, 'b-+');
hold on
semilogy(NumOfIntervals, K_Xstar_X_PL_1(1,ones(size(NumOfIntervals))), 'r')
title('K_P2L_1')
legend('segmeneted','Integral')

figure;
semilogy(NumOfIntervals, K_P2L_2, 'b-+');
hold on
semilogy(NumOfIntervals, K_Xstar_X_PL_2(1,ones(size(NumOfIntervals))), 'r')
title('K_P2L_2')
legend('segmeneted','Integral')

figure;
semilogy(NumOfIntervals, K_L2L, 'b-+');
hold on
semilogy(NumOfIntervals, K_L_NewL(1,ones(size(NumOfIntervals))), 'r')
title('K_L2L')
legend('segmeneted','Integral')

