%RUN AFTER RUNNING DemoSeqGPInt2DQuad

numLines = size(X.Start,2)
LineX=[]
LineY=[]
for i=1:numLines
    LineX(i,:) = linspace(X.Start(1,i),X.End(1,i),10);
    LineY(i,:) = linspace(X.Start(2,i),X.End(2,i),10);
   
end

LineX(:,end-2:end)=[];
LineY(:,end-2:end)=[];

LineXVec = LineX(:)'
LineYVec = LineY(:)'

LinePoints = [LineXVec;LineYVec]

hold on; plot(LineXVec,LineYVec,'+')

OccPoints = X.Point

XPoints = [LineXVec OccPoints(1,:)]
YPoints = [LineYVec OccPoints(2,:)]
ZPoints = [zeros(1,length(LineYVec)) ones(1,length(OccPoints))]

% ZI = griddata(XPoints,YPoints,ZPoints,Mesh.x,Mesh.y,'cubic');
% ZI = griddata(XPoints,YPoints,ZPoints,Mesh.x,Mesh.y,'nearest');
ZI = griddata(XPoints,YPoints,ZPoints,Mesh.x,Mesh.y);
% ZI = griddata(XPoints,YPoints,ZPoints,Mesh.x,Mesh.y,'v4');



 figure(5);

    surf(Mesh.x,Mesh.y,ZI);
    title('Prob Occ Vs Location');
    view(0,90);
    axis([min(Test.x),max(Test.x),min(Test.y),max(Test.y)]) 
    colorbar