function X = buildActiveSet(X,y,gp,ScanNum,ActThresh,squashparams,ActSwitch,NodeWeightArray,ShowRedundantSet)

% disp('*Function: buildActiveSet')
% pause;

%Divide observations into old observations and new observations
NewScan.Start = X.Start(:,X.PoseNum==ScanNum);
NewScan.End = X.End(:,X.PoseNum==ScanNum);
NewScan.Point = X.Point(:,X.PoseNum==ScanNum);
NewScan.yPoint = [y.Point(X.PoseNum==ScanNum)];
NewScan.yIntSeg = [y.IntSeg(X.PoseNum==ScanNum)];

%     disp('*NewScan created');
%     pause;

if ActSwitch == 1 %Switch for active selection
%     disp('*ActSwitch == 1');
%     pause;
    for Obs = 1:size(NewScan.Start,2)
        size(NewScan.Start,2)-Obs
        if Obs==1 && ScanNum == 1 %Initial observation
            
            InitialObs.Start = NewScan.Start(:,Obs);
            InitialObs.End = NewScan.End(:,Obs);
            InitialObs.Point = NewScan.Point(:,Obs);
            InitialObs.yPoint = NewScan.yPoint(Obs);
            InitialObs.yIntSeg = NewScan.yIntSeg(Obs);
            
            [X.ActiveSet]= initialiseActiveSet(InitialObs, gp, gp.order,NodeWeightArray);
            X.RedundantSet = X.ActiveSet; %Bit of a hack to just initialise
%             disp('*InitialObs ActiveSet initialised');
%             pause;
            continue
        end
        
        
        %First check a point
        CandidatePoint.Start = [];
        CandidatePoint.End = [];
        CandidatePoint.Point = NewScan.Point(:,Obs);
        CandidatePoint.yPoint = NewScan.yPoint(Obs) ;
        CandidatePoint.yIntSeg = [];
        
        
        [X.ActiveSet X.RedundantSet] = evalActiveSetFromPoint(...
            X.ActiveSet, X.RedundantSet,...
            CandidatePoint, gp, ActThresh.Point,...
            gp.order,squashparams,NodeWeightArray,ShowRedundantSet);
        
        %Then check a line
        CandidateLine.Start = NewScan.Start(:,Obs);
        CandidateLine.End = NewScan.End(:,Obs);
        CandidateLine.Point = [] ;
        CandidateLine.yPoint = [];
        CandidateLine.yIntSeg = NewScan.yIntSeg(Obs);
        
        [X.ActiveSet X.RedundantSet] = evalActiveSetFromLine(X.ActiveSet, X.RedundantSet,...
            CandidateLine, gp, ActThresh.Line, gp.order,ActThresh.NumSamples,squashparams,...
            NodeWeightArray,ShowRedundantSet);
        
        %         X.ActiveSet =X ActiveSet;
        %         X.RedundantSet = RedundantSet;
%         disp(['*Obs ' num2str(Obs) ' initialised']);
%         pause;
        
    end
else
%     disp('*ActSwitch != 1');
%     pause;
    
    if ScanNum ==1
        X.ActiveSet.X= [];
        X.ActiveSet.y = [];
        X.ActiveSet.ObsId = [];
        X.ActiveSet.L = [];
        X.ActiveSet.alpha = [];
    end
    X.ActiveSet = mergeScan2Set(X.ActiveSet,NewScan,gp,gp.order,NodeWeightArray)
%     disp('*ActiveSet created');
%     pause;
    
%     disp('*Function buildActiveSet: ok');
%     pause;
end