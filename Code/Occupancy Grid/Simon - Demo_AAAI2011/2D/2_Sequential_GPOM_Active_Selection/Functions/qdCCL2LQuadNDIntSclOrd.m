

function K = qdCCL2LQuadNDIntSclOrd(funs,X1S,X1E,X2S,X2E,par,order,NodeWeightArray)
% disp('-Function: qdCCL2LQuadNDIntSclOrd')
% pause;

% X1S = [2 3;4 3]
% X1E = [5 1;6 1]
% X2S = [4; 4]
% X2E = [2.5; 3]
% order=10
% params=[1 1 1]
% fun=@cov_sqexp

% if nargin<8
%     load NodeWeightArray
% end



maxOrderWarning = 0;

Num_Lines1 = size(X1S,2);
Num_Lines2 = size(X2S,2);
D = size(X1S,1);


%% Determine Nodes and Weights for X1
%Scale dimensions to suit lengthscales
Lengthscales = par(1:D)';
X1SScaled = X1S./Lengthscales(:,ones(1,Num_Lines1));
X1EScaled = X1E./Lengthscales(:,ones(1,Num_Lines1));


%Determines the length of lines
LineLengths1 = sqrt(sum((X1S-X1E).^2,1))';
LineLengths1Scaled = sqrt(sum((X1SScaled-X1EScaled).^2,1))';
ScaledOrder = round(order*(LineLengths1Scaled).^(1/2)); %Used to be .^(1/2)
% ScaledOrder = round(order*(LineLengths2Scaled));

ScaledOrder(ScaledOrder>100)=100;
ScaledOrder(ScaledOrder<3)=3;
%Give a warning if order is becoming saturated.
if max(ScaledOrder) > 70
    MaxOrderWarning = 1;
end
% % % % % disp('Mean Scaled Order:')
% % % % % mean(ScaledOrder)
% % % % % disp('Max Scaled Order:')
% % % % % max(ScaledOrder)
% % % % % disp('Min Scaled Order:')
% % % % % min(ScaledOrder)


% figure(99)
% hist(ScaledOrder)
% pause(0.1)


% figure
% plot([X1S(1,:);X1E(1,:)],[X1S(2,:);X1E(2,:)])
% hold on
% plot(X2(1,:),X2(2,:),'+')


 Nodes1 = NodeWeightArray(1,ScaledOrder);
 Weights1 = NodeWeightArray(2,ScaledOrder);
 
 
 
% Num_Points2 = size(X2S,2);
D = size(X1S,1);

X1SAug = X1S;       %Augment X1 to 3 dimensions
X1EAug = X1E;

if D == 2
    X1SAug = [X1S;zeros(1,Num_Lines1)];
    X1EAug = [X1E;zeros(1,Num_Lines1)];
    
elseif D == 1
    X1SAug = [X1S;zeros(2,Num_Lines1)];
    X1EAug = [X1E;zeros(2,Num_Lines1)];
end

% order
[costhet sinthet cosw sinw xstart ystart zstart r]= determLineParameters3D(X1SAug, X1EAug);
  
[NodesScaleShift1 Weights1] = scaleShiftNodesWeightsOrdTest(r, Nodes1, Weights1);

X1 = cellfun(@lineparams2cart,NodesScaleShift1,num2cell(cosw'),...
    num2cell(sinw'),num2cell(costhet'), num2cell(sinthet'), ...
    num2cell(xstart'),num2cell(ystart'),num2cell(zstart'),'Un',0);

if D == 2
    X1=cellfun(@removeLastDim,X1,'Un',0);
elseif D == 1
    X1=cellfun(@removeLastDim,X1,'Un',0);
    X1=cellfun(@removeLastDim,X1,'Un',0);
end






%% Determine Nodes and Weights for X2
%Scale dimensions to suit lengthscales
Lengthscales = par(1:D)';
X2SScaled = X2S./Lengthscales(:,ones(1,Num_Lines2));
X2EScaled = X2E./Lengthscales(:,ones(1,Num_Lines2));


%Determines the length of lines
LineLengths2 = sqrt(sum((X2S-X2E).^2,1))';
LineLengths2Scaled = sqrt(sum((X2SScaled-X2EScaled).^2,1))';
ScaledOrder = round(order*(LineLengths2Scaled).^(1/2));
% ScaledOrder = round(order*(LineLengths2Scaled));

ScaledOrder(ScaledOrder>100)=100;
ScaledOrder(ScaledOrder<3)=3;
%Give a warning if order is becoming saturated.
if max(ScaledOrder) > 70
    MaxOrderWarning = 1;
end
% disp('Mean Scaled Order:')
% mean(ScaledOrder)
% % disp('Max Scaled Order:')
% % max(ScaledOrder)
% % disp('Min Scaled Order:')
% % min(ScaledOrder)

% % % % figure(99)
% % % % hist(ScaledOrder)
% % % % pause(0.1)

% figure
% plot([X1S(1,:);X1E(1,:)],[X1S(2,:);X1E(2,:)])
% hold on
% plot(X2(1,:),X2(2,:),'+')

 Nodes2 = NodeWeightArray(1,ScaledOrder);
 Weights2 = NodeWeightArray(2,ScaledOrder);

% Num_Points2 = size(X2S,2);
D = size(X1S,1);

X2SAug = X2S;
X2EAug = X2E;

if D == 2
    X2SAug = [X2S;zeros(1,Num_Lines2)];
    X2EAug = [X2E;zeros(1,Num_Lines2)];
elseif D == 1
    X2SAug = [X2S;zeros(2,Num_Lines2)];
    X2EAug = [X2E;zeros(2,Num_Lines2)];
end

% order
[costhet2 sinthet2 cosw2 sinw2 xstart2 ystart2 zstart2 s]= determLineParameters3D(X2SAug, X2EAug);
  
[NodesScaleShift2 Weights2] = scaleShiftNodesWeightsOrdTest(s, Nodes2, Weights2);

X2 = cellfun(@lineparams2cart,NodesScaleShift2,num2cell(cosw2'),...
    num2cell(sinw2'),num2cell(costhet2'), num2cell(sinthet2'), ...
    num2cell(xstart2'),num2cell(ystart2'),num2cell(zstart2'),'Un',0);

if D == 2
    X2=cellfun(@removeLastDim,X2,'Un',0);
elseif D == 1
    X2=cellfun(@removeLastDim,X2,'Un',0);
    X2=cellfun(@removeLastDim,X2,'Un',0);
end


X1rep = repmat(X1',1,Num_Lines2);
X2rep =  repmat(X2,Num_Lines1,1);

Weights1rep = repmat(Weights1',1,Num_Lines2);
Weights2rep = repmat(Weights2,Num_Lines1,1);


parcell =  mat2cell(par,1,numel(par));
parrep = repmat(parcell,Num_Lines1, Num_Lines2);



%% Cell Array Approach
knodescell = cellfun(funs,X1rep,X2rep,parrep,'Un',0);
WeightsCell = cellfun(@timesmatrix,Weights1rep,Weights2rep,'Un',0);
KnodesWeighted=cellfun(@dottimes,knodescell,WeightsCell,'Un',0);
KnodesWeightedSum = cellfun(@sum,cellfun(@sum,KnodesWeighted,'Un',0),'Un',0);
K = cell2mat(KnodesWeightedSum);


%% For Loop Approach
% K = zeros(size(parrep));
% for i=1:numel(X1)
%      numel(X1)-i
%     for j = 1:numel(X2)
%         knodes = feval(funs,X1{i},X2{j},parrep{i,j});
%         Weights= Weights1{i}'*Weights2{j};
%         KnodesWeighted=knodes.*Weights;
%         K(i,j) = sum(sum(KnodesWeighted));
%     end
% end



% disp('-Function: qdCCL2LQuadNDIntSclOrd: OK')
% pause;

end


