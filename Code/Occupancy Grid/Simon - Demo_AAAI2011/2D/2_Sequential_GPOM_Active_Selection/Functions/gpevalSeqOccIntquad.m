% Copyright The University of Sydney, 2001 - 2008.  ABN 15 211 513 464
% herafter 'The University'.
%
% The copyright of the computer program(s) herein is the property of
% The University of Sydney, Australia.  The program(s) may be used
% and/or copied only with the written permission of The University of
% Sydney, or in accordance with the terms and conditions stipulated
% in the contract under which the program(s) have been supplied.
% 
% Disclaimer:
% The Universitiy makes no representation or warranty
% that these materials, including any software, are free from errors;
% about the quality or performance of these materials;
% that these materials are fit for any particular purpose.
% These materials are made available on the strict basis that The University
% and its employees or agents have no liability for any direct or indirect
% loss or damage (including for negligence) suffered by any person as a
%consequence of the use of this  material.
%
% Evaluate a Gaussian Process (see page 19 of
% "Gaussian Processes for Machine Learning" by
% C. Rasmussen and C. Williams)
%
%INPUTS
% X - D x N inputs
% y - 1 x N outputs (targets)
% kfun - covariance function
% kpar - covariance parameters (hyperparameters)
% K - N x N computed covariance 
% noise - scalar noise level
% x - D x M test inputs
% invK - N x N inverted K if available
% 
%OUTPUTS
% mf - predictive mean
% vf - predictive variance
% lml - log marginal likelihood
%
% Fabio Tozeto Ramos 04/06/07
% [lml,mf,vf] = gpeval(X,y,kfun,kpar,K,noise,x,invK)



% Different Modes of operation:
% All Inputs present
% NewScan is Empty... just work with ActiveSet.L and alpah
% NewScan is full... merge with activeset.L and alpha
% NewScan is only point... merge with activeset.L and alpha
% NewScan is only line... merge with activeset.L and alpha
% ActiveSet is empty... just use new scan



function [mf,vf] = gpevalSeqOccIntquad(TrainingData,gp,TestPoints,order, NodeWeightArray)

% load NodeWeightArray

% Extract Line Segs and Points from the Training Data
 X.Start = TrainingData.X(:,TrainingData.ObsId==1,1);
 X.End = TrainingData.X(:,TrainingData.ObsId==1,2);
 X.Point = TrainingData.X(:,TrainingData.ObsId==0,1);

[d,N,temp] = size(TrainingData.X); %number of inputs
[nx]  = size(TestPoints,2); %number of testing points 
  

    p1 = 1;
    for j = 1:numel(gp.npar)
        p2 = p1 + gp.npar-1;
       
            ksLP = quadCCLine2PointIntSclOrd(gp.cov_funs{j},X.Start,X.End,...
             TestPoints,gp.par(p1:p2),order,NodeWeightArray);
      
%             ksPP =cov_sum(X.Point,TestPoints,gp.cov_funs(j),...
%                 gp.npar(j),gp.par(p1:p2));
            ksPP =feval(gp.cov_funs{j},X.Point,TestPoints,gp.par(p1:p2));
            
            ks=zeros(N,nx);
            ks(TrainingData.ObsId==1,:) = ksLP;
            ks(TrainingData.ObsId==0,:) = ksPP;
             
             if j==1
                 ksSum = ks;
             else
                ksSum = ks+ksSum;
             end
  
        p1 = p2 + 1;
    end

    mf = ksSum'*TrainingData.alpha;
    mf=real(mf);

    %Compute the variance
    opts.TRANSA = true;
    opts.UT     = true;
    v           = real(linsolve(TrainingData.L,ksSum,opts));
    xc          = mat2cell(TestPoints,d,ones(1,nx));
    
    nfuns = length(gp.cov_funs);
    p1 = 1;
    a = zeros(size(xc'));
    for i=1:nfuns 
        p2 = p1 + gp.npar(i)-1;
        %Eval each covariance function
        cellsolution =  cellfun(gp.cov_funs{i},xc,xc,repmat({gp.par(p1:p2)},1,nx))';
        a = a +cellsolution;
        p1 = p2 + 1;
    end
    
%     a           = cellfun(cov_fun, xc, xc, repmat({kpar},1,nx))';
    vf          = a - sum(v.*v)' + gp.noise^2;
    %vf = feval(kfun,x,x,kpar) - v'*v + noise^2;

%Compute the log marginal likelihood
% lml = -0.5*y*alpha-sum(log(diag(L)))-0.5*N*log(2*pi);
%nlml = -0.5*y*alpha - 0.5*log(det(A)) - 0.5*n*log(2*pi);