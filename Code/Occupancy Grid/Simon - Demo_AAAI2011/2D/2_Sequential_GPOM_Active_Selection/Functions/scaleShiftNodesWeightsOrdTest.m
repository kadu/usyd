
function [NodesScaleShift WeightsScaled] = ... 
    scaleShiftNodesWeightsOrdTest(r, Nodes, Weights)

    %Scale nodes
    scalefactor = (r'/2)';
   
   ScaledNodes = cellfun(@times,Nodes,num2cell(scalefactor'),'Un',0);
   
    NodesScaleShift =cellfun(@plus,ScaledNodes,num2cell(scalefactor'),'Un',0);
    WeightsScaled =cellfun(@times,Weights,num2cell(scalefactor'),'Un',0);

end

