function [ActiveSet RedundantSet] = evalActiveSetFromPoint(ActiveSet, RedundantSet,...
    Candidate, gp, Thresh, order,squashparams,NodeWeightArray,ShowRedundantSet)

    TestPoint = Candidate.Point;
    %Evaluate mf and vf before candidate is added
    [mfBefore,vfBefore] = gpevalSeqOccIntquad(ActiveSet, gp,TestPoint, order,NodeWeightArray);
    SquashedBefore= mfBefore;
    SquashedBefore((find(SquashedBefore >1))) = 1;
    SquashedBefore((find(SquashedBefore<-1))) = -1;
    ProbOccBefore= sigmoid(squashparams(1),squashparams(2),1,SquashedBefore,vfBefore);
    
    ActivePlusCandSet = mergeScan2Set(ActiveSet,Candidate,gp,order,NodeWeightArray);
    
    %Evaluate mf and vf after candidate is added
    [mfAfter,vfAfter] = gpevalSeqOccIntquad(ActivePlusCandSet,gp,TestPoint, order,NodeWeightArray);   
    SquashedAfter= mfAfter;
    SquashedAfter((find(SquashedAfter >1))) = 1;
    SquashedAfter((find(SquashedAfter<-1))) = -1;
    ProbOccAfter= sigmoid(squashparams(1),squashparams(2),1,SquashedAfter,vfAfter);
    
%     %Check KL Divergence
%     D = 0.5*(log(vfAfter/vfBefore)+...
%         (mfAfter^2+mfBefore^2-2*mfBefore*mfAfter+vfBefore)/vfAfter-1);
    
    if abs(ProbOccAfter-ProbOccBefore)>Thresh 
        %Add candidate to active set
        ActiveSet = ActivePlusCandSet;
    end
    if ShowRedundantSet ==1  
        RedundantSet=mergeScan2Set(RedundantSet,Candidate,gp,order,NodeWeightArray);
    end
    
    