



function K = quadCCLine2PointIntSclOrd(funs,X1S,X1E,X2,par,order,NodeWeightArray)
% disp('-Function: quadCCLine2PointIntSclOrd')
% pause;

% if nargin<7
%     load NodeWeightArray
% end

maxOrderWarning = 0;

Num_Lines = size(X1S,2);
Num_Points = size(X2,2);
D = size(X1S,1);


%% Determine Nodes and Weights for X1
%Scale dimensions to suit lengthscales
Lengthscales = par(1:D)';
X1SScaled = X1S./Lengthscales(:,ones(1,Num_Lines));
X1EScaled = X1E./Lengthscales(:,ones(1,Num_Lines));


%Determines the length of lines
LineLengths1 = sqrt(sum((X1S-X1E).^2,1))';
LineLengths1Scaled = sqrt(sum((X1SScaled-X1EScaled).^2,1))';
ScaledOrder = round(order*(LineLengths1Scaled).^(1/2)); % used to be .*(1/2)
% ScaledOrder = round(order*(LineLengths2Scaled));

ScaledOrder(ScaledOrder>100)=100;
ScaledOrder(ScaledOrder<3)=3;
%Give a warning if order is becoming saturated.
if max(ScaledOrder) > 70
    MaxOrderWarning = 1;
end
% disp('Mean Scaled Order:')
% mean(ScaledOrder)
% % disp('Max Scaled Order:')
% % max(ScaledOrder)
% % disp('Min Scaled Order:')
% % min(ScaledOrder)


% % % % figure(99)
% % % % hist(ScaledOrder)
% % % % pause(0.1)


% figure
% plot([X1S(1,:);X1E(1,:)],[X1S(2,:);X1E(2,:)])
% hold on
% plot(X2(1,:),X2(2,:),'+')


 Nodes1 = NodeWeightArray(1,ScaledOrder);
 Weights1 = NodeWeightArray(2,ScaledOrder);
 
 
 
% Num_Points2 = size(X2S,2);
D = size(X1S,1);

if D == 2
    X1S = [X1S;zeros(1,Num_Lines)];
    X1E = [X1E;zeros(1,Num_Lines)];
    
elseif D == 1
    X1S = [X1S;zeros(2,Num_Lines)];
    X1E = [X1E;zeros(2,Num_Lines)];
end

% order
[costhet sinthet cosw sinw xstart ystart zstart r]= determLineParameters3D(X1S, X1E);
  
[NodesScaleShift1 Weights1] = scaleShiftNodesWeightsOrdTest(r, Nodes1, Weights1);

X1 = cellfun(@lineparams2cart,NodesScaleShift1,num2cell(cosw'),...
    num2cell(sinw'),num2cell(costhet'), num2cell(sinthet'), ...
    num2cell(xstart'),num2cell(ystart'),num2cell(zstart'),'Un',0);

if D == 2
    X1=cellfun(@removeLastDim,X1,'Un',0);
elseif D == 1
    X1=cellfun(@removeLastDim,X1,'Un',0);
    X1=cellfun(@removeLastDim,X1,'Un',0);
end

% X1rep = repmat(X1',1,Num_Points);
% Weights1rep = repmat(Weights1',1,Num_Points);
X2rep = repmat(mat2cell(X2,D,size(X2,2)),Num_Lines,1);

parcell =  mat2cell(par,1,numel(par));
parrep = repmat(parcell,Num_Lines, 1);


%% Cell function approach
knodescell = cellfun(funs,X1',X2rep,parrep,'Un',0);
WeightsCell = cellfun(@transAndRep,Weights1,repmat(num2cell(Num_Points),1,Num_Lines),'Un',0)';
KnodesWeighted=cellfun(@dottimes,knodescell,WeightsCell,'Un',0);
KnodesWeightedSum = cellfun(@sum,KnodesWeighted,'Un',0);
K = cell2mat(KnodesWeightedSum);


%% For Loop Approach
% K = zeros(size(parrep));
% for i=1:numel(X1)
%      numel(X1)-i
%     for j = 1:size(X2,2)
%         knodes = feval(funs,X1{i},X2(:,j),parrep{i});
%         KnodesWeighted=knodes.*Weights1{i}';
%         K(i,j) = sum(KnodesWeighted);
%     end
% end


% disp('-Function: quadCCLine2PointIntSclOrd: OK')
% pause;
  
end


