

% Copyright The University of Sydney, 2001 - 2008.  ABN 15 211 513 464
% herafter 'The University'.
%
% The copyright of the computer program(s) herein is the property of
% The University of Sydney, Australia.  The program(s) may be used
% and/or copied only with the written permission of The University of
% Sydney, or in accordance with the terms and conditions stipulated
% in the contract under which the program(s) have been supplied.
% 
% Disclaimer:
% The Universitiy makes no representation or warranty
% that these materials, including any software, are free from errors;
% about the quality or performance of these materials;
% that these materials are fit for any particular purpose.
% These materials are made available on the strict basis that The University
% and its employees or agents have no liability for any direct or indirect
% loss or damage (including for negligence) suffered by any person as a
%consequence of the use of this  material.
%
% Train a GP model with scaled conjugade gradient

function [params] = gplearnparamsoccline(gp,X,y,params0,NodeWeightArray,options)


params = [params0];

opts = optimset('LargeScale','off','MaxIter',10,'Diagnostics', 'on',...
'GradObj', 'off', 'Display', 'iter','TolFun',  1e-10, 'MaxFunEvals', 5000);

 
% [params, fval] = fminunc(@(params) gplmlevalocclinequad(gp,X,y,params), params, opts);
[params, fval] = anneal(@(params) gplmlevalocclinequad(gp,X,y,params,NodeWeightArray), params);


% FixedParams = params(1:end-3)
% params = params(end-2:end)
% [params, fval] = anneal(@(params) gplmlevallinesegquadcc2DSumFixNoiseOcc(cov_funs,npar,TrainingSegStart,TrainingSegEnd,y,params,noise,order,FixedParams), params);

%tic;[params, options]=fminunc(@(params)gplmlevallinesegcovsqexp(TrainingSegStart,TrainingSegEnd,y,params), params,opts);toc;

% tic;[params, options]=fminunc(@(params)gplmlevallinesegquadcc2DCovSumFixNoise(cov_funs,npar,TrainingSegStart,TrainingSegEnd,y,params,noise,order), params,opts);toc;


