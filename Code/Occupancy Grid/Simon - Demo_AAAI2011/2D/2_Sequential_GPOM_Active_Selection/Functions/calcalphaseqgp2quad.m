% Copyright The University of Sydney, 2001 - 2008.  ABN 15 211 513 464
% herafter 'The University'.
% 


function [alpha L K] = calcalphaseqgp2quad(cov_funs,npar,Kold,LOld,TrainingObsOld,TrainingyOld,...
    ObsIdOld,NewSegStart,NewSegEnd,NewXPoint,Newy,NewObsId,params,noise,order, NodeWeightArray)

% [alpha L K invK] = calcalphaseqgp2quad(cov_funs,npar,Kold,invKold,TrainingObsOld,TrainingyOld,...
%     ObsIdOld,NewSegStart,NewSegEnd,NewXPoint,Newy,NewObsId,params,noise,order)

% load NodeWeightArray

if numel(Kold)~=0
    XStartOld = TrainingObsOld(:,ObsIdOld==1,1);
    XEndOld = TrainingObsOld(:,ObsIdOld==1,2);
    XPointOld = TrainingObsOld(:,ObsIdOld==0,1);
    ySegOld = TrainingyOld(ObsIdOld==1);
    yPointOld = TrainingyOld(ObsIdOld==0);
    y = [TrainingyOld Newy];
    
    LineLengthsOld = sqrt(sum((XStartOld-XEndOld).^2,1))';

else
    XStartOld = [];
    XEndOld = [];
    XPointOld = [];
    ySegOld = [];
    yPointOld = [];
    y = Newy;
end 
    
    NewySeg = Newy(NewObsId==1);
    NewyPoint = Newy(NewObsId==0);

    d = size(NewSegStart,1);

%     LineLengthsNew = sqrt(sum((NewSegStart-NewSegEnd).^2,1))';

    ySegCombined = [ySegOld NewySeg];
    XStartCombined = [XStartOld NewSegStart];
    XEndCombined = [XEndOld NewSegEnd];
%     XPointCombined = [XPointOld NewXPoint];

    yPointCombined =[yPointOld NewyPoint];

    myLine = calcmeanyInt(ySegCombined,XStartCombined,XEndCombined);
    myPoint = mean(yPointCombined);

    my = (myLine+myPoint)/2; %Assumes random sampling
%     disp('Artifically Changing Mean')
%     my = -0.4;
%     pause(1)

    if numel(Kold)~=0
      ySegOld      = ySegOld - my*sqrt(sum((XStartOld-XEndOld).^2,1));
      yPointOld = yPointOld-my;
    end
      NewySeg     = NewySeg - my*sqrt(sum((NewSegStart-NewSegEnd).^2,1));
      NewyPoint = NewyPoint - my;
  

%   y = [ySeg yPoint];

kpar = params;
Newn = length(NewSegStart)+length(NewXPoint);

%Evaluate the quadrants of the K matrix
OrderTooLow =1;

%evaluate covariance between old obs and new observations
while OrderTooLow ==1
    %catch orders that are too low
%     try
        p1 = 1;
        for j =  1:numel(npar)
            p2 = p1 + npar(j)-1;
            if numel(Kold)~=0
                KLL = qdCCL2LQuadNDIntSclOrd(cov_funs{j},XStartOld,XEndOld,NewSegStart,NewSegEnd,kpar(p1:p2),order,NodeWeightArray);
                KPL= quadCCLine2PointIntSclOrd(cov_funs{j},NewSegStart,NewSegEnd,XPointOld,kpar(p1:p2),order,NodeWeightArray)';
                KLP= quadCCLine2PointIntSclOrd(cov_funs{j},XStartOld,XEndOld,NewXPoint,kpar(p1:p2),order,NodeWeightArray);
                KPP = cov_sum(XPointOld,NewXPoint,cov_funs(j),npar(j),kpar(p1:p2));
                
                
                
                K12 = zeros(length(TrainingObsOld),length([NewSegEnd NewXPoint]));
                K12(ObsIdOld==1,:)=[KLL KLP];
                K12(ObsIdOld==0,:)=[KPL KPP];
              
                if j==1
                    K12Sum =K12;
                else
                    K12Sum = K12Sum + K12;
                end
                
                %Determine Covariance of New Observations only
                K22LL = qdCCL2LQuadNDIntSclOrd(cov_funs{j},NewSegStart,NewSegEnd,NewSegStart,NewSegEnd,kpar(p1:p2),order,NodeWeightArray);
                K22LP= quadCCLine2PointIntSclOrd(cov_funs{j},NewSegStart,NewSegEnd,NewXPoint,kpar(p1:p2),order,NodeWeightArray);
                K22PP = cov_sum(NewXPoint,NewXPoint,cov_funs(j),npar(j),kpar(p1:p2));

                
                if j==1
                    K22Sum = real([K22LL K22LP;K22LP' K22PP]);
                else
                    K22Sum = real(K22Sum+[K22LL K22LP;K22LP' K22PP]);
                end
                

                    K = [Kold K12Sum;K12Sum' K22Sum];
            
            else %if this is the first observation
                 K22LL = qdCCL2LQuadNDIntSclOrd(cov_funs{j},NewSegStart,NewSegEnd,NewSegStart,NewSegEnd,kpar(p1:p2),order,NodeWeightArray);
                 K22LP= quadCCLine2PointIntSclOrd(cov_funs{j},NewSegStart,NewSegEnd,NewXPoint,kpar(p1:p2),order,NodeWeightArray);
                 K22PP = cov_sum(NewXPoint,NewXPoint,cov_funs(j),npar(j),kpar(p1:p2));
                
                K22 = [K22LL K22LP;K22LP' K22PP];
                
                if j==1
                    K = K22;
                else
                    K = K + K22;
                end
                
            end
                
                p1 = p2 + 1;
        end
        if isreal(K)~=1
            K=real(K);
        end
        K = triu(K,1)'+triu(K);
        
%         L = chol(KT+(noise^2)*eye(n)); 
        OrderTooLow = 0;
%     catch ME
%                 disp(ME)
%         if ME.identifier == 'MATLAB:posdef'
%             order = order+5;
%             disp ('Order incremented by 5. Order:')
%             disp(order);
%             if order > 9
% %                 paramssemipos = [kpar noise]
% %                 save paramssemipos 
%               
%                 OrderTooLow=2;
%             end
%         else
%             rethrow(ME)
%         end
%     end
end



%Block inversion method
if numel(Kold)~=0
%     invK = blockinv(invKold,K12Sum,K12Sum',K22Sum+(noise^2)*eye(Newn));
%     alpha = invK*y';
%     L = chol(K+(noise^2)*eye(length(K))); 

    L = updateCholK(LOld,K12Sum,K22Sum+(noise^2)*eye(length(K22Sum)));
%     invK = L'\(L\eye(length(K)));
    alpha = solve_chol(L,y');

else
    L = chol(K+(noise^2)*eye(length(K))); 
%     invK = L'\(L\eye(length(K)));
    alpha = solve_chol(L,y');
end



