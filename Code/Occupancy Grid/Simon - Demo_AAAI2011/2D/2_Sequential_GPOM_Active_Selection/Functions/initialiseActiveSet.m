

function [ActiveSet]=initialiseActiveSet(InitalObs,gp,order,NodeWeightArray)
% load NodeWeightArray
ActiveSet.X = [InitalObs.Start InitalObs.Point];
ActiveSet.X= cat(3,ActiveSet.X,[InitalObs.End zeros(size(InitalObs.Point))]) ;   
ActiveSet.y = [InitalObs.yIntSeg InitalObs.yPoint];
ActiveSet.ObsId = [ones(1,size(InitalObs.Start,2)) zeros(1,size(InitalObs.Point,2))];


%Evaluate K
 p1 = 1;
        for j =  1:numel(gp.npar)
            p2 = p1 + gp.npar(j)-1;
   
             KLL = qdCCL2LQuadNDIntSclOrd(gp.cov_funs{j},InitalObs.Start,InitalObs.End,InitalObs.Start,InitalObs.End,gp.par(p1:p2),order,NodeWeightArray);
             KLP= quadCCLine2PointIntSclOrd(gp.cov_funs{j},InitalObs.Start,InitalObs.End,InitalObs.Point,gp.par(p1:p2),order,NodeWeightArray);
             KPP = feval(gp.cov_funs{j},InitalObs.Point,InitalObs.Point,gp.par(p1:p2));
%              KPP = cov_sum(InitalObs.Point,InitalObs.Point,gp.cov_funs(j),gp.npar(j),gp.par(p1:p2));
                
            KSum = [KLL KLP;KLP' KPP];
                
                if j==1
                    K = KSum;
                else
                    K = K + KSum;
                end

                p1 = p2 + 1;
        end
        if isreal(K)~=1
            K=real(K);
        end
        K = triu(K,1)'+triu(K);

ActiveSet.L = chol(K+(gp.noise^2)*eye(length(K))); 
ActiveSet.alpha = solve_chol(ActiveSet.L,ActiveSet.y');





