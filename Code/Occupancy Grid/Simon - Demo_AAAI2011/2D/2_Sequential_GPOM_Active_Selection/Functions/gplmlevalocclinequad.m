% Copyright The University of Sydney, 2001 - 2008.  ABN 15 211 513 464
% herafter 'The University'.
%
% The copyright of the computer program(s) herein is the property of
% The University of Sydney, Australia.  The program(s) may be used
% and/or copied only with the written permission of The University of
% Sydney, or in accordance with the terms and conditions stipulated
% in the contract under which the program(s) have been supplied.
% 
% Disclaimer:
% The Universitiy makes no representation or warranty
% that these materials, including any software, are free from errors;
% about the quality or performance of these materials;
% that these materials are fit for any particular purpose.
% These materials are made available on the strict basis that The University
% and its employees or agents have no liability for any direct or indirect
% loss or damage (including for negligence) suffered by any person as a
%consequence of the use of this  material.
%
%Compute the negate log marginal likelihood of a GP

function nlml = gplmlevalocclinequad(gp,X,y,params, NodeWeightArray)

% load NodeWeightArray



myLine = calcmeanyInt(y.IntSeg,X.Start,X.End);
myPoint = mean(y.Point);

my = (myLine+myPoint)/2; 
  y.IntSeg      = y.IntSeg - my*sqrt(sum((X.Start-X.End).^2,1));
  y.Point = y.Point - my;

  y.Combined = [y.IntSeg y.Point];

% pause

gp.par = params(1:end-1);
noise = params(end);
n = size([X.Start X.Point],2);



 p1 = 1;
for j =  1:numel(gp.npar)
    p2 = p1 + gp.npar(j)-1;

        
            KLL = qdCCL2LQuadNDIntSclOrd(gp.cov_funs{j},X.Start,X.End,X.Start,X.End,gp.par(p1:p2),gp.order,NodeWeightArray);


            KLP= quadCCLine2PointIntSclOrd(gp.cov_funs{j},X.Start,X.End,X.Point,gp.par(p1:p2),gp.order,NodeWeightArray);
            KPP = feval(gp.cov_funs{j},X.Point,X.Point,gp.par(p1:p2));


        K = [KLL KLP; KLP' KPP];


        if j==1
            KSum =K;
        else
            KSum = KSum + K;
        end
   

        p1 = p2 + 1;
end

    K=KSum;

        if isreal(K)~=1
            K=real(K);
        end
        K = triu(K,1)'+triu(K);
        L = chol(K+(noise^2)*eye(n)); 

    alpha = solve_chol(L,y.Combined');
    nlml = 0.5*y.Combined*alpha + sum(log(diag(L))) + 0.5*n*log(2*pi);
    
   



