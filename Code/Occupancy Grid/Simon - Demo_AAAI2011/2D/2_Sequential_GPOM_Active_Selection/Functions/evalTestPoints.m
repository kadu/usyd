
function [mf vf S2]= evalTestPoints(X,gp,Test,NodeWeightArray)

for i=1:ceil(size(Test.x,2)/1000)  % For single covariance function 
        PercentageComplete = i*100/(size(Test.x,2)/1000);
        if PercentageComplete<100
         disp('Percentage Complete:');
         disp(PercentageComplete);
         pause(0.1)
        end

        ist = (i-1)*1000+1;
        if i*1000<size(Test.x,2)
            ien = i*1000;
        else
            ien = size(Test.x,2);
        end

        [mf(ist:ien),vf(ist:ien)] = gpevalSeqOccIntquad(X.ActiveSet,gp,...
            [Test.x(ist:ien);Test.y(ist:ien)],gp.order,NodeWeightArray);

        S2(ist:ien) = vf(ist:ien) - gp.noise^2; 
 end