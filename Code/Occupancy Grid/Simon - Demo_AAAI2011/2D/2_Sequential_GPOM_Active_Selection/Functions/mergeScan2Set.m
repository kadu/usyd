

% Assumes that Set if full
%

function     [MergedSet] = mergeScan2Set(Set,Scan,gp,order, NodeWeightArray)
% load NodeWeightArray
 
% disp('+Function: mergeScan2Set');
% pause;

if size(Set.X,2)>0
    Set.Start = Set.X(:,Set.ObsId==1,1);
    Set.End = Set.X(:,Set.ObsId==1,2);
    Set.Point = Set.X(:,Set.ObsId==0,1);
    Set.yIntSeg = Set.y(Set.ObsId==1);
    Set.yPoint = Set.y(Set.ObsId==0);
    
%     disp('+size(Set.X,2)>0');
%     pause;
    
    
else
    Set.Start = [];
    Set.End =  [];
    Set.Point =  [];
    Set.yIntSeg =  [];
    Set.yPoint = [];
    Set.yIntSeg   = [];
    Set.yPoint =[];
    
%     disp('+size(Set.X,2)<=0');
%     pause;
    
end

Scan.X = [Scan.Start Scan.Point];
Scan.X = cat(3,Scan.X,[Scan.End zeros(size(Scan.Point))]) ;

MergedSet.yIntSeg = [Set.yIntSeg Scan.yIntSeg];
MergedSet.yPoint = [Set.yPoint Scan.yPoint];
MergedSet.X = cat(2,Set.X,Scan.X);
MergedSet.ObsId = [Set.ObsId ones(1,size(Scan.Start,2)) zeros(1,size(Scan.Point,2))];
MergedSet.y = [Set.y Scan.yIntSeg Scan.yPoint];


myLine = calcmeanyInt([Set.yIntSeg Scan.yIntSeg],[Set.Start Scan.Start],[Set.End Scan.End]);
myPoint = mean(MergedSet.yPoint);

my = (myLine+myPoint)/2;
if size(Set.X,2)>0
    Set.yIntSeg   = Set.yIntSeg - my*sqrt(sum((Set.Start-Set.End).^2,1));
    Set.yPoint = Set.yPoint-my;
end

if numel(Scan.yIntSeg)
    Scan.yIntSeg     = Scan.yIntSeg - my*sqrt(sum((Scan.Start-Scan.End).^2,1));
end
if numel(Scan.yPoint)
    Scan.yPoint = Scan.yPoint - my;
end


p1 = 1;
for j =  1:numel(gp.npar)
    p2 = p1 + gp.npar(j)-1;
    if size(Set.X,2)>0
        if numel(Scan.Start)~=0
            KLL= qdCCL2LQuadNDIntSclOrd(gp.cov_funs{j},Set.Start,Set.End,Scan.Start,Scan.End,gp.par(p1:p2),order,NodeWeightArray);
            KPL= quadCCLine2PointIntSclOrd(gp.cov_funs{j},Scan.Start,Scan.End,Set.Point,gp.par(p1:p2),order,NodeWeightArray)';
        else
            KLL = [];
            KPL= [];
        end
        
        if numel(Scan.Point)~=0
            KLP= quadCCLine2PointIntSclOrd(gp.cov_funs{j},Set.Start,Set.End,Scan.Point,gp.par(p1:p2),order,NodeWeightArray);
            %             KPP = cov_sum(Set.Point,Scan.Point,gp.cov_funs(j),gp.npar(j),gp.par(p1:p2));
            KPP = feval(gp.cov_funs{j},Set.Point,Scan.Point,gp.par(p1:p2));
        else
            KLP=[];
            KPP=[];
        end
        
        
        K12 = zeros(size(Set.X,2),size([Scan.End Scan.Point],2));
        K12(Set.ObsId==1,:)=[KLL KLP];
        K12(Set.ObsId==0,:)=[KPL KPP];
        
        if j==1
%             disp('+if 5');
%             pause;
            K12Sum =K12;
        else
%             disp('+if 6');
%             pause;
            K12Sum = K12Sum + K12;
        end
    else
%         disp('+big else');
%         pause;
        K12Sum = [];
    end
    
    %Determine Covariance of New Observations only
    if numel(Scan.Start)~=0
        K22LL = qdCCL2LQuadNDIntSclOrd(gp.cov_funs{j},Scan.Start,Scan.End,Scan.Start,Scan.End,gp.par(p1:p2),order,NodeWeightArray);
    else
        K22LL = [];
    end
    
    if numel(Scan.Start)~=0 && numel(Scan.Point)~=0
        K22LP= quadCCLine2PointIntSclOrd(gp.cov_funs{j},Scan.Start,Scan.End,Scan.Point,gp.par(p1:p2),order,NodeWeightArray);
    else
        K22LP= [];
    end
    
    if numel(Scan.Point)~=0
        %             K22PP = cov_sum(Scan.Point,Scan.Point,gp.cov_funs(j),gp.npar(j),gp.par(p1:p2));
        K22PP = feval(gp.cov_funs{j},Scan.Point,Scan.Point,gp.par(p1:p2));
    else
        K22PP = [];
    end
    
    if j==1
        K22Sum = real([K22LL K22LP;K22LP' K22PP]);
    else
        K22Sum = real(K22Sum+[K22LL K22LP;K22LP' K22PP]);
    end
    
    
    %             K = [Kold K12Sum;K12Sum' K22Sum];
    
    p1 = p2 + 1;
end

% disp('+Loop done');
% pause;

% if isreal(K)~=1
%     K=real(K);
% end
% K = triu(K,1)'+triu(K);

if size(Set.X,2)>0
%     disp('-Function: updateCholK')
%     pause;
    MergedSet.L = updateCholK(Set.L,K12Sum,K22Sum+(gp.noise^2)*eye(length(K22Sum)));
%     disp('-Function: updateCholK: OK')
%     pause;
else
%     disp('-Function: chol')
%     pause;
    MergedSet.L = chol(K22Sum+(gp.noise^2)*eye(length(K22Sum)));
%     disp('-Function: chol: OK')
%     pause;
end
MergedSet.alpha = solve_chol(MergedSet.L,MergedSet.y');

% disp('+Function mergeScan2Set: ok');
% pause;
%     L = chol(K+(gp.noise^2)*eye(length(K)));

%     alpha = solve_chol(L,y');









