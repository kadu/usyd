
% Uses quadratures to train hyperparam.
% The observations line segments and the target 
% is the integral along those segments and points 2 D
% Similar to GPOM - unocc line seg occ points

close all
clear all

%% Add Paths
addpath ../../../General_GP_Files
addpath ../Common_Functions
addpath ../CellFunctions
addpath ../Data_And_Params

%% Initalise Variables
order=5; % Quadrature order

%% Load Data
load NodeWeightArray
% load CleanRoom1LibraryOfTrainingData
load DemoTrainingData
% load TrainingData

load params3sqexp2D


%% Initialize Training Data
tic;
[X y] = InitTrainingData(TrainingData);


gp.cov_funs={@cov_sqexp,@cov_sqexp,@cov_sqexp,};
gp.npar = [3 3 3];
gp.params = [1 1 1 1 1 1 1 1 1 1];
% 
% gp.cov_funs={@covMat3_iso @covMat3_iso @covMat3_iso}
% gp.npar = [2 2 2]
% params = [1 1 1 1 1 1 1];
% % 

% gp.cov_funs={@cov_nn2}
% gp.npar = [4]
% params = [-0.0001   -0.0082   0.9411    4.2932    0.0100];

gp.order = order;


%% Initialise Parameters
params0 = params;

%% Train Hyperparameters
options = zeros(1,18);
options(1)=1;options(9)=1;options(14)=40;
params = gplearnparamsoccline(gp,X,y,params0,NodeWeightArray,options)


gp.par = params(1:end-1);
gp.noise = params(end);

save ../Data_And_Params/Latest_GP gp

DemoTimeTest
toc
