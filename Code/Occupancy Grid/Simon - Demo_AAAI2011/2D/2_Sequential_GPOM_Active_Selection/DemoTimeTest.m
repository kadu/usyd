
% Uses quadrature and closed form solutio to train the 
%hyperparameters. It uses just the closed form 
%solution to evaluate the test set.
% The observations line segments and the target 
% is the integral along those segments and points 2 D
% Similar to GPOM - unocc line seg occ points

close all
clear all

%% Add Paths
addpath ../../General_GP_Files
addpath ../Common_Functions
addpath ../CellFunctions
addpath ../Data_And_Params
addpath Functions


%% Initalise Variables
res = 0.5
ActiveSelSwitch = 1;
UseLatestGP = 1;
ShowRedundantSet = 0; %Turning this on really slows down the algorithm

%ActThresh: Lowering the ActThresh increases the number of points accepted
ActThresh.Point = 0.2%2.5e+004;% Threshold for active set selection for Occ Points
ActThresh.Line = 0.2%0.4;% Threshold for active set selection for Occ Points
ActThresh.NumSamples = 10; %Number of samples on the line segment to decide whether to accept or reject

order=5; % Quadrature order


%% Load Data
load NodeWeightArray
load Simon2TrainingData
load SquashParameters
load params3sqexp2D
% load Testx


%% Initialize Training Data
[X y] = InitTrainingData(TrainingData,1);


%% Generate Test Points  
Mins = min([X.Start X.End]');
xmin = Mins(1)-1;
ymin = Mins(2)-1;
Maxs = max([X.Start X.End]');
xmax = Maxs(1)+1;
ymax = Maxs(2)+1;


[Mesh.x Mesh.y] = meshgrid(xmin:res:xmax,ymin:res:ymax);
clear xmin ymin Mins Maxs xmax ymax
Test.x = reshape(Mesh.x,[1 numel(Mesh.x)]);
Test.y = reshape(Mesh.y,[1 numel(Mesh.y)]);

% Test.x = tx.Point.Coord(1,:);
% Test.y = tx.Point.Coord(2,:);

%% Initialise GP
if UseLatestGP == 1
    load Latest_GP
    gp.cov_funs
    gp.noise=0.05;
else
    % gp.cov_funs={@cov_sqexp,@cov_sqexp,@cov_sqexp}
    % gp.npar = [3 3 3]

    gp.cov_funs={@covMat3 @covMat3};
    gp.npar = [3 3];
    params = [2.7300    1.2360    0.7898    2.6022    1.7004    0.9815   -0.0009];

    % gp.cov_funs={@cov_nn2}
    % gp.npar = [4]
    % params = [-0.0001   -0.0082   0.9411    4.2932    0.0100];

    gp.par = params(1:end-1);
    gp.noise = params(end);
    gp.order = order;
end

%% Build Occ Map By Inputting Each Scan Sequentially
X.PoseNum = X.PoseNum./X.PoseNum;
NumScans = max(X.PoseNum);
tic;
disp('Number of Scans')
%time = zeros(1,NumScans)
for ScanNum = 1:NumScans 
    ScanNum;

    X = buildActiveSet(X,y,gp,ScanNum,ActThresh,squashparams...
        ,ActiveSelSwitch,NodeWeightArray,ShowRedundantSet);


%% Evaluate Test Points
% tic
[mf vf S2] = evalTestPoints(X,gp,Test,NodeWeightArray);
time = toc
 %% Plot Results
   
    Active.Start = X.ActiveSet.X(:,X.ActiveSet.ObsId==1,1);
    Active.End = X.ActiveSet.X(:,X.ActiveSet.ObsId==1,2);
    Active.Point = X.ActiveSet.X(:,X.ActiveSet.ObsId==0,1);
    Active.yIntSeg = X.ActiveSet.y(X.ActiveSet.ObsId==1);
    Active.yPoint = X.ActiveSet.y(X.ActiveSet.ObsId==0);
    try
        Redund.Start = X.RedundantSet.X(:,X.RedundantSet.ObsId==1,1);
        Redund.End = X.RedundantSet.X(:,X.RedundantSet.ObsId==1,2);
        Redund.Point = X.RedundantSet.X(:,X.RedundantSet.ObsId==0,1);
        Redund.yIntSeg = X.RedundantSet.y(X.RedundantSet.ObsId==1);
        Redund.yPoint = X.RedundantSet.y(X.RedundantSet.ObsId==0);
    end
 
%Plot GP data   
% screen_size = get(0, 'ScreenSize');
f1 = figure(404); 
% set(f1, 'Position', [0 0 screen_size(3) screen_size(4) ] );
% subplot(2,2,1)
hold on
try     %Plots redundant set if the Redundant Set Switch is turned on
plot([Redund.Start(1,:);Redund.End(1,:)],[Redund.Start(2,:);Redund.End(2,:)],'b');
      
        plot(Redund.Point(1,:),Redund.Point(2,:),'b+')
end
        view(0,90)
        title('Observations (Active = Red. Redundant = Blue)')
        axis([min(Test.x),max(Test.x),min(Test.y),max(Test.y)]) 
        plot([Active.Start(1,:);Active.End(1,:)],[Active.Start(2,:);Active.End(2,:)],'r');
        plot(Active.Point(1,:),Active.Point(2,:),'r+')
        hold off
        axis equal

 
 % Plot Mean
  SquashedMean = mf;
    for i = 1:numel(SquashedMean)  
      if mf(i)>1
        SquashedMean(i) = 1;  
      elseif mf(i)<-1
        SquashedMean(i) = -1;  
      end
    end
    
  for i = 1:numel(SquashedMean)    
    SquashedMeanOccupied(i) = sigmoid(squashparams(1),squashparams(2),1,SquashedMean(i),vf(i));  
  end
 figure(304); 
% subplot(2,2,2)

    surf(Mesh.x,Mesh.y,reshape(SquashedMeanOccupied,size(Mesh.x)));
    title('Prob Occ Vs Location');
    view(0,90);
    axis([min(Test.x),max(Test.x),min(Test.y),max(Test.y)]) 
    colorbar
    axis equal
    

%Classify
      classifiedmf = SquashedMeanOccupied;
  for i = 1:numel(classifiedmf)
      
      if classifiedmf(i) >=0.66
          classifiedmf(i) =1;
      elseif classifiedmf(i) <=0.33
          classifiedmf(i) =0;
      else
          classifiedmf(i) =0.5;
      end
      
  end
   figure(204); 
%     subplot(2,2,3)
    surf(Mesh.x,Mesh.y,reshape(classifiedmf,size(Mesh.x))); 
    title('Classified Environment');
    view(0,90);
    axis([min(Test.x),max(Test.x),min(Test.y),max(Test.y)]) 
    colormap(flipud(gray))
    axis equal
    
 %Variance
     figure(104); 
%     subplot(2,2,4)
    surf(Mesh.x,Mesh.y,reshape(vf,size(Mesh.x))); 
    title('Variance vs Location');
    view(0,90);
    axis([min(Test.x),max(Test.x),min(Test.y),max(Test.y)]) 
    colorbar
    axis equal

end
TotalTime = sum(time)

% %% Save ActiveSet as training data
% TrainingData.LibraryOfStartPoints = X.ActiveSet.X(:,X.ActiveSet.ObsId==1,1);
% TrainingData.LibraryOfEndPoints = X.ActiveSet.X(:,X.ActiveSet.ObsId==1,2);
% TrainingData.OccupiedPoints =  X.ActiveSet.X(:,X.ActiveSet.ObsId==0,1);
% save ../Data_And_Params/ActiveTrainingData TrainingData


