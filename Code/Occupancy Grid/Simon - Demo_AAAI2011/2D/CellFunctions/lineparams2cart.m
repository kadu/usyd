%X = x y z cartesian coords of line

function [X] = lineparams2cart(r, cosw,sinw,costhet, sinthet, xstart,ystart,zstart)

X = [r*cosw*costhet+xstart;...
    r*cosw*sinthet+ystart;...
    r*sinw+zstart];
