
function [A] = dottimesrow(B,rowvec)

A = B.*rowvec(ones(1,size(B,1)),:);
