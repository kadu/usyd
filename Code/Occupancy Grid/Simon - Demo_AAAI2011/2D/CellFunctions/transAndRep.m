
function [A] = transAndRep(B,NumRep)

% A =repmat(B',1,NumRep);
A = B(ones(1,NumRep),:)';
