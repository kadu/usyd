

function [Xreduced] = removeLastDim(X)

Xreduced = X(1:end-1,:);