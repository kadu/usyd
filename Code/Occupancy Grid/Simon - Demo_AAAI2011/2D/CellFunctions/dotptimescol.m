
function [A] = dottimescol(B,colvec)

A = B.*colvec(:,ones(1,size(B,2)));
