% Copyright The University of Sydney, 2001 - 2008.  ABN 15 211 513 464
% herafter 'The University'.
%
% The copyright of the computer program(s) herein is the property of
% The University of Sydney, Australia.  The program(s) may be used
% and/or copied only with the written permission of The University of
% Sydney, or in accordance with the terms and conditions stipulated
% in the contract under which the program(s) have been supplied.
% 
% Disclaimer:
% The Universitiy makes no representation or warranty
% that these materials, including any software, are free from errors;
% about the quality or performance of these materials;
% that these materials are fit for any particular purpose.
% These materials are made available on the strict basis that The University
% and its employees or agents have no liability for any direct or indirect
% loss or damage (including for negligence) suffered by any person as a
%consequence of the use of this  material.
%
% Compute the squared exponential covariance function for a point to
% a line segment - 1 Dimensional
% 
%INPUTS
% X1S - 3 x N start of line segment
% X1E - 3 x N end of line segment
% X2 - 3 x N input points 2
% par - cell with the following fields 
%   par(1) - characteristic length-scale x direction
%   par(2) - characteristic length-scale y direction
%   par(end) - sigma f square
%OUTPUT
% K - covariance function
%
% Simon O'Callaghan 21-11-09
% K = cov_sqexp_line_point(X1S,X1E,X2,par)

function K = cov_sqexp_line_point_3d(X1S,X1E,X2,par)

% close all
% 
% X1S = [1.1 1 3 2;1.1 2 6 4]
% X1E = [1 5 2 6;1 5 7 9]
% X2 = [0 2 4;0 0.5 2]
% 

% X1S = [1 2;1 2;1 2];
% X1E = [3 4 ;3 5;3 1];
% X2 = [1.5 5;2 5;1.5 5]
% par=[1 1 1 1]

% plot([X1S(1,:);X1E(1,:)],[X1S(2,:);X1E(2,:)])
% hold on
% plot(X2(1,:),X2(2,:),'+')


N1 = size(X1S,2);
N2 = size(X2,2);
% D = size(X1S,1);

if size(X1S,1)~=size(X2,1) || size(X1S,1)~=size(X1E,1)
    error('Dimensionality of X1 and X2 must be the same');
end

if size(X1S,1)~=3 || size(X1S,1)~=size(X1E,1) || size(X1S,1)~=size(X2,1)
    error('This function is for 2 Dimensional data only');
end

if size(X1S,2)~=size(X1E,2)
    error('Must be equal number of start and end points');
end

%Scale dimensions to suit lengthscales
X1S(1,:) = X1S(1,:)/par(1);
X1E(1,:) = X1E(1,:)/par(1);
X2(1,:) = X2(1,:)/par(1);
X1S(2,:) = X1S(2,:)/par(2);
X1E(2,:) = X1E(2,:)/par(2);
X2(2,:) = X2(2,:)/par(2);
X1S(3,:) = X1S(3,:)/par(3);
X1E(3,:) = X1E(3,:)/par(3);
X2(3,:) = X2(3,:)/par(3);


r = sqrt((X1E(1,:)-X1S(1,:)).^2+(X1E(2,:)-X1S(2,:)).^2+(X1E(3,:)-X1S(3,:)).^2);
xyhyp = sqrt(r.^2-(X1E(3,:)-X1S(3,:)).^2);
xyhyp(xyhyp==0)=0.000000000000000001; % Elimates risk of dividing by 0.
costhet = ((X1E(1,:)-X1S(1,:))./xyhyp)';
sinthet = ((X1E(2,:)-X1S(2,:))./xyhyp)';
cosw = (xyhyp./r)';
sinw = ((X1E(3,:)-X1S(3,:))./r)';

xstart=X1S(1,:);
ystart = X1S(2,:);
zstart = X1S(3,:);
xpoint = X2(1,:);
ypoint = X2(2,:);
zpoint = X2(3,:);

% rad = linspace(0,r(2),100)

% x = rad.*cosw(2).*costhet(2)+xstart(2)
% y = rad.*cosw(2).*sinthet(2)+ystart(2)
% z = rad.*sinw(2) +zstart(2)
                 
% figure()
% plot3(x,y,z,'+')

      %  -0.2673 0.5454 0
A = (2*sinw(:,ones(1,N2)).*(zstart(ones(1,N2),:)'-zpoint(ones(1,N1),:))...
    +2*cosw(:,ones(1,N2)).*costhet(:,ones(1,N2)).*(xstart(ones(1,N2),:)'-xpoint(ones(1,N1),:))...
    +2*cosw(:,ones(1,N2)).*sinthet(:,ones(1,N2)).*(ystart(ones(1,N2),:)'-ypoint(ones(1,N1),:)));

% B = xstart(ones(1,N2),:)'.^2+ystart(ones(1,N2),:)'.^2+ zstart(ones(1,N2),:)'.^2 ...
%     +xpoint(ones(1,N1),:).^2+ypoint(ones(1,N1),:).^2+zpoint(ones(1,N1),:).^2 ...
%     -2*xstart(ones(1,N2),:)'.*xpoint(ones(1,N1),:)...
%         -2*ystart(ones(1,N2),:)'.*ypoint(ones(1,N1),:)...
%         -2*zstart(ones(1,N2),:)'.*zpoint(ones(1,N1),:);

    
B = (xstart(ones(1,N2),:)'-xpoint(ones(1,N1),:)).^2+...
    (ystart(ones(1,N2),:)'-ypoint(ones(1,N1),:)).^2+...
    (zstart(ones(1,N2),:)'-zpoint(ones(1,N1),:)).^2;

    %This formula was derived by performing line integration along the
    %squared exponential covariance function with the line segment
    %parameterised in r*costhet and r*sinthet
   
K = par(end)^2*((-(2^(1/2)*pi^(1/2)*exp(A.^2/8 - B/2)...
    .*erf(- (2^(1/2)*r(ones(1,N2),:)')/2 - (2^(1/2)*A)/4))/2)...
    -(-(2^(1/2)*pi^(1/2)*exp(A.^2/8 - B/2).*erf(-(2^(1/2)*A)/4))/2))./r(ones(1,N2),:)';
% 
% K = ((par(end)^2)/2)*sqrt(2*pi)...
%     *exp((A^2-4*B)/8)*...
%     (erf(-A/(2*sqrt(2)))-erf(-((2*r+A)/(2*sqrt(2)))))./r(ones(1,N2),:)'
% 
% ((-(2^(1/2)*pi^(1/2)*exp(A.^2/8 - B/2)...
%     .*erf(- (2^(1/2)*r(ones(1,N2),:)')/2 - (2^(1/2)*A)/4)))...
%     -(-(2^(1/2)*pi^(1/2)*exp(A.^2/8 - B/2).*erf(-(2^(1/2)*A)/4))))./r(ones(1,N2),:)';
% 
% 
