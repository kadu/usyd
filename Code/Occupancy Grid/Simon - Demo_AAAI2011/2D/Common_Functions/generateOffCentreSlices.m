
function [Start End]=generateOffCentreSlices(Radius,NumSens)


Bearings = linspace(0,2*pi,NumSens+1);
Bearings(end) = [];

[SensorX SensorY]=pol2cart(Bearings,Radius.*ones(1,numel(Bearings)));

Start=[];
End=[];
for i = 1:(NumSens-1)
   
    Start = [Start [repmat(SensorX(i),1,NumSens-i); repmat(SensorY(i),1,NumSens-i)]];
    End = [End [SensorX(i+1:end);SensorY(i+1:end)]];
        
end
   
    

% 
% figure
% plot([Start(1,:); End(1,:)],[Start(2,:); End(2,:)])