function py = sigmoid(a,b,y,m,v)

    % Squashes a likelihood function (defined by the mean m and variance v)
    % through a sigmoid and returns a valid probability that the 
    % class is y. 
    
    % for predictions pass y = yplus*ones(n,1)
    
    % Paul Rigby June 2008
       
    z = y.*(a*m + b) ./ sqrt(1+a^2 * v);    
    py = normcdf(z);
    



