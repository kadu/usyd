
function [Start End]=generateOffCentreSlices3d(Radius,NumSensPerLvl,NumOfLvls)
% 
% NumSens = NumSensPerLvl^2;
% resol=(2*pi/(NumSensPerLvl));
% [BearingsThet,BearingsPhi] = meshgrid(0:resol:(2*pi-resol));
% 
% BearingsThet = reshape(BearingsThet,1,numel(BearingsThet));
% BearingsPhi = reshape(BearingsPhi,1,numel(BearingsPhi));
% 
% [SensorX SensorY SensorZ]=sph2cart...
%     (BearingsThet,BearingsPhi,Radius.*ones(1,numel(BearingsThet)));
% plot3(SensorX,SensorY,SensorZ,'+')



NumSens = NumSensPerLvl*NumOfLvls;

BearingsThet = []
BearingsPhi = []

for lvl=1:NumOfLvls
   
    BearingsThet = [BearingsThet linspace(0,2*pi,NumSensPerLvl+1)];
    BearingsThet(end) = [];
    
    BearingsPhi = [BearingsPhi (-pi+(lvl-1)*2*pi/NumOfLvls)*ones(1,NumSensPerLvl)];
    
end
% 
% resol=(2*pi/(NumSensPerLvl));
% [BearingsThet,BearingsPhi] = meshgrid(0:resol:(2*pi-resol));
% 
% BearingsThet = reshape(BearingsThet,1,numel(BearingsThet));
% BearingsPhi = reshape(BearingsPhi,1,numel(BearingsPhi));

[SensorX SensorY SensorZ]=sph2cart...
    (BearingsThet,BearingsPhi,Radius.*ones(1,NumSens));

% figure
% plot3(SensorX,SensorY,SensorZ,'+')


Start=[];
End=[];

%Draw beams connecting the sensors
for i = 1:(NumSens-1)
   
    Start = [Start [repmat(SensorX(i),1,NumSens-i); repmat(SensorY(i),1,NumSens-i);repmat(SensorZ(i),1,NumSens-i)]];
    End = [End [SensorX(i+1:end);SensorY(i+1:end);SensorZ(i+1:end)]];
        
end
% figure
%     plot3([Start(1,:);End(1,:)],[Start(2,:);End(2,:)],[Start(3,:);End(3,:)])
%     axis equal
% % 
% figure
% plot([Start(1,:); End(1,:)],[Start(2,:); End(2,:)])