% Copyright The University of Sydney, 2001 - 2008.  ABN 15 211 513 464
% herafter 'The University'.
%
% The copyright of the computer program(s) herein is the property of
% The University of Sydney, Australia.  The program(s) may be used
% and/or copied only with the written permission of The University of
% Sydney, or in accordance with the terms and conditions stipulated
% in the contract under which the program(s) have been supplied.
% 
% Disclaimer:
% The Universitiy makes no representation or warranty
% that these materials, including any software, are free from errors;
% about the quality or performance of these materials;
% that these materials are fit for any particular purpose.
% These materials are made available on the strict basis that The University
% and its employees or agents have no liability for any direct or indirect
% loss or damage (including for negligence) suffered by any person as a
%consequence of the use of this  material.
%
% Compute the squared exponential covariance function for a point to
% a line segment - 1 Dimensional
% 
%INPUTS
% X1S - 1 x 1 start of line segment
% X1E - 1 x 1 end of line segment
% X2 - 1 x 1 input points 2
% par - cell with the following fields 
%   par(1) - characteristic length-scales
%   par(end) - sigma f square
%OUTPUT
% K - covariance function
%
% Simon O'Callaghan 21-11-09
% K = cov_sqexp_line_point(X1S,X1E,X2,par)

function K = cov_sqexp_line_point_1d(X1S,X1E,X2,par)

N1 = size(X1S,2);
N2 = size(X2,2);
if size(X1S,1)~=size(X2,1) || size(X1S,1)~=size(X1E,1)
    error('Dimensionality of X1 and X2 must be the same');
end

if size(X1S,1)~=1 || size(X1S,1)~=size(X1E,1)
    error('This function is for 1 Dimensional data only');
end

if size(X1S,2)~=size(X1E,2)
    error('Must be equal number of start and end points');
end



%(X1S-X2)/L
w = par(1).^(-1);
XX1SFlat = w(:,ones(1,N1)).*X1S;
XX1S = XX1SFlat(:,:,ones(1,N2));
XX1SPer = permute(XX1S, [2 3 1]);

XX2Flat = w(:,ones(1,N2)).*X2;
XX2 = XX2Flat(:,:,ones(1,N1));
XX2Per = permute(XX2, [3 2 1]);

z1 = sum((XX1SPer-XX2Per),3);

%(X1E-X2)/L
XX1EFlat = w(:,ones(1,N1)).*X1E;
XX1E = XX1EFlat(:,:,ones(1,N2));
XX1EPer = permute(XX1E, [2 3 1]);

z2 = sum((XX1EPer-XX2Per),3);

erf1 = erf(z1/(sqrt(2)));
erf2 = erf(z2/(sqrt(2)));

Integral = par(end)^2*(sqrt(pi)/sqrt(2))*abs(erf1-erf2);

DistInterval = sqrt(sum((XX1SPer'-permute(XX1E, [3 2 1])).^2,3))';

%Find mean value of K between 2 points by dividing area by diff between
%inteveral
K = Integral./DistInterval;
% K = Integral;


