function[X y] = InitTrainingData2(TrainingData, plotfig)
   
    if nargin ==1
        plotfig=0;
    end

    X.Start = TrainingData.LibraryOfStartPoints;
    X.End = TrainingData.LibraryOfEndPoints;
    X.Point =  TrainingData.OccupiedPoints;
    X.PoseNum = TrainingData.PoseNumber ./ TrainingData.PoseNumber;

    y.Segment =  -1*ones(1,size(X.Start,2));
    y.Point = 1*ones(1,size(X.Point,2));

    % Create target vector for line segments
    y.IntSeg = y.Segment .*sqrt(sum((X.Start-X.End).^2,1));

    
    if plotfig
        figure(1)
            plot3([X.Start(1,:);X.End(1,:)],[X.Start(2,:);X.End(2,:)],[y.IntSeg;y.IntSeg],'b');
            hold on
            plot3(X.Point(1,:),X.Point(2,:),[y.Point;y.Point],'+')
            view(0,90)
            title('Raw Data')

        pause(1)
    end