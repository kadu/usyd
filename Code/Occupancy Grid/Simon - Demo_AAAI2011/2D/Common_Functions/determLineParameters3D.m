

function [costhet sinthet cosw sinw xstart ystart zstart r]= ...
    determLineParameters3D(Start, End)

%   Start = [1 0.5;4 0.25]
%   End = [-4 -3; 10 -2]
%   

    r = sqrt((End(1,:)-Start(1,:)).^2+(End(2,:)-Start(2,:)).^2+(End(3,:)-Start(3,:)).^2);  
    xyhyp1 = sqrt(r.^2-(End(3,:)-Start(3,:)).^2)+0.0000000001;            %3.6056
    costhet = ((End(1,:)-Start(1,:))./xyhyp1 )' ;              %0.5547
    sinthet = ((End(2,:)-Start(2,:))./xyhyp1 )' ;                 %0.8321
    cosw = (xyhyp1./r)';                                  %0.9361                 
    sinw = ((End(3,:)-Start(3,:))./r)';    
    r=r';
  xstart = Start(1,:)';
  ystart = Start(2,:)';
  zstart = Start(3,:)';

end

