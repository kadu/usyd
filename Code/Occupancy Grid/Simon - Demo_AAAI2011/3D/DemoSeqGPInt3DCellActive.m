
% Uses quadrature and closed form solutio to train the 
%hyperparameters. It uses just the closed form 
%solution to evaluate the test set.
% The observations line segments and the target 
% is the integral along those segments and points 3 D
% Similar to GPOM - unocc line seg occ points

close all
clear all

%% Add Paths
addpath ../General_GP_Files
addpath Data
addpath Functions

%% Initalise Parameters
res = 0.45  %Resolution of map in x and y directions
zres = 0.45 %Resolution of map in z direction
order=3; % Quadrature order

%Thresholds for Active Selection: Lowering the threshold increases the number of points accepted
Threshold.Point = 0.15  %  Threshold for active set selection for Occ Points
Threshold.Line = 0.08   % Threshold for active set selection for Occ Points
NumSamples = 10; %Number of samples on the line segment to decide whether to accept or reject

% Parameters for dividing the map into independent cuboid cells. (Naive Clustering)
MaxLinesPerCell = 200; %Limits the number of lines in any one cell. - Random Selection
NumRows = 2; %For the rows in the grid
NumCols = 2; %Number of columns in the grid
NumLays = 2; %Number of Layers in the grid
ExpandCellRatio = 1 % Grows grid cells to overlap with others around it. 1 = no growth


%% Load Data
load NodeWeightArray
load RoomBitComplexLibraryOfTrainingData

load SquashParameters

% Initialise Variables
X.Start = TrainingData.LibraryOfStartPoints
X.End = TrainingData.LibraryOfEndPoints
X.Point =  TrainingData.OccupiedPoints;
X.PoseNum = ones(1,size(X.Start,2));
clear PoseNumber

y.Segment =  -1*ones(1,size(X.Start,2));
y.Point = 1*ones(1,size(X.Start,2));

% Create target vector for line segments
y.IntSeg = y.Segment .*sqrt(sum((X.Start-X.End).^2,1));

figure(1) % Plot Rays and Points
    plot3([X.Start(1,:);X.End(1,:)],[X.Start(2,:);X.End(2,:)],[X.Start(3,:);X.End(3,:)],'b');
    hold on
    plot3(X.Point(1,:),X.Point(2,:),X.Point(3,:),'g+')
    view(0,90)
    title('Line Observations')
pause(1)

%% Generate Test Points and Cells 
Mins = min([X.Start X.End]');
xmin = Mins(1)-1;
ymin = Mins(2)-1;
zmin = Mins(3)-1;
Maxs = max([X.Start X.End]');
xmax = Maxs(1)+1;
ymax = Maxs(2)+1;
zmax = Maxs(3)+1;


[Mesh.x Mesh.y Mesh.z] = meshgrid(xmin:res:xmax,ymin:res:ymax,zmin:zres:zmax);
Test.x = reshape(Mesh.x,[1 numel(Mesh.x)]);
Test.y = reshape(Mesh.y,[1 numel(Mesh.y)]);
Test.z = reshape(Mesh.z,[1 numel(Mesh.z)]);

%% Divide Maps into Independent Cells
NumCells = NumCols*NumRows*NumLays
CellIdTable = flipdim(reshape(linspace(1,NumCells,NumCells),NumRows,NumCols,NumLays),1); %Matrix identifying the number of each cell
Boundary.Cols = linspace(xmin,xmax,NumCols+1); %Boundaries of each cell
Boundary.Rows = linspace(ymin,ymax,NumRows+1);
Boundary.Lays = linspace(zmin,zmax,NumLays+1);
Test.CellId = getCellId3d([Test.x;Test.y;Test.z],Boundary,CellIdTable); %Assigns a cell id number to each test point
clear xmin ymin Mins Maxs xmax ymax NumRows NumCols res


%Assigns Data to the relevent Cells
CellData = populateCellData3D(X,y,CellIdTable,Boundary,ExpandCellRatio)
save Data/CellData CellData

%% Initialise GP

gp.cov_funs={@covMat3 @covMat3}
gp.npar = [4 4]
params = [0.9846    0.9042    1.0304    0.9294    0.6973    1.2188    1.2630    0.6812    0.0288]

gp.par = params(1:end-1);
gp.noise = params(end);
clear params

%% Evaluate Active Data Set and GP model (inv(K)) for each cell
tic
CellModel = evalActiveSetOccMap2(CellData,CellIdTable,Boundary,ExpandCellRatio,gp,squashparams,order,Threshold,MaxLinesPerCell,NumSamples,NodeWeightArray)
time1 = toc
save Data/CellModel CellModel
    
%% Evaluate Test Points

%Initialise mean and variance functions
mf = zeros(size(Test.x)); 
vf = ones(size(Test.x))*2; % Large number to show no info is uncertain
S2 = vf - gp.noise^2; 
ProbOcc= 0.5*ones(size(Test.x));
tic
for CellNum=1:NumCells
    CellNum
    if numel(CellModel{CellNum}.ActiveSet.L)>0
      Cell.x = Test.x(find(Test.CellId == CellNum));
      Cell.y = Test.y(find(Test.CellId == CellNum));
      Cell.z = Test.z(find(Test.CellId == CellNum));         
        
        [Cell.mf,Cell.vf] =gpevalSeqOccIntquad(CellModel{CellNum}.ActiveSet,gp,[Cell.x;Cell.y;Cell.z],order,NodeWeightArray);
       Cell.S2 = Cell.vf - gp.noise^2; 
       Cell.Squashedmf = Cell.mf;
       mf(find(Test.CellId == CellNum))=Cell.mf;
       vf(find(Test.CellId == CellNum))=Cell.vf;
           
       S2(find(Test.CellId == CellNum)) = vf(find(Test.CellId == CellNum)) - gp.noise^2; 
       Cell.SquashedMean= Cell.mf;
       Cell.SquashedMean((find(Cell.mf >1))) = 1;
       Cell.SquashedMean((find(Cell.mf <-1))) = -1;
       
       Cell.ProbOcc = sigmoid(squashparams(1),squashparams(2),1,...
           Cell.SquashedMean,Cell.vf);
       ProbOcc(find(Test.CellId == CellNum))=Cell.ProbOcc;
       
    end
     
end

time = toc

S2reshape = reshape(S2,size(Mesh.x));
ProbOccreshape = reshape(ProbOcc,size(Mesh.x));

figure; visualise_anomaly(Mesh.x,Mesh.y,Mesh.z,ProbOccreshape)

spacing_x = mean(diff(Mesh.x(1,:,1)))
spacing_y = mean(diff(Mesh.y(:,1,1)))
spacing_z = mean(diff(Mesh.z(1,1,:)))
minx = min(Mesh.x(1,:,1));maxx = max(Mesh.x(1,:,1));
miny = min(Mesh.y(:,1,1));maxy = max(Mesh.y(:,1,1));
minz = min(Mesh.z(1,1,:));maxz = max(Mesh.z(1,1,:));
[XEdges,YEdges,ZEdges] = meshgrid(minx-spacing_x/2:spacing_x:maxx+spacing_x/2,...
    miny-spacing_y/2:spacing_y:maxy+spacing_y/2,...
    minz-spacing_z/2:spacing_z:maxz+spacing_z/2)
figure; visualise_3D(XEdges,YEdges,ZEdges,ProbOccreshape)



figure
NumSlices = 15
MinSlice = min(min(min(Mesh.z)))
MaxSlice = max(max(max(Mesh.z)))
for i = 1:NumSlices
    
    Level = (i-1)*(MaxSlice-MinSlice)/NumSlices-min(min(min(Mesh.z)))
    hz = slice(Mesh.x,Mesh.y,Mesh.z,ProbOccreshape,[],[],Level);
    set(hz,'FaceColor','interp','EdgeColor','none')
    title('Probability of Occupancy Vs Location')
    hold on
    view(0,90)
    colorbar
    pause(0.5)
end

figure
for i = 1:NumSlices
    
    Level = (i-1)*(MaxSlice-MinSlice)/NumSlices-min(min(min(Mesh.z)))
    hz = slice(Mesh.x,Mesh.y,Mesh.z,S2reshape,[],[],Level);
    set(hz,'FaceColor','interp','EdgeColor','none')
    title('S2')
    hold on
    pause(0.5)
end


%% Plot only occupied points
    probthreshold = 0.50;
    varthreshold =1;
    varindex = find(vf<varthreshold);
    RemainingOcc = ProbOcc(varindex);
    RemainingCoords = [Test.x(varindex);Test.y(varindex);Test.z(varindex)];

    
    OccIndex = find(RemainingOcc>probthreshold); 
    RemainingCoords = RemainingCoords(:,OccIndex);
    RemainingOcc=RemainingOcc(OccIndex);

    figure
    hold off
    plot3(RemainingCoords(1,:),RemainingCoords(2,:),RemainingCoords(3,:),'.');
    axis equal
    view(0,90)
    
   
    
    %% Save Occupied Points in a text file
%     dlmwrite('OccPoints.txt', RemainingCoords', 'precision','%.4f', 'newline','pc','delimiter',' ')
%     disp('Occupied Points saved in OccPoints.txt')
%     
%     dlmwrite('OccPointsScalar.txt', [RemainingCoords' RemainingCoords(3,:)'], 'precision','%.4f', 'newline','pc','delimiter',' ')
%     disp('Occupied Points with Z axis as colour scalar saved in OccPointsScalar.txt')
%     
%     dlmwrite('TrainingPointsScalar.txt', [TrainingData.OccupiedPoints' TrainingData.OccupiedPoints(3,:)'], 'precision','%.4f', 'newline','pc','delimiter',' ')
%     disp('Training Points with Z axis as colour scalar saved in TrainingPointsScalar.txt')
% 
%     save OccupiedPoints RemainingCoords
    
