
% Uses quadrature and closed form solutio to train the 
%hyperparameters. It uses just the closed form 
%solution to evaluate the test set.
% The observations line segments and the target 
% is the integral along those segments and points 2 D
% Similar to GPOM - unocc line seg occ points

close all
clear all

%% Add Paths
addpath ../General_GP_Files
addpath Data
addpath Functions

%% Initalise Variables
res = 0.25
zres = 0.25
order=3; % Quadrature order
%Thresholds: Lowering the threshold increases the number of points accepted
Threshold.Point = 0.15% 0.15;% Threshold for active set selection for Occ Points
Threshold.Line = 0.15%0.08;% Threshold for active set selection for Occ Points
NumSamples = 10; %Number of samples on the line segment to decide whether to accept or reject
MaxLinesPerCell = 200;
NumRows = 2; %For the cells
NumCols = 2;
NumLays = 2;
ExpandCellRatio = 1 % Grows cells to overlap with others around it. 1 = no growth


%% Load Data
load NodeWeightArray
% load Outdoor3DSparseLibraryOfTrainingData
load RoomBitComplexLibraryOfTrainingData
% load ACFRParkPruned

load SquashParameters
% load squashparamsmanualNov2010

% load CleanRoomPoseNumber
% load params3sqexp2D

X.Start = TrainingData.LibraryOfStartPoints
X.End = TrainingData.LibraryOfEndPoints
X.Point =  TrainingData.OccupiedPoints;
X.PoseNum = ones(1,size(X.Start,2));
clear PoseNumber

y.Segment =  -1*ones(1,size(X.Start,2));
y.Point = 1*ones(1,size(X.Start,2));

% Create target vector for line segments
y.IntSeg = y.Segment .*sqrt(sum((X.Start-X.End).^2,1));

figure(1)
    plot3([X.Start(1,:);X.End(1,:)],[X.Start(2,:);X.End(2,:)],[X.Start(3,:);X.End(3,:)],'b');
%     plot3([X.Start(1,:);X.End(1,:)],[X.Start(2,:);X.End(2,:)],[y.IntSeg;y.IntSeg],'b');
    hold on
%     plot3(X.Point(1,:),X.Point(2,:),[y.Point;y.Point],'g+')
    plot3(X.Point(1,:),X.Point(2,:),X.Point(3,:),'g+')
    view(0,90)
    title('Line Observations')

pause(1)

%% Generate Test Points and Cells 

res = 0.2
    Mins = min([X.Start X.End]');
    xmin = Mins(1)-1;
    ymin = Mins(2)-1;
    zmin = Mins(3)-1;
    Maxs = max([X.Start X.End]');
    xmax = Maxs(1)+1;
    ymax = Maxs(2)+1;
    zmax = Maxs(3)+1;

    [Mesh.x Mesh.y Mesh.z] = meshgrid(xmin:res:xmax,ymin:res:ymax,zmin:res:zmax);
    Test.x = reshape(Mesh.x,[1 numel(Mesh.x)]);
    Test.y = reshape(Mesh.y,[1 numel(Mesh.y)]);
    Test.z = reshape(Mesh.z,[1 numel(Mesh.z)]);

   
    %Divide Maps into Cells
    NumCells = NumCols*NumRows*NumLays
    CellIdTable = flipdim(reshape(linspace(1,NumCells,NumCells),NumRows,NumCols,NumLays),1);
    Boundary.Cols = linspace(xmin,xmax,NumCols+1);
    Boundary.Rows = linspace(ymin,ymax,NumRows+1);
    Boundary.Lays = linspace(zmin,zmax,NumLays+1);
    Test.CellId = getCellId3d([Test.x;Test.y;Test.z],Boundary,CellIdTable);
% clear xmin ymin Mins Maxs xmax ymax NumRows NumCols NumCells res


% load CellDataObsNum
CellData = populateCellData3D(X,y,CellIdTable,Boundary,ExpandCellRatio)
save CellDataSparse CellData

%% Initialise GP

% gp.cov_funs={@cov_sqexp,@cov_sqexp,@cov_sqexp}
% gp.npar = [3 3 3]

gp.cov_funs={@covMat3 @covMat3}
gp.npar = [4 4]
% params = [2.7300    1.2360   1 0.7898    2.6022    1.7004  1  0.9815   -0.0009];
params = [0.9846    0.9042    1.0304    0.9294    0.6973    1.2188    1.2630    0.6812    0.0288]

% gp.cov_funs={@cov_nn2}
% gp.npar = [4]
% params = [-0.0001   -0.0082   0.9411    4.2932    0.0100];

gp.par = params(1:end-1);
gp.noise = params(end);
clear params

%% Build Occ Map By Inputting Each Scan Sequentially
NumCells = numel(CellIdTable);
disp('Number of Cells')
disp(NumCells)

%% Evaluate Active Set
% CellModel = evalCellModel(CellData,CellIdTable,Boundary,ExpandCellRatio,gp,squashparams,order)
tic
CellModel = evalActiveSetOccMap2(CellData,CellIdTable,Boundary,ExpandCellRatio,gp,squashparams,order,Threshold,MaxLinesPerCell,NumSamples,NodeWeightArray)
time1 = toc
save CellModelReviewReal CellModel
    
%% Evaluate Test Points
% Reses = [1 0.35 0.28 0.23 0.2]
Reses = [2 1 0.7 0.6 0.53 0.45]
TimeArray = []
for row = 1:5
for j=1:6
    
    res = Reses(j);
    
    Mins = min([X.Start X.End]');
    xmin = Mins(1)-1;
    ymin = Mins(2)-1;
    zmin = Mins(3)-1;
    Maxs = max([X.Start X.End]');
    xmax = Maxs(1)+1;
    ymax = Maxs(2)+1;
    zmax = Maxs(3)+1;

    [Mesh.x Mesh.y Mesh.z] = meshgrid(xmin:res:xmax,ymin:res:ymax,zmin:res:zmax);
    Test.x = reshape(Mesh.x,[1 numel(Mesh.x)]);
    Test.y = reshape(Mesh.y,[1 numel(Mesh.y)]);
    Test.z = reshape(Mesh.z,[1 numel(Mesh.z)]);
    ResArray(j) = (size(Test.x,2));


%     ResArray(j) = size(Test.x,2)
    %Divide Maps into Cells
    NumCells = NumCols*NumRows*NumLays
    CellIdTable = flipdim(reshape(linspace(1,NumCells,NumCells),NumRows,NumCols,NumLays),1);
    Boundary.Cols = linspace(xmin,xmax,NumCols+1);
    Boundary.Rows = linspace(ymin,ymax,NumRows+1);
    Boundary.Lays = linspace(zmin,zmax,NumLays+1);
    Test.CellId = getCellId3d([Test.x;Test.y;Test.z],Boundary,CellIdTable);

    mf = zeros(size(Test.x));
    vf = ones(size(Test.x))*2; % Large number to show no info is uncertain
    S2 = vf - gp.noise^2; 
    ProbOcc= 0.5*ones(size(Test.x));
    tic
    for CellNum=1:NumCells
        CellNum
        if numel(CellModel{CellNum}.ActiveSet.L)>0
          Cell.x = Test.x(find(Test.CellId == CellNum));
          Cell.y = Test.y(find(Test.CellId == CellNum));
          Cell.z = Test.z(find(Test.CellId == CellNum));


            [Cell.mf,Cell.vf] =gpevalSeqOccIntquad(CellModel{CellNum}.ActiveSet,gp,[Cell.x;Cell.y;Cell.z],order,NodeWeightArray);
           Cell.S2 = Cell.vf - gp.noise^2; 
           Cell.Squashedmf = Cell.mf;
           mf(find(Test.CellId == CellNum))=Cell.mf;
           vf(find(Test.CellId == CellNum))=Cell.vf;

           S2(find(Test.CellId == CellNum)) = vf(find(Test.CellId == CellNum)) - gp.noise^2; 
           Cell.SquashedMean= Cell.mf;
           Cell.SquashedMean((find(Cell.mf >1))) = 1;
           Cell.SquashedMean((find(Cell.mf <-1))) = -1;

           Cell.ProbOcc = sigmoid(squashparams(1),squashparams(2),1,...
               Cell.SquashedMean,Cell.vf);
           ProbOcc(find(Test.CellId == CellNum))=Cell.ProbOcc;

        end

    end

    TimeArray(row,j) = toc
end
end


ResArray1=ResArray;
TimeArray1=TimeArray;
figure; 
hold on;
plot(ResArray1,TimeArray1+69,'k+','MarkerSize',20,'LineWidth',4)
plot(ResArray1,TimeArray1+69,'k-.','LineWidth',4)
plot(ResArray1,TimeArray1,'+','MarkerSize',20,'LineWidth',4)
plot(ResArray1,TimeArray1,'LineWidth',4)
plot(ResArray1,[  1.4190    10.1880...
    29.5965   46.5194 68.7186  108.2529],'r+','MarkerSize',20,'LineWidth',4)
plot(ResArray1,[  1.4190    10.1880...
    29.5965   46.5194 68.7186  108.2529],'r--','LineWidth',4)
xlabel('Number Of Query Points')
ylabel('Time (seconds)')
errorbar(ResArray1,mean(TimeArray),sqrt(var(TimeArray)))


S2reshape = reshape(S2,size(Mesh.x));
ProbOccreshape = reshape(ProbOcc,size(Mesh.x));
figure

NumSlices = 15
MinSlice = min(min(min(Mesh.z)))
MaxSlice = max(max(max(Mesh.z)))
for i = 1:NumSlices
    
    Level = (i-1)*(MaxSlice-MinSlice)/NumSlices-min(min(min(Mesh.z)))
    hz = slice(Mesh.x,Mesh.y,Mesh.z,ProbOccreshape,[],[],Level);
    set(hz,'FaceColor','interp','EdgeColor','none')
    title('Mean')
    hold on
    view(0,90)
    colorbar
    pause(0.5)
end

figure
for i = 1:NumSlices
    
    Level = (i-1)*(MaxSlice-MinSlice)/NumSlices-min(min(min(Mesh.z)))
    hz = slice(Mesh.x,Mesh.y,Mesh.z,S2reshape,[],[],Level);
    set(hz,'FaceColor','interp','EdgeColor','none')
    title('S2')
    hold on
    pause(0.5)
end


    probthreshold = 0.50;
    varthreshold =1;
    varindex = find(vf<varthreshold);
    RemainingOcc = ProbOcc(varindex);
    RemainingCoords = [Test.x(varindex);Test.y(varindex);Test.z(varindex)];

    
    OccIndex = find(RemainingOcc>probthreshold); 
    RemainingCoords = RemainingCoords(:,OccIndex);
    RemainingOcc=RemainingOcc(OccIndex);
%     S = repmat(1*10,size(RemainingCoords,2),1)';

    figure
    hold off
    plot3(RemainingCoords(1,:),RemainingCoords(2,:),RemainingCoords(3,:),'.');
    axis equal
    view(0,90)
    
    
%     dlmwrite('OccPoints.txt', RemainingCoords', 'precision','%.4f', 'newline','pc','delimiter',' ')
%     disp('Occupied Points saved in OccPoints.txt')
%     
%     dlmwrite('OccPointsScalar.txt', [RemainingCoords' RemainingCoords(3,:)'], 'precision','%.4f', 'newline','pc','delimiter',' ')
%     disp('Occupied Points with Z axis as colour scalar saved in OccPointsScalar.txt')
%     
%     dlmwrite('TrainingPointsScalar.txt', [TrainingData.OccupiedPoints' TrainingData.OccupiedPoints(3,:)'], 'precision','%.4f', 'newline','pc','delimiter',' ')
%     disp('Training Points with Z axis as colour scalar saved in TrainingPointsScalar.txt')
% 
%     save OccupiedPoints RemainingCoords
    
    
    
    
    
    

% NumActivePoints = 0
% NumEmptyCells = 0
% for i=1:500
%     i
%     CellModel{i}.ActiveSet
% %     if numel(CellModel{i}.ActiveSet.X) >0
%     try
%      NumActivePoints = NumActivePoints + numel(CellModel{i}.ActiveSet.yPoint)
%     catch
%     NumEmptyCells = NumEmptyCells+1;
%     end
%     %     if numel(CellData{i}.RedundantSetSize >0)
% %    CellData{i}.RedundantSetSize
% %     end
% %     pause
% end
