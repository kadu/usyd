
function visualise_anomaly(X,Y,Z,V,OpaquePower,NumShells)


if nargin < 5
    OpaquePower = 1;
    NumShells = 5;
elseif nargin < 6
    NumShells = 5;
end

AddToBoundary = 0.05; % increase the boundary of the visualisation by a fraction of the range

figure; clf;

% Define the levels at which you want to plot layers
MaxV = max(max(max(V))); MinV = min(min(min(V)));
levels = linspace(MinV,MaxV,NumShells+2);
levels = levels(2:end-1);


% Build a colormap
cmap = jet(NumShells); %hsv(nlevels); %hot(nlevels); %gray(nlevels); copper(nlevels)
hh = zeros(1, numel(levels));
 
AbsMax = max(abs(MaxV),abs(MinV));
MinX = min(min(min(X)));  MaxX = max(max(max(X)));
MinY = min(min(min(Y)));  MaxY = max(max(max(Y)));
MinZ = min(min(min(Z)));  MaxZ = max(max(max(Z)));
XExtra = AddToBoundary*(abs(MaxX)-abs(MinX));
YExtra = AddToBoundary*(abs(MaxY)-abs(MinY));
ZExtra = AddToBoundary*(abs(MaxZ)-abs(MinZ));

for ii = 1:NumShells
    camlight
    lighting gouraud
    hh(ii) = patch(isosurface(X, Y, Z, V, levels(ii)));
    set(hh(ii), 'Facecolor', cmap(ii,:), 'Edgecolor', 'none', 'facealpha', (1-(AbsMax-abs(levels(ii)))/AbsMax)^(1/OpaquePower));
    axis([MinX-XExtra*sign(MinX) MaxX+XExtra*sign(MaxX) MinY-YExtra*sign(MinY) MaxY+YExtra*sign(MaxY) MinZ-ZExtra*sign(MinZ) MaxZ+ZExtra*sign(MaxZ)])
    caxis([MinV,MaxV]);
    xlabel('Longitude');ylabel('Latitude');zlabel('-Depth')
    colorbar
    pause(0.5)
end
pause(1)
 


