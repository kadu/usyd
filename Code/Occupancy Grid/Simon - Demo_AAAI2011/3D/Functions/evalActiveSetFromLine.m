




function [ActiveSet RedundantSet] = evalActiveSetFromLine(ActiveSet, RedundantSet,...
    Candidate, gp, Thresh, order, NumSamps,squashparams,Boundary,NodeWeightArray)


    TestPoints = [linspace(Candidate.Start(1),Candidate.End(1),NumSamps);...
        linspace(Candidate.Start(2),Candidate.End(2),NumSamps);...
        linspace(Candidate.Start(3),Candidate.End(3),NumSamps)];
    
    InBoxInd = find((TestPoints(1,:)>Boundary.x.min).*(TestPoints(1,:)<Boundary.x.max)...
            .*(TestPoints(2,:)>Boundary.y.min).*(TestPoints(2,:)<Boundary.y.max)...
            .*(TestPoints(3,:)>Boundary.z.min).*(TestPoints(3,:)<Boundary.z.max)==1);
        TestPoints = TestPoints(:,InBoxInd);
    
    
    %Evaluate mf and vf before candidate is added
    [mfBefore,vfBefore] = gpevalSeqOccIntquad(ActiveSet, gp,TestPoints, order,NodeWeightArray);
    SquashedBefore= mfBefore;
    SquashedBefore((find(SquashedBefore >1))) = 1;
    SquashedBefore((find(SquashedBefore<-1))) = -1;
    ProbOccBefore= sigmoid(squashparams(1),squashparams(2),1,SquashedBefore,vfBefore);
    
    ActivePlusCandSet = mergeScan2Set(ActiveSet,Candidate,gp,order,NodeWeightArray);
    
    
    %Evaluate mf and vf after candidate is added
    [mfAfter,vfAfter] = gpevalSeqOccIntquad(ActivePlusCandSet,gp,TestPoints, order,NodeWeightArray);   
     SquashedAfter= mfAfter;
    SquashedAfter((find(SquashedAfter >1))) = 1;
    SquashedAfter((find(SquashedAfter<-1))) = -1;
    ProbOccAfter= sigmoid(squashparams(1),squashparams(2),1,SquashedAfter,vfAfter);
%     pause

%     %Check KL Divergence
%     DLine = max(0.5*(log(vfAfter./vfBefore)+...
%         (mfAfter.^2+mfBefore.^2-2*mfBefore.*mfAfter+vfBefore)./vfAfter-1));
  
%     if DLine > Thresh  
    if max(abs(ProbOccAfter-ProbOccBefore)) > Thresh  
        %Add candidate to active set
        ActiveSet = ActivePlusCandSet;
%          else  
%         RedundantSet=mergeScan2Set(RedundantSet,Candidate,gp,order,NodeWeightArray);
    end
    
    