


function [CellId] = getCellId3d(Location,Boundary,CellIdTable)

NumLoc = size(Location,2);
XLocations = Location(1,:)';
ColumnInd = sum(XLocations(:,ones(1,size(Boundary.Cols,2)))>=Boundary.Cols(ones(1,NumLoc),:),2);

YLocations = Location(2,:)';
RowInd = sum(YLocations(:,ones(1,size(Boundary.Rows,2)))>=Boundary.Rows(ones(1,NumLoc),:),2);
RowIndCorrected = size(Boundary.Rows,2)-RowInd;

ZLocations = Location(3,:)';
LayerInd = sum(ZLocations(:,ones(1,size(Boundary.Lays,2)))>=Boundary.Lays(ones(1,NumLoc),:),2);


Index = sub2ind(size(CellIdTable),RowIndCorrected,ColumnInd,LayerInd);
CellId=CellIdTable(Index)';
