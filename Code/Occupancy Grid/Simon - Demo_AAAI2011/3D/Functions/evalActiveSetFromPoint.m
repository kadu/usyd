


function [ActiveSet RedundantSet] = evalActiveSetFromPoint(ActiveSet, RedundantSet,...
    Candidate, gp, Thresh, order,squashparams,NodeWeightArray)

    TestPoint = Candidate.Point;
    %Evaluate mean and variance before candidate is added
    [mfBefore,vfBefore] = gpevalSeqOccIntquad(ActiveSet, gp,TestPoint, order,NodeWeightArray);
    
    SquashedBefore= mfBefore;
    SquashedBefore((find(SquashedBefore >1))) = 1;
    SquashedBefore((find(SquashedBefore<-1))) = -1;
    ProbOccBefore= sigmoid(squashparams(1),squashparams(2),1,SquashedBefore,vfBefore);
    
    %Add the Candidate Point to the rest of the Active Set and see if
    %there's and improvement
    ActivePlusCandSet = mergeScan2Set(ActiveSet,Candidate,gp,order,NodeWeightArray);
    
    
    %Evaluate mean function and variance after candidate is added
    [mfAfter,vfAfter] = gpevalSeqOccIntquad(ActivePlusCandSet,gp,TestPoint, order,NodeWeightArray);   
    SquashedAfter= mfAfter;
    SquashedAfter((find(SquashedAfter >1))) = 1;
    SquashedAfter((find(SquashedAfter<-1))) = -1;
    ProbOccAfter= sigmoid(squashparams(1),squashparams(2),1,SquashedAfter,vfAfter);
       
%     %Check KL Divergence
%     DPoint = 0.5*(log(vfAfter/vfBefore)+...
%         (mfAfter^2+mfBefore^2-2*mfBefore*mfAfter+vfBefore)/vfAfter-1);
%     pause(0.1)
    
%     if DPoint > Thresh 
    if abs(ProbOccAfter-ProbOccBefore)>Thresh
        %Add candidate to active set
        ActiveSet = ActivePlusCandSet;
%          else  
%         RedundantSet=mergeScan2Set(RedundantSet,Candidate,gp,order,NodeWeightArray);
    end
    
    