

function CellData = populateCellData3D(X,y,CellIdTable,Boundary,ExpandCellRatio)

for CellNum =1:numel(CellIdTable)
%    figure(6); 
%    clf
    CellNum
   [a b c] =ind2sub(size(CellIdTable),CellNum);
    CellXBoundMin = Boundary.Cols(b);
    CellXBoundMax = Boundary.Cols(b+1);
    CellYBoundMin = Boundary.Rows(a);
    CellYBoundMax = Boundary.Rows(a+1);
    CellZBoundMin = Boundary.Lays(c);
    CellZBoundMax = Boundary.Lays(c+1);
%      figure(6)
%     hold on
%     plot([CellXBoundMin CellXBoundMax], [CellYBoundMin CellYBoundMin],'r')
%     plot([CellXBoundMin CellXBoundMax], [CellYBoundMax CellYBoundMax],'r')
%     plot([CellXBoundMin CellXBoundMin], [CellYBoundMin CellYBoundMax],'r')
%     plot([CellXBoundMax CellXBoundMax], [CellYBoundMin CellYBoundMax],'r')
%     pause
    CellWidth = (CellXBoundMax -CellXBoundMin)*(ExpandCellRatio-1);
    CellLength = (CellYBoundMax - CellYBoundMin)*(ExpandCellRatio-1);
    CellHeight = (CellZBoundMax - CellZBoundMin)*(ExpandCellRatio-1);
    CellXBoundMin = CellXBoundMin - CellWidth/2;
    CellXBoundMax = CellXBoundMax + CellWidth/2;
    CellYBoundMin = CellYBoundMin - CellLength/2;
    CellYBoundMax = CellYBoundMax + CellLength/2;
    CellZBoundMin = CellZBoundMin - CellHeight/2;
    CellZBoundMax = CellZBoundMax + CellHeight/2;
    

%     plot3([CellXBoundMin CellXBoundMax], [CellYBoundMin CellYBoundMin],[CellZBoundMin CellZBoundMin],'b')
%     plot3([CellXBoundMin CellXBoundMax], [CellYBoundMax CellYBoundMax],[CellZBoundMin CellZBoundMin],'b')
%     plot3([CellXBoundMin CellXBoundMin], [CellYBoundMin CellYBoundMax],[CellZBoundMin CellZBoundMin],'b')
%     plot3([CellXBoundMax CellXBoundMax], [CellYBoundMin CellYBoundMax],[CellZBoundMin CellZBoundMin],'b')
%     plot3([CellXBoundMin CellXBoundMax], [CellYBoundMin CellYBoundMin],[CellZBoundMax CellZBoundMax],'b')
%     plot3([CellXBoundMin CellXBoundMax], [CellYBoundMax CellYBoundMax],[CellZBoundMax CellZBoundMax],'b')
%     plot3([CellXBoundMin CellXBoundMin], [CellYBoundMin CellYBoundMax],[CellZBoundMax CellZBoundMax],'b')
%     plot3([CellXBoundMax CellXBoundMax], [CellYBoundMin CellYBoundMax],[CellZBoundMax CellZBoundMax],'b')
%     
    
    %     pause
   
    CellData{CellNum}.X.Point = [];
    CellData{CellNum}.X.PointID = [];
    CellData{CellNum}.X.Start = [];
    CellData{CellNum}.X.End = [];
    CellData{CellNum}.X.LineID = [];
    CellData{CellNum}.y.Point = [];
    CellData{CellNum}.y.Segment = [];
    CellData{CellNum}.y.IntSeg = [];
    
    
    %check OccPoints first
    for obs = 1:size(X.Point,2)
        
        %Discretise line
         LineDiscretX = linspace2(X.Start(1,obs),X.End(1,obs),20);
         LineDiscretY = linspace2(X.Start(2,obs),X.End(2,obs),20);
         LineDiscretZ = linspace2(X.Start(3,obs),X.End(3,obs),20);
        
%         plot3(LineDiscretX,LineDiscretY,LineDiscretZ,'r+')
%         plot(X.Point(1,obs),X.Point(2,obs),'b+')
        
        if(X.Point(1,obs)>CellXBoundMin)*(X.Point(1,obs)<CellXBoundMax)...
            *(X.Point(2,obs)>CellYBoundMin)*(X.Point(2,obs)<CellYBoundMax)...
             *(X.Point(3,obs)>CellZBoundMin)*(X.Point(3,obs)<CellZBoundMax)
%         CellNum
%         obs
        CellData{CellNum}.X.Point = [CellData{CellNum}.X.Point  X.Point(:,obs)];
        CellData{CellNum}.y.Point = [CellData{CellNum}.y.Point  y.Point(obs)];
        CellData{CellNum}.X.PointID = [CellData{CellNum}.X.PointID  obs];
%        disp(' point accepted')
%        pause
        else
%             disp(' point rejected')
            
        end
        
        

         if sum((LineDiscretX>CellXBoundMin).*(LineDiscretX<CellXBoundMax)...
            .*(LineDiscretY>CellYBoundMin).*(LineDiscretY<CellYBoundMax)...
            .*(LineDiscretZ>CellZBoundMin).*(LineDiscretZ<CellZBoundMax));
        
            CellData{CellNum}.X.Start = [CellData{CellNum}.X.Start  X.Start(:,obs)];
            CellData{CellNum}.X.End = [CellData{CellNum}.X.End  X.End(:,obs)];
            CellData{CellNum}.y.Segment = [CellData{CellNum}.y.Segment  y.Segment(obs)];
            CellData{CellNum}.y.IntSeg = [CellData{CellNum}.y.IntSeg  y.IntSeg(obs)];
            CellData{CellNum}.X.LineID =[CellData{CellNum}.X.LineID obs];
%             figure(98)
%             clf
%             hold on
%                 plot3([CellXBoundMin CellXBoundMax], [CellYBoundMin CellYBoundMin],[CellZBoundMin CellZBoundMin],'b')
%     plot3([CellXBoundMin CellXBoundMax], [CellYBoundMax CellYBoundMax],[CellZBoundMin CellZBoundMin],'b')
%     plot3([CellXBoundMin CellXBoundMin], [CellYBoundMin CellYBoundMax],[CellZBoundMin CellZBoundMin],'b')
%     plot3([CellXBoundMax CellXBoundMax], [CellYBoundMin CellYBoundMax],[CellZBoundMin CellZBoundMin],'b')
%     plot3([CellXBoundMin CellXBoundMax], [CellYBoundMin CellYBoundMin],[CellZBoundMax CellZBoundMax],'b')
%     plot3([CellXBoundMin CellXBoundMax], [CellYBoundMax CellYBoundMax],[CellZBoundMax CellZBoundMax],'b')
%     plot3([CellXBoundMin CellXBoundMin], [CellYBoundMin CellYBoundMax],[CellZBoundMax CellZBoundMax],'b')
%     plot3([CellXBoundMax CellXBoundMax], [CellYBoundMin CellYBoundMax],[CellZBoundMax CellZBoundMax],'b')
%     
%           
%             plot3([X.Start(1,obs);X.End(1,obs)],[X.Start(2,obs);X.End(2,obs)],[X.Start(3,obs);X.End(3,obs)],'r')
%     pause
%             disp(' line accepted')
%          pause
         else
%              disp(' line rejected')
         end
          
%         plot(X.Point(1,obs),X.Point(2,obs),'g+')
%         plot(LineDiscretX,LineDiscretY,'g+')
    end
  
    
    figure(98)
    
       clf
    hold on
       
        plot3([CellXBoundMin CellXBoundMax], [CellYBoundMin CellYBoundMin],[CellZBoundMin CellZBoundMin],'b')
    plot3([CellXBoundMin CellXBoundMax], [CellYBoundMax CellYBoundMax],[CellZBoundMin CellZBoundMin],'b')
    plot3([CellXBoundMin CellXBoundMin], [CellYBoundMin CellYBoundMax],[CellZBoundMin CellZBoundMin],'b')
    plot3([CellXBoundMax CellXBoundMax], [CellYBoundMin CellYBoundMax],[CellZBoundMin CellZBoundMin],'b')
    plot3([CellXBoundMin CellXBoundMax], [CellYBoundMin CellYBoundMin],[CellZBoundMax CellZBoundMax],'b')
    plot3([CellXBoundMin CellXBoundMax], [CellYBoundMax CellYBoundMax],[CellZBoundMax CellZBoundMax],'b')
    plot3([CellXBoundMin CellXBoundMin], [CellYBoundMin CellYBoundMax],[CellZBoundMax CellZBoundMax],'b')
    plot3([CellXBoundMax CellXBoundMax], [CellYBoundMin CellYBoundMax],[CellZBoundMax CellZBoundMax],'b')
    

       
       if numel(CellData{CellNum}.X.Start)>0
       Start = CellData{CellNum}.X.Start;
       End = CellData{CellNum}.X.End;
       plot3([Start(1,:);End(1,:)],[Start(2,:);End(2,:)],[Start(3,:);End(3,:)],'g')
       end
       if numel(CellData{CellNum}.X.Point)>0
       Point = CellData{CellNum}.X.Point;
       plot3(Point(1,:),Point(2,:),Point(3,:),'r+')
       end
%         pause
        
    
    
    
    
    
end


%  LineDiscret.x = linspace2(X.Start(1,:),X.End(1,:),10)
% LineDiscret.y = linspace2(X.Start(2,:),X.End(2,:),10)
% for i = 1:40
%     i
%     CellData{i}.X
%     pause
% end