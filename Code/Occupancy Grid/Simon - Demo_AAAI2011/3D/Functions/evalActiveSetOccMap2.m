
function [CellData] = evalActiveSetOccMap(CellData,CellIdTable,Boundary,ExpandCellRatio,gp,squashparams,order,Threshold,MaxLinesPerCell,NumSamples,NodeWeightArray)

CellNum = 0
NumCells = size(CellData,2);
for CellNum = CellNum+1:NumCells 
% CellNum = 175
CurrentCell = CellData{CellNum};
  
%% Intialise variables
CellData{CellNum}.ActiveSet.X = [];
CellData{CellNum}.ActiveSet.y = [];
CellData{CellNum}.ActiveSet.ObsId=[];
CellData{CellNum}.ActiveSet.alpha=[];
CellData{CellNum}.ActiveSet.L=[];

CellData{CellNum}.RedundantSet.X = [];
CellData{CellNum}.RedundantSet.y = [];
CellData{CellNum}.RedundantSet.ObsId=[];
CellData{CellNum}.RedundantSet.alpha=[];
CellData{CellNum}.RedundantSet.L=[];

    CellNum
    
%% Determine Boundaries of Cells (This might already have been done in the
% main script)
    [a b c] =ind2sub(size(CellIdTable),CellNum);
    Boundary.x.min = Boundary.Cols(b);
    Boundary.x.max = Boundary.Cols(b+1);
    Boundary.y.min = Boundary.Rows(a);
    Boundary.y.max = Boundary.Rows(a+1);
    c=1;
    Boundary.z.min = Boundary.Lays(c);
    Boundary.z.max = Boundary.Lays(c+1);
    CellWidth = ( Boundary.x.max - Boundary.x.min)*(ExpandCellRatio-1);
    CellLength = ( Boundary.y.max -  Boundary.y.min)*(ExpandCellRatio-1);
    CellHeight = ( Boundary.z.max -  Boundary.z.min)*(ExpandCellRatio-1);
    Boundary.x.min = Boundary.x.min - CellWidth/2;
    Boundary.x.max = Boundary.x.max + CellWidth/2;
    Boundary.y.min = Boundary.y.min - CellLength/2;
    Boundary.y.max = Boundary.y.max + CellLength/2;
    Boundary.z.min = Boundary.z.min - CellHeight/2;
    Boundary.z.max = Boundary.z.max + CellHeight/2;
    
    %Subsamples the cell's data if the number of lines exceeds a user-definted limit.
     if size(CellData{CellNum}.X.Start,2)>MaxLinesPerCell %Remove Excess Lines
         ShuffledIndex = shuffle(linspace(1,size(CellData{CellNum}.X.Start,2),size(CellData{CellNum}.X.Start,2)));
         CellDataPruned.X.Start = CellData{CellNum}.X.Start(:,ShuffledIndex(1:MaxLinesPerCell));
         CellDataPruned.X.End = CellData{CellNum}.X.End(:,ShuffledIndex(1:MaxLinesPerCell));
         CellDataPruned.y.IntSeg = CellData{CellNum}.y.IntSeg(:,ShuffledIndex(1:MaxLinesPerCell));
         CellDataPruned.y.Segment = CellData{CellNum}.y.Segment(:,ShuffledIndex(1:MaxLinesPerCell));
     else
         CellDataPruned = CellData{CellNum};

     end
    
     
   %% Find the active set for the cell
    %Makes sure there is data in the cell
    if   max(size(CellData{CellNum}.X.Start,2),size(CellData{CellNum}.X.Point,2))>0  
        Obs = 1;
        FindingActiveSet =1; 
        % Cycles through the observations and adds them to the active set
        % if necessary
       while FindingActiveSet == 1
           MaxNumCycles = max(size(CellDataPruned.X.Start,2),size(CellData{CellNum}.X.Point,2));
            LinesFinished = 0 ; %Switches that are used to break out of the while loop
            PointsFinished = 0;
        
            if Obs==1 %Initial observation in that cell
                 if   size(CellData{CellNum}.X.Start,2)>0
                     InitialObs.Start = CellData{CellNum}.X.Start(:,Obs);
                     InitialObs.End = CellData{CellNum}.X.End(:,Obs);
                     InitialObs.yIntSeg = CellData{CellNum}.y.IntSeg(Obs);
                     InitialObs.LineID = CellData{CellNum}.X.LineID(Obs);

                 else
                    InitialObs.Start = [0;0;0]; % Hack just to fill in initial obs
                    InitialObs.End = CellData{CellNum}.X.Point(:,Obs);
                    InitialObs.yIntSeg = -1*sqrt(sum((InitialObs.Start-InitialObs.End).^2,1));
                    InitialObs.LineID = 0;
                 end
                if size(CellData{CellNum}.X.Point,2)>0
                    InitialObs.Point = CellData{CellNum}.X.Point(:,Obs); 
                    InitialObs.PointID = CellData{CellNum}.X.PointID(Obs);
                    InitialObs.yPoint = CellData{CellNum}.y.Point(Obs);
                else
                     InitialObs.Point = CellData{CellNum}.X.End(:,Obs); %HACK! - included to fill InitialObs
                     InitialObs.yPoint = 1;
                     InitialObs.PointID = 0;
                end
              
               
            %Automatically adds the first observation to the active set
                [CellData{CellNum}.ActiveSet]= initialiseActiveSet(InitialObs, gp, order,NodeWeightArray);
%                 CellData{CellNum}.RedundantSet = CellData{CellNum}.ActiveSet; %Bit of a hack to just initialise
                %RedundantSet is not saved (It was slowing up the code)
                clear InitialObs
                Obs = 2;

                continue            
            end


            %First check a point to see if it should be added to the active
            %set
            if size( CellData{CellNum}.X.Point,2)>=Obs
                CandidatePoint.Start = [];
                CandidatePoint.End = [];
                CandidatePoint.Point = CellData{CellNum}.X.Point(:,Obs); 
                CandidatePoint.yPoint = CellData{CellNum}.y.Point(Obs) ;
                CandidatePoint.ID = CellData{CellNum}.X.PointID(Obs);
                CandidatePoint.yIntSeg = [];

                [CellData{CellNum}.ActiveSet CellData{CellNum}.RedundantSet] = evalActiveSetFromPoint(...
                    CellData{CellNum}.ActiveSet, CellData{CellNum}.RedundantSet,...
                    CandidatePoint, gp, Threshold.Point, order,squashparams,NodeWeightArray);  
                if CandidatePoint.ID == CellData{CellNum}.ActiveSet.ID(end) %Point accempted
                    CandidatePointAccepted = 1;
                else
                    CandidatePointAccepted = 0;
                end
            else
                PointsFinished = 1;
            end

            %Then check a line
             if max(size( CellDataPruned.X.Start,2),size( CellData{CellNum}.X.Point,2))>=Obs
                 ObsIndex = find(CellData{CellNum}.X.LineID == CellData{CellNum}.ActiveSet.ID(end));
                 if CandidatePointAccepted ==1 && numel(ObsIndex)>0%See if an associated line exists in this cell
                         CandidateLine.Start = CellData{CellNum}.X.Start(:,ObsIndex);
                        CandidateLine.End = CellData{CellNum}.X.End(:,ObsIndex);
                        CandidateLine.Point = [] ;
                        CandidateLine.yPoint = [];
                        CandidateLine.ID = CellData{CellNum}.X.LineID(ObsIndex);
                        CandidateLine.yIntSeg = CellData{CellNum}.y.IntSeg(ObsIndex);
                        
                        [CellData{CellNum}.ActiveSet CellData{CellNum}.RedundantSet] = evalActiveSetFromLine(CellData{CellNum}.ActiveSet, CellData{CellNum}.RedundantSet,...
                            CandidateLine, gp, Threshold.Line, order, NumSamples,squashparams,Boundary,NodeWeightArray);
                 elseif size( CellDataPruned.X.Start,2)>=Obs
                        CandidateLine.Start = CellDataPruned.X.Start(:,Obs);
                        CandidateLine.End = CellDataPruned.X.End(:,Obs);
                        CandidateLine.Point = [] ;
                        CandidateLine.yPoint = [];
                        CandidateLine.ID = CellData{CellNum}.X.LineID(Obs);
                        CandidateLine.yIntSeg = CellDataPruned.y.IntSeg(Obs);
                        
                        [CellData{CellNum}.ActiveSet CellData{CellNum}.RedundantSet] = evalActiveSetFromLine(CellData{CellNum}.ActiveSet, CellData{CellNum}.RedundantSet,...
                            CandidateLine, gp, Threshold.Line, order, NumSamples,squashparams,Boundary,NodeWeightArray);
                 else
                        LinesFinished =1;
                        
                 end
%                  figure(98)
%                  plot3([CandidateLine.Start(1,:);CandidateLine.End(1,:)],[CandidateLine.Start(2,:);CandidateLine.End(2,:)],[CandidateLine.Start(3,:);CandidateLine.End(3,:)],'y')
%                  pause
                  
             else
                 LinesFinished =1;
                    
             end
             
             if LinesFinished == 1 && PointsFinished == 1
                 
                FindingActiveSet =0;
           
             end
            Obs = Obs+1;
        end
    
%     CellData{CellNum}.RedundantSetSize=[size(CellData{CellNum}.RedundantSet.y)]; %number of rejected points and lines
%     CellData{CellNum}.RedundantSet=[];
    
%      figure(98)
%     clf
%     hold on 
%     plot3([Boundary.x.min Boundary.x.max], [Boundary.y.min  Boundary.y.min],[Boundary.z.min  Boundary.z.min ],'b')
%     plot3([Boundary.x.min Boundary.x.max], [Boundary.y.max Boundary.y.max],[Boundary.z.min  Boundary.z.min ],'b')
%     plot3([Boundary.x.min Boundary.x.min], [Boundary.y.min Boundary.y.max],[Boundary.z.min  Boundary.z.min ],'b')
%     plot3([Boundary.x.max Boundary.x.max], [Boundary.y.min Boundary.y.max],[Boundary.z.min Boundary.z.min ],'b')
%     plot3([Boundary.x.min Boundary.x.max], [Boundary.y.min Boundary.y.min],[Boundary.z.max Boundary.z.max],'b')
%     plot3([Boundary.x.min Boundary.x.max], [Boundary.y.max Boundary.y.max],[Boundary.z.max Boundary.z.max],'b')
%     plot3([Boundary.x.min Boundary.x.min], [Boundary.y.min Boundary.y.max],[Boundary.z.max Boundary.z.max],'b')
%     plot3([Boundary.x.max Boundary.x.max], [Boundary.y.min Boundary.y.max],[Boundary.z.max Boundary.z.max],'b')
%     
%     
% %        if numel(CellData{CellNum}.X.Start)>0
% %        Start = CellData{CellNum}.X.Start;
% %        End = CellData{CellNum}.X.End;
% %        plot3([Start(1,:);End(1,:)],[Start(2,:);End(2,:)],[Start(3,:);End(3,:)],'g')
% %        end
% %        if numel(CellData{CellNum}.X.Point)>0
% %        Point = CellData{CellNum}.X.Point;
% %        plot3(Point(1,:),Point(2,:),Point(3,:),'r+')
% %        end
%        if sum(CellData{CellNum}.ActiveSet.ObsId)>0
%            Start = CellData{CellNum}.ActiveSet.X(:,CellData{CellNum}.ActiveSet.ObsId==1,1);
%            End = CellData{CellNum}.ActiveSet.X(:,CellData{CellNum}.ActiveSet.ObsId==1,2);
%            plot3([Start(1,:);End(1,:)],[Start(2,:);End(2,:)],[Start(3,:);End(3,:)],'b')
%        end
%        if numel(find(CellData{CellNum}.ActiveSet.ObsId==0))>0
%            Point = CellData{CellNum}.ActiveSet.X(:,CellData{CellNum}.ActiveSet.ObsId==0,1);
%            plot3(Point(1,:),Point(2,:),Point(3,:),'b+')
%        end
%        pause(2)
    end
%    save  CellData3DInProg CellData
%    save  CellNumInProg CellNum
end