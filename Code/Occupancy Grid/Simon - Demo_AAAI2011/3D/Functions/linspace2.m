function y = linspace2(d1, d2, n)
%LINSPACE Linearly spaced vector.
%   LINSPACE(X1, X2) generates a row vector of 100 linearly
%   equally spaced points between X1 and X2.
%
%   LINSPACE(X1, X2, N) generates N points between X1 and X2.
%   For N < 2, LINSPACE returns X2.
%
%   Class support for inputs X1,X2:
%      float: double, single
%
%   See also LOGSPACE, :.

%   Copyright 1984-2004 The MathWorks, Inc. 
%   $Revision: 5.12.4.1 $  $Date: 2004/07/05 17:01:20 $

if nargin == 2
    n = 100;
end

n = double(n);
spacing = (0:n-2);
diff = (d2-d1)';
d1tran = d1';
d2tran = d2';
y = [d1tran(:,ones(1,length(spacing)))+spacing(ones(1,length(d1)),:).*diff(:,ones(1,length(spacing)))/(floor(n)-1) d2tran];


