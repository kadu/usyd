% Visualisation of 3D meshes using transparent faces.
% 07 Aug 2012
% Alistair Reid

function visualise_3D(X_Edges,Y_Edges,Z_Edges,input, alphamod)
    if (nargin == 4)
        alphamod = 1.0;
    end
    
    % There is a matlab bug when there are nonzeros on the edges
    % workaround : add padding in all dimensions with zeros
            xx = unique(X_Edges)';
            yy = unique(Y_Edges)';
            zz = unique(Z_Edges)';

            % extend
            diffx = xx(2) - xx(1);
            diffy = yy(2) - yy(1);
            diffz = zz(2) - zz(1);
            xx = [-2*diffx, -diffx,  xx, xx(end)+diffx, xx(end)+2*diffx];
            yy = [-2*diffy, -diffy,  yy, yy(end)+diffy, yy(end)+2*diffy];
            zz = [-2*diffz, -diffz,  zz, zz(end)+diffz, zz(end)+2*diffz];

            % re-constitute
            [X_Edges, Y_Edges, Z_Edges] = meshgrid(xx,yy,zz);
            s = size(input)+4;
            inp = zeros(s);
            inp(3:end-2,3:end-2,3:end-2) = input;
            input = inp;
            
    % End workaround
    
    set(gcf,'Renderer','OpenGL')
    scale = 1/max(abs(input(:)));
    val = (input*scale); % is this out of 100?
    order = {[1 2 3], [2 3 1], [1 3 2]};
    for j = 1:3
        perm = order{j};
        xx = permute(X_Edges, perm);
        yy = permute(Y_Edges, perm);
        zz = permute(Z_Edges, perm);
        vv = permute(val, perm); % cell centered grid
        for i=1:size(vv,3) % Slice through dataset
            if (i==1)
                cc = vv(:,:,i);
                vvi = vv(:,:,i);
            else
                % handle negative values now
                cc = max(vv(:,:,i), vv(:,:,i-1));
                cc2 = min(vv(:,:,i), vv(:,:,i-1));
                compare = abs(cc2)>abs(cc);
                cc(compare) = cc2(compare); % always take the largest magnitude
                vvi = abs(vv(:,:,i-1) - vv(:,:,i)); + ...
                      0.2*max(abs(vv(:,:,i-1)), abs(vv(:,:,i)));
            end
            cc = cc/scale;
            vvi = vvi*50*alphamod; % Oddly alpha seems to be 0-100 rather than 0-1
            vvi = vvi([2:end,end,end], [2:end,end,end]); 
            surf(xx(:,:,i), yy(:,:,i), zz(:,:,i),...
                 cc, 'FaceAlpha','flat', 'AlphaDataMapping','direct',...
                 'AlphaData',vvi, 'EdgeAlpha', 0.1,'LineStyle','none');
            hold on
        end
    end
    axis equal
    %axis([min(xx) max(xx) min(yy) max(yy) min(zz) max(zz)])
    

