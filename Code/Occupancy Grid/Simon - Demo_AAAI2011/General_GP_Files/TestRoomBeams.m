
clear all
close all

load CleanRoom1LibraryOfTrainingData
NumOfRays = size(TrainingData.LibraryOfStartPoints,2);
UnOccStart = TrainingData.LibraryOfStartPoints;
UnOccEnd = TrainingData.LibraryOfEndPoints;
OccStart = TrainingData.OccupiedPoints;
OccEnd = TrainingData.OccupiedPoints+0.001*randn(2,NumOfRays);

TrainingSegStart = [UnOccStart OccStart];
TrainingSegEnd = [UnOccEnd OccEnd];
target = [-1*ones(1,NumOfRays) 1*ones(1,NumOfRays)];

figure()
plot3([TrainingSegStart(1,:);TrainingSegEnd(1,:)],[TrainingSegStart(2,:);TrainingSegEnd(2,:)],[target;target],'b');
view(180,90)
title('Training Points')
pause

Mins = min([TrainingSegStart TrainingSegEnd]');
xmin = Mins(1);
ymin = Mins(2);
Maxs = max([TrainingSegStart TrainingSegEnd]');
xmax = Maxs(1);
ymax = Maxs(2);


%Test Points
res = 0.25
[x y] = meshgrid(xmin:res:xmax,ymin:res:ymax);

xvec = reshape(x,[1 numel(x)]);
yvec = reshape(y,[1 numel(y)]);


% Define the covariance functions
cov_funs={@cov_sqexp,@cov_sqexp}
npar = [3 3]
params0 = [1 1 5 0.1 0.1 1 0.01 1 10];
noise = 0.01
order=4;



options = zeros(1,18);
options(1)=1; options(9)=1; options(14)=40;
tic

[params]=gplearnlineseg2DSumFixNoiseOcc(cov_funs,npar,TrainingSegStart,TrainingSegEnd,target,...
    params0,noise,order,options)

toc

params = [params noise]




 for i=1:ceil(size(xvec,2)/1000)  % For single covariance function
   
        PercentageComplete = i*100/(size(xvec,2)/1000);
        if PercentageComplete<100
         disp('Percentage Complete:');
         disp(PercentageComplete);
         pause(0.1)
        end

        ist = (i-1)*1000+1;
        if i*1000<size(xvec,2)
            ien = i*1000;
        else
            ien = size(xvec,2);
        end

[lml,mfline(ist:ien),vfline(ist:ien)] = gpevalLineSeg2DquadccSumOcc(cov_funs,npar,TrainingSegStart,...
    TrainingSegEnd,target,[],params(1:end-1),params(end),[xvec(ist:ien);yvec(ist:ien)],order);

    S2line(ist:ien) = vfline(ist:ien) - params(end)^2; 

 end



 figure();
    surf(x,y,reshape(mfline,[size(x,1) size(x,2)])); hold on;
    title('mf');
    %colormap(gray);
    view(180,90);
    colormap(gray)
    axis equal


    