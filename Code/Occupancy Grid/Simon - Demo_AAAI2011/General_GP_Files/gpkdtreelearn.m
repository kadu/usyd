% Copyright The University of Sydney, 2001 - 2008.  ABN 15 211 513 464
% herafter 'The University'.
%
% The copyright of the computer program(s) herein is the property of
% The University of Sydney, Australia.  The program(s) may be used
% and/or copied only with the written permission of The University of
% Sydney, or in accordance with the terms and conditions stipulated
% in the contract under which the program(s) have been supplied.
% 
% Disclaimer:
% The Universitiy makes no representation or warranty
% that these materials, including any software, are free from errors;
% about the quality or performance of these materials;
% that these materials are fit for any particular purpose.
% These materials are made available on the strict basis that The University
% and its employees or agents have no liability for any direct or indirect
% loss or damage (including for negligence) suffered by any person as a
%consequence of the use of this  material.
%
% Train a GP model with scaled conjugade gradient

function [params, options] = gpkdtreelearn(kfun,kgfun,kpar0,noise,options,nn,actset)

global X y
params = [kpar0 noise];
%[params, options] = scg(@(params) gplmleval(params, kfun), ...
%    params,options,@(params) gpgradeval(params, kgfun, kfun));
%[params, options] = quasinew(@(params) gplmleval(params, kfun), ...
%    params,options,@(params) gpgradeval(params, kgfun, kfun));
 
%Matlab optimisation toolbox 
opts = optimset('LargeScale','on','MaxIter',500,'Diagnostics', 'on',...
'GradObj', 'on', 'Display', 'iter','TolFun',  1e-10, 'MaxFunEvals', 5000);

%KNITRO options
opts_knitro = optimset('Display', 'iter','MaxIter',500, 'GradObj','on', 'TolFun', 1e-10,...
    'MaxFunEvals', 5000,'Algorithm','active-set');

lb=-inf*ones(1,length(params));
lb(end) = -0.1;
ub=inf*ones(1,length(params));
ub(end) = 0.1;
%tic;[params, options]=fminunc(@(params) gplmlgrad(params, kfun, kgfun), params,opts);toc;
%tic;[params, options]=fmincon(@(params) gplmlgrad(params, kfun, kgfun), params,...
%    [],[],[],[],lb,ub,[],opts);toc
 
%Simulated Annealing
%[params, fval] = anneal(@(params) gplmlkdtreegrad(params, kfun, kgfun, nn, actset), params);
%tic;[params, options]=fminunc(@(params) gplmlgrad(params, kfun, kgfun), params,opts);toc;
tic; [params, options]=ktrlink(@(params) gplmlkdtreegrad(params, kfun, kgfun, nn, actset), params,...
    [],[],[],[],[],[],[],opts_knitro);toc;
%tic;[params, options]=fminunc(@(params) gpsrlmlgrad(params, kfun, kgfun, 1:4:20), params,opts);toc;
%Genetic Algorithm Optimization
%[params, fval] = ga(@(params) gplmlgrad(params, kfun, kgfun), length(params));
%disp(fval);

%Simulated Annealing in Matlab
%[params, fval] = simulannealbnd(@(params) gplmlgrad(params, kfun, kgfun), params);
%disp(fval);