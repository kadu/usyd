% Copyright The University of Sydney, 2001 - 2008.  ABN 15 211 513 464
% herafter 'The University'.
%
% The copyright of the computer program(s) herein is the property of
% The University of Sydney, Australia.  The program(s) may be used
% and/or copied only with the written permission of The University of
% Sydney, or in accordance with the terms and conditions stipulated
% in the contract under which the program(s) have been supplied.
% 
% Disclaimer:
% The Universitiy makes no representation or warranty
% that these materials, including any software, are free from errors;
% about the quality or performance of these materials;
% that these materials are fit for any particular purpose.
% These materials are made available on the strict basis that The University
% and its employees or agents have no liability for any direct or indirect
% loss or damage (including for negligence) suffered by any person as a
%consequence of the use of this  material.
%
% Evaluate a Gaussian Process Classifier with Multiple Regressors using
% the subset of regressor approximation.
%
%INPUTS
% X - D x N inputs
% y - 1 x N outputs (targets)
% kfun - covariance function
% kpar - covariance parameters (hyperparameters)
% noise - scalar noise level
% x - D x M test inputs
% nn - number of nearest neighbours
% 
%OUTPUTS
% mf - predictive mean
% vf - predictive variance
% lml - log marginal likelihood
%
% Fabio Tozeto Ramos 02/10/09
% [lml,mf,vf] = gpmcreeval(X,y,kfun,kpar,x,si)
function [p,mf,vf] = gpmcreval(X,y,kfun,kpar,x,si)
N     = size(x,2);  %number of queries 
C     = size(y,1);  %number of classes
mf    = zeros(C,N);
vf    = zeros(C,N);

for i = 1:C
    [mf(i,:),vf(i,:)] = gpsreval(X,y(i,:),kfun{i},...
        kpar{i}(1:end-1),[],kpar{i}(end),x,[],si);
end
%p = hermintlogit(0.85*mf,vf);
p = hermintlogit(kpar{end}*mf,(kpar{end}^2)*vf);

