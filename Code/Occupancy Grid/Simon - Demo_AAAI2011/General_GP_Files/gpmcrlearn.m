% Train a GP classification model with scaled conjugade gradient

function [params, options] = gpmcrlearn(kfun,kgfun,kpar0,kmap,si)

params = cell2mat(kpar0);
%[params, options] = scg(@(params) gplmleval(params, kfun), ...
%    params,options,@(params) gpgradeval(params, kgfun, kfun));
%[params, options] = quasinew(@(params) gplmleval(params, kfun), ...
%    params,options,@(params) gpgradeval(params, kgfun, kfun));
 
%Matlab optimisation toolbox 
opts = optimset('LargeScale','off','MaxIter',500,'Diagnostics', 'on',...
'GradObj', 'on', 'Display', 'iter','TolFun',  1e-10, 'MaxFunEvals', 5000);


%KNITRO options
opts_knitro = optimset('Display', 'iter','MaxIter',100, 'GradObj','on', 'TolFun', 1e-20,...
    'MaxFunEvals', 5000,'Algorithm','active-set');

%lb=0.001*ones(1,size(X,1));
%ub=inf*ones(1,size(X,1));
 
%Simulated Annealing
%[params, fval] = anneal(@(params) gpmcrlmlgrad(params, kfun, [], kmap, si), params);
%disp(fval);
%tic;[params, options]=fminunc(@(params) gpmclaplmlgrad(params, kfun, kgfun), params,opts);toc;

[params, options]=ktrlink(@(params) gpmcrlmlgrad(params, kfun, kgfun, kmap, si), params,...
    [],[],[],[],[],[],[],opts_knitro);



