%Demonstrates mapping from Geophisics to grade using GPs
%Inputs: Density, caliper, mag_sus, gamma
%Output: Iron concentration
clear global
global X y 

if isequal(computer,'GLNXA64')
    direc = '~/sshfs/Datasets/WAA Test Site/';
else
    direc = '~/Datasets/WAA Test Site/';
end
adir = dir(direc);
adir = adir(3:end-1);
disp(adir);
mt = 3; %material (2 or 3 for iron)
inp = [23 24 26 27]; %input columns
training = [6:14 19:21];
testing = [15:17];
Xt = [];
yt = [];
%Assemble training data
for i=training
    data = xlsread([direc adir(i).name]);
    ind = find(data(:,23)~=-99 & data(:,24)~=-99 & data(:,26)~=-99 & data(:,27)~=-99);
    Xt = [Xt data(ind,inp)'];
    yt = [yt data(ind,mt)'];
end
xtest = [];
ytest = [];
%Assemble test data
for i=testing
    data = xlsread([direc adir(i).name]);
    ind = find(data(:,23)~=-99 & data(:,24)~=-99 & data(:,26)~=-99 & data(:,27)~=-99);
    xtest = [xtest data(ind,inp)'];
    ytest = [ytest data(ind,mt)'];
end


%Learn the model
%params0 = [rand(1,7) 1];
%params = [std(X,0,2)' 1 0.1];
%params0 = [9.4877    0.6340   -2.3391   44.7137   -6.0246   38.9963   27.7249   0.5761];
params0 = [-0.5431    1.0695  175.6335   36.4353    2.9213    9.4498];
options = zeros(1,18);
options(1)=1; options(9)=1; options(14)=40;
%Subsample
ind = randperm(size(Xt,2));
npts = 10000;
X = Xt(:,ind(1:npts));
y = yt(:,ind(1:npts))-mean(yt(:,ind(1:npts)));
%[params]=gplearn(@cov_nn2,[],params0(1:end-1),params0(end),options)
[params]=gplearn(@cov_sparse,@grad_sparse,params0(1:end-1),params0(end),options)
return;
X = Xt;
y = yt - mean(yt(:,ind(1:npts)));
%[lml,mf,vf] = gpkdtreeeval(X,y,...
%    @cov_sparse,params(1:end-1),params(end),xtest,50);
tic;
[lml,mf,vf] = gpspeval(X,y,...
    @cov_sparse,params(1:end-1),[],params(end),xtest(:,1:10));
toc
S2 = vf' - params(end)^2;

%figure; imagesc(reshape(mf,[sx sz])');
v = mf' + mean(yt(:,ind(1:npts)));
disp(sum(abs(v-ytest)));

clf
f = [v+2*sqrt(S2);flipdim(v-2*sqrt(S2),1)];
fill([[1:length(v)]; flipdim([1:length(v)],1)], f, [7 7 7]/8, 'EdgeColor', [7 7 7]/8);
hold on
plot(1:length(v),v,'k-','LineWidth',2);
plot(1:length(v), ytest, 'g+');
