%Compute the negate log marginal likelihood of a multi-class GP classifier
%and its gradient using the conventional regression approximation. 
%It is useful for optimisers like
%matlab which requires a single function passing lml and
%the gradient.
%
%INPUT
%
%OUTPUT
%nlml - negative log marginal likelihood
%ng - gradient of the negative log marginal likelihood
%ttau - natural parameter
%tnu - natural parameter
%
% Fabio Tozeto Ramos 03/10/09
% [nlml,ng,ttau,tnu] = gpmcrlmlgrad(params,kfun,kgradfun,si)
function [nlml,ng] = gpmcrlmlgrad(params,kfun,kgradfun,kmap,si)
global X y ;%K;%L invK K alpha;

n     = size(X,2);  %number of inputs
C     = size(y,1);  %number of classes
nlml  = 0;
mf    = zeros(C,n);
vf    = zeros(C,n);
ng    = zeros(1,length(cell2mat(kmap))+1);

% Compute the marginal likelihood for each individual process
for i = 1:C
    if ~isempty(kgradfun)
        [temp,ng(kmap{i}),mf(i,:),vf(i,:)] = gpsrlmlgrad(X,y(i,:),params(kmap{i}),...
            kfun{i},kgradfun{i},si);
    else
        [temp,ng(kmap{i}),mf(i,:),vf(i,:)] = gpsrlmlgrad(X,y(i,:),params(kmap{i}),...
            kfun{i},[],si);
    end
    %nlml = nlml + temp - sum(log(erf(params(end)*y(i,:).*mf(i,:)...
    %    /sqrt(1+(params(end)^2)*vf(i,:)))));
    nlml = nlml + temp;
end
%nlml = nlml - sum(params(end)*mf(y==1)) + ...
%    sum(log(sum(exp(params(end)*mf),1)));
p     = hermintlogit(params(end)*mf,(params(end)^2)*vf);
%p     = sum(params(end)*mf(y==1)) - sum(log(sum(exp(params(end)*mf),1)));
sig   = 100;
nlml  = nlml - sum(log(p(y==1))) + (params(end).^2)/sig;
%nlml  = nlml + (params(end).^2)/1;
if nargout>1 
    %Compute the gradient of params(end) numerically
    dt      = sqrt(eps)*params(end);
    p2      = hermintlogit((params(end)+dt)*mf,((params(end)+dt)^2)*vf);
    ng(end) = (-sum(log(p2(y==1))) + sum(log(p(y==1))))/dt + 2*params(end)/sig;
end

function [nlml,ng,mf,vf] = gpsrlmlgrad(X,y,params,kfun,kgradfun,si)
noise = params(end);
kpar  = params(1:end-1);
n     = size(X,2);
meany = mean(y);
yt    = y - meany;


if isempty(si)

else
    m = length(si);
end

KI  = feval(kfun,X(:,si),X(:,si),kpar);
KIn = feval(kfun,X(:,si),X,kpar);
if issparse(KI)
    I = speye(m);
else
    I = eye(m);
end

L  = jitChol(KI)';
V  = L\KIn;
M  = V*V'+(noise^2)*I;
Lm = jitChol(M)';
b  = (Lm\V)*yt';


%Compute the log marginal likelihood
yb   = yt*yt'-b'*b;
nlml =  sum(log(diag(Lm))) + 0.5*(n-m)*log(noise^2) + ...
    0.5*(1/(noise^2))*yb+0.5*n*log(2*pi);
%Adds a prior
%nlml = nlml;% + ((params-[4 2 0.4])./[0.1 0.1 0.005])*(params-[4 2 0.4])';
if isnan(nlml) || isinf(nlml)
    error('nlml is nan of inf');
end



ks   = KIn;
%mf   = ks'*(((noise^2)*KI + KIn*KIn')\(KIn*yt'));  % pred mean eq. (8.14)
%e    = ((noise^2)*KI + KIn*KIn') \ ks;
jit  = 1e-3*eye(length(si));
mf   = ks'*(((noise^2)*KI + KIn*KIn'+ jit)\(KIn*yt'));  % pred mean eq. (8.14)
e    = ((noise^2)*KI + KIn*KIn'+jit)\ks;


S2SR = (noise^2)*sum(ks.*e,1)';         % noise-free SR variance, eq. 8.15
vf   = abs(S2SR);% + noise^2);                  % SR variance including noise
mf   = mf + meany;

%[nlml] = epComputeParams(KI, yt, vf.^(-1), mf.*(vf.^(-1)));


if ~isempty(kgradfun) && nargout>1 
    %Compute necessary matrices
    Lt = L*Lm;
    B1 = Lt'\(Lm\V);
    %Compute the variance
    %opts.TRANSA = true;
    %opts.LT     = true;
    %v           = linsolve(L,ks,opts);
    
    %B1 = linsolve(Lt,linsolve(Lm,V,opts),opts);
    %B1 = solve_tril(Lt,solve_tril(Lm,V));
    b1    = Lt'\b;
    invKI = L'\(L\I);
    invA  = Lt'\(Lt\I);
    
    %Compute the gradient
    KId = feval(kgradfun,kpar,KI,X(:,si));
    tmp = feval(kgradfun,kpar,KIn,X(:,si),X);
    
    
    mu = V'*(Lm'\b);
    ng = zeros(1,length(KId)+1);

    for i = 1:length(tmp)
        ng(i) = -0.5*sum(sum((invKI-(noise^2)*invA).*KId{i})) + ...
            sum(sum(B1.*tmp{i})) - (noise^(-2))*b1'*tmp{i}*(yt-mu')'...
            +0.5*b1'*KId{i}*b1;
    end
    %Noise gradient
    ng(end) = ((noise^2)*trace(Lm'\(Lm\I)) + n - m + ...
        sum((Lm'\b).^2) - (noise^(-2))*yb)*(noise)^(-1);
    
else
    ng = 0;
end


% function to compute the parameters of the Gaussian approximation, Sigma and
% mu, and the log marginal likelihood, lml, from the current site parameters,
% ttau and tnu. The function also may return L (useful for predictions).

function [nlml] = epComputeParams(K, y, ttau, tnu)

n      = length(ttau);                                     % number of training cases
ssi    = sqrt(ttau);                                    % compute Sigma and mu

L     = chol(eye(n)+ssi*ssi'.*K);
V     = solve_tril(L',(bsxfun(@times,ssi,K)));
Sigma = K - V'*V;
mu    = Sigma*tnu;

tau_n = 1./diag(Sigma) - ttau;              % compute the log marginal likelihood
nu_n  = mu./diag(Sigma) - tnu;                      % vectors of cavity parameters
z     = y'.*nu_n ./ sqrt(tau_n.*(1 + tau_n));
lml   = -sum(log(diag(L))) + sum(log(1 + ttau ./ tau_n))/2 + ...
        sum(log((1 + erf(z/sqrt(2)))/2)) ...
        + tnu'*Sigma * tnu/2 + nu_n'*((ttau./tau_n.*nu_n - 2*tnu)...
        ./(ttau + tau_n))/2 - sum(tnu.^2./(tau_n + ttau))/2;
nlml  = -lml;  
% transform Sigma into a cell
%for i = 1:C
%    ii = (i-1)*n + 1;
%    ee = i*n;
%    Sigma2{i} = Sigma(ii:ee,ii:ee);
%end  


