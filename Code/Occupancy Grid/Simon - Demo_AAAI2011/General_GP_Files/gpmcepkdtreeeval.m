% Copyright The University of Sydney, 2001 - 2008.  ABN 15 211 513 464
% herafter 'The University'.
%
% The copyright of the computer program(s) herein is the property of
% The University of Sydney, Australia.  The program(s) may be used
% and/or copied only with the written permission of The University of
% Sydney, or in accordance with the terms and conditions stipulated
% in the contract under which the program(s) have been supplied.
% 
% Disclaimer:
% The Universitiy makes no representation or warranty
% that these materials, including any software, are free from errors;
% about the quality or performance of these materials;
% that these materials are fit for any particular purpose.
% These materials are made available on the strict basis that The University
% and its employees or agents have no liability for any direct or indirect
% loss or damage (including for negligence) suffered by any person as a
%consequence of the use of this  material.
%
% Evaluate a Gaussian Process Classifier with Expectation Propagation using
% a local approximation with nn nearest neighbours.
%
%INPUTS
% X - D x N inputs
% y - 1 x N outputs (targets)
% kfun - covariance function
% kpar - covariance parameters (hyperparameters)
% noise - scalar noise level
% x - D x M test inputs
% nn - number of nearest neighbours
% 
%OUTPUTS
% mf - predictive mean
% vf - predictive variance
% lml - log marginal likelihood
%
% Fabio Tozeto Ramos 04/06/08
% [lml,mf,vf] = gpmcepkdtreeeval(X,y,kfun,kpar,noise,x,nn)

function [p,mf,vf,nlml,ttau,tnu] = gpmcepkdtreeeval(X,y,kfun,kpar,x,nn)
n     = size(X,2);  %number of inputs
C     = length(y)/n;  %number of classes
[d,N] = size(x);  %dimensionality number of query points
mf    = zeros(C,N);
vf    = zeros(C,N);
nlml  = zeros(1,N);
Xc    = cell(1,C);
kd    = cell(1,C);
Xsor  = cell(1,C);
%ysor  = cell(1,C);
Xt    = cell(1,C);
rsy   = reshape(y,n,C)'; %reshaped y
yt    = -ones(C,C*nn);
ttau  = nan(C*n,1);
tnu   = nan(C*n,1);
tind  = zeros(C,n); %indexes for ttau and tnu

%Build the output yt and 
for i = 1:C
    tind(i,:) = (i-1)*n + (1:n);
    yt(i,(i-1)*nn+1:nn*i) = 1;
end
yt  = reshape(yt',1,nn*C*C);
[rrrr,ind] = max(rsy,[],1);
%Build one KDtree per class
for i = 1:C
    Xc{i} = X(:,ind==i);  
    if size(Xc{i},2)<nn
        error('not enough points. Decrease number of neighbours.');
    end
    [kd{i},Xsor{i}] = vgg_kdtree2_build(Xc{i},1);
    %ysor{i}         = y(kd{i}.indices);
    tt = find(ind==i);
    tind(:,tt)  = tind(:,tt(kd{i}.indices));
    %Necessary according to the KDTree author
    kd{i}.indices   = uint32(1:size(Xc{i},2));
end

ii = zeros(1,C*C*nn);
for i = 1:N 
    for j = 1:C
        %Searches for the nearest neighbours
        [is]     = vgg_kdtree2_N_closest_points(kd{j},Xsor{j},x(:,i),nn);
        is       = is(is~=0);
        Xt{j}    = Xsor{j}(:,is);
        in       = find(ind==j);
        ii((j-1)*C*nn+1:j*C*nn) = reshape(tind(:,in(is))',1,C*nn);
    end
    [mf(:,i),vf(:,i),nlml(i),ttau(ii),tnu(ii)] = ...
        gpmcep(cell2mat(Xt),yt,kfun,kpar,[],x(:,i),ttau(ii),tnu(ii));
end    
p = hermintlogit(0.85*mf,vf);


% Same as gpmcepeval but does not compute the Gauss-Hermite quadrature at
% the end.
function [mf,vf,nlml,ttau,tnu] = gpmcep(X,y,kfun,kpar,K,x,ttau,tnu)
n     = size(X,2); %number of inputs
tol   = 1e-3; max_sweep = 10;  
C     = length(y)/n; %number of classes
[d,N] = size(x); %dimensionality number of query points
mf    = zeros(C,N);
vf    = zeros(C,N);
%Sigma = cell(1,C);

if isempty(K)
    %Build K
    K = cell(1,C);
    for i = 1:C
        K{i} = feval(kfun{i},X(:,:),X(:,:),kpar{i});
     %   Sigma{i} = K{i};
    end
end
%Kb   = blkdiag(K{:});

%Computes ks
ks  = cell(1,C);
for i = 1:C
    ks{i} = feval(kfun{i},X(:,:),x(:,:),kpar{i});
end


%Compute tnu and ttau in case they are not provided
ttau(isnan(ttau)) = 0;            % initialize to zero if we have no better guess
tnu(isnan(tnu))   = 0;
lml               = -n*C*log(2);
lml_old           = -inf; 
sweep             = 0;  
ind               = find(ttau==0)';

[Sigma, mu] = epComputeParams(K, y, ttau, tnu, C); % initialize Sigma and mu
%mu                = zeros(n*C,1);
%Sigma             = blkdiag(K{:});

if ~isempty(ind)
    while lml - lml_old > tol && sweep < max_sweep    % converged or maximum sweeps?
        lml_old = lml; 
        sweep   = sweep + 1;
        for i = ind                     % iterate EP updates over examples
            tau_ni   = 1/Sigma(i,i)-ttau(i);      % first find the cavity distribution ..
            nu_ni    = mu(i)/Sigma(i,i)-tnu(i);           % .. parameters tau_ni and nu_ni
            z_i      = y(i)*nu_ni/sqrt(tau_ni*(1+tau_ni));     % compute the desired moments 
            gosz_i   = sqrt(2/pi)*exp(-z_i.^2/2)./(1+erf(z_i/sqrt(2)));  % gauss / sigmoid
            hmu      = nu_ni/tau_ni + y(i)*gosz_i/sqrt(tau_ni*(1+tau_ni));
            hs2      = (1-gosz_i*(z_i+gosz_i)/(1+tau_ni))/tau_ni;
            ttau_old = ttau(i);                   %  then find the new tilde parameters
            ttau(i)  = 1/hs2 - tau_ni;
            tnu(i)   = hmu/hs2 - nu_ni;
            ds2      = ttau(i) - ttau_old;                  % finally rank-1 update Sigma ..
            %Sigma = Sigma - ds2/(1+ds2*Sigma(i,i))*Sigma(:,i)*Sigma(i,:);
            % Efficient direct Lapack call (using Mathias Seeger's
            % function
            fst_dgemm({Sigma, [1 1 n*C n*C], 'L '},Sigma(:,i),0,Sigma(:,i),1,...
                -ds2/(1+ds2*Sigma(i,i)),1);
            mu       = Sigma*tnu;                                  % .. and recompute mu
        end
        [Sigma, mu, lml] = epComputeParams(K, y, ttau, tnu, C); % recompute Sigma and
        % mu since repeated rank-one updates eventually destroys numerical
        % precision
    end
end
    
for i=1:C
    a       = feval(kfun{i},x,x,kpar{i});          
    b       = ks{i};
    tnuc    = tnu((i-1)*n+1:i*n);
    ttauc   = ttau((i-1)*n+1:i*n);
    ssi     = sqrt(ttauc);                                        % compute Sigma and mu
    L       = chol(eye(n)+ssi*ssi'.*K{i});
    mus     = b'*(tnuc-sqrt(ttauc).*solve_chol(L,sqrt(ttauc).*(K{i}*tnuc)));   % test means
    v       = L'\bsxfun(@times,sqrt(ttauc),b);
    s2s     = a' - sum(v.*v,1)';                     % latent test predictive variance
    mf(i,:) = mus;                             % return latent test predictive means
    vf(i,:) = s2s;                      % return latent test predictive variances
end
nlml = -lml;                        % return negative log marginal likelihood



% function to compute the parameters of the Gaussian approximation, Sigma and
% mu, and the log marginal likelihood, lml, from the current site parameters,
% ttau and tnu. The function also may return L (useful for predictions).

function [Sigma, mu, lml, L] = epComputeParams(K, y, ttau, tnu, C)

n     = length(y)/C;                             % number of training cases
ssi   = sqrt(ttau);                              % compute Sigma and mu
L     = zeros(C*n);
Sigma = zeros(C*n);
for i = 1:C
    ii = (i-1)*n+1; 
    ee = i*n;
    L(ii:ee,ii:ee) = chol(eye(n)+ssi(ii:ee)*ssi(ii:ee)'.*K{i});
    V = solve_tril(L(ii:ee,ii:ee)',(bsxfun(@times,ssi(ii:ee),K{i})));
    Sigma(ii:ee,ii:ee) = K{i} - V'*V;
end

mu = Sigma*tnu;

if nargout>2
    tau_n = 1./diag(Sigma)-ttau;              % compute the log marginal likelihood
    nu_n = mu./diag(Sigma)-tnu;                      % vectors of cavity parameters
    z = y'.*nu_n./sqrt(tau_n.*(1+tau_n));
    lml = -sum(log(diag(L)))+sum(log(1+ttau./tau_n))/2+sum(log((1+erf(z/sqrt(2)))/2)) ...
          +tnu'*Sigma*tnu/2+nu_n'*((ttau./tau_n.*nu_n-2*tnu)./(ttau+tau_n))/2 ...
          -sum(tnu.^2./(tau_n+ttau))/2;
end
  
    
    