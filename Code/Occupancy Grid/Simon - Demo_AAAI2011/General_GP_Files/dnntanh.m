%Derivative of the transformation function for the Warped Gaussian process based on
%neural nets tanh(x) type.
%df(x) = 1 + sum_i a_1*b_i - sum_i a_i*b_i*tanh(b_i*x+c_i)^2
function [fy] = dnntanh(params,x)
I = size(params,1);
fy = zeros(1,length(x));
for i = 1 : I
    fy = fy + abs(params(i,1)*params(i,2))...
        - abs(params(i,1)*params(i,2))*tanh(abs(params(i,2))*x+params(i,3)).^2;
end
fy = fy + 1;