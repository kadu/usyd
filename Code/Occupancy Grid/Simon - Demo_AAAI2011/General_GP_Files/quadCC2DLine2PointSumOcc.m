



function K = quadCC2DLine2PointSumOcc(funs,npar,X1S,X1E,X2,par,order)
Num_Lines = size(X1S,2);
Num_Points = size(X2,2);

% figure
% plot([X1S(1,:);X1E(1,:)],[X1S(2,:);X1E(2,:)])
% hold on
% plot(X2(1,:),X2(2,:),'+')

%Find line segments where start=end i.e. its a point
X1E=X1E+[(X1S(1,:)==X1E(1,:)).*(X1S(2,:)==X1E(2,:))*0.0001;(X1S(1,:)==X1E(1,:)).*(X1S(2,:)==X1E(2,:))*0.0001];

[ Nodes, Weights ] = clenshaw_curtis_compute_nd ( 1, order );

% order
[costhet sinthet xstart ystart r]= determLineParameters(X1S, X1E);
    
    
    [NodesScaleShift Weights] = scaleShiftNodesWeights(r, Nodes, Weights);

    %Create X1 as a long list of all the nested points in order
X1 = [reshape((NodesScaleShift.*costhet(:,ones(1,size(Nodes,2)))+xstart(:,ones(1,order)))',1,order*Num_Lines);...
    reshape((NodesScaleShift.*sinthet(:,ones(1,size(Nodes,2)))+ystart(:,ones(1,order)))',1,order*Num_Lines)];

Weights = reshape(Weights',1,order*Num_Lines)';

% figure
% plot3(X1(1,:),X1(2,:),Weights,'+')


% knodes = feval(fun,X1,X2,par);
knodes = cov_sum(X1,X2,funs,npar,par(1:end-2));
WeightsMat = Weights(:,ones(1,Num_Points));

KnodesWeighted=knodes.*WeightsMat;
KnodesWeighted = reshape(KnodesWeighted,[order Num_Lines Num_Points]);


KnodesWeighted = permute(KnodesWeighted,[2 3 1]);

  KPreSig = sum(KnodesWeighted,3);
  
  M = [par(end-2) par(end-1);0 par(end)]'*[par(end-2) par(end-1);0 par(end)];

  K =[ M(3)*KPreSig(1:(Num_Lines/2),1:end);...
    M(4)*KPreSig((Num_Lines/2)+1:end,1:end)];

end


