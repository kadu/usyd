%Computes the inverse of a function at a point x using the Newton-Raphson
%method.
%Inputs
%   f is the function 
%   df is the derivative of the function
%   fx is the point to compute the inverse
%   iter maximum number of iterations
function [x,i] = invnewton(f,df,par,fx,x0,iter)

if nargin < 5
    x0 = rand;
    iter = 100;
end
if nargin < 6
    iter = 100;
end
x    = x0;
px   = inf;
conv = false;
i    = 1;
while ~conv && i<=iter
    fv = feval(f,par,x) - fx;
    dfv = feval(df,par,x);
    x = x - fv/dfv;
    i = i + 1;
    if abs(x-px) < eps
        conv = true;
    else
        px = x;
    end
end