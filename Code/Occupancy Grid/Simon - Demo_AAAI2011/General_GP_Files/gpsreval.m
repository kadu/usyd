% Copyright The University of Sydney, 2001 - 2008.  ABN 15 211 513 464
% herafter 'The University'.
%
% The copyright of the computer program(s) herein is the property of
% The University of Sydney, Australia.  The program(s) may be used
% and/or copied only with the written permission of The University of
% Sydney, or in accordance with the terms and conditions stipulated
% in the contract under which the program(s) have been supplied.
% 
% Disclaimer:
% The Universitiy makes no representation or warranty
% that these materials, including any software, are free from errors;
% about the quality or performance of these materials;
% that these materials are fit for any particular purpose.
% These materials are made available on the strict basis that The University
% and its employees or agents have no liability for any direct or indirect
% loss or damage (including for negligence) suffered by any person as a
%consequence of the use of this  material.
%
% Evaluate a Gaussian Process with the Subset of Regressor approximation 
% (see page 175 of "Gaussian Processes for Machine Learning" by
% C. Rasmussen and C. Williams)
%
%INPUTS
% X - D x N inputs
% y - 1 x N outputs (targets)
% kfun - covariance function
% kpar - covariance parameters (hyperparameters)
% KI - N x N computed covariance 
% noise - scalar noise level
% x - D x M test inputs
% invK - N x N inverted K if available
% si - index of points in the active set
% 
%OUTPUTS
% mf - predictive mean
% vf - predictive variance
% lml - log marginal likelihood
%
% Fabio Tozeto Ramos 19/08/09
% [mf,vf] = gpsreval(X,y,kfun,kpar,K,noise,x,invK,si)

function [mf,vf] = gpsreval(X,y,kfun,kpar,KI,noise,x,invK,si)
%[d,N] = size(X); %number of inputs
%[nx]  = size(x,2); %number of testing points 
y = reshape(y,length(y),1);
% check is mean is zero
my = mean(y);
if abs(my)>1e-3
    y      = y - my;
    myflag = true;
end

if isempty(KI)
    KI     = feval(kfun,X(:,si),X(:,si),kpar);
    jitter = 1e-9*trace(KI); %necessary for stability
    KI     = KI + jitter*eye(length(si));
    KIn    = feval(kfun,X(:,si),X,kpar);
end

ks = feval(kfun,X(:,si),x,kpar);
if isempty(invK)
    mf = ks'*(((noise^2)*KI + KIn*KIn')\(KIn*y));  % pred mean eq. (8.14)
    e  = ((noise^2)*KI + KIn*KIn') \ ks;
else
    mf = ks'*(invK*(KIn*y));  % pred mean eq. (8.14)
    e  = invK*ks;
end
S2SR = (noise^2)*sum(ks.*e,1)';                 % noise-free SR variance, eq. 8.15
vf   = S2SR + noise^2;                            % SR variance inclusing noise

if myflag
    mf = mf + my;
end
%Compute the log marginal likelihood
%lml = -0.5*y*alpha-sum(log(diag(L)))-0.5*N*log(2*pi);
%nlml = -0.5*y*alpha - 0.5*log(det(A)) - 0.5*n*log(2*pi);