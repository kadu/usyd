%Compute the negate log marginal likelihood of a multi-class GP classifier
%and its gradient using the EP approximation. The algorithm is described
%on page 58 of "Gaussian Processes for Machine Learning" by Rasmussen and
%Williams. Most of the code was adapted from Rasmussen's gpml toolbox. 
%It is useful for optimisers like
%matlab which requires a single function passing lml and
%the gradient.
%
%INPUT
%
%OUTPUT
%nlml - negative log marginal likelihood
%ng - gradient of the negative log marginal likelihood
%ttau - natural parameter
%tnu - natural parameter
%
% Fabio Tozeto Ramos 05/08/09
% [nlml,ng,ttau,tnu] = gpmceplmlgrad(params,kfun,kgradfun)
function [nlml,ng,ttau,tnu] = gpmceplmlgrad(params,kfun,kgradfun,kmap)
global X y K;%L invK K alpha;
persistent best_ttau best_tnu best_lml;   % keep tilde parameters between calls

tol = 1e-3; max_sweep = 10; 
[dim,n] = size(X);
C       = length(y)/n;      % number of classes
kpar    = cell(1,C);
Sigma   = cell(1,C);
%Build K and initialize Sigma
for i = 1:C
    kpar{i}  = params(kmap{i});
    K{i}     = feval(kfun{i},X(:,:),X(:,:),kpar{i});
    Sigma{i} = K{i};
end
Kb    = blkdiag(K{:});


% initialize ttau, tnu and mu, the parameters of ..
if any(size(best_ttau) ~= [n*C 1])
    ttau  = zeros(n*C,1);            % initialize to zero if we have no better guess
    tnu   = zeros(n*C,1);
    mu    = zeros(n*C,1);                   % .. the Gaussian posterior approximation
    lml   = -n*C*log(2);
    best_lml = -inf;
else
    ttau    = best_ttau;                   % try the tilde values from previous call
    tnu     = best_tnu;
    [Sigma, mu, lml] = epComputeParams(K, y, ttau, tnu, C); 
    if lml < -n*C*log(2)                                     % if zero is better ..
        ttau  = zeros(n*C,1);                   % .. then initialize with zero instead
        tnu   = zeros(n*C,1); 
        mu    = zeros(n*C,1);                 % .. the Gaussian posterior approximation
        lml   = -n*C*log(2);
    end        
end  
    
    %best_lml = -inf;

lml_old = -inf; sweep = 0;                        % make sure while loop starts
while lml - lml_old > tol && sweep < max_sweep    % converged or maximum sweeps?

    lml_old = lml; sweep = sweep + 1;
    for j = 1:C
        for i = 1:n                                % iterate EP updates over examples
            % index for ttau, tnu, etc
            ii = i + (j-1)*n;          
            if ttau(ii) > 0.01 || sweep==1
            tau_ni = 1/Sigma{j}(i,i) - ttau(ii);      % first find the cavity distribution ..
            nu_ni  = mu(ii)/Sigma{j}(i,i) - tnu(ii);           % .. parameters tau_ni and nu_ni

            z_i    = y(ii)*nu_ni/sqrt(tau_ni*(1+tau_ni));     % compute the desired moments
            gosz_i = sqrt(2/pi)*exp(-z_i.^2/2)./(1+erf(z_i/sqrt(2))); % gauss / sigmoid
            hmu    = nu_ni/tau_ni + y(ii)*gosz_i/sqrt(tau_ni*(1+tau_ni));
            hs2    = (1-gosz_i*(z_i+gosz_i)/(1+tau_ni))/tau_ni;

            ttau_old = ttau(ii);                   %  then find the new tilde parameters
            ttau(ii) = 1/hs2 - tau_ni;
            tnu(ii)  = hmu/hs2 - nu_ni;

            ds2      = ttau(ii) - ttau_old;          % finally rank-1 update Sigma ..
            %Sigma{j} = Sigma{j} - ds2/(1+ds2*Sigma{j}(i,i))*Sigma{j}(:,i)*Sigma{j}(i,:);
            
            % Efficient direct Lapack call (using Mathias Seeger's function)
            %fst_dsyrk({Sigma{j}, [1 1 n n], 'L '},...
            %    Sigma{j}(:,i),0,-ds2/(1+ds2*Sigma{j}(i,i)),1);
            
            fst_dgemm({Sigma{j}, [1 1 n n], 'L '},Sigma{j}(:,i),0,Sigma{j}(:,i),1,...
                -ds2/(1+ds2*Sigma{j}(i,i)),1);
            mu((j-1)*n+1:j*n) = Sigma{j}*tnu((j-1)*n+1:j*n);            % .. and recompute mu
            end
        end
    end
    [Sigma, mu, lml, L] = epComputeParams(K, y, ttau, tnu, C); % recompute Sigma and
    % mu since repeated rank-one updates eventually destroys numerical precision
end


nlml = -lml;

if sweep == max_sweep
  disp('Warning: maximum number of sweeps reached in function gpmceplmlgrad.')
end

if lml > best_lml
  best_ttau = ttau; best_tnu = tnu; best_lml = lml; % keep values for next call
end

if nargout > 1                                      % do we want derivatives?
    ng = 0*cell2mat(kpar);                         % allocate space for derivatives
    b = tnu-sqrt(ttau).*solve_chol(L,(sqrt(ttau).*(Kb*tnu)));
    F = b*b'-repmat(sqrt(ttau),1,n*C).*solve_chol(L,diag(sqrt(ttau)));
    temp = cell(1,C);
    for i = 1:C
        temp{i} = feval(kgradfun{i},kpar{i},K{i},X);        
    end
    %Builds kgrad
    kgrad = cell(1,length(ng));
    for j=1:length(ng)
        kgrad{j} = zeros(C*n);
    end    
    ing = 1;
    for i = 1:C
        for j = 1:length(kpar{i})
            kgrad{ing}((i-1)*n+1:i*n,(i-1)*n+1:i*n) = temp{i}{j};
            ing = ing + 1;
        end
    end   
    %Compute the gradients
    for j=1:length(ng)                   
        ng(j) = -sum(sum(F.*kgrad{j}))/2;
    end    
end



% function to compute the parameters of the Gaussian approximation, Sigma and
% mu, and the log marginal likelihood, lml, from the current site parameters,
% ttau and tnu. The function also may return L (useful for predictions).

% function [Sigma2, mu, lml, L] = epComputeParams(K, y, ttau, tnu, C)
% 
% sigmoid = @(x)(1+erf(x/sqrt(2)))/2;
% n = length(y)/C;                                       % number of training cases
% ssi = sqrt(ttau);                                        % compute Sigma and mu
% L = chol(eye(n*C)+ssi*ssi'.*K);
% V = solve_tril(L',(K.*ssi(:,ones(1,n*C))));
% Sigma = K - V'*V;
% mu = Sigma*tnu;
% Sigma2 = cell(1,C);
% 
% tau_n = 1./diag(Sigma)-ttau;              % compute the log marginal likelihood
% nu_n = mu./diag(Sigma)-tnu;                      % vectors of cavity parameters
% z = y'.*nu_n./sqrt(tau_n.*(1+tau_n));
% lml = -sum(log(diag(L)))+sum(log(1+ttau./tau_n))/2+sum(log(feval(sigmoid,z))) ...
%       +tnu'*Sigma*tnu/2+nu_n'*((ttau./tau_n.*nu_n-2*tnu)./(ttau+tau_n))/2 ...
%       -sum(tnu.^2./(tau_n+ttau))/2;
% % transform Sigma into a cell
% for i = 1:C
%     ii = (i-1)*n + 1;
%     ee = i*n;
%     Sigma2{i} = Sigma(ii:ee,ii:ee);
% end
  
  

% function to compute the parameters of the Gaussian approximation, Sigma and
% mu, and the log marginal likelihood, lml, from the current site parameters,
% ttau and tnu. The function also may return L (useful for predictions).

function [Sigma2, mu, lml, L] = epComputeParams(K, y, ttau, tnu, C)

n      = length(y)/C;                                     % number of training cases
ssi    = sqrt(ttau);                                    % compute Sigma and mu
L      = zeros(C*n);
Sigma  = zeros(C*n);
Sigma2 = cell(1,C);

for i = 1:C
    ii = (i-1)*n+1; 
    ee = i*n;
    L(ii:ee,ii:ee) = chol(eye(n)+ssi(ii:ee)*ssi(ii:ee)'.*K{i});
    V  = solve_tril(L(ii:ee,ii:ee)',(bsxfun(@times,ssi(ii:ee),K{i})));
    Sigma(ii:ee,ii:ee) = K{i} - V'*V;
end
mu = Sigma*tnu;

tau_n = 1./diag(Sigma) - ttau;              % compute the log marginal likelihood
nu_n  = mu./diag(Sigma) - tnu;                      % vectors of cavity parameters
z     = y'.*nu_n ./ sqrt(tau_n.*(1 + tau_n));
lml   = -sum(log(diag(L))) + sum(log(1 + ttau ./ tau_n))/2 + ...
        sum(log((1 + erf(z/sqrt(2)))/2)) ...
        + tnu'*Sigma * tnu/2 + nu_n'*((ttau./tau_n.*nu_n - 2*tnu)...
        ./(ttau + tau_n))/2 - sum(tnu.^2./(tau_n + ttau))/2;
  
% transform Sigma into a cell
for i = 1:C
    ii = (i-1)*n + 1;
    ee = i*n;
    Sigma2{i} = Sigma(ii:ee,ii:ee);
end  
  