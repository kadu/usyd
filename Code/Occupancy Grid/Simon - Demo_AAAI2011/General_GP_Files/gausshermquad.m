%Computes the generalised Gauss-Hermite quadrature for multiple dimensions
%given means mu and covariance sig.
%Inputs
%   f - function handle
%   params - function parameters   
%   mu - mean 
%   sig - covariance
%
%Output
%   v - value of the integral
%
%  Fabio Tozeto Ramos 02/07/2009
%
%function [v] = gausshermquad(f,params,mu,Sig,w,x)
function [v] = gausshermquad(f,params,mu,Sig,w,x)

dim_num = size(mu,1); 
level_max = 2;


    
if nargin < 6
    %Generate weights and points 
    %Determine the number of points
    point_num = sparse_grid_herm_size ( dim_num, level_max );
 
    %Compute the weights and points.
    [w, x] = sparse_grid_herm ( dim_num, level_max, point_num );    
end


sig     = sqrtm(Sig);
y       = sqrt(2)*sig*x + mu(:,ones(1,size(x,2)));


% Evaluates the function f in points y
fv      = feval(f,params,y);
v       = sum(fv.*w)/sqrt(pi^dim_num);


