%Compute the negate log marginal likelihood of a GP classifier
%and its gradient using the EP approximation. The algorithm is described
%on page 58 of "Gaussian Processes for Machine Learning" by Rasmussen and
%Williams. Most of the code was adapted from Rasmussen's gpml toolbox. 
%It is useful for optimisers like
%matlab which requires a single function passing lml and
%the gradient.
%
%INPUT
%
%OUTPUT
%nlml - negative log marginal likelihood
%ng - gradient of the negative log marginal likelihood
%ttau - natural parameter
%tnu - natural parameter
%
% Fabio Tozeto Ramos 18/02/09
% [nlml,ng,ttau,tnu] = gpceplmlgrad(params,kfun,kgradfun)
function [nlml,ng,ttau,tnu] = gpceplmlgrad(params,kfun,kgradfun)
global X y K;%L invK K alpha;
kpar = params;
tol = 1e-3; max_sweep = 10; 
gos = inline('sqrt(2/pi)*exp(-x.^2/2)./(1+erf(x/sqrt(2)))');  % gauss / sigmoid
n = size(X,2);
K = feval(kfun,X,X,kpar);



ttau = zeros(n,1);            % initialize to zero if we have no better guess
tnu = zeros(n,1);
Sigma = K;                    % initialize Sigma and mu, the parameters of ..
mu = zeros(n, 1);                   % .. the Gaussian posterior approximation
lml = -n*log(2);
%best_lml = -inf;

lml_old = -inf; sweep = 0;                        % make sure while loop starts
while lml - lml_old > tol && sweep < max_sweep    % converged or maximum sweeps?

    lml_old = lml; sweep = sweep + 1;
    for i = 1:n                                % iterate EP updates over examples

        tau_ni = 1/Sigma(i,i)-ttau(i);      % first find the cavity distribution ..
        nu_ni = mu(i)/Sigma(i,i)-tnu(i);           % .. parameters tau_ni and nu_ni

        z_i = y(i)*nu_ni/sqrt(tau_ni*(1+tau_ni));     % compute the desired moments
        hmu = nu_ni/tau_ni + y(i)*gos(z_i)/sqrt(tau_ni*(1+tau_ni));
        hs2 = (1-gos(z_i)*(z_i+gos(z_i))/(1+tau_ni))/tau_ni;

        ttau_old = ttau(i);                   %  then find the new tilde parameters
        ttau(i) = 1/hs2 - tau_ni;
        tnu(i) = hmu/hs2 - nu_ni;

        ds2 = ttau(i) - ttau_old;                  % finally rank-1 update Sigma ..
        Sigma = Sigma - ds2/(1+ds2*Sigma(i,i))*Sigma(:,i)*Sigma(i,:);
        mu = Sigma*tnu;                                       % .. and recompute mu

    end
    [Sigma, mu, lml, L] = epComputeParams(K, y, ttau, tnu); % recompute Sigma and
    % mu since repeated rank-one updates eventually destroys numerical precision
end


nlml = -lml;
if nargout > 1                                      % do we want derivatives?
    ng = 0*kpar;                         % allocate space for derivatives
    b = tnu-sqrt(ttau).*solve_chol(L,(sqrt(ttau).*(K*tnu)));
    F = b*b'-repmat(sqrt(ttau),1,n).*solve_chol(L,diag(sqrt(ttau)));
    kgrad = feval(kgradfun, kpar);
    for j=1:length(kpar)       
        ng(j) = -sum(sum(F.*kgrad{j}))/2;
    end    
end



% function to compute the parameters of the Gaussian approximation, Sigma and
% mu, and the log marginal likelihood, lml, from the current site parameters,
% ttau and tnu. The function also may return L (useful for predictions).

function [Sigma, mu, lml, L] = epComputeParams(K, y, ttau, tnu)

sigmoid = inline('(1+erf(x/sqrt(2)))/2');
n = length(y);                                       % number of training cases
ssi = sqrt(ttau);                                        % compute Sigma and mu
L = chol(eye(n)+ssi*ssi'.*K);
V = L'\(repmat(ssi,1,n).*K);
Sigma = K - V'*V;
mu = Sigma*tnu;

tau_n = 1./diag(Sigma)-ttau;              % compute the log marginal likelihood
nu_n = mu./diag(Sigma)-tnu;                      % vectors of cavity parameters
z = y'.*nu_n./sqrt(tau_n.*(1+tau_n));
lml = -sum(log(diag(L)))+sum(log(1+ttau./tau_n))/2+sum(log(sigmoid(z))) ...
      +tnu'*Sigma*tnu/2+nu_n'*((ttau./tau_n.*nu_n-2*tnu)./(ttau+tau_n))/2 ...
      -sum(tnu.^2./(tau_n+ttau))/2;