% Copyright The University of Sydney, 2001 - 2008.  ABN 15 211 513 464
% herafter 'The University'.
%
% The copyright of the computer program(s) herein is the property of
% The University of Sydney, Australia.  The program(s) may be used
% and/or copied only with the written permission of The University of
% Sydney, or in accordance with the terms and conditions stipulated
% in the contract under which the program(s) have been supplied.
% 
% Disclaimer:
% The Universitiy makes no representation or warranty
% that these materials, including any software, are free from errors;
% about the quality or performance of these materials;
% that these materials are fit for any particular purpose.
% These materials are made available on the strict basis that The University
% and its employees or agents have no liability for any direct or indirect
% loss or damage (including for negligence) suffered by any person as a
%consequence of the use of this  material.
%
%Approximates the negate log marginal likelihood of a GP
%and its gradient by selecting points according to a deterministic 
%sampling procedure. It is useful for optimisers like
%fminunc which requires a single function passing lml and
%the gradient.
%
%INPUT
%
%OUTPUT
%
% Fabio Tozeto Ramos 12/06/07
% [nlml,ng] = gpamdlmlgrad(params,kfun,kgradfun)
function [nlml,ng] = gpsellmlgrad(params,kfun,kgradfun)
global X y K;%L invK K alpha;
noise = params(end);
kpar = params(1:end-1);
[D,N] = size(X);
ind = zeros(1,5000);
ii = 1;
ppl = 10; %points per length
%Select points
for i = 1:D
    %Build the KDtree
    [kd,Xsor] = vgg_kdtree2_build(X(i,:),1);
    %Necessary according to the KDTree author for efficiency
    indXsor = kd.indices;
    kd.indices = uint32(1:N);
    mind = min(X(i,:));
    maxd = max(X(i,:));
    pts = linspace(mind,maxd,ceil((maxd-mind)/(params(i)/ppl)));
    %pts = linspace(mind,maxd,ppl);
    for j=1:length(pts)
        [is] = vgg_kdtree2_closest_point(kd,Xsor,pts(j),params(i));
        %[is] = vgg_kdtree2_closest_point(kd,Xsor,pts(j),20);
        if ~isempty(is) && is~=0
            is = is(is~=0);
            ind(ii) = indXsor(is);
            ii = ii + 1;
        end
    end
end
ind = unique(ind(1:(ii-1)));

%n = size(X,2);
K = feval(kfun,X(:,ind),X(:,ind),kpar);

%Selects 1000 points with more non-zero elements
K2 = full(K); 
y2 = y(ind);
I = eye(length(ind));

try   
    L = chol(K2+(noise^2)*I)'; 
    invL = L\I;
    invK = invL'*invL;
    alpha = invK*y2';
    
    %Compute the log marginal likelihood
    nlml = 0.5*y2*alpha + sum(log(diag(L))) + 0.5*length(ind)*log(2*pi);
    
catch   
    invK = pinv(K2+(noise^2)*I);
    alpha = invK*y2';
    %Compute the log marginal likelihood
    nlml = 0.5*y2*alpha + 0.5*log(det(K2+(noise^2)*I)) + 0.5*length(ind)*log(2*pi);
end

if ~isempty(kgradfun)
    %Compute the gradient
    kgrad = feval(kgradfun,kpar,K2,X(:,ind));
    ng = zeros(1,length(kgrad)+1);

    % precompute for convenience
    W = invK - alpha*alpha';
    for i = 1:length(kgrad)
        ng(i) = sum(sum(W.*kgrad{i}))/2;
    end
    %Noise gradient
    ng(end) = trace(W*noise);
else
    ng = 0;
end