%Demonstrates the use of Gaussian processes classification with the
%Expectation propagation approximation. Adapted from Rasmussen's code.
global X y

disp(' ')
disp('Demonstrates GPC for an environment with a wall')
disp(' ')


x1 = [1:5 9:12 1:5 9:12 1:5 9:12 1:12 1:12];
x2 = [ones(1,9) 2*ones(1,9) 3*ones(1,9) 4*ones(1,12) 5*ones(1,12)];

X = [x1;x2];
y = [-1*ones(1,4) 1 -1*ones(1,4) -1*ones(1,4) 1 -1*ones(1,4) -1*ones(1,4) 1 -1*ones(1,4)...
    -1*ones(1,4) ones(1,4) -1*ones(1,4) -1*ones(1,12)];

figure(1); hold on
plot(x1(y==-1),x2(y==-1),'b+')
plot(x1(y==1),x2(y==1),'r+')




[t1 t2] = meshgrid(-5:0.5:20,-5:0.5:8);
t = [t1(:) t2(:)]';


kpar = [1 1 1];
[kpar1] = gpceplearn(@cov_sqexp,@grad_sqexp,kpar);
[p2,mf2,v2] = gpcepeval(X,y,@cov_sqexp,kpar1,[],t);


%figure(2); hold on
% title('Results after learning with Sq Exp');
% plot(t(1,p2>0.5),t(2,p2>0.5),'ro')
% plot(t(1,p2==0.5),t(2,p2==0.5),'go')
% plot(t(1,p2<0.5),t(2,p2<0.5),'bo')
title('Results after learning with Sq Exp');
plot(t(1,p2>0.6),t(2,p2>0.6),'ro')
plot(t(1,p2>0.4&p2<0.6),t(2,p2>0.4&p2<0.6),'go')
plot(t(1,p2<0.4),t(2,p2<0.4),'bo')



figure(2); hold on
kpar = [1 1 1 1];
[kpar2] = gpceplearn(@cov_nn2,@grad_nn2,kpar);
[p3,mf3,v3] = gpcepeval(X,y,@cov_nn2,kpar2,[],t);

plot(x1(y==-1),x2(y==-1),'b+')
plot(x1(y==1),x2(y==1),'r+')


% title('Results after learning with NN');
% plot(t(1,p3>0.5),t(2,p3>0.5),'ro')
% plot(t(1,p3==0.5),t(2,p3==0.5),'go')
% plot(t(1,p3<0.5),t(2,p3<0.5),'bo')
title('Results after learning with NN');
plot(t(1,p2>0.6),t(2,p2>0.6),'ro')
plot(t(1,p2>0.4&p2<0.6),t(2,p2>0.4&p2<0.6),'go')
plot(t(1,p2<0.4),t(2,p2<0.4),'bo')

