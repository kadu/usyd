%Generalised Conjugate gradient method for solving AX=B
%where B can have multiple columns
%function [x,k] = gcg(A,B,x,kmax,epsi)
%
function [x,k] = gcg(A,B,x,kmax,epsi)
[nrows,ncols] = size(B);
if nargin<3
    x = rand(nrows,ncols);
    kmax = 5*nrows;
    epsi = 1e-6;
end
if nargin<4
    kmax = 5*nrows;
    epsi = 1e-6;
end
if nargin<5
    epsi = 1e-6;
end
if isempty(x)
    x = sprand(nrows,ncols,0.1);
end

%Vectorised Conjugate Gradient for Multiple Columns in B
v = zeros(kmax,ncols);
k = 1;
r = B - A*x;
v(1,:) = sum(r.^2);
while (k<kmax) && ...
        ~isempty(find(sqrt(v(k,:))-epsi*sqrt(sum(B.^2))>0,1))
    k = k + 1;
    if k == 2
        p = r;
    else
        beta = v(k-1,:)./v(k-2,:);
        p = r + spdotmult(beta,p);
    end
    %w = ssmult(A,p);
    w = A*p;
    a = v(k-1,:)./sum(p.*w);
    
    x = x + spdotmult(a,p);
    r = r - spdotmult(a,w);
    v(k,:) = sum(r.^2);
end
%disp(k);


% for i=1:ncols
%     b = B(:,i);    
%     v = zeros(1,kmax);
%     %Conjugate Gradient 
%     k = 1;
%     r = b - A*x(:,i);
%     v(1) = sum(r.^2);
%     while (sqrt(v(k))>epsi*norm(b)) && (k<kmax)
%         k = k + 1;
%         if k == 2
%             p = r;
%         else
%             beta = v(k-1)/v(k-2);
%             p = r + beta*p;
%         end
%         w = A*p;
%         a = v(k-1)/(p'*w);
%         x(:,i) = x(:,i) + a*p;
%         r = r - a*w;
%         v(k) = sum(r.^2);
%     end
%     %disp(k);
% end
% 



%}





function A=reprow(B,N)
A = B(ones(1,N),:);
        
function A=spdotmult(a,b)
[nrows,ncols] = size(b);
[i,j] = find(b);
v = a(j);
%v = zeros(1,length(j));
%is = 1;
%for k=1:ncols
%    tmp = nonzeros(b(:,k));
%    ie = is + length(tmp) - 1;
%    v(is:ie) = a(k)*tmp;
%    is = ie + 1;
%end
tmp = sparse(i,j,v,nrows,ncols);
A = tmp.*b;