% Copyright The University of Sydney, 2001 - 2008.  ABN 15 211 513 464
% herafter 'The University'.
%
% The copyright of the computer program(s) herein is the property of
% The University of Sydney, Australia.  The program(s) may be used
% and/or copied only with the written permission of The University of
% Sydney, or in accordance with the terms and conditions stipulated
% in the contract under which the program(s) have been supplied.
% 
% Disclaimer:
% The Universitiy makes no representation or warranty
% that these materials, including any software, are free from errors;
% about the quality or performance of these materials;
% that these materials are fit for any particular purpose.
% These materials are made available on the strict basis that The University
% and its employees or agents have no liability for any direct or indirect
% loss or damage (including for negligence) suffered by any person as a
%consequence of the use of this  material.
%
% Train a GP model with scaled conjugade gradient

function [params, fval] = gplearnNoGlobCovSumFixedNoise(X,y,kfuns,npar,kgfun,kpar0,noise,options)


params = [kpar0];
%[params, options] = scg(@(params) gplmleval(params, kfun), ...
%    params,options,@(params) gpgradeval(params, kgfun, kfun));
%[params, options] = quasinew(@(params) gplmleval(params, kfun), ...
%    params,options,@(params) gpgradeval(params, kgfun, kfun));
 
%Matlab optimisation toolbox 
% <<<<<<< gplearn.m
% opts = optimset('LargeScale','on','MaxIter',500,'Diagnostics', 'on',...
% 'GradObj', 'on', 'Display', 'iter','TolFun',  1e-10, 'MaxFunEvals', 5000);
% 
% =======
opts = optimset('LargeScale','off','MaxIter',100,'Diagnostics', 'on',...
'GradObj', 'on', 'Display', 'iter','TolFun',  1e-10, 'MaxFunEvals', 5000);

% >>>>>>> 1.18
%KNITRO options
% <<<<<<< gplearn.m
% opts_knitro = optimset('Display', 'iter','MaxIter',500, 'GradObj','on', 'TolFun', 1e-10,...
%     'MaxFunEvals', 5000,'Algorithm','active-set');
% =======
% opts_knitro = optimset('Display', 'iter','MaxIter',100, 'GradObj','on', 'TolFun', 1e-10,...
%     'MaxFunEvals', 5000,'Algorithm','active-set');
% >>>>>>> 1.18

% <<<<<<< gplearn.m
% lb=-inf*ones(1,length(params));
% lb(end) = -0.1;
% ub=inf*ones(1,length(params));
% ub(end) = 0.1;
% =======
if isempty(kgfun)
    opts.GradObj = 'off';
    opts_knitro.GradObj = 'off';
end


lb=-inf*ones(1,length(params));
lb(end) = -0.1;
ub=inf*ones(1,length(params));
ub(end) = 0.1;
% >>>>>>> 1.18
%tic;[params, options]=fminunc(@(params) gplmlgrad(params, kfun, kgfun), params,opts);toc;
%tic;[params, options]=fmincon(@(params) gplmlgrad(params, kfun, kgfun), params,...
%    [],[],[],[],lb,ub,[],opts);toc
 
%Simulated Annealing
% <<<<<<< gplearn.m
% [params, fval] = anneal(@(params) gplmlgrad(params, kfun, kgfun), params);
[params, fval] = anneal(@(params) gplmlevalNoGlobFixedNoise(X,y,params,noise, kfuns,npar), params);
% [params, options]=fminunc(@(params) gplmlevalNoGlob(X,y,params, kfun), params,opts);

% tic;[params, options]=fminunc(@(params) gplmlgrad(params, kfun, kgfun), params,opts);toc;
%tic; [params, options]=ktrlink(@(params) gplmlgrad(params, kfun, kgfun), params,...
 %   [],[],[],[],[],[],[],opts_knitro);toc;
% =======
%[params, fval] = anneal(@(params) gplmlgrad(params, kfun, kgfun), params);
% tic;[params, options]=fminunc(@(params) gplmlgrad(params, kfun, kgfun), params,opts);toc;
%tic; [params, options]=ktrlink(@(params) gplmlgrad(params, kfun, kgfun), params,...
%    [],[],[],[],[],[],[],opts_knitro);toc;
% >>>>>>> 1.18
%tic;[params, options]=fminunc(@(params) gpsrlmlgrad(params, kfun, kgfun, 1:4:20), params,opts);toc;
%Genetic Algorithm Optimization
%[params, fval] = ga(@(params) gplmlgrad(params, kfun, kgfun), length(params));
%disp(fval);

%Simulated Annealing in Matlab
%[params, fval] = simulannealbnd(@(params) gplmlgrad(params, kfun, kgfun), params);
%disp(fval);