% Original K Matrix = K11
% Deciding to add 2 points to the covariance matrix.
% KUpdated would then look like [K11 K12;K12' K22]
% Where K12 is the covariance between the new points and the old matrix
% and K22 is the covariance of the new points.

% demo cholesky factor update

K11 = [0.8 0.7 0.6 0.65; 0.7 0.95 0.5 0.8;...
      0.6 0.5 0.75 0.4; 0.65 0.8 0.4 1.2]

K12 = [0.7 0.05;0.8 0.1; 0.55 0.15;1.1 0.2]
K22 = [1.4 0.25;0.25 0.3]

Chol11 = chol(K11)

KUpdated = [K11 K12;K12' K22]

%Updated Cholesky Ground Truth
CholUpdatedTruth = chol(KUpdated)
%Updated inverted K Ground Truth
invKTruth = CholUpdatedTruth'\(CholUpdatedTruth\eye(length(KUpdated)))

%Updated Cholesky Approximation:
CholUpdatedApprox = updateCholK(Chol11,K12,K22)
%Updated inverted K Approximation:
invKApprox = CholUpdatedApprox'\(CholUpdatedApprox\eye(length(KUpdated)))

CholDiff = CholUpdatedTruth - CholUpdatedApprox
invDiff = invKTruth - invKApprox


