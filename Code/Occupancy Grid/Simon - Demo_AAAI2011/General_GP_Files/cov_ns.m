% Copyright The University of Sydney, 2001 - 2008.  ABN 15 211 513 464
% herafter 'The University'.
%
% The copyright of the computer program(s) herein is the property of
% The University of Sydney, Australia.  The program(s) may be used
% and/or copied only with the written permission of The University of
% Sydney, or in accordance with the terms and conditions stipulated
% in the contract under which the program(s) have been supplied.
% 
% Disclaimer:
% The Universitiy makes no representation or warranty
% that these materials, including any software, are free from errors;
% about the quality or performance of these materials;
% that these materials are fit for any particular purpose.
% These materials are made available on the strict basis that The University
% and its employees or agents have no liability for any direct or indirect
% loss or damage (including for negligence) suffered by any person as a
%consequence of the use of this  material.
%
% Compute the Nonstationary covariance function as

% appears in "Nonstationary Covariance Functions for Gaussian Process

% Regression" by Paciorek and Schervish (NIPS 2003)

% 

%INPUTS

% X1 - D x N input points 1

% X2 - D x M input points 2

% par - cell with the following fields 

%   par(1:end-1) - characteristic length-scales

%   par(end) - sigma f square

%OUTPUT

% K - covariance function

%

% Fabio Tozeto Ramos 06/05/2008

% K = cov_ns(X1,X2,par)



function K = cov_ns(X1,X2,par)

global tempK

N1 = size(X1,2); %number of points in X1

N2 = size(X2,2); %number of points in X2

D = size(X1,1);



if size(X1,1)~=size(X2,1)

    error('Dimensionality of X1 and X2 must be the same');

end





%Compute K only when not available

if isempty(tempK)

    K = zeros(N1,N2);

    for i=1:N1

        %Compute the kernel matrix for X1

        t1 = exp(-0.5*(X1(:,setxor(1:N1,i)) - repmat(X1(:,i),[1 N1-1]))/10);

        S1 = (1/(N1-1))*t1*t1';

        for j=1:N2        

            t2 = exp(-0.5*(X1 - repmat(X2(:,j),[1 N1]))/10);

            S2 = (1/(N1))*t2*t2';

            Q = (X1(:,i)-X2(:,j))'*inv(0.5*(S1+S2))*(X1(:,i)-X2(:,j));

            K(i,j) = (det(S1)^0.25)*(det(S2)^0.25)*(det(0.5*S1+S2)^(-0.5))*exp(-Q);

        end

    end

    tempK = K;

else

    K = tempK;

end



K = ((par(end).^2)*2^(0.5*D))*K; 



