

  function Graph2DCovFunc(cov_func, npar, params, TestPoint, LowerBound,UpperBound)

%  Function that graphs the correlation between a TestPoint and its
%  surrounding points for a given covariance function or set of covariance functions
%  and hyperparameters.
% 
% -cov_func should be a cell array with elements referencing the covariance
%  functions. 
%  e.g. cov_func = {@cov_nn2, cov_sqexp}
% 
%-npar is the number of parameters per cov func. Stored in a row matrix in
% the same order as their corresponding covariance function appears in cov_func
% e.g. npar = [5,3]
%
% -params should be a row matrix containing the hyperparameters of the
%  covariance functions in the same order which they are listed in
%  cov_func. 
%  e.g. params = [0.2,0.12,0.8, 0.16,-0.3,0.24,1.2,0.89,0.79] for a 2D 
%  neural network and squared exponential combination 
% 
% -TestPoint is the point which you want to the covariance to be measured
%  relative to. Must be of the form [x;y]
% 
% -LowerBound and UpperBound are the bounds of the graph.
% 
% Simon O'Callaghan 22/07/2008


Resolution = (UpperBound - LowerBound)/100;
                                                    
[xeva,yeva] = meshgrid(LowerBound:Resolution:UpperBound,...
 LowerBound:Resolution:UpperBound);
 xeva2 = reshape(xeva,[1 size(xeva,1)*size(xeva,2)]);
 yeva2 = reshape(yeva,[1 size(yeva,1)*size(yeva,2)]);

 
 x = [xeva2;yeva2];

%  y = feval(cov_func{1},x,TestPoint,params);
 y = cov_sum(x,TestPoint,cov_func,npar,params);
 yshaped = reshape(y,size(xeva,1),size(yeva,1));
 figure
 surf(xeva,yeva,yshaped)
 title('Covariance over a 2D plane');
