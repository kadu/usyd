global X y
clear X2 y2
X = []; y = [];
N = 500; % number of points for learning

%Imports
%[temp]   = importdata('/home/ACFR/f.ramos/acai/Datasets/se10_fabio.txt',',');
[temp]   = importdata('~/Datasets/se10_fabio.txt',',');
textdata = temp.textdata;

for i=1:3 
    for j=1:2378 
        X2(j,i)=str2double(textdata{j+1,i+1}); 
    end
end
for i=1:2378 
    y2(i) = str2double(textdata{i+1,14}); 
end

my2 = mean(y2);
X2  = X2/1e4;
X2  = X2';
y2  = y2 - my2;

%params0 = [0.000000589284718  0.000000441114811   0.000000086121546 ...
%    32.071822004798022  38.892460282552570  4.219390904423386];
params0 = 0.1*rand(1,2*(3+5+4)+1);
options = zeros(1,18);
options(1)=1; options(9)=1; options(14)=40;


ind = randperm(length(X2));
X   = X2(:,ind(1:N));
y   = y2(ind(1:N)) - mean(y2(ind(1:N)));

%[params] = gpkdtreelearn(@cov_nn2,@grad_nn2,params0(1:end-1),params0(end),options,100,[1:100:2200])
[params] = gplearn(@cov_nsmix,[],params0(1:end-1),params0(end),options);
disp(params);

%Necessary to avoid numerical problems
xmax = 22870/1e4;
ymax = 20410/1e4;
zmax = 770/1e4;

xmin = 22690/1e4;
ymin = 19300/1e4;
zmin = 500/1e4;


[xt,yt,zt] = meshgrid(xmin:(3*7.5/1e4):xmax,ymin:(3*7.5/1e4):ymax,zmin:(3*2/1e4):zmax);
[sx,sy,sz] = size(xt);
x1d = reshape(xt,[1 sx*sy*sz]);
y1d = reshape(yt,[1 sx*sy*sz]);
z1d = reshape(zt,[1 sx*sy*sz]); 



%tic
[lml,mf,vf] = gpkdtreeeval(X2,y2,...
    @cov_nsmix,params(1:end-1),params(end),[x1d;y1d;z1d],200);
S2 = vf - params(end)^2;
%toc


v = mf;% + mean(material);
v = reshape(v,[sx,sy,sz]);

hslice = surf(linspace(xmin,xmax,100),linspace(ymin,ymax,100),(zmax+zmin)/2*ones(100));

rotate(hslice,[(xmax+xmin)/2,(ymax+ymin)/2,(zmax+zmin)/2],-45);
xd = get(hslice,'XData');
yd = get(hslice,'YData');
zd = get(hslice,'ZData');
delete(hslice)

%Draw the slices
h = slice(xt,yt,zt,v,xd,yd,zd);
set(h,'FaceColor','interp',...
 'EdgeColor','none',...
 'DiffuseStrength',.8);

hold on
%hx = slice(xt,yt,zt,v,max(x1d),[],[]);
%set(hx,'FaceColor','interp','EdgeColor','none')

hx1 = slice(xt,yt,zt,v,2.2835,[],[]);
set(hx1,'FaceColor','interp','EdgeColor','none')

hx2 = slice(xt,yt,zt,v,2.2778,[],[]);
set(hx2,'FaceColor','interp','EdgeColor','none')

%hx3 = slice(xt,yt,zt,v,2.2707,[],[]);
%set(hx3,'FaceColor','interp','EdgeColor','none')


hy = slice(xt,yt,zt,v,[],max(y1d),[]);
set(hy,'FaceColor','interp','EdgeColor','none')


hz = slice(xt,yt,zt,v,[],[],min(z1d));
set(hz,'FaceColor','interp','EdgeColor','none')


%Defining the conce(1,:)iew

daspect([1,1,1])
axis tight
box on
view(-38.5,16)
camzoom(1)
camproj perspective

%lightangle(-45,45)
colormap (jet(24))
set(gcf,'Renderer','zbuffer')

figure(1); hold on;
scatter3(X2(1,:),X2(2,:),X2(3,:), [],y2);
axis([xmin xmax ymin ymax zmin zmax]);

poses     = [x1d;y1d;z1d]*1e4;
grade     = mf + my2;
variances = vf;
alldata   = [poses' grade' variances'];
save -ASCII ./results.txt alldata