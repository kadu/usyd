function [kd,X] = vgg_kdtree2_build(D,max_leaf_count,verbose)



% [kd,X] = vgg_kdtree2_build_mex(D,max_leaf_count,verbose)

% 

% Takes an M-by-N matrix with one M-D datum per column and returns a

% vgg_kdtree2 structure. If required, an M-by-N matrix, X, containing the 

% input data in a different order (grouped by leaf), can be returned. It is

% more efficient to use X instead of D in searches, but kd.indices must be

% set to [1:N] before this will work.

%

% NOTES: 

%   N must be less than 2^32. 

%   D must be a numeric, i.e. real and one of the data types listed below

%   The following data type association is assumed:

%     MATLAB    C++

%     int8      char

%     uint8     unsigned char

%     int16     short

%     uint16    unsigned short

%     int32     int

%     uint32    unsigned int

%     single    float

%     double    double

% 

% Override the default target data count for each leaf (100) by giving a 

% value for the optional argument max_leaf_count.

% 

% Set verbose = 1 to get excessive output. Default is no verbose output.

% NB: verbose not implemented.

%

% See also

%   vgg_kdtree2_closest_point

%   vgg_kdtree2_N_closest_points

%   vgg_kdtree2_ball_search

%   vgg_kdtree2_demo



% author: Aeron Buchanan <amb@robots.ox.ac.uk>

% date: 14th April 2005

% revised: 2nd December 2005



if nargin<3, verbose = 0;

	if nargin<2, max_leaf_count = 100;

	end

end



[m,n] = size(D);



min_num_leaves = ceil(n/max_leaf_count);

num_levels = ceil(log(min_num_leaves)/log(2));

num_leaves = 2^num_levels;

num_nodes = num_leaves - 1;

		

% mex array creation seems excrutiatingly slow so allocate memory here



% No need to create matrix for tree nodes as all the following can be 

% directly inferred from position in a breadth-first flattening of a binary tree:

% axis, left leaf/node pointer, right leaf/node pointer. See code and note above.

	

% Create vector of thresholds (one per node)

thresholds = zeros(1,num_nodes);



% Create vector of splitting dimensions (one per node)

% axis_dimensions = zeros(1,num_nodes,'uint32'); % matlab 7 version

axis_dimensions = uint32(zeros(1,num_nodes));



% Create matrix for tree leaves

% Each leaf is column index into X for where that chunk starts

% leaves = zeros(1,num_leaves,'uint32'); % matlab 7 version

leaves = uint32(zeros(1,num_leaves));



% Create index vector

% indices = zeros(1,n,'uint32'); % matlab 7 version

indices = uint32(zeros(1,n));



% Build Tree

vgg_kdtree2_build_mex(D,thresholds,axis_dimensions,leaves,indices);



% Create output structure

kd = struct('breadth_first_thresholds',thresholds,'axis_dimensions',axis_dimensions,'leaves',leaves,'indices',indices);



if nargout>1

	% mex data copy too hard to get to work [without crashing], so included here

	X = D(:,indices);

end

