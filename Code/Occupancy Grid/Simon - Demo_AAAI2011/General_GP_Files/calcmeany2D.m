
function [ymean] = calcmeany2D(y,XS,XE)

% y = [5 3 -8];
% XS = [-10 -1 9; 3 5 6];
% XE = [-2 5 12; 4 7 9];

% y = [1 2 3];
% XS = [1 3 9; 1 4 6];
% XE = [2 5 9; 1 4 8];

LineLength = sqrt(sum(abs(XS-XE).^2,1));
TotalLineLength = sum(LineLength);

ymean = sum(y.*LineLength)/TotalLineLength;