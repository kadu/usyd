%Computes the probability of an WGP after passing through a nonlinear
%warping function.
%function [p] = probwgp(x,mf,vf,wf,wdf,par)
function [p] = probwgp(x,mf,vf,wf,wdf,par)

p = feval(wdf,par,x)*inv(sqrt(2*pi*vf));
p = p.*exp(-0.5*inv(vf)*(feval(wf,par,x)-mf).^2);