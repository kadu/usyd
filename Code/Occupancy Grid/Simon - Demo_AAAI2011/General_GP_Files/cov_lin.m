% Copyright The University of Sydney, 2001 - 2008.  ABN 15 211 513 464
% herafter 'The University'.
%
% The copyright of the computer program(s) herein is the property of
% The University of Sydney, Australia.  The program(s) may be used
% and/or copied only with the written permission of The University of
% Sydney, or in accordance with the terms and conditions stipulated
% in the contract under which the program(s) have been supplied.
% 
% Disclaimer:
% The Universitiy makes no representation or warranty
% that these materials, including any software, are free from errors;
% about the quality or performance of these materials;
% that these materials are fit for any particular purpose.
% These materials are made available on the strict basis that The University
% and its employees or agents have no liability for any direct or indirect
% loss or damage (including for negligence) suffered by any person as a
%consequence of the use of this  material.
%
% Compute the linear covariance function
% 
%INPUTS
% X1 - D x N input points 1
% X2 - D x M input points 2
% par - cell with the following fields 
%   par(1:end-1) - characteristic length-scales
%   par(end) - bias
%
%OUTPUT
% K - covariance function
%
% Fabio Tozeto Ramos 03/05/08
% K = cov_nn(X1,X2,par)

function K = cov_lin(X1,X2,par)
N1 = size(X1,2); %number of points in X1

if size(X1,1)~=size(X2,1)
    error('Dimensionality of X1 and X2 must be the same');
end

K = par(end).^2 + (repmat((par(1:end-1).^(-2))',[1 N1]).*X1)'*X2;
