% Approximates the integral \int logistic(x) N(x; mu, var) dx using the Gauss
% Hermite approximation
function pout = hermintlogit(mu,Sig)
[C,N] = size(mu);
pout  = zeros(C,N);

dim_num = size(mu,1); 
level_max = 2;
    
if nargin < 6
    %Generate weights and points 
    %Determine the number of points
    point_num = sparse_grid_herm_size ( dim_num, level_max );
 
    %Compute the weights and points.
    [w, x] = sparse_grid_herm ( dim_num, level_max, point_num );    
end

for i = 1:C
    %logitc = @(x) exp(x(i))./repmat(sum(exp(x,1)),C,1);
    for j = 1:N   
        sig      = sqrt(diag(Sig(:,j)));
        y        = bsxfun(@plus,sqrt(2)*sig*x, mu(:,j));
        % Evaluates the function f in points y
        %fv       = feval(f,y);
        fv       = exp(y(i,:))./sum(exp(y),1);
        pout(i,j)= sum(fv.*w)/sqrt(pi^dim_num);
    end
end