%Transforms parameters from cells to array.
function tparams = transpar(params)
[Np] = length(params{1}); 
[N1,N2] = size(params{2});
t1 = reshape(params{1},1,Np);
t2 = reshape(params{2}',1,N1*N2);
tparams = [t1 t2];