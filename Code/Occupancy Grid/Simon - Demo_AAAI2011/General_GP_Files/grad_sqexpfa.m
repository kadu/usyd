% Copyright The University of Sydney, 2001 - 2008.  ABN 15 211 513 464
% herafter 'The University'.
%
% The copyright of the computer program(s) herein is the property of
% The University of Sydney, Australia.  The program(s) may be used
% and/or copied only with the written permission of The University of
% Sydney, or in accordance with the terms and conditions stipulated
% in the contract under which the program(s) have been supplied.
% 
% Disclaimer:
% The Universitiy makes no representation or warranty
% that these materials, including any software, are free from errors;
% about the quality or performance of these materials;
% that these materials are fit for any particular purpose.
% These materials are made available on the strict basis that The University
% and its employees or agents have no liability for any direct or indirect
% loss or damage (including for negligence) suffered by any person as a
%consequence of the use of this  material.
%
% Compute the gradient of the squared exponential 
% covariance function.
%
%INPUT
%  kpar - parameters 
%OUTPUT
%  g - gradient matrix
%
%
% Fabio Tozeto Ramos 01/05/08
% g = grad_sqexpfa(kpar)

function g = grad_sqexpfa(kpar)
global K X;
[d,n] = size(X);
g = cell(1,d^2+1); %weights plus sigma f

for i=1:d
    %Compute weighted squared distance 
    w = kpar(i)^(-3);
    XX = X(i,:).*X(i,:);
    XTX = X(i,:)'*X(i,:);
    XXT = XX';
    z = XXT(:,ones(1,n)) + XX(ones(1,n),:) - 2*XTX;
    g{i} = w*K.*z; 
end

%XX = X.*X;
%XTX = X'*X;
%XXT = XX';
%z = XXT(:,ones(1,n)) + XX(ones(1,n),:) - 2*XTX;
%z = X-;

for i=d+1:d^2  
    XX = X(i-2,:).*X(i-2,:);
    XTX = X(i-2,:)'*X(i-2,:);
    XXT = XX';
    z = XXT(:,ones(1,n)) + XX(ones(1,n),:) - 2*XTX;
    g{i} = -kpar(i)*K.*z;
end
g{end} = K*(2/kpar(end));