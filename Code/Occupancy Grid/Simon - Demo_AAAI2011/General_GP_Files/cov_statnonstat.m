% Copyright The University of Sydney, 2001 - 2008.  ABN 15 211 513 464
% herafter 'The University'.
%
% The copyright of the computer program(s) herein is the property of
% The University of Sydney, Australia.  The program(s) may be used
% and/or copied only with the written permission of The University of
% Sydney, or in accordance with the terms and conditions stipulated
% in the contract under which the program(s) have been supplied.
% 
% Disclaimer:
% The Universitiy makes no representation or warranty
% that these materials, including any software, are free from errors;
% about the quality or performance of these materials;
% that these materials are fit for any particular purpose.
% These materials are made available on the strict basis that The University
% and its employees or agents have no liability for any direct or indirect
% loss or damage (including for negligence) suffered by any person as a
%consequence of the use of this  material.
%
% Computes a covariance function which contains both stationary and
% nonstationary behaviours
% Taken from Computation with Infinite Neural Networks. - Christopher K. I.
% Williams. September 1997. Equation 14.
% Had difficulty training the hyperparameters. 
% 
%INPUTS
% X1 - D x N input points 1
% X2 - D x M input points 2
% par - cell with the following fields 
%   par(1:end-1) - characteristic length-scale
%   par(end) - sigma f square
% The non stationary aspect also contains a learnable bias
%OUTPUT
% K - covariance function
%
% Simon O'Callaghan 25/06/07
% K = cov_statnonstat(X1,X2,par)
    
 function K = cov_statnonstat(X1,X2,par)


N1 = size(X1,2); %number of points in X1

N2 = size(X2,2); %number of points in X2

D  = size(X1,1); %Dimensions of the training points


if size(X1,1)~=size(X2,1)
    error('Dimensionality of X1 and X2 must be the same');
end

    K = zeros(N1,N2);

%----------Computes the Squared Exponential Component------------
w = par(1:D)'.^(-2);

XX1 = sum(w(:,ones(1,N1)).*X1.*X1,1);
XX2 = sum(w(:,ones(1,N2)).*X2.*X2,1);
X1X2 = (w(:,ones(1,N1)).*X1)'*X2;
XX1T = XX1';
z = XX1T(:,ones(1,N2)) + XX2(ones(1,N1),:) - 2*X1X2;
%----------------------------------------------------------------


%-----------Computes The Dot Product Components------------------
X1DotArray = ones(N1).*(par(D+1).^(-2));    %Bias Component
X2DotArray = ones(N2).*(par(D+1).^(-2));

for counter = 1:D
 
    X1DotArray = X1DotArray + ((X1(counter,:)'*X1(counter,:)).*(par(D+1+counter).^(-2))); % Evaluates Dot Product Matrix for X1
 
    X2DotArray = X2DotArray + ((X2(counter,:)'*X2(counter,:)).*(par(D+1+counter).^(-2))); % Evaluates Dot Product Matrix for X1
  
end
%----------------------------------------------------------------


K = exp(2*log(par(end))).*(exp(-0.5*X1DotArray)*exp(-0.5*(z))*exp(-0.5*X2DotArray)); %put the sf2 inside the exponential
%to avoid numerical problems.
