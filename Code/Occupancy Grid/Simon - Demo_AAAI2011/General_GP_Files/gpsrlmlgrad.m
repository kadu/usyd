% Copyright The University of Sydney, 2001 - 2008.  ABN 15 211 513 464
% herafter 'The University'.
%
% The copyright of the computer program(s) herein is the property of
% The University of Sydney, Australia.  The program(s) may be used
% and/or copied only with the written permission of The University of
% Sydney, or in accordance with the terms and conditions stipulated
% in the contract under which the program(s) have been supplied.
% 
% Disclaimer:
% The Universitiy makes no representation or warranty
% that these materials, including any software, are free from errors;
% about the quality or performance of these materials;
% that these materials are fit for any particular purpose.
% These materials are made available on the strict basis that The University
% and its employees or agents have no liability for any direct or indirect
% loss or damage (including for negligence) suffered by any person as a
%consequence of the use of this  material.
%
%Compute the Subset of Regressors approximation of the 
%negate log marginal likelihood of a GP
%and its gradient. It is useful for optimisers like
%matlab which requires a single function passing lml and
%the gradient.
%
% The notation follows the paper "Fast Forward Selection to Speed Up
% Sparse Gaussian Process Regression" by Matthias Seeger et al.
%
%INPUT
% si indexes for the active set
%OUTPUT
%
% Fabio Tozeto Ramos 04/11/08
% [nlml,ng] = gpsrlmlgrad(params,kfun,kgradfun,si)
function [nlml,ng] = gpsrlmlgrad(params,kfun,kgradfun,si)
global X y %L invK K alpha;
noise = params(end);
kpar = params(1:end-1);
n = size(X,2);
yt = reshape(y,1,length(y));
yt = yt - mean(y);


if isempty(si)

else
    m = length(si);
end

KI = feval(kfun,X(:,si),X(:,si),kpar);
KIn = feval(kfun,X(:,si),X,kpar);
if issparse(KI)
    I = speye(m);
else
    I = eye(m);
end

L = jitChol(KI)';
V = L\KIn;
M = V*V'+(noise^2)*I;
Lm = jitChol(M)';
b = (Lm\V)*yt';


%Compute the log marginal likelihood
yb = yt*yt'-b'*b;
nlml =  sum(log(diag(Lm))) + 0.5*(n-m)*log(noise^2) + ...
    0.5*(1/(noise^2))*yb+0.5*n*log(2*pi);
%Adds a prior
%nlml = nlml;% + ((params-[4 2 0.4])./[0.1 0.1 0.005])*(params-[4 2 0.4])';
if isnan(nlml) || isinf(nlml)
    error('nlml is nan of inf');
end


if ~isempty(kgradfun)
    %Compute necessary matrices
    Lt = L*Lm;
    B1 = Lt'\(Lm\V);
    %Compute the variance
    %opts.TRANSA = true;
    %opts.LT     = true;
    %v           = linsolve(L,ks,opts);
    
    %B1 = linsolve(Lt,linsolve(Lm,V,opts),opts);
    %B1 = solve_tril(Lt,solve_tril(Lm,V));
    b1 = Lt'\b;
    invKI = L'\(L\I);
    invA = Lt'\(Lt\I);
    
    %Compute the gradient
    KId = feval(kgradfun,kpar,KI,X(:,si));
    tmp = feval(kgradfun,kpar,KIn,X(:,si),X);
    
    
    mu = V'*(Lm'\b);
    ng = zeros(1,length(KId)+1);

    for i = 1:length(tmp)
        ng(i) = -0.5*sum(sum((invKI-(noise^2)*invA).*KId{i})) + ...
            sum(sum(B1.*tmp{i})) - (noise^(-2))*b1'*tmp{i}*(yt-mu')'...
            +0.5*b1'*KId{i}*b1;
    end
    %Noise gradient
    ng(end) = ((noise^2)*trace(Lm'\(Lm\I)) + n - m + ...
        sum((Lm'\b).^2) - (noise^(-2))*yb)*(noise)^(-1);
    
else
    ng = 0;
end