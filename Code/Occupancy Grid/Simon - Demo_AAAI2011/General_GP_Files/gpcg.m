%Generalised Preconditioned Conjugate gradient method for solving AX=B
%where B can have multiple columns
%function [x,k] = gpcg(A,B,x,kmax,epsi)
%
function [x,k] = gpcg(A,B,M,x,kmax,epsi)
[nrows,ncols] = size(B);
if nargin<4
    x = rand(nrows,ncols);
    kmax = nrows;
    epsi = 1e-3;
end
if nargin<5
    kmax = nrows;
    epsi = 1e-3;
end
if nargin<6
    epsi = 1e-3;
end
if isempty(x)
    x = sprand(nrows,ncols,0.1);
end

  
%Preconditioned Conjugate Gradients
for i=1:ncols
    b = B(:,i);
    [x(:,i),flag,err,iter,res] = pcg(A,b,epsi,kmax,M,M');
    %disp(iter);
%     k = 0;
%     b = B(:,i);
%     r = b - A*x(:,i);
%     while norm(r)>epsi && (k<kmax)        
%         if k~=0
%             pz = z;
%         end
%         z = M\r;
%         k = k + 1;
%         if k == 1
%             p = z;
%         else
%             beta = (r'*z)/(pr'*pz);
%             p = z + beta*p;
%         end
%         a = (r'*z)/(p'*A*p);
%         x(:,i) = x(:,i) + a*p;
%         pr = r;
%         r = r - a*A*p;
%     end
%    disp(k);
end


function A=reprow(B,N)
A = B(ones(1,N),:);
        


