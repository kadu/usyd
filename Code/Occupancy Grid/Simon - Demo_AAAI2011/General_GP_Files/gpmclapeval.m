%Evaluate a Gaussian Process Classifier using the Laplace Approximation
%(see page 59 of "Gaussian Processes for Machine Learning" by
% C. Rasmussen and C. Williams)
%
%INPUTS
% X - D x N inputs
% y - 1 x N outputs (targets)
% kfun - covariance function
% kpar - covariance parameters (hyperparameters)
% K - N x N computed covariance 
% noise - scalar noise level
% x - D x M test inputs
% invK - N x N inverted K if available
% 
%OUTPUTS
% p - predictive probability
% mf - predictive mean
% vf - predictive variance
%
%
% Fabio Tozeto Ramos 12/07/09
% [p,mf,vf,nlml] = gpmclapeval(X,y,kfun,kpar,K,noise,x,f)

function [p,mf,vf] = gpmclapeval(X,y,kfun,kpar,K,x,f)
%persistent best_a;   % keep a copy of the best "a" and its obj value
n    = size(X,2); %number of inputs
C    = length(y)/n; %number of classes
[d,N]= size(x); %dimensionality number of query points
tol  = 1e-6;
E    = cell(1,C);
ks   = cell(1,C);
sumE = zeros(n,n);
z    = zeros(1,C);
mu   = zeros(C,N);
Sig  = zeros(C,N);
R    = repmat(eye(n),C,1);  % stacked identity matrices

if isempty(K)
    %Build K
    K = cell(1,C);
    for i = 1:C
        %ind  = (y((i-1)*n+1:i*n) == 1);
        K{i} = feval(kfun{i},X(:,:),X(:,:),kpar{i});
    end
end
%Computes ks
for i = 1:C
    ks{i} = feval(kfun{i},X(:,:),x(:,:),kpar{i});
end

Kb   = blkdiag(K{:});
%invKb= inv(Kb+0.0001*eye(C*n));


%Finds f if not supplied
if nargin<7
    %f       = [y(1:n) 2*y(n+1:end)]'; 
if 1
    f       = zeros(C*n,1);
    a       = f; 
    Psi_new = -a'*f/2 + y*f - sum(logsum(reshape(f,n,C)',1));
    Psi_old = -inf;                                   % make sure while loop starts
    bPi     = zeros(C*n,n);
    %z       = zeros(1,C);
    while Psi_new - Psi_old > tol                       % begin Newton's iterations
        Psi_old = Psi_new; 
        a_old   = a;
        temp    = logistic(f, C);
        bpi     = reshape(temp',C*n,1);  %bold pi
        for i = 1 : C
            bPi((i-1)*n+1:i*n,:) = diag(temp(i,:));  %bold Pi
        end
        D       =  diag(bpi);
        sumE(:) =  0;
        for i = 1:C
            ii   = (i-1)*n+1;
            ff   = i*n;
            sDc  = sqrt(D(ii:ff,ii:ff));         
            %try
            L    = jitChol(eye(n)+sDc*K{i}*sDc);  
            %catch
            %    L    = chol(3*eye(n)+sDc*K{i}*sDc);
            %end
            E{i} = sDc*(L'\(L\sDc)); 
            %E{i} = sDc*solve_chol(L,sDc)';
            %z(i) = sum(log(diag(L)));
            sumE = sumE + E{i};
        end
        Eb = blkdiag(E{:});
        M  = chol(sumE);
        b  = (D - bPi*bPi')*f + y' - bpi;
        c  = Eb*(Kb*b);
        a  = b - c + Eb*R*(M'\(M\(R'*c)));
        f  = Kb*a;

        Psi_new = -a'*f/2 + y*f - ...
            sum(logsum(reshape(f,n,C)',1));
        i = 0;
        while i < 10 && Psi_new < Psi_old               % if objective didn't increase
            a = (a_old+a)/2;                                 % reduce step size by half
            f = Kb*a;
            Psi_new = -a'*f/2 + y*f - ...
                sum(logsum(reshape(f,n,C)',1));
            i = i + 1;
        end
    end                                                   % end Newton's iterations    
end

%Uses the Matlab optimiser

% opts = optimset('LargeScale','on','MaxIter',500,'Diagnostics', 'off',...
% 'GradObj', 'on', 'Hessian','on','Display', 'off','TolFun',  1e-5, 'MaxFunEvals', 5000);
% f = rand(C*n,1);
% f = fminunc(@(f) fobjective(f,C,n,y,Kb), f,opts);




end



% temp    = logistic(f, C);
% bpi     = reshape(temp',C*n,1);  %bold pi
% D       = diag(bpi);
% sumE(:) = 0;
% 
% for i = 1:C
%     ii   = (i-1)*n+1;
%     ff   = i*n;
%     sDc  = sqrtm(D(ii:ff,ii:ff));                     
%     L    = chol(eye(n)+sDc*K{i}*sDc')';  
%     E{i} = sDc*(L'\(L\sDc)); 
%     z(i) = sum(log(diag(L)));
%     sumE = sumE + E{i};
% end  
% M  = chol(sumE);


%Given all the 
for i = 1:C
    ii   = (i-1)*n+1;
    ff   = i*n;
    mu(i,:) = (y(ii:ff)' - bpi(ii:ff))'*ks{i};                 
    b       = E{i}*ks{i};
    %c       = E{i}*(R*solve_chol(M',solve_chol(M,R'*b)));
    c       = E{i}*(M'\(M\b));
    %c       = E{i}*solve_chol(M,b);
    for j = 1:C
        Sig(i,:) = sum(c.*ks{j},1);
    end
    xcell = mat2cell(x,d,ones(1,N));
    Sig(i,:) = Sig(i,:) + ...
        cellfun(kfun{i}, xcell, xcell, repmat({kpar{i}},1,N)) - ...
        sum(b.*ks{i},1);           
end
p  = hermintlogit(mu,Sig); 

if nargout > 1
    mf = mu; vf = Sig;
end



%Compute the Logit function
function [p] = logistic(f, C)
n  = length(f)/C; %number of points
ft = reshape(f,n,C)';
%yt = reshape(y,C,n);  
%lp = exp(ft(yt==1)')./sum(exp(ft));
lp = ft - repmat(logsum(ft,1),C,1);
p  = exp(lp);



%Objective for f
function [v,dv,ddv] = fobjective(f,C,n,y,Kb)
%a    = Kb\f;
a    = linsolve(Kb,f); warning off verbose;
%v    = 0.5*f'*invKb*f - y*f + sum(logsum(reshape(f,n,C)',1));
v    = 0.5*a'*f - y*f + sum(logsum(reshape(f,n,C)',1));

temp = logistic(f, C);
bpi  = reshape(temp',C*n,1);  %bold pi


dv      = a - y' + bpi;
bPi     = zeros(C*n,n);

for i = 1 : C
     bPi((i-1)*n+1:i*n,:) = diag(temp(i,:));  %bold Pi
end
ddv  = f\a + diag(bpi) - bPi*bPi';

