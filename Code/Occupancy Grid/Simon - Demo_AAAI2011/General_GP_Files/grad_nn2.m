% Copyright The University of Sydney, 2001 - 2008.  ABN 15 211 513 464
% herafter 'The University'.
%
% The copyright of the computer program(s) herein is the property of
% The University of Sydney, Australia.  The program(s) may be used
% and/or copied only with the written permission of The University of
% Sydney, or in accordance with the terms and conditions stipulated
% in the contract under which the program(s) have been supplied.
% 
% Disclaimer:
% The Universitiy makes no representation or warranty
% that these materials, including any software, are free from errors;
% about the quality or performance of these materials;
% that these materials are fit for any particular purpose.
% These materials are made available on the strict basis that The University
% and its employees or agents have no liability for any direct or indirect
% loss or damage (including for negligence) suffered by any person as a
%consequence of the use of this  material.
%
% Compute Neural Network covariance function's derivatives with respect to all parameters based on analytical expressions
% DEFINITION
% The Neural Network covariance function is defined as
%   K(x1,x2) = (Sigma0^2)*asin((beta+2*x1'*SigmaM*x2)/sqrt((1+beta+2*x1'*SigmaM*x1)*(1+beta+2*x2'*SigmaM*x2)))
% where
%   x1, x2 are D dimensional vectors;
%   SigmaM is a D x D matrix;
%   Sigma1, Sigma2, ..., SigmaD, Sigma0, beta are scalars.
%
% INPUTS
%   X1 - D x N1 matrix of D dimensional N1 points
%   X2 - D x N2 matrix of D dimensional N2 points
%   SigmaV = [Sigma1; Sigma2; ..., SigmaD] - D x 1 vector defining SigmaM = diag(SigmaV);
%   Sigma0, beta - function parameters
%       
% OUTPUT is [K_Sigma, K_Sigma0, K_beta] where
%   K_Sigma  - D x N1 x N2 array with elements K_Sigma[m,i,j] = dK(X1(:,i),X2(:,j))/dSigma_m
%   K_Sigma0 - N1 x N2 matrix with elements K_Sigma0[i,j] = dK(X1(:,i),X2(:,j))/dSigma0
%   K_beta   - N1 x N2 matrix with elements K_beta[i,j] = dK(X1(:,i),X2(:,j))/dbeta
%
% NOTE
%   As the Neural Network covariance function K differs from K_Sigma0 just by
%   a constant we can use the output K_Sigma0 to obtain the value of K:
%   K = K_Sigma0*Sigma0/2;
%
% Arman Melkumyan 13/08/2008

%function [g] = grad_nn2(par,K,X2,X1)
function [g] = grad_nn2(kpar,K2,X2,X1)
global K X

if nargin<4
    if nargin<3
        Xg = X;
        Kg = K;
    else
        Xg = X2;
        Kg = K2;
    end

    % Computing number and dimensionality of points
    N      = size(Xg,2); % number of points in X1
    D      = size(Xg,1); % dimensionality
    g      = cell(1,D+2); %output cells
    Sigma0 = kpar(end);
    beta   = kpar(end-1);
    SigmaV = kpar(1:end-2).^(-2);
    SigmaV = reshape(SigmaV,[D 1]);

    % Checking dimensionalities
    if (size(SigmaV,1)~=D)
      error('Dimensionalities of X and SigmaV must be the same');
    end

    % Computing frequently used expressions
    XXv = 1+beta + 2*(Xg.^2)'*SigmaV;
    XXm = beta + 2*(SigmaV(:,ones(1,N)).*Xg)'*Xg;
    Y0  = sqrt(XXv*XXv'-(XXm.^2));
    %Fix numerical problems
    Y0(Y0==0) = realmin;
    XXv_ij = XXv(:,ones(1,N));

    % Computing K_Sigma0
    g{end} = 2*Kg/Sigma0;

    % Computing K_beta
    g{end-1} = (Sigma0^2/2)*(2-(1./XXv_ij+1./XXv_ij').*XXm)./Y0;

    % Computing K_Sigma
    for m=1:D
        Xm = (Xg(m,:).^2)'./XXv;
        Xm_ij = Xm(:,ones(1,N));
        g{m} = (Sigma0^2)*(2*(Xg(m,:)')*Xg(m,:)-(Xm_ij+Xm_ij').*XXm)./Y0*(-2/(kpar(m)^3));
    end
else
    Kg = K2;
    % Computing number and dimensionality of points
    N1 = size(X1,2); %number of points in X1
    N2 = size(X2,2); %number of points in X2
    D = size(X1,1);
    
    g      = cell(1,D+2); %output cells
    Sigma0 = kpar(end);
    beta   = kpar(end-1);
    SigmaV = kpar(1:end-2).^(-2);
    SigmaV = reshape(SigmaV,[D 1]);

    % Checking dimensionalities
    if (size(SigmaV,1)~=D)
      error('Dimensionalities of X and SigmaV must be the same');
    end

    % Computing frequently used expressions
    XXv1 = 1+beta + 2*(X1.^2)'*SigmaV;
    XXv2 = 1+beta + 2*(X2.^2)'*SigmaV;
    
    XXm = beta + 2*(SigmaV(:,ones(1,N2)).*X2)'*X1;
    
    Y0  = sqrt(XXv2*XXv1'-(XXm.^2));
    
    %Fix numerical problems
    Y0(Y0==0) = realmin;
    
    
    XXv_ij1 = XXv1(:,ones(1,N2));
    XXv_ij2 = XXv2(:,ones(1,N1));
    
    % Computing K_Sigma0
    g{end} = 2*Kg/Sigma0;

    % Computing K_beta
    g{end-1} = (Sigma0^2/2)*(2-(1./XXv_ij1'+1./XXv_ij2).*XXm)./Y0;

    % Computing K_Sigma
    for m=1:D
        Xm1 = (X1(m,:).^2)'./XXv1;
        Xm2 = (X2(m,:).^2)'./XXv2;
        Xm_ij1 = Xm1(:,ones(1,N2));
        Xm_ij2 = Xm2(:,ones(1,N1));
        g{m} = (Sigma0^2)*(2*(X2(m,:))'*X1(m,:)-(Xm_ij1'+Xm_ij2).*XXm)./Y0*(-2/(kpar(m)^3));
    end
    
    
    
end