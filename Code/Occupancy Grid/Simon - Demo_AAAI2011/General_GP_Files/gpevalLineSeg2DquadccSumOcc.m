% Copyright The University of Sydney, 2001 - 2008.  ABN 15 211 513 464
% herafter 'The University'.
%
% The copyright of the computer program(s) herein is the property of
% The University of Sydney, Australia.  The program(s) may be used
% and/or copied only with the written permission of The University of
% Sydney, or in accordance with the terms and conditions stipulated
% in the contract under which the program(s) have been supplied.
% 
% Disclaimer:
% The Universitiy makes no representation or warranty
% that these materials, including any software, are free from errors;
% about the quality or performance of these materials;
% that these materials are fit for any particular purpose.
% These materials are made available on the strict basis that The University
% and its employees or agents have no liability for any direct or indirect
% loss or damage (including for negligence) suffered by any person as a
%consequence of the use of this  material.
%
% Evaluate a Gaussian Process (see page 19 of
% "Gaussian Processes for Machine Learning" by
% C. Rasmussen and C. Williams)
%
%INPUTS
% X - D x N inputs
% y - 1 x N outputs (targets)
% kfun - covariance function
% kpar - covariance parameters (hyperparameters)
% K - N x N computed covariance 
% noise - scalar noise level
% x - D x M test inputs
% invK - N x N inverted K if available
% 
%OUTPUTS
% mf - predictive mean
% vf - predictive variance
% lml - log marginal likelihood
%
% Fabio Tozeto Ramos 04/06/07
% [lml,mf,vf] = gpeval(X,y,kfun,kpar,K,noise,x,invK)

function [lml,mf,vf] = gpevalLineSeg2DquadccSumOcc(cov_funs,npar,XStart,XEnd,y,K,kpar,noise,x,order,invK)
[d,N] = size(XStart); %number of inputs

[nx]  = size(x,2); %number of testing points 

% check is mean is zero
% my = mean(y);           %Check this. I think this should be an integral too.
my = calcmeany2D(y,XStart,XEnd); 

% XS=X(1,:);
% XE=X(2,:);

if abs(my)>1e-3
    y      = y - my;
    myflag = true;
end

%         K = feval(@cov_sqexp_line_line_2d,XStart,XEnd,XStart,XEnd,kpar);
         K = quadCC2DLine2LineSumOcc(cov_funs,npar,XStart,XEnd,XStart,XEnd,kpar,order);
         
if sum(sum(imag(K)))~=0
    K = real(K);
end
    
if nargin<11
    %Cholesky factorisation for covariance inversion
    if issparse(K)
        A = K+(noise^2)*speye(N);
        p = symrcm(A);
        %Lt = chol(A(p,p))';
        %L(p,:) = Lt;
        %alpha = L'\(L\y');
        alpha(p) = A(p,p)\y(p)';
    else
        L = chol(K+(noise^2)*eye(N)); 
        alpha = solve_chol(L,y');
    end    
else
    %If the inverted covariance is given use it to 
    %compute alpha
    alpha = invK*y;
end

if ~isempty(x)    %Evaluate the marginal log-likelihood only
    % K(X,x)
%     ks=zeros(numel(x),N);
    
%  addpath LineSegMultiDim
%     ks= feval(@cov_sqexp_line_point_2d,[XS; zeros(size(XS))],...
%         [XE;zeros(size(XE))],[x;zeros(size(x))],kpar);
%     ks= feval(@cov_sqexp_line_point_2d,XStart,XEnd,x,kpar);
   ks= quadCC2DLine2PointSumOcc(cov_funs,npar,XStart,XEnd,x,kpar,order);

    if sum(sum(imag(ks)))~=0
        ks = real(ks);
    end
   
%     ks=ks';
    %Compute the mean
    mf = ks'*alpha;

    %Compute the variance
    opts.TRANSA = true;
    opts.UT     = true;
    v           = linsolve(L,ks,opts);
    xc          = mat2cell(x,d,ones(1,nx));
    
    nfuns = length(cov_funs);
    p1 = 1;
    a = zeros(size(xc'));
    for i=1:nfuns 
        p2 = p1 + npar(i)-1;
        %Eval each covariance function
        cellsolution =  cellfun(cov_funs{i},xc,xc,repmat({kpar(p1:p2)},1,nx))';
        a = a +cellsolution;
        p1 = p2 + 1;
    end
%     a           = cellfun(cov_fun, xc, xc, repmat({kpar},1,nx))';
    vf          = a - sum(v.*v)' + noise^2;
    %vf = feval(kfun,x,x,kpar) - v'*v + noise^2;
else
    mf = [];
    vf = [];
end

if myflag
    mf = mf + my;
end
%Compute the log marginal likelihood
lml = -0.5*y*alpha-sum(log(diag(L)))-0.5*N*log(2*pi);
% mf
%nlml = -0.5*y*alpha - 0.5*log(det(A)) - 0.5*n*log(2*pi);