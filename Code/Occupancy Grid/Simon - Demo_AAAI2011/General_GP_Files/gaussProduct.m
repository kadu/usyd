function [mu, Sigma, const] = gaussProduct(varargin)
%% [mu, Sigma, const] = gaussProduct(a, A, b, B, c, C, ...)
% product of the Gaussians N(a,A), N(b,B), N(c,C) ...
% At least two Gaussians have the be given as arguments

K = length(varargin);
error(nargchk(4, nargin, nargin));
assert(mod(K,2) == 0, 'Usage: gaussProduct(a, A, b, B, c, C, ...)')

Sigma = 0;
mu    = 0;
const = 1;

for i=1:K/2
    R     = chol(varargin{2*i});
    mun   = varargin{2*i-1};
    Sigma = Sigma + R\(R'\eye(size(R)));
    mu    = mu + R\(R'\mun(:));
    
    if nargout > 2
        if i<2
            continue
        end
        mu1 = varargin{2*i-3};
        mu2 = varargin{2*i-1};
        S1  = varargin{2*i-2};
        S2  = varargin{2*i  };
        const = const * mvnpdf(mu1, mu2, S1+S2);
    end
end

R     = chol(Sigma);
mu    = R\(R'\mu);
Sigma = R\(R'\eye(size(R)));


end