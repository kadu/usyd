


%Function for updating a cholesky factor when additional rows and columns
%are appended to the original positive definite matrix.

% K11 = Original Matrix 
% [K11 K12;K21 K22] = Updated Positive Definite Matrix
% Chol11 = Cholesky Factor of the original positive definite matrix
%
% CholUpdated = Updated Cholesky Matrix


function [CholUpdated] = updateCholK(Chol11,K12,K22)

S11 = Chol11;
S12 = Chol11'\K12;
S22 = chol(K22 - S12'*S12);


CholUpdated = [S11,S12;zeros(size(S12')),S22];