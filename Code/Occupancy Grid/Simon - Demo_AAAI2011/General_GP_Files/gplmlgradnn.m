% Copyright The University of Sydney, 2001 - 2008.  ABN 15 211 513 464
% herafter 'The University'.
%
% The copyright of the computer program(s) herein is the property of
% The University of Sydney, Australia.  The program(s) may be used
% and/or copied only with the written permission of The University of
% Sydney, or in accordance with the terms and conditions stipulated
% in the contract under which the program(s) have been supplied.
% 
% Disclaimer:
% The Universitiy makes no representation or warranty
% that these materials, including any software, are free from errors;
% about the quality or performance of these materials;
% that these materials are fit for any particular purpose.
% These materials are made available on the strict basis that The University
% and its employees or agents have no liability for any direct or indirect
% loss or damage (including for negligence) suffered by any person as a
%consequence of the use of this  material.
%
%Compute the negate log marginal likelihood of a GP
%and its gradient. It is useful for optimisers like
%matlab which requires a single function passing lml and
%the gradient.
%
%IT DOES NOT COMPUTE NOISE GRADIENTS
%INPUT
%
%OUTPUT
%
% Fabio Tozeto Ramos 12/06/07
% [nlml,ng] = gplmlgradnn(params,kfun,kgradfun)
function [nlml,ng] = gplmlgradnn(params,kfun,kgradfun)

noise = 0.1;
kpar = params;
global X y K;%L invK K alpha;
n = size(X,2);
K = feval(kfun,X,X,kpar);

try
    L = chol(K+(noise^2)*eye(n))'; 
    invK = L'\(L\eye(n));
    alpha = invK*y';
    %Compute the log marginal likelihood
    nlml = 0.5*y*alpha + sum(log(diag(L))) + 0.5*n*log(2*pi);
catch   
    invK = inv(K);
    alpha = invK*y';
    %Compute the log marginal likelihood
    nlml = 0.5*y*alpha + 0.5*log(det(K)) + 0.5*n*log(2*pi);
end

if ~isempty(kgradfun)
    %Compute the gradient
    kgrad = feval(kgradfun,kpar);
    ng = zeros(1,length(kgrad));

    % precompute for convenience
    W = invK - alpha*alpha';
    for i = 1:length(kgrad)
        ng(i) = sum(sum(W.*kgrad{i}))/2;
    end
    %Noise gradient
    %ng(end) = trace(W*noise);
else
    ng = 0;
end