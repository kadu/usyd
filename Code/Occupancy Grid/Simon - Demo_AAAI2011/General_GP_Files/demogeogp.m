% Copyright The University of Sydney, 2001 - 2008.  ABN 15 211 513 464
% herafter 'The University'.
%
% The copyright of the computer program(s) herein is the property of
% The University of Sydney, Australia.  The program(s) may be used
% and/or copied only with the written permission of The University of
% Sydney, or in accordance with the terms and conditions stipulated
% in the contract under which the program(s) have been supplied.
% 
% Disclaimer:
% The Universitiy makes no representation or warranty
% that these materials, including any software, are free from errors;
% about the quality or performance of these materials;
% that these materials are fit for any particular purpose.
% These materials are made available on the strict basis that The University
% and its employees or agents have no liability for any direct or indirect
% loss or damage (including for negligence) suffered by any person as a
%consequence of the use of this  material.
%
%Demonstrate Gaussian process for geological prediction
close all;
global X y;

%The function it is trying to model is a 6-degree polynomial whose
%coeficients are given below:
P = [7.7991   -3.5715  -17.3408    6.8891   12.0755   -3.3696   -0.0776];
xreal = linspace(-1,1,500);
yreal = polyval(P,xreal);

%Plot the real Geological model
figure(1); axis([-1 1 0 3]); hold on;
plot(xreal, yreal, 'b--','LineWidth',2);
title('Actual Geology');
xlabel('Distance');
ylabel('Depth');
line([-1 1],[1 1]);
line([-1 1],[2 2]);
text(0.78,0.1,'Bench 1');
text(0.78,1.1,'Bench 2');
text(0.78,2.1,'Bench 3');
text(0.1,1.5,'Iron Ore');
text(-0.7,1.5,'Waste');
text(0.7,1.5,'Waste');
hold off
pause

%Evaluate a GP model with 3 observations from the second bench
X = [-0.2 0.55 0.6];
y = [1.2 1.1 1.6];

my = mean(y);
y = y - my;

disp('GP Model with 3 observations')
figure(2); axis([-1 1 0 3]); hold on
kpar = [0.5,0.8]; noise = 0.05; 
xstar = linspace(-1, 1, 200);

%Evaluate a GP over the whole domain [-1; 1];
[lml,mf,vf] = gpeval(X,y,@cov_sqexp,kpar,[],noise,xstar);

%Plot GP 
S2 = vf - exp(2*log(noise));

f = [my+mf+2*sqrt(S2);flipdim(my+mf-2*sqrt(S2),1)];
fill([xstar'; flipdim(xstar',1)], f, [7 7 7]/8, 'EdgeColor', [7 7 7]/8);
plot(xstar,my+mf,'k-','LineWidth',1);
plot(X, y+my, 'k+', 'MarkerSize', 17);

%Plot the actual data
plot(xreal, yreal, 'b--','LineWidth',2);
title('GP Geological Model With 3 Observations');
xlabel('Distance');
ylabel('Depth');
line([-1 1],[1 1]);
line([-1 1],[2 2]);
text(0.78,0.1,'Bench 1');
text(0.78,1.1,'Bench 2');
text(0.78,2.1,'Bench 3');
text(0.1,1.5,'Iron Ore');
text(-0.7,1.5,'Waste');
text(0.7,1.5,'Waste');
hold off
pause

%Now let's take 4 more observation from the top bench and
%fuse with current bench
X = [-0.2 0.55 0.6 -0.8 -0.4 0.75 0.85];
y = [1.2 1.1 1.6 2.85 2.25 2.1 2.3];
my = mean(y);
y = y - my;

disp('GP Model with 7 observations (4 from previous bench)')
figure(3); axis([-1 1 0 3]); hold on

%Evaluate a GP over the whole domain [-1; 1];
[lml,mf,vf] = gpeval(X,y,@cov_sqexp,kpar,[],noise,xstar);

%Plot GP 
S2 = vf - exp(2*log(noise));

f = [my+mf+2*sqrt(S2);flipdim(my+mf-2*sqrt(S2),1)];
fill([xstar'; flipdim(xstar',1)], f, [7 7 7]/8, 'EdgeColor', [7 7 7]/8);
plot(xstar,my+mf,'k-','LineWidth',1);
plot(X, y+my, 'k+', 'MarkerSize', 17);

%Plot the actual data
plot(xreal, yreal, 'b--','LineWidth',2);
title('GP Geological Model After Fusing the Top Bench');
xlabel('Distance');
ylabel('Depth');
line([-1 1],[1 1]);
line([-1 1],[2 2]);
text(0.78,0.1,'Bench 1');
text(0.78,1.1,'Bench 2');
text(0.78,2.1,'Bench 3');
text(0.1,1.5,'Iron Ore');
text(-0.7,1.5,'Waste');
text(0.7,1.5,'Waste');
hold off
pause

%Now let's take observations from the underneath bench as well
X = [-0.2 0.55 0.6 -0.8 -0.4 0.75 0.85 -0.2 -0.07 0.3 0.5];
y = [1.2 1.1 1.6 2.85 2.25 2.1 2.3 0.85 0.1 0.1 0.9];
my = mean(y);
y = y - my;

disp('GP Model with 11 observations (8 from other benches)')
figure(4); axis([-1 1 0 3]); hold on

%Evaluate a GP over the whole domain [-1; 1];
[lml,mf,vf] = gpeval(X,y,@cov_sqexp,kpar,[],noise,xstar);

%Plot GP 
S2 = vf - exp(2*log(noise));

f = [my+mf+2*sqrt(S2);flipdim(my+mf-2*sqrt(S2),1)];
fill([xstar'; flipdim(xstar',1)], f, [7 7 7]/8, 'EdgeColor', [7 7 7]/8);
plot(xstar,my+mf,'k-','LineWidth',1);
plot(X, y+my, 'k+', 'MarkerSize', 17);

%Plot the actual data
plot(xreal, yreal, 'b--','LineWidth',2);
title('GP Geological Model After Fusing Top and Bottom Benches');
xlabel('Distance');
ylabel('Depth');
line([-1 1],[1 1]);
line([-1 1],[2 2]);
text(0.78,0.1,'Bench 1');
text(0.78,1.1,'Bench 2');
text(0.78,2.1,'Bench 3');
text(0.1,1.5,'Iron Ore');
text(-0.7,1.5,'Waste');
text(0.7,1.5,'Waste');
hold off




