%Evaluate a multi-class Gaussian Process Classifier using Expectation Propagation
%(see page 59 of "Gaussian Processes for Machine Learning" by
% C. Rasmussen and C. Williams)
%
%INPUTS
% X - D x N inputs
% y - 1 x N outputs (targets)
% kfun - covariance function
% kpar - covariance parameters (hyperparameters)
% K - N x N computed covariance 
% noise - scalar noise level
% x - D x M test inputs
% invK - N x N inverted K if available
% 
%OUTPUTS
% p - predictive probability
% mf - predictive mean
% vf - predictive variance
% nlml - negative log marginal likelihood
%
% Fabio Tozeto Ramos 06/08/09
% [p,mf,vf,nlml] = gpmcepeval(X,y,kfun,kpar,K,noise,x,nu,tal)

function [p,mf,vf,nlml,ttau,tnu] = gpmcepeval(X,y,kfun,kpar,K,x,ttau,tnu)
n    = size(X,2); %number of inputs
tol  = 1e-3; max_sweep = 10;  
C    = length(y)/n; %number of classes
[d,N]= size(x); %dimensionality number of query points
mf   = zeros(C,N);
vf   = zeros(C,N);

if isempty(K)
    %Build K
    K = cell(1,C);
    for i = 1:C
        K{i} = feval(kfun{i},X(:,:),X(:,:),kpar{i});
    end
end
Kb   = blkdiag(K{:});

%Computes ks
ks  = cell(1,C);
for i = 1:C
    ks{i} = feval(kfun{i},X(:,:),x(:,:),kpar{i});
end




%Compute tnu and ttau in case they are not provided
if nargin<8
    ttau = zeros(n*C,1);            % initialize to zero if we have no better guess
    tnu = zeros(n*C,1);
    Sigma = Kb;                    % initialize Sigma and mu, the parameters of ..
    mu = zeros(n*C, 1);
    lml = -n*C*log(2);
    lml_old = -inf; sweep = 0;  
    while lml - lml_old > tol && sweep < max_sweep    % converged or maximum sweeps?

        lml_old = lml; sweep = sweep + 1;
        for i = 1:n*C                                % iterate EP updates over examples

            tau_ni = 1/Sigma(i,i)-ttau(i);      % first find the cavity distribution ..
            nu_ni  = mu(i)/Sigma(i,i)-tnu(i);           % .. parameters tau_ni and nu_ni

            z_i    = y(i)*nu_ni/sqrt(tau_ni*(1+tau_ni));     % compute the desired moments
          
            gosz_i = sqrt(2/pi)*exp(-z_i.^2/2)./(1+erf(z_i/sqrt(2)));  % gauss / sigmoid
            hmu    = nu_ni/tau_ni + y(i)*gosz_i/sqrt(tau_ni*(1+tau_ni));
            hs2    = (1-gosz_i*(z_i+gosz_i)/(1+tau_ni))/tau_ni;

            ttau_old = ttau(i);                   %  then find the new tilde parameters
            ttau(i)  = 1/hs2 - tau_ni;
            tnu(i)   = hmu/hs2 - nu_ni;

            ds2   = ttau(i) - ttau_old;                  % finally rank-1 update Sigma ..
            %Sigma = Sigma - ds2/(1+ds2*Sigma(i,i))*Sigma(:,i)*Sigma(i,:);
            % Efficient direct Lapack call (using Mathias Seeger's
            % function
            fst_dgemm({Sigma, [1 1 n*C n*C], 'L '},Sigma(:,i),0,Sigma(:,i),1,...
                -ds2/(1+ds2*Sigma(i,i)),1);
            
            mu    = Sigma*tnu;                                       % .. and recompute mu

        end
        [Sigma, mu, lml] = epComputeParams(Kb, y, ttau, tnu); % recompute Sigma and
        % mu since repeated rank-one updates eventually destroys numerical precision
    end
end


xcell = mat2cell(x,d,ones(1,N));
    
for i=1:C
    a       = cellfun(kfun{i}, xcell, xcell, repmat({kpar{i}},1,N));          
    b       = ks{i};
    tnuc    = tnu((i-1)*n+1:i*n);
    ttauc   = ttau((i-1)*n+1:i*n);
    ssi     = sqrt(ttauc);                                        % compute Sigma and mu
    L       = chol(eye(n)+ssi*ssi'.*K{i});
    mus     = b'*(tnuc-sqrt(ttauc).*solve_chol(L,sqrt(ttauc).*(K{i}*tnuc)));   % test means
    %mus    = b'*(tnu-sqrt(ttau).*(L\(L'\sqrt(ttau).*(K*tnu))));
    v       = L'\(repmat(sqrt(ttauc),1,size(x,2)).*b);
    s2s     = a' - sum(v.*v,1)';                     % latent test predictive variance
    %p      = sigmoid(mus./sqrt(1+s2s));      % return predictive test probabilities
    mf(i,:) = mus;                             % return latent test predictive means
    vf(i,:) = s2s;                      % return latent test predictive variances
end

p  = hermintlogit(0.85*mf,vf); 

nlml = -lml;                        % return negative log marginal likelihood


% function to compute the parameters of the Gaussian approximation, Sigma and
% mu, and the log marginal likelihood, lml, from the current site parameters,
% ttau and tnu. The function also may return L (useful for predictions).

function [Sigma, mu, lml, L] = epComputeParams(K, y, ttau, tnu)

sigmoid = @(x) (1+erf(x/sqrt(2)))/2;
n = length(y);                                       % number of training cases
ssi = sqrt(ttau);                                        % compute Sigma and mu
L = chol(eye(n)+ssi*ssi'.*K);
V = L'\(repmat(ssi,1,n).*K);
Sigma = K - V'*V;
mu = Sigma*tnu;

tau_n = 1./diag(Sigma)-ttau;              % compute the log marginal likelihood
nu_n = mu./diag(Sigma)-tnu;                      % vectors of cavity parameters
z = y'.*nu_n./sqrt(tau_n.*(1+tau_n));
lml = -sum(log(diag(L)))+sum(log(1+ttau./tau_n))/2+sum(log(sigmoid(z))) ...
      +tnu'*Sigma*tnu/2+nu_n'*((ttau./tau_n.*nu_n-2*tnu)./(ttau+tau_n))/2 ...
      -sum(tnu.^2./(tau_n+ttau))/2;

  


