%Computes the partial inverse of a square matrix A given a matrix B with
%booleans where 1 are the cells the inverse should be computed.
%The resulting matrix invA is sparse
%L is the Cholesky factorisation of A (if available)
%function invA = parinv(A,B,L)
function invA = partinv(A,B,L)

[N1,N2] = size(A);
[NB1,NB2] = size(B);
if N1~=N2
    error('A is not square');
end
if NB1~=NB2
    error('B is not square');
end
if N1~=NB1
    error('A and B should be of the same size');
end
if nargin<3
    L = chol(A,'lower');
end

nv = nnz(B);
ii = zeros(1,nv);
jj = zeros(1,nv);
v = zeros(1,nv);
ie = 1; 

for k=1:N1
    NC = nnz(B(:,k));
    %Full column
    if NC==N1
        ind = find(B(:,k));
        %Set columns inverse
        ii(ie:ie+NC-1) = ind;
        jj(ie:ie+NC-1) = k;
        c = zeros(N1,1);
        c(k,1) = 1;
        b = L\c;
        d = L'\b;
        v(ie:ie+NC-1) = d;
        ie = ie + NC;
        %Set rows inverse
        ii(ie:ie+NC-1) = k;
        jj(ie:ie+NC-1) = ind;
        v(ie:ie+NC-1) = d;
        ie = ie + NC;
        
    else
        ind = find(B(:,k));
        %Set columns inverse
        ii(ie:ie+NC-1) = ind;
        jj(ie:ie+NC-1) = k;
        c = zeros(N1,1);
        c(k,1) = 1;
        b = L\c;
        d = L'\b;
        v(ie:ie+NC-1) = d;
        ie = ie + NC;
        %Set rows inverse
        ii(ie:ie+NC-1) = k;
        jj(ie:ie+NC-1) = ind;
        v(ie:ie+NC-1) = d;
        ie = ie + NC;
    end
%     if NC~=0
%         ind = find(B(:,k));
%         ii(ie:ie+NC-1) = ind;
%         jj(ie:ie+NC-1) = k;
%         p = permL(B,ind);
%         %Cholesky factorisation
%         L = tril(A(p,p));
%         
%         for j=1:N1
%             if j>1
%                 L(j:N1,j) = L(j:N1,j)-L(j:N1,1:j-1)*L(j,1:j-1)';
%             end
%             L(j:N1,j) = L(j:N1,j)/sqrt(L(j,j));
%         end
%         L(:,j+1:end) = 0;
%         %L(j+1:end,:) = 0;
%         %Forward substitution
%         b = zeros(N1,1);
%         
%         b(k,1) = 1;
%         
%         b = b(p);
%         %b = b(1:NC,1);
%         b(1) = b(1)/L(1,1);
%         for i=2:NC
%             b(i) = (b(i)-L(i,1:i-1)*b(1:i-1))/L(i,i);
%         end
%         
%         L=L';
%         %Back substitution
%         b(N1) = b(N1)/L(N1,N1);
%         for i=N1-1:-1:1
%             b(i) = (b(i)-L(i,i+1:N1)*b(i+1:N1))/L(i,i);
%         end
%         
%         v(ie:ie+NC-1) = b;
%         ie = ie + NC;
%     end
end
invA = sparse(ii,jj,v,N1,N2);


function p = permL(B,ind)
[N1] = size(B,1);
p = [ind' setxor(ind, 1:N1)];

