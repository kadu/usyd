%Derivative of the Transformation function for the Warped Gaussian process and GP
%classification. 
%f(x) = 2./(1-x.^2)
function [fy] = dgpcatanh(params,x)
fy = 2./((1-(2*x-1).^2)*params(1));
ind = fy == -inf;
fy(ind) = 10000;
ind = fy == inf;
fy(ind) = 10000;
