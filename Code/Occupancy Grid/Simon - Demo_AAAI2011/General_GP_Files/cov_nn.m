% Compute the neural network covariance function using Rasmussen's
% implementation of the neural network.
% 
%INPUTS
% X1 - D x N input points 1
% X2 - D x M input points 2
% par - cell with the following fields 
%   par(1:end-1) - characteristic length-scales
%   par(end) - sigma f square
%OUTPUT
% K - covariance function
%
% Simon O'Callaghan 25/03/08
% K = cov_nn(X1,X2,par)

function K = cov_nn(X1,X2,par)
N1 = size(X1,2); %number of points in X1
N2 = size(X2,2); %number of points in X2

if size(X1,1)~=size(X2,1)
    error('Dimensionality of X1 and X2 must be the same');
end

%Compute the weighted squared distances between X1 and X2
%in a vectorised way 
w = par(1:end-1)'.^(-2);

 X1 = X1';      %Changes to inputs to columns
 X2 = X2';

K = exp(2*log(par(end)))*asin((w+X1*X2')./sqrt((1+w+sum(X1.*X1,2))*(1+w+sum(X2.*X2,2)')));

