

% function K = quadCC2DLine2LineSumOcc(funs,npar,X1S,X1E,X2S,X2E,params,order,FixedParams)
function K = quadCC2DLine2LineSumOcc(funs,npar,X1S,X1E,X2S,X2E,params,order)

% X1S = [2 3;4 3]
% X1E = [5 1;6 1]
% X2S = [4; 4]
% X2E = [2.5; 3]
% order=10
% params=[1 1 1]
% fun=@cov_sqexp


Num_Lines1 = size(X1S,2);
Num_Lines2 = size(X2S,2);

%Find line segments where start=end i.e. its a point and add a small offset
X1E=X1E+[(X1S(1,:)==X1E(1,:)).*(X1S(2,:)==X1E(2,:))*0.0001;(X1S(1,:)==X1E(1,:)).*(X1S(2,:)==X1E(2,:))*0.0001];
X2E=X2E+[(X2S(1,:)==X2E(1,:)).*(X2S(2,:)==X2E(2,:))*0.0001;(X2S(1,:)==X2E(1,:)).*(X2S(2,:)==X2E(2,:))*0.0001];

% figure
% plot([X1S(1,:);X1E(1,:)],[X1S(2,:);X1E(2,:)],'b')
% hold on
% plot([X2S(1,:);X2E(1,:)],[X2S(2,:);X2E(2,:)],'r')


[ Nodes, Weights ] = clenshaw_curtis_compute_nd ( 1, order );
% order
%Outputs are in a column of length Num_Line1
[costhet sinthet xstart1 ystart1 r]= determLineParameters(X1S, X1E);
%Outputs are in a column of length Num_Line2
[cosphi sinphi xstart2 ystart2 s]= determLineParameters(X2S, X2E);

    
[NodesScaleShift1 Weights1] = scaleShiftNodesWeights(r, Nodes, Weights);
[NodesScaleShift2 Weights2] = scaleShiftNodesWeights(s, Nodes, Weights);


%Create X1 as a long list of all the nested points in order
X1 = [reshape((NodesScaleShift1.*costhet(:,ones(1,size(Nodes,2)))+xstart1(:,ones(1,order)))',1,order*Num_Lines1);...
    reshape((NodesScaleShift1.*sinthet(:,ones(1,size(Nodes,2)))+ystart1(:,ones(1,order)))',1,order*Num_Lines1)];

Weights1 = reshape(Weights1',1,order*Num_Lines1)';

X2 = [reshape((NodesScaleShift2.*cosphi(:,ones(1,size(Nodes,2)))+xstart2(:,ones(1,order)))',1,order*Num_Lines2);...
    reshape((NodesScaleShift2.*sinphi(:,ones(1,size(Nodes,2)))+ystart2(:,ones(1,order)))',1,order*Num_Lines2)];

Weights2 = reshape(Weights2',1,order*Num_Lines2)';
% 
% figure
% plot3(X1(1,:),X1(2,:),Weights1,'+')
% hold on
% plot3(X2(1,:),X2(2,:),Weights2,'r+')


knodes = cov_sum(X1,X2,funs,npar,params(1:end-3));
% knodes = cov_sum(X1,X2,funs,npar,FixedParams);

WeightsMat = Weights1*Weights2';

KnodesWeighted=knodes.*WeightsMat;

KnodesWeighted = reshape(KnodesWeighted,[order Num_Lines1 order Num_Lines2]);
KnodesWeighted = permute(KnodesWeighted,[2 4 1 3]);
KnodesWeighted = reshape(KnodesWeighted, [Num_Lines1 Num_Lines2 order*order]);

KPreSig = sum(KnodesWeighted,3);

M = [params(end-2) params(end-1);0 params(end)]'*[params(end-2) params(end-1);0 params(end)];
% M = [params(1) params(2);0 params(3)]'*[params(1) params(2);0 params(3)];

K =[ M(1)*KPreSig(1:(Num_Lines1/2),1:(Num_Lines2/2)) M(3)*KPreSig(1:(Num_Lines1/2),(Num_Lines2/2)+1:end);...
    M(2)*KPreSig((Num_Lines1/2)+1:end,1:(Num_Lines2/2)) M(4)*KPreSig((Num_Lines1/2)+1:end,(Num_Lines1/2)+1:end)];


end


