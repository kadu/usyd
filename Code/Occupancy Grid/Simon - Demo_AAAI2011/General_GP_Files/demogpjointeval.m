%Demogpjointeval
%Demonstrates the benefits of performing joint inference using gpjointeval
global X y tempK

if 0
X = 0.01*[ones(1,10) 6*ones(1,10); 1:10 1:10];
y = [1 1 1 3 9 3 1 6 6 1 3 9 3 1 6 6 1 1.2 1 1];
y = y - mean(y);

%Check the gradients

%params0 = [1 1 1 2 1 0.01];
params0 = 10*rand(1,6);
options = zeros(1,18);
options(1)=1; options(9)=1; options(14)=40;
[params]=gplearn(@cov_nn2,[],params0(1:end-1),params0(end),options)
params(1) = 1;
params(2) = 1;

%params(5) = 4;
%params(2) = 0.05;
%params(3) = 1;

xstar = 0.01*[2*ones(1,10) 3*ones(1,10) 4*ones(1,10) 5*ones(1,10); 1:10 1:10 1:10 1:10];
%[lml,mf,vf] = gpjointeval(X,y,@cov_nsinf,params(1),params(2),xstar);
[lml2,mf2,vf2] = gpeval(X,y,@cov_nn2,params(1:end-1),[],params(end),xstar);


%figure; imagesc([y(1:10)' reshape(mf,[10,4])  y(11:20)']);
figure; imagesc([y(1:10)' reshape(mf2,[10,4])  y(11:20)']);
end

%Test with Gamma ray from 
if 1
[data]=csvread('~/sshfs/Geophysics/Drilling/Trial2/Row1BlastGamma.csv');
ind = randperm(length(data));
tpts = ind(1:end);
%u = [-1; -0.0119];
X = [data(tpts,2)'-683114.386; data(tpts,1)'-7435303.887; data(tpts,3)'-615.888];
y = 0.1*data(tpts,4)'-mean(0.1*data(tpts,4));    

params0 = 10*rand(1,5);
options = zeros(1,18);
options(1)=1; options(9)=1; options(14)=40;
[params]=gplearn(@cov_sqexp,@grad_sqexp,params0(1:end-1),params0(end),options)


xmax = (683237-683114.386);
%xmax = (683231-min(data(:,1)))/10;
ymax = (7435423-7435303.887);
%ymax = (7435450-min(data(:,2)))/10;
zmin = (702-615.888);
xmin = (683229-683114.386);
%xmin = (683230-min(data(:,1)))/10;
ymin = (7435345-7435303.887);
%ymin = (7435300-min(data(:,2)))/10;
zmax = (718-615.888);


[x,y,z] = meshgrid(xmin:1:xmax,ymin:1:ymax,zmin:0.2:zmax);
[sx,sy,sz] = size(x);
x1d = reshape(x,[1 sx*sy*sz]);
y1d = reshape(y,[1 sx*sy*sz]);
z1d = reshape(z,[1 sx*sy*sz]); 

[lml,mf,vf] = gpkdtreeeval(X,y,...
    @cov_sqexp,params(1:end-1),params(end),[x1d;y1d;z1d],50);
S2 = vf - params(end)^2;

%figure; imagesc(reshape(mf,[sx sz])');
v = 10*mf + mean(data(tpts,4));
v = reshape(v,[sx,sy,sz]);
figure;
hslice = surf(linspace(xmin,xmax,100),linspace(ymin,ymax,100),(zmax+zmin)/2*ones(100));

rotate(hslice,[(xmax+xmin)/2,(ymax+ymin)/2,(zmax+zmin)/2],-45);
xd = get(hslice,'XData');
yd = get(hslice,'YData');
zd = get(hslice,'ZData');
delete(hslice)

%Draw the slices
h = slice(x,y,z,v,xd,yd,zd);
set(h,'FaceColor','interp',...
 'EdgeColor','none',...
 'DiffuseStrength',.8);

hold on
hx = slice(x,y,z,v,max(x1d),[],[]);
set(hx,'FaceColor','interp','EdgeColor','none')

hy = slice(x,y,z,v,[],max(y1d),[]);
set(hy,'FaceColor','interp','EdgeColor','none')

hy2 = slice(x,y,z,v,[],max(y1d)-1,[]);
set(hy2,'FaceColor','interp','EdgeColor','none')

hy3 = slice(x,y,z,v,[],max(y1d)-2,[]);
set(hy3,'FaceColor','interp','EdgeColor','none')

hy4 = slice(x,y,z,v,[],max(y1d)-3,[]);
set(hy4,'FaceColor','interp','EdgeColor','none')

hy5 = slice(x,y,z,v,[],max(y1d)-4,[]);
set(hy5,'FaceColor','interp','EdgeColor','none')

hy6 = slice(x,y,z,v,[],max(y1d)-5,[]);
set(hy6,'FaceColor','interp','EdgeColor','none')

hy7 = slice(x,y,z,v,[],max(y1d)-6,[]);
set(hy7,'FaceColor','interp','EdgeColor','none')

hz = slice(x,y,z,v,[],[],min(z1d));
set(hz,'FaceColor','interp','EdgeColor','none')


%Defining the conce(1,:)iew
daspect([1,1,1])
axis tight
box on
view(-38.5,16)
camzoom(1.4)
camproj perspective

lightangle(-45,45)
colormap (jet(24))
set(gcf,'Renderer','zbuffer')

end

%Test with Gamma ray using sum of covariance functions
if 0
[data]=xlsread('~/Geophysics/Drilling/Trial2/Row1BlastGamma.xls');
ind = randperm(length(data));
tpts = ind(1:500);
u = [-1; -0.0119];
X = 0.1*[data(tpts,1)'; -data(tpts,3)'];
y = [data(tpts,4)']-mean(data(tpts,4));    

%params0 = [0.9989    2.5688    1.0001   -0.3357   34.8996   15.6634];
%params0 = [10 10 10 -1 -0.0119 1];
%params0 = [10.308    10.9103    1.0026    -0.3547   10.7834   10.7543];
%params0 = [3.0951    0.5312  -1    -0.0119 39.6677   10.9569];
%params0 = [10 10 10 10 10 10];
params0 = 10*rand(1,2);
%params0 = [0.9985  -32.2905   -2.2654   -7.8561   35.4063 9.2644];
%params0 = [params0 params0 params0 0.1];
%params0 = [-5.6312  -33.1985   -0.6874   -1.6897   39.7972    9.2644    3.8794  -32.2896    6.3795...
%-0.0001   -0.3224    7.0705   -0.3783  -30.5887  -17.4853   -5.0427];
options = zeros(1,18);
options(1)=1; options(9)=1; options(14)=40;
cov_funs{1} = @cov_ns;
%cov_funs{2} = @cov_nn2;
%cov_funs{3} = @cov_nn2;
npar = [1]; 
tempK = [];
%[params]=gplearn(@(X1,X2,pars) cov_sum(X1,X2,cov_funs,npar,pars),[],params0(1:end-1),params0(end),options)
params = [17.5511 25.3690];
%Evaluated positions
X = 0.1*[data(:,1)'; -data(:,3)'];
y = [data(:,4)']-mean(data(:,4));    
[xeva,yeva] = meshgrid(min(X(1,:)):0.1:max(X(1,:)),min(X(2,:)):0.01:max(X(2,:)));
[sx,sy]=size(xeva);
xeva = reshape(xeva, [1 sx*sy]);
yeva = reshape(yeva, [1 sx*sy]);
cov_funs{1} = @cov_nsinf;
[lml,mf,vf] = gpevalmulti(X,y,@(X1,X2,pars) cov_sum(X1,X2,cov_funs,npar,pars),params(1:end-1),[],params(end),[xeva;yeva],1);
figure; imagesc(reshape(mf',[sx sy]));

end

if 0
[data]=xlsread('~/Geophysics/Drilling/Trial2/Row1BlastGamma.xls');
clear cov_funs lml mf vf
options = zeros(1,18);
options(1)=1; options(9)=1; options(14)=40;
cov_funs{1} = @cov_sqexpfa;
npar = [5];
step = 500;
for i=1:ceil(length(data)/step)
    ist = (i-1)*step+1;
    if i*step<length(data)
        ien = i*step;
    else
        ien = length(data);
    end
    tpts = ist:ien;
    X = 0.1*[data(tpts,1)'; -data(tpts,3)'];
    y = [data(tpts,4)']-mean(data(tpts,4));    
    params0 = 10*rand(1,6);
    [params{i}]=gplearn(@(X1,X2,pars) cov_sum(X1,X2,cov_funs,npar,pars),[],...
        params0(1:end-1),params0(end),options)
end
%Evaluated positions
X = 0.1*[data(:,1)'; -data(:,3)'];
y = [data(:,4)']-mean(data(:,4));    
[xeva,yeva] = meshgrid(min(X(1,:)):0.1:max(X(1,:)),min(X(2,:)):0.01:max(X(2,:)));
[sx,sy]=size(xeva);
xeva = reshape(xeva, [1 sx*sy]);
yeva = reshape(yeva, [1 sx*sy]);
%Evaluates
for i=1:length(params)
    [lml{i},mf{i},vf{i}] = gpevalmulti(X,y,...
        @(X1,X2,pars) cov_sum(X1,X2,cov_funs,npar,pars),params{i}(1:end-1),[],params{i}(end),[xeva;yeva]);
end
%Reshape 
%lml2 = reshape(cell2mat(lml),[length(mf{1}) length(params)])';
mf2 = reshape(cell2mat(mf),[length(mf{1}) length(params)])';
vf2 = reshape(cell2mat(vf),[length(mf{1}) length(params)])';

[v,ind] = min(vf2,[],1);
ii = sub2ind([length(params) length(mf{1})],ind,1:length(mf{1}));
figure; imagesc(reshape(mf2(ii)',[sx sy]));

end
%Learn a model for every hole interval
if 0
[data]=xlsread('~/Geophysics/Drilling/Trial2/Row1BlastGamma.xls');
clear cov_funs lml mf vf
options = zeros(1,18);
options(1)=1; options(9)=1; options(14)=40;
cov_funs{1} = @cov_sqexpfa;
npar = [5];
step = 200;

%Evaluated positions
X = 0.1*[data(:,1)'; -data(:,3)'];
y = [data(:,4)']-mean(data(:,4));    

[xeva,yeva] = meshgrid(min(X(1,:)):0.1:max(X(1,:)),min(X(2,:)):0.01:max(X(2,:)));
[sx,sy]=size(xeva);
xeva = reshape(xeva, [1 sx*sy]);
yeva = reshape(yeva, [1 sx*sy]);

for i=1:ceil(length(data)/step)
    ist = (i-1)*step+1;
    if i*step<length(data)
        ien = i*step;
    else
        ien = length(data);
    end
    tpts = ist:ien;
    X = 0.1*[data(tpts,1)'; -data(tpts,3)'];
    y = [data(tpts,4)']-mean(data(tpts,4));    
    params0 = 10*rand(1,6);
    [params{i}]=gplearn(@(X1,X2,pars) cov_sum(X1,X2,cov_funs,npar,pars),[],...
        params0(1:end-1),params0(end),options)
    
    %Evaluated positions
    X = 0.1*[data(:,1)'; -data(:,3)'];
    y = [data(:,4)']-mean(data(:,4));    

    [lml{i},mf{i},vf{i}] = gpkdtreeeval(X,y,@cov_sqexpfa,params{i}(1:5),params{i}(6),[xeva;yeva],200);
end

figure; imagesc(reshape(mf{1}',[sx sy]));

end
