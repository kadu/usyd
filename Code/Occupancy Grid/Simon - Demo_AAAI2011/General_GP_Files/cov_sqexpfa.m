% Compute the squared exponential covariance function with 
% automatic relevance determination and factor analysis 
% 
%INPUTS
% X1 - D x N input points 1
% X2 - D x M input points 2
% par - cell with the following fields 
%   par(1:end-1) - characteristic length-scales
%   par(end) - sigma f square
%OUTPUT
% K - covariance function
%
% Fabio Tozeto Ramos 01/05/08
% K = cov_sqexpfa(X1,X2,par)

function K = cov_sqexpfa(X1,X2,par)
[D,N1] = size(X1); %number of points in X1
N2 = size(X2,2); %number of points in X2
lam = reshape(par(D+1:end-1),[D,D]); %lambda

if size(X1,1)~=size(X2,1)
    error('Dimensionality of X1 and X2 must be the same');
end

%Compute the weighted squared distances between X1 and X2
%in a vectorised way 
l = par(1:D)'.^(-2);
M = lam*lam' + diag(l);

XX1 = sum(M*X1.*X1,1);
XX2 = sum(M*X2.*X2,1);
X1X2 = (M*X1)'*X2;
XX1T = XX1';
z = XX1T(:,ones(1,N2)) + XX2(ones(1,N1),:) - 2*X1X2;

K = exp(log((par(end)).^2)-0.5*z); %put the sf2 inside the exponential
%to avoid numerical problems.