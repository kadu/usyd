%Evaluate a Gaussian Process Classifier using Expectation Propagation
%(see page 59 of "Gaussian Processes for Machine Learning" by
% C. Rasmussen and C. Williams)
%
%INPUTS
% X - D x N inputs
% y - 1 x N outputs (targets)
% kfun - covariance function
% kpar - covariance parameters (hyperparameters)
% K - N x N computed covariance 
% noise - scalar noise level
% x - D x M test inputs
% invK - N x N inverted K if available
% 
%OUTPUTS
% p - predictive probability
% mf - predictive mean
% vf - predictive variance
% nlml - negative log marginal likelihood
%
% Fabio Tozeto Ramos 17/02/08
% [p,mf,vf,nlml] = gpcepeval(X,y,kfun,kpar,K,noise,x,nu,tal)

function [p,mf,vf,nlml,ttau,tnu] = gpcepeval(X,y,kfun,kpar,K,x,ttau,tnu)
[d,n] = size(X); %number of inputs
N     = size(x,2); %number of testing points
tol   = 1e-3; 
max_sweep = 10;  
sigmoid = inline('(1+erf(x/sqrt(2)))/2'); %cummulative Gaussian
gos = inline('sqrt(2/pi)*exp(-x.^2/2)./(1+erf(x/sqrt(2)))');  % gauss / sigmoid

if isempty(K)
    K = feval(kfun,X,X,kpar);
end

%Compute tnu and ttau in case they are not provided
if nargin<8
    ttau = zeros(n,1);            % initialize to zero if we have no better guess
    tnu = zeros(n,1);
    Sigma = K;                    % initialize Sigma and mu, the parameters of ..
    mu = zeros(n, 1);
    lml = -n*log(2);
    lml_old = -inf; sweep = 0;  
    while lml - lml_old > tol && sweep < max_sweep    % converged or maximum sweeps?

        lml_old = lml; sweep = sweep + 1;
        for i = 1:n                                % iterate EP updates over examples

            tau_ni = 1/Sigma(i,i)-ttau(i);      % first find the cavity distribution ..
            nu_ni = mu(i)/Sigma(i,i)-tnu(i);           % .. parameters tau_ni and nu_ni

            z_i = y(i)*nu_ni/sqrt(tau_ni*(1+tau_ni));     % compute the desired moments
            hmu = nu_ni/tau_ni + y(i)*gos(z_i)/sqrt(tau_ni*(1+tau_ni));
            hs2 = (1-gos(z_i)*(z_i+gos(z_i))/(1+tau_ni))/tau_ni;

            ttau_old = ttau(i);                   %  then find the new tilde parameters
            ttau(i) = 1/hs2 - tau_ni;
            tnu(i) = hmu/hs2 - nu_ni;

            ds2 = ttau(i) - ttau_old;                  % finally rank-1 update Sigma ..
            Sigma = Sigma - ds2/(1+ds2*Sigma(i,i))*Sigma(:,i)*Sigma(i,:);
            mu = Sigma*tnu;                                       % .. and recompute mu

        end
        [Sigma, mu, lml, L] = epComputeParams(K, y, ttau, tnu); % recompute Sigma and
        % mu since repeated rank-one updates eventually destroys numerical precision
    end
end

xc  = mat2cell(x,d,ones(1,N));
a   = cellfun(kfun, xc, xc, repmat({kpar},1,N))';
b = feval(kfun,X,x,kpar);
mus = b'*(tnu-sqrt(ttau).*solve_chol(L,sqrt(ttau).*(K*tnu)));    % test means
%mus = b'*(tnu-sqrt(ttau).*(L\(L'\sqrt(ttau).*(K*tnu))));
v = L'\(repmat(sqrt(ttau),1,size(x,2)).*b);
s2s = a - sum(v.*v,1)';                     % latent test predictive variance
p = sigmoid(mus./sqrt(1+s2s));      % return predictive test probabilities
mf = mus;                             % return latent test predictive means
vf = s2s;                         % return latent test predictive variances
nlml = -lml;                        % return negative log marginal likelihood


% function to compute the parameters of the Gaussian approximation, Sigma and
% mu, and the log marginal likelihood, lml, from the current site parameters,
% ttau and tnu. The function also may return L (useful for predictions).

function [Sigma, mu, lml, L] = epComputeParams(K, y, ttau, tnu)

sigmoid = inline('(1+erf(x/sqrt(2)))/2');
n = length(y);                                       % number of training cases
ssi = sqrt(ttau);                                        % compute Sigma and mu
L = chol(eye(n)+ssi*ssi'.*K);
V = L'\(repmat(ssi,1,n).*K);
Sigma = K - V'*V;
mu = Sigma*tnu;

tau_n = 1./diag(Sigma)-ttau;              % compute the log marginal likelihood
nu_n = mu./diag(Sigma)-tnu;                      % vectors of cavity parameters
z = y'.*nu_n./sqrt(tau_n.*(1+tau_n));
lml = -sum(log(diag(L)))+sum(log(1+ttau./tau_n))/2+sum(log(sigmoid(z))) ...
      +tnu'*Sigma*tnu/2+nu_n'*((ttau./tau_n.*nu_n-2*tnu)./(ttau+tau_n))/2 ...
      -sum(tnu.^2./(tau_n+ttau))/2;


