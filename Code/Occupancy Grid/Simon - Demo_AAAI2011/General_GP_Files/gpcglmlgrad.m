% Copyright The University of Sydney, 2001 - 2008.  ABN 15 211 513 464
% herafter 'The University'.
%
% The copyright of the computer program(s) herein is the property of
% The University of Sydney, Australia.  The program(s) may be used
% and/or copied only with the written permission of The University of
% Sydney, or in accordance with the terms and conditions stipulated
% in the contract under which the program(s) have been supplied.
% 
% Disclaimer:
% The Universitiy makes no representation or warranty
% that these materials, including any software, are free from errors;
% about the quality or performance of these materials;
% that these materials are fit for any particular purpose.
% These materials are made available on the strict basis that The University
% and its employees or agents have no liability for any direct or indirect
% loss or damage (including for negligence) suffered by any person as a
%consequence of the use of this  material.
%
%Approximates the negate log marginal likelihood of a GP
%and its gradient with conjugate gradients. It is useful for optimisers like
%fminunc which requires a single function passing lml and
%the gradient.
%
%INPUT
%
%OUTPUT
%
% Fabio Tozeto Ramos 07/11/08
% [nlml,ng] = gpcglmlgrad(params,kfun,kgradfun)
function [nlml,ng] = gpcglmlgrad(params,kfun,kgradfun)
global X y K invK;%L invK K alpha;

noise = params(end);
kpar = params(1:end-1);
n = length(y);
K = feval(kfun,X,X,kpar);
I = speye(n);   
K2 = K+(noise^2)*I;
p = symrcm(K2);
L = chol(K2(p,p))'; 
%L = cholinc(K2(p,p),1e-3)';
%[invK] = gpcg(K2(p,p),I(p,p),L,invK);
iter = min(20,size(X,2));
[invK] = gcg(full(K2(p,p)),I(p,p),invK,iter);

alpha = invK*y(p)';
    
%Compute the log marginal likelihood
nlml = 0.5*y(p)*alpha + sum(log(diag(L))) + 0.5*n*log(2*pi);
        
if ~isempty(kgradfun)
    %Compute the gradient
    kgrad = feval(kgradfun,kpar);
    ng = zeros(1,length(kgrad)+1);
    % precompute for convenience
    W = invK - alpha*alpha';
    for i = 1:length(kgrad)
        ng(i) = 0.5*sum(sum(W.*kgrad{i}(p,p)));
    end
    %Noise gradient
    ng(end) = trace(W*abs(noise));
else
    ng = 0;
end