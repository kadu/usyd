% Copyright The University of Sydney, 2001 - 2008.  ABN 15 211 513 464
% herafter 'The University'.
%
% The copyright of the computer program(s) herein is the property of
% The University of Sydney, Australia.  The program(s) may be used
% and/or copied only with the written permission of The University of
% Sydney, or in accordance with the terms and conditions stipulated
% in the contract under which the program(s) have been supplied.
% 
% Disclaimer:
% The Universitiy makes no representation or warranty
% that these materials, including any software, are free from errors;
% about the quality or performance of these materials;
% that these materials are fit for any particular purpose.
% These materials are made available on the strict basis that The University
% and its employees or agents have no liability for any direct or indirect
% loss or damage (including for negligence) suffered by any person as a
%consequence of the use of this  material.
%
% Evaluate a Gaussian Process model using
% a local approximation with nn nearest neighbours.
%
%INPUTS
% X - D x N inputs
% y - 1 x N outputs (targets)
% kfun - covariance function
% kpar - covariance parameters (hyperparameters)
% noise - scalar noise level
% x - D x M test inputs
% nn - number of nearest neighbours
% 
%OUTPUTS
% mf - predictive mean
% vf - predictive variance
% lml - log marginal likelihood
%
% Fabio Tozeto Ramos 04/06/08
% [lml,mf,vf] = gpkdtreeeval(X,y,kfun,kpar,noise,x,nn)

function [lml,mf,vf] = gpkdtreeeval(X,y,kfun,kpar,noise,x,nn)
N = size(X,2); %number of inputs
nx = size(x,2); %number of query points
mf = zeros(1,nx);
vf = zeros(1,nx);
lml = zeros(1,nx);

%Build the KDtree
[kd,Xsor] = vgg_kdtree2_build(X,1);
ysor = y(kd.indices);
%Necessary according to the KDTree author
kd.indices = uint32(1:N);
for i=1:nx    
    %Searches for the nearest neighbours
    [is] = vgg_kdtree2_N_closest_points(kd,Xsor,x(:,i),nn);
    is = is(is~=0);
    Xt = Xsor(:,is);
    yt = ysor(is);
    myt = mean(yt);
    yt = yt - myt;
    
    %Compute the local covariance function
    K = feval(kfun,Xt,Xt,kpar);

    %Cholesky factorisation for covariance inversion
    L = chol(K+(noise^2)*eye(length(yt))); 
    %alpha = L'\(L\yt');
    alpha = solve_chol(L,yt');

    % K(Xtemp,x)
    ks = feval(kfun,Xt,x(:,i),kpar);

    %Compute the mean
    mf(i) = ks'*alpha + myt;

    %Compute the variance
    %v = L\ks;
    opts.TRANSA = true;
    opts.UT     = true;
    v = linsolve(L,ks,opts);
    vf(i) = diag(feval(kfun,x(:,i),x(:,i),kpar))-sum(v.*v)'+noise^2;

    %Compute the log marginal likelihood
    lml(i) = -0.5*yt*alpha-sum(log(diag(L)))-0.5*N*log(2*pi);
end