% Copyright The University of Sydney, 2001 - 2008.  ABN 15 211 513 464
% herafter 'The University'.
%
% The copyright of the computer program(s) herein is the property of
% The University of Sydney, Australia.  The program(s) may be used
% and/or copied only with the written permission of The University of
% Sydney, or in accordance with the terms and conditions stipulated
% in the contract under which the program(s) have been supplied.
% 
% Disclaimer:
% The Universitiy makes no representation or warranty
% that these materials, including any software, are free from errors;
% about the quality or performance of these materials;
% that these materials are fit for any particular purpose.
% These materials are made available on the strict basis that The University
% and its employees or agents have no liability for any direct or indirect
% loss or damage (including for negligence) suffered by any person as a
%consequence of the use of this  material.
%
% Demonstrate a GP regression

global X y;

%Use the same data as Rasmussen in Figure 2.5
X = [-2.1775;-0.9235;0.7502;-5.8868;-2.7995;4.2504;2.4582;6.1426;...
    -4.0911;-6.3481;1.0004;-4.7591;0.4715;4.8933;4.3248;-3.7461;...
    -7.3005;5.8177;2.3851;-6.3772]';
y = [1.4121;1.6936;-0.7444;0.2493;0.3978;-1.2755;-2.221;-0.8452;...
    -1.2232;0.0105;-1.0258;-0.8207;-0.1462;-1.5637;-1.098;-1.1721;...
    -1.7554;-1.0712;-2.6937;-0.0329]';


disp('  plot(x, y, ''k+'')')
plot(X, y, 'k+', 'MarkerSize', 17)
disp('  hold on')
hold on

disp(' ')
disp('Press any key to continue.')
pause

disp(' ')
disp('We now compute the predictions using a Gaussian process at 201 test') 
disp('points evenly distributed in the interval [-7.5, 7.5]. In this simple')
disp('example, we use a covariance function whose functional form matches')
disp('the covariance function which was used to generate the data. In this')
disp('case, this was a sum of a squared exponential (SE) covariance term, and')
disp('independent noise. Thus, the test cases and covariance function are')
disp('specified as follows:')
disp(' ')
% xstar = linspace(-7.5, 7.5, 201);
xstar = linspace(0, 15, 201);

disp('  xstar = linspace(-7.5, 7.5, 201)'';')
disp(' ')
disp('where the specification of covfunc says that the covariance should be a')
disp('sum of contributions from the two functions covSEiso and covNoise. The')
disp('help for covFunctions give more details about how to specify covariance')
disp('functions.')
disp(' ')
disp('Press any key to continue.')
pause

kpar = [1,1]; noise = 0.1; 
[lml,mf,vf] = gpeval(X,y,@cov_sqexp,kpar,[],noise,xstar);

disp('  S2 = S2 - exp(2*loghyper(3));')
S2 = vf - noise^2;
disp('  errorbar(xstar, mu, 2*sqrt(S2), ''g'');')
errorbar(xstar, mf, 2*sqrt(S2), 'g');
plot(X, y, 'k+', 'MarkerSize', 17)       % refresh points covered by errorbars!
disp(' ')
disp('Note that since we are interested in the distribution of the function')
disp('values and not the noisy examples, we subtract the noise variance, which')
disp('is stored in hyperparameter number 3, from the predictive variance S2.')
disp(' ')
disp('Press any key to continue.')
pause
disp(' ')
disp('Alternatively, the error bar range can be displayed in gray-scale to')
disp('reproduce Figure 2.5(a):')

clf
f = [mf+2*sqrt(S2);flipdim(mf-2*sqrt(S2),1)];
fill([xstar'; flipdim(xstar',1)], f, [7 7 7]/8, 'EdgeColor', [7 7 7]/8);
hold on
plot(xstar,mf,'k-','LineWidth',2);
plot(X, y, 'k+', 'MarkerSize', 17);

disp(' ')
disp('Press any key to continue.')
pause

disp(' ')
disp('We now investigate changing the hyperparameters to have the values:')
disp(' ')
disp('  loghyper = [log(0.3); log(1.08); log(5e-5)];')
loghyper = [log(0.3); log(1.08); log(5e-5)];
kpar = [0.3,1.08]; noise = 5e-5; 
disp(' ')
disp('so as to reproduce Figure 2.5(b). The lengthscale is now shorter (0.3)')
disp('and the noise level is much reduced, so the predicted mean almost')
disp('interpolates the data points. Notice that the error bars grow rapidly')
disp('away from the data points due to the short lengthscale.')
disp(' ')

disp('  [mu, S2] = gpr(loghyper, covfunc, x, y, xstar);')
%[mu S2] = gpr(loghyper, covfunc, x, y, xstar);
[lml,mf,vf] = gpeval(X,y,@cov_sqexp,kpar,[],noise,xstar);

disp('  S2 = S2 - exp(2*loghyper(3));')
S2 = vf - exp(2*loghyper(3));

clf
f = [mf+2*sqrt(S2);flipdim(mf-2*sqrt(S2),1)];
fill([xstar'; flipdim(xstar',1)], f, [7 7 7]/8, 'EdgeColor', [7 7 7]/8);
hold on
plot(xstar,mf,'k-','LineWidth',2);
plot(X, y, 'k+', 'MarkerSize', 17);

disp(' ')
disp('Press any key to continue.')
pause

disp(' ')
disp('Alternatively we can change the hyperparameters to have the values:')
disp(' ')
disp('  loghyper = [log(3.0); log(1.16); log(0.89)]')
loghyper = [log(3.0); log(1.16); log(0.89)];
kpar = [3.0,1.16]; noise = 0.89; 
disp(' ')
disp('so as to reproduce Figure 2.5(c). The lengthscale is now longer than')
disp('initially and the noise level is higher. Thus the predictive mean')
disp('function varies more slowly than before.')
disp(' ')
disp('  [mu S2] = gpr(loghyper, covfunc, x, y, xstar);')
%[mu S2] = gpr(loghyper, covfunc, x, y, xstar);
[lml,mf,vf] = gpeval(X,y,@cov_sqexp,kpar,[],noise,xstar);

disp('  S2 = S2 - exp(2*loghyper(3));')
S2 = vf - exp(2*loghyper(3));

clf
f = [mf+2*sqrt(S2);flipdim(mf-2*sqrt(S2),1)];
fill([xstar'; flipdim(xstar',1)], f, [7 7 7]/8, 'EdgeColor', [7 7 7]/8);
hold on
plot(xstar,mf,'k-','LineWidth',2);
plot(X, y, 'k+', 'MarkerSize', 17);

disp(' ')
disp('Press any key to continue.')
pause

[params]=gplearn(@cov_sqexp,@grad_sqexp,params0(1:end-1),params0(end),options)

