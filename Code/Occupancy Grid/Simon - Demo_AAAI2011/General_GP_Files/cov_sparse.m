% Copyright The University of Sydney, 2001 - 2008.  ABN 15 211 513 464
% herafter 'The University'.
%
% The copyright of the computer program(s) herein is the property of
% The University of Sydney, Australia.  The program(s) may be used
% and/or copied only with the written permission of The University of
% Sydney, or in accordance with the terms and conditions stipulated
% in the contract under which the program(s) have been supplied.
% 
% Disclaimer:
% The Universitiy makes no representation or warranty
% that these materials, including any software, are free from errors;
% about the quality or performance of these materials;
% that these materials are fit for any particular purpose.
% These materials are made available on the strict basis that The University
% and its employees or agents have no liability for any direct or indirect
% loss or damage (including for negligence) suffered by any person as a
%consequence of the use of this  material.
%
% Compute multi-dimensional sparse and finite covariance function
% constructed by Arman. This implementation uses kd-trees for efficiency.
%
% INPUTS
%   X1 - D x N1 matrix of D dimensional N1 points
%   X2 - D x N2 matrix of D dimensional N2 points
%   par(1:end-1) = [Length1; Length2; ..., LengthD] - D x 1 vector describing the D dimensional ellipsoid with
%                                                centre at the origin where the covariance function is nonzero
%   par(end) - covariance function coefficient Sigma0
%       
% OUTPUT
%   K - N1 x N2 sparse covariance function
%
% Fabio Ramos 23/10/2008

function K = cov_sparse(X1,X2,par)
[D N1] = size(X1);
N2 = size(X2,2);
if (size(X2,1)~=D)
  error('Dimensionalities of X1, X2 and length parameter must be the same');
end
par = reshape(par,D+1,1);
Sigma0 = abs(par(end));
dEucl = zeros(N1,1);
%Indexes for sparse matrix 
isize = ceil(1*N1*N2);
ii = zeros(1,isize);
jj = zeros(1,isize);
vv = zeros(1,isize);

L = abs(par(1:end-1));

%Build the KDtree
[kd,Xsor] = vgg_kdtree2_build(X1,1);
%Necessary according to the KDTree author for efficiency
indXsor = kd.indices;
kd.indices = uint32(1:N1);
ib = 1;
for i=1:N2
    [is] = vgg_kdtree2_ballsearch(kd,Xsor,X2(:,i),max(L));
    is = is(is~=0);
    ind = indXsor(is);
    X1t = Xsor(:,is);
    X2t = X2(:,i);
    dMat = abs(X1t - X2t(:,ones(1,length(is))))./L(:,ones(1,length(is)));
    temp = sqrt(sum(dMat.^2,1));
    dEuclOut = temp-1<0;
    dEucl(:) = 0;
    dEucl(ind,1) = temp.*dEuclOut;
    
    ind = ind(dEuclOut);
    ie = ib + length(ind) - 1;
    if ie>isize
        disp('Be careful. Size of non-zero elements larger than pre-allocated');
    end
    ii(ib:ie) = ind;
    jj(ib:ie) = i*ones(1,length(ind));
    vv(ib:ie) = Sigma0*((2 + cos(2*pi*dEucl(ind,1)))/3.*(1-dEucl(ind,1)) + ...
        1/(2*pi)*sin(2*pi*dEucl(ind,1)));
    ib = ie + 1;        
end

K = sparse(ii(1:ie),jj(1:ie),vv(1:ie),N1,N2);
end