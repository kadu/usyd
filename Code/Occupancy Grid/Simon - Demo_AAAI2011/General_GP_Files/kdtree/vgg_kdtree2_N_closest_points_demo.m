function vgg_linear_search_demo(N,K,L)

% vgg_inear_search_demo(N,K,L)
%
% Demonstrate linear search functions.
%
% N is number of closest points [10]
% K is number of points [1000]
% L is max number of points per leaf [100]

% Created: Aeron Buchanan <amb@robots.ox.ac.uk>
% Date: 15th December 2005

LARGE_NUMBER_OF_POINTS = 10000;

if nargin < 3, L = 100;
	if nargin < 2, K = 1000;
		if nargin < 1, N = 10;
		end
	end
end

switch 2
	case 1
		X = rand(2,K);
	case 2
		t = rand(1,K) * 2 * pi;
		X = [cos(2*t); sin(t)]/3 + .5;
		X = X + randn(size(X)) * .1;
	case 3
		[m,n] = abm_squarish_factors(K);
		K = m*n;
		X = zeros(2,K);
		for i=0:m-1
			for j=0:n-1
				X(:,i*n+j+1) = [i/(m-1);j/(n-1)];
			end
		end
end

MAX_VALUE = 200;
X = uint8(X*MAX_VALUE);
disp(['m-file: data is of class ''' class(X) '''']) % DEBUG

disp building
MAX_PER_LEAF = L;
vgg_tic
[kd,Xmapped] = vgg_kdtree2_build(X, MAX_PER_LEAF);
Xindices = kd.indices;
kd.indices = uint32(1:K);
%%% DEBUG
kd
disp(['m-file: Xmapped is of class ''' class(Xmapped) ''''])
%%% DEBUG END
vgg_toc


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% benchmarks

TTT = 1;

%%%%% _closest_point
nruns = 10;
fprintf('benchmark:');
while 1
	tic;
	for k=1:nruns
		index = vgg_kdtree2_closest_point(kd, Xmapped, rand(2,1)*MAX_VALUE);
	end
	t = toc;
	fprintf('[%g]', t);
	if t >= TTT
		fprintf(' per closest point query = %f ms (1 point returned)\n', t/nruns*1000);
		break
	end
	if t==0, t = 1e-3; end
	nruns=nruns*min(TTT/t,10);
end

%%%%% _N_closest_points
dists = [0];
nruns = 10;
fprintf('benchmark:');
while 1
	tic;
	for k=1:nruns
		[indices,dists] = vgg_kdtree2_N_closest_points(kd, Xmapped, rand(2,1)*MAX_VALUE, N);
	end
	t = toc;
	fprintf('[%g]', t);
	if t >= TTT
		fprintf(' per N closest points query = %f ms (%i points returned)\n', t/nruns*1000, length(find(indices)));
		break
	end
	if t==0, t = 1e-3; end
	nruns=nruns*min(TTT/t,10);
end

%%%%% _ballsearch
nruns = 10;
fprintf('benchmark:');
while 1
	tic;
	for k=1:nruns
		indices = vgg_kdtree2_ballsearch(kd, Xmapped, rand(2,1)*MAX_VALUE, dists(end));
	end
	t = toc;
	fprintf('[%g]', t);
	if t >= TTT
		fprintf(' per ballsearch query = %f ms (%i points returned)\n', t/nruns*1000, length(indices));
		break
	end
	if t==0, t = 1e-3; end
	nruns=nruns*min(TTT/t,10);
end

%%%%% end benchmarks
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% return

fig_h = gcf;
hold off;
if size(X,2)<LARGE_NUMBER_OF_POINTS
	plot(X(1,:), X(2,:), 'r.')
else
	points_to_show = abm_random_permutation(LARGE_NUMBER_OF_POINTS,size(X,2));
	plot(X(1,points_to_show), X(2,points_to_show), 'r.');
end
hold on
h2 = plot(.5,.5,'b+', 'markersize', 10);
h = plot(.5,.5,'g.', 'markersize', .1);
hh = plot(.5,.5,'ko', 'markersize', 5);
h1 = plot(.5,.5,'ko', 'markersize', 10, 'linewidth', 2);
hc = plot(0,0,'m-');
vgg_kdtree2_draw(kd, X, [0 0; 1 1]*MAX_VALUE);
axis square

set(fig_h,'doublebuffer','on')

drawnow

set(h, 'erasemode', 'xor');
set(h1, 'erasemode', 'xor');

title(['VGG\_KDTREE2\_N\_CLOSEST\_POINT\_DEMO (' num2str(K) ' points)'])
legend('data','query point',[num2str(N) ' closest points'],'ballsearch','closest point','dist to Nth closest point','kdtree partition',-1)
ylabel('double click to exit')
xlabel(sprintf('_N_closest_points found --; _ballsearch found --\n(ballsearch when more than N points within N closest point boundary)'),'interpreter','none'); %%% DEBUG

vgg_event_wait clear
while 1
	[x, y, button, type] = vgg_event_wait;

	% button:
	% 0 not down,
	% 1 2 3 left-to-right,
	% 4 doubleclick

	% event type:
	% -1 button up
	%  0 mouse motion
	%  1 button down
	%  2 keypress

	if button == 4
		disp('demo_event_wait: returning');
		close(fig_h)
		return
	end

	if type == 0
		% motion
		if button ~= 0
			[indices, dists, flag] = vgg_kdtree2_N_closest_points(kd, Xmapped, [x y]', N);
			% clean indices
			valid = find(indices>0);
			indices = indices(valid);
			dists = dists(valid);

			cutoff = 1000;
			if length(indices) > cutoff
				% Don't display more than 1000 as it will be too slow.
				indices = indices(abm_random_permutation(1000,length(indices)));
			end
			set(h, 'xdata', X(1,Xindices(indices)), 'ydata', X(2,Xindices(indices)));

			if flag
				N_found = length(indices); %%% DEBUG
				indices = vgg_kdtree2_ballsearch(kd, Xmapped, [x y]', dists(end));
				B_found = length(indices); %%% DEBUG
				if B_found<=N_found, xlabel('False positive on _N_closest_points flag set - needs to be fixed'); %%% DEBUG
				else xlabel(sprintf('_N_closest_points found %i; _ballsearch found %i\n(ballsearch when more than N points within N closest point boundary)',N_found,B_found),'interpreter','none'); end %%% DEBUG
				if length(indices)>cutoff, indices = indices(abm_random_permutation(1000,length(indices))); end
				set(hh, 'xdata',  X(1,Xindices(indices)), 'ydata', X(2,Xindices(indices)));
			else
				set(hh, 'xdata', [], 'ydata', []);
				xlabel(sprintf('_N_closest_points found %i; _ballsearch found --\n(ballsearch when more than N points within N closest point boundary)',length(indices)),'interpreter','none'); %%% DEBUG
			end

			index = vgg_kdtree2_closest_point(kd, Xmapped, [x y]');
			set(h1, 'xdata', X(1,Xindices(index)), 'ydata', X(2,Xindices(index)));
			if flag, set(h1,'color',[1 0 0])
			else set(h1,'color',[0 0 0])
			end

			set(h2, 'xdata', x, 'ydata', y);
			Nc = max(floor(dists(end)*500/MAX_VALUE),20);
			pc = vggkdt1Ncpd_circle_points(dists(end), Nc );
			set(hc, 'xdata', pc(1,:)+x, 'ydata', pc(2,:)+y)
		end
	elseif type == 1
		% down
	elseif type == -1
		% up
	elseif type == 2
		% key
		text(x, y, setstr(button));
	else
		type
	end

end

function P = vggkdt1Ncpd_circle_points(R,N)
if nargin<2, N = 20; end
t = linspace(0,2*pi,N);
P = zeros(2,N);
P(1,:) = R*cos(t);
P(2,:) = R*sin(t);
return

