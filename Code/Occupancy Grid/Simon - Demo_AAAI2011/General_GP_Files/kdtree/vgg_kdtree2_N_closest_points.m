function [is,ds,flag] = vgg_kdtree2_N_closest_points(kd,D,q,N,max_d)

% [is,ds,flag] = vgg_kdtree2_N_closest_points(kd,D,q,N[,max_d])
%
% kd is the output of vgg_kdtree2_build.
% D is columnwise data.
% q is a query point of the same dimension as the data in D.
% N is the number of points to be returned
% [max_d] is the distance beyond which points are ignored*
%
% is are the indices into D of closest points to p.
% ds are the distance bewtween D(:,is) and q.
% flag is 1 if more than N indices should hace been returned, in which case
%   the searh should be repeated using _ballsearch with radius = ds(end).
%
% If there are not N pointers within max_d of q, then is will contain
% zeros in the remaining 'slots'.
%
% *Due to the nature of kd tree searches, providing a maximum search radius
% will speed up queries.
%
% See also
%   vgg_kdtree2_build
%   vgg_kdtree2_closest_point
%   vgg_kdtree2_ball_search
%   vgg_kdtree2_demo

% author: Aeron Buchanan <amb@robots.ox.ac.uk>
% date: 2nd May 2005
% revised: 2nd December 2005

if nargin>4
	if max_d<0, max_d = inf; end
	[is,ds,flag] = vgg_kdtree2_N_closest_points_mex(kd,D,double(q),N,max_d);	
else
	[is,ds,flag] = vgg_kdtree2_N_closest_points_mex(kd,D,double(q),N);
end
