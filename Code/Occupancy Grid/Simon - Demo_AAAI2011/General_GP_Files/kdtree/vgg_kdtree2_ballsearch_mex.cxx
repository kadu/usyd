/*  
 * see vgg_kdtree2_ballsearch.m
 *
 */

// author: Aeron Buchanan <amb@robots.ox.ac.uk>
// date: 15th April 2005
// revised: 2nd December 2005

#include <mex.h>
#include <stdlib.h>
#include <math.h>

typedef unsigned int uint;

/*
 * The 'position' in binary tree is the node number as when labelled
 * breadth-first: on each level, the nodes, from left to right, as numbered
 * from 2^(n-1) to 2^n-1 where n is the number of the level (root is level 
 * 1). When expressed in binary, the leading bit denotes the level number
 * and the successive bits (read from most to least significant) specify 
 * the route from the top of the tree to that node; zeros being left and 
 * ones being right.
 *
 */

template <class T>
double vgg_kdtree2_ballsearch_templated_distance(T * D_data, int M, uint index, double * q_data){
	--index; // convert from matlab to C-style indexing
	double sqrd_distance = 0;
	for (int i=0; i<M; ++i){
		double dim_dist = fabs(q_data[i]-(double)D_data[index*M+i]);
		sqrd_distance += dim_dist*dim_dist;
	}
	return sqrt(sqrd_distance);
}


double vgg_kdtree2_ballsearch_distance(mxArray const * D_ptr, uint index, double * q_data){
	double distance = 0;

	int M = mxGetM(D_ptr);

	switch (mxGetClassID(D_ptr)) {
		case (mxINT8_CLASS):
			distance = vgg_kdtree2_ballsearch_templated_distance<char>((char*)mxGetPr(D_ptr), M, index, q_data);
			break;
		case (mxUINT8_CLASS):
			distance = vgg_kdtree2_ballsearch_templated_distance<unsigned char>((unsigned char*)mxGetPr(D_ptr), M, index, q_data);
			break;
		case (mxINT16_CLASS):
			distance = vgg_kdtree2_ballsearch_templated_distance<short>((short*)mxGetPr(D_ptr), M, index, q_data);
			break;
		case (mxUINT16_CLASS):
			distance = vgg_kdtree2_ballsearch_templated_distance<unsigned short>((unsigned short*)mxGetPr(D_ptr), M, index, q_data);
			break;
		case (mxINT32_CLASS):
			distance = vgg_kdtree2_ballsearch_templated_distance<int>((int*)mxGetPr(D_ptr), M, index, q_data);
			break;
		case (mxUINT32_CLASS):
			distance = vgg_kdtree2_ballsearch_templated_distance<uint>((unsigned int*)mxGetPr(D_ptr), M, index, q_data);
			break;
		case (mxSINGLE_CLASS):
			distance = vgg_kdtree2_ballsearch_templated_distance<float>((float*)mxGetPr(D_ptr), M, index, q_data);
			break;
		case (mxDOUBLE_CLASS):
			distance = vgg_kdtree2_ballsearch_templated_distance<double>((double*)mxGetPr(D_ptr), M, index, q_data);
			break;
		default:
			mexErrMsgTxt("vgg_kdtree2_ballsearch_mex: unexpected failure in attempt to assertain data type.");
	}

	return distance;
} 

double vgg_kdtree2_ballsearch_axis_differance(double * q_data, uint position, double * thresholds_data, uint * axis_dims_data){
	return q_data[axis_dims_data[position-1]-1] - thresholds_data[position-1];
} 

uint vgg_kdtree2_ballsearch_recurse(mxArray const * D_ptr,
                                    double * thresholds_data,
                                    uint * axis_dims_data,
                                    uint * indices,
                                    uint * leaves,
                                    uint * leaf_counts,
                                    uint position,
                                    uint max_position,
                                    double * q_data,
                                    double distance,
                                    uint * search_indices,
                                    double * search_dists,
                                    char record_dists
){
	uint num_indices = 0;

	if (position>=max_position) { // LEAF

		//printf("LEAF: "); // DEBUG

    double d = 0;
		uint leaf_index_base = leaves[position-max_position];
		for (uint i=0; i<leaf_counts[position-max_position]; ++i){
			d = vgg_kdtree2_ballsearch_distance(D_ptr,indices[leaf_index_base+i],q_data);

			//printf("%f ",d); // DEBUG

			if (d<=distance) { 
				if(record_dists) search_dists[num_indices] = d;
				search_indices[num_indices] = indices[leaf_index_base+i];
				++num_indices;
			}
		}

	}
	else { // BRANCH NODE
		//printf("NODE: \n"); // DEBUG
		int M = mxGetM(D_ptr);

		// look left
		if (vgg_kdtree2_ballsearch_axis_differance(q_data,position,thresholds_data,axis_dims_data)<distance){
			uint left_position = (position<<1);
			num_indices += vgg_kdtree2_ballsearch_recurse(D_ptr,thresholds_data,axis_dims_data,indices,leaves,leaf_counts,left_position,max_position,q_data,distance,search_indices,search_dists,record_dists);
		}

		// look right
		if (-vgg_kdtree2_ballsearch_axis_differance(q_data,position,thresholds_data,axis_dims_data)<=distance){
			uint right_position = (position<<1)+1;
			if(record_dists) num_indices += vgg_kdtree2_ballsearch_recurse(D_ptr,thresholds_data,axis_dims_data,indices,leaves,leaf_counts,right_position,max_position,q_data,distance,&search_indices[num_indices],&search_dists[num_indices],record_dists);
			else             num_indices += vgg_kdtree2_ballsearch_recurse(D_ptr,thresholds_data,axis_dims_data,indices,leaves,leaf_counts,right_position,max_position,q_data,distance,&search_indices[num_indices],search_dists,record_dists);
		}
	}

	//printf("found %i at distance %f\n",index,*distance); // DEBUG

	return num_indices;
}


void mexFunction(int nlhs, mxArray * plhs[],
                 int nrhs, mxArray const * prhs[])
{
	// function [is,ds] = vgg_kdtree2_ballsearch_mex(kd,D,q,max_d)
	// is are the indices into D of the closest points and ds the distances
	
	if (nrhs!=4) mexErrMsgTxt("vgg_kdtree2_ballsearch_mex: four input variables must be provided.");
	if (nlhs>2) mexErrMsgTxt("vgg_kdtree2_ballsearch_mex: no more than two output variables may be taken.");
	if (nlhs<1) mexErrMsgTxt("vgg_kdtree2_ballsearch_mex: at least one output variable must be taken.");
		
	mxArray const * D_ptr = prhs[1];
	if (mxGetNumberOfDimensions(D_ptr)>2) mexErrMsgTxt("vgg_kdtree2_ballsearch_mex: the data should be a 2D matrix.");
	int m = mxGetM(D_ptr); // dimension of points
	int n = (uint)mxGetN(D_ptr); // number of points
	
	// extract tree data from kd structure
	mxArray const * kd_ptr = prhs[0];
	if (!mxIsStruct(kd_ptr) ) mexErrMsgTxt("vgg_kdtree2_ballsearch_mex: first argument must be bona fide kd structure.");
	if (mxGetNumberOfFields(kd_ptr) < 4) mexErrMsgTxt("vgg_kdtree2_ballsearch_mex: wrong number of fields in kd structure (should be 4).");
	if (mxGetNumberOfElements(kd_ptr) != 1) mexErrMsgTxt("vgg_kdtree2_ballsearch_mex: wrong number of elements in kd structure (should be 1).");
	// get thresholds
	int kd_breadth_first_thresholds_index = mxGetFieldNumber(kd_ptr,"breadth_first_thresholds");
	if (kd_breadth_first_thresholds_index<0) mexErrMsgTxt("vgg_kdtree2_ballsearch_mex: kd structure must have a 'breadth_first_thresholds' field.");
	mxArray const * breadth_first_thresholds_ptr = mxGetFieldByNumber(kd_ptr,0,kd_breadth_first_thresholds_index);
	if (!mxIsDouble(breadth_first_thresholds_ptr)) mexErrMsgTxt("vgg_kdtree2_ballsearch_mex: breadth_first_thresholds (arg #2) must be of type double.");
	if (mxGetM(breadth_first_thresholds_ptr)!=1) mexErrMsgTxt("vgg_kdtree2_ballsearch_mex: breadth_first_thresholds (arg #2) must be a 1D array.");
	uint num_nodes = mxGetN(breadth_first_thresholds_ptr);
	uint num_leaves = num_nodes + 1;
	uint power_of_two = 1;
	int bit_count = 0; while(power_of_two>0 && bit_count<2){ if ( num_leaves&power_of_two) ++bit_count; power_of_two*=2; }
	if (bit_count!=1) mexErrMsgTxt("vgg_kdtree2_ballsearch_mex: kd.breadth_first_thresholds must have 2^n-1 elements.");
	// get axis dimensions
	int kd_axis_dims_index = mxGetFieldNumber(kd_ptr,"axis_dimensions");
	if (kd_axis_dims_index<0) mexErrMsgTxt("vgg_kdtree2_ballsearch_mex: kd structure must have a 'axis_dimensions' field.");
	mxArray const * axis_dims_ptr = mxGetFieldByNumber(kd_ptr,0,kd_axis_dims_index);
	if (!mxIsUint32(axis_dims_ptr)) mexErrMsgTxt("vgg_kdtree2_ballsearch_mex: kd.axis_dimensions must be of class uint32.");
	if (mxGetM(axis_dims_ptr)!=1) mexErrMsgTxt("vgg_kdtree2_ballsearch_mex: kd.axis_dimensions must be a 1D array.");
	if (mxGetN(axis_dims_ptr)!=(int)num_nodes) mexErrMsgTxt("vgg_kdtree2_mex: kd.axis_dimensions must have same number of elements as breadth_first_thresholds.");
	// get leaves
	int kd_leaves_index = mxGetFieldNumber(kd_ptr,"leaves");
	if (kd_leaves_index<0) mexErrMsgTxt("vgg_kdtree2_ballsearch_mex: kd structure must have a 'leaves' field.");
	mxArray const * leaves_ptr = mxGetFieldByNumber(kd_ptr,0,kd_leaves_index);
	if (!mxIsUint32(leaves_ptr)) mexErrMsgTxt("vgg_kdtree2_ballsearch_mex: kd.leaves must be of class uint32.");
	if (mxGetM(leaves_ptr)!=1) mexErrMsgTxt("vgg_kdtree2_ballsearch_mex: kd.leaves must be a 1D array.");
	if (mxGetN(leaves_ptr)!=(int)num_leaves) mexErrMsgTxt("vgg_kdtree2_mex: kd.leaves must have one more element than breadth_first_thresholds.");
	// get indices
	int kd_indices_index = mxGetFieldNumber(kd_ptr,"indices");
	if (kd_indices_index<0) mexErrMsgTxt("vgg_kdtree2_ballsearch_mex: kd structure must have an 'indices' field.");
	mxArray const * indices_ptr = mxGetFieldByNumber(kd_ptr,0,kd_indices_index);
	if (!mxIsUint32(indices_ptr)) mexErrMsgTxt("vgg_kdtree2_ballsearch_mex: kd.indices must be of class uint32.");
	if (mxGetM(indices_ptr)!=1) mexErrMsgTxt("vgg_kdtree2_ballsearch_mex: kd.indices must be a 1D array.");
	if (mxGetN(indices_ptr)!=n) mexErrMsgTxt("vgg_kdtree2_ballsearch_mex: kd.indices must have the same number of elements as data.");

	// check query point
	mxArray const * q_ptr = prhs[2];
	if (!mxIsDouble(q_ptr)) mexErrMsgTxt("vgg_kdtree2_ballsearch_mex: query must be of class double.");
	if (mxGetM(q_ptr)!=m) mexErrMsgTxt("vgg_kdtree2_ballsearch_mex: query point must have the same dimensionality as the data.");
	if (mxGetN(q_ptr)!=1) mexErrMsgTxt("vgg_kdtree2_ballsearch_mex: query point must be a column vector.");
	double * q_data = mxGetPr(q_ptr);

	// obtain data handles
	double * thresholds_data = mxGetPr(breadth_first_thresholds_ptr);
	uint * axis_dims_data = (uint*)mxGetPr(axis_dims_ptr);
	uint * leaves_data = (uint*)mxGetPr(leaves_ptr);
	uint * indices_data = (uint*)mxGetPr(indices_ptr);

	// organize minimum distance
	mxArray const * distance_ptr = prhs[3];
	if (!mxIsDouble(distance_ptr)) mexErrMsgTxt("vgg_kdtree2_ballsearch_mex: maximum distance must be of class double.");
	if (mxGetM(distance_ptr)!=1) mexErrMsgTxt("vgg_kdtree2_ballsearch_mex: maximum distance must be a single value.");
	if (mxGetN(distance_ptr)!=1) mexErrMsgTxt("vgg_kdtree2_ballsearch_mex: maximum distance must be a single value.");
	double * distance = mxGetPr(distance_ptr);

	// declare loop variables (for greater compiler compatibility)
	uint i;

	// calculate the number of data at each leaf
	uint * leaf_counts = (uint*)mxMalloc(num_leaves*sizeof(uint));
	for (i=0; i<num_leaves-1; ++i) leaf_counts[i] = leaves_data[i+1]-leaves_data[i];
	leaf_counts[num_leaves-1] = n - leaves_data[num_leaves-1];

	// create holder for all possible closest point indices and distances
	uint * search_indices = (uint*)mxMalloc(n*sizeof(uint));
	double * search_dists = (double*)0;
	if (nlhs>1)	search_dists = (double*)mxMalloc(n*sizeof(double));
	char record_dists = (nlhs>1);

	//printf("*** GO!\n"); // DEBUG

	// go get 'em
	uint num_indices = vgg_kdtree2_ballsearch_recurse(D_ptr,thresholds_data,axis_dims_data,indices_data,leaves_data,leaf_counts,1,num_leaves,q_data,*distance,search_indices,search_dists,record_dists);

	//printf("*** FINAL: found %i at distance %f\n",*index,*distance); // DEBUG

	// create and assign output variables
	if (nlhs>0) {
		mxArray * search_indices_ptr = mxCreateNumericMatrix(1,num_indices,mxUINT32_CLASS,mxREAL);
		uint * search_indices_data = (uint*)mxGetPr(search_indices_ptr);
		for(i=0; i<num_indices; ++i) search_indices_data[i] = search_indices[i];

		plhs[0] = search_indices_ptr;

		if (nlhs>1) {
			mxArray * search_dists_ptr = mxCreateNumericMatrix(1,num_indices,mxDOUBLE_CLASS,mxREAL);
			double * search_dists_data = (double*)mxGetPr(search_dists_ptr);
			for(i=0; i<num_indices; ++i) search_dists_data[i] = search_dists[i];

			plhs[1] = search_dists_ptr;
		}
	}

	mxFree(leaf_counts);
	mxFree(search_indices);
	if (nlhs>1)	mxFree(search_dists);

	// All done
}



