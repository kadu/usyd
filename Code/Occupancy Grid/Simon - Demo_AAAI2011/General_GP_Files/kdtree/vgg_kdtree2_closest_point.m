function [i,d] = vgg_kdtree2_closest_point(kd,D,q,max_d)

% [i,d] = vgg_kdtree2_closest_point(kd,D,q[,max_d])
%
% kd is the output of vgg_kdtree2_build.
% D is columnwise data.
% q is a query point of the same dimension as the data in D.
% [max_d] is the distance beyond which points are ignored*
%
% i is the index into D of closest point to p.
% d is the distance bewtween D(:,i) and q.
%
% *Due to the nature of kd tree searches, providing a maximum search radius
% will speed up queries.
%
% See also
%   vgg_kdtree2_build
%   vgg_kdtree2_N_closest_points
%   vgg_kdtree2_ball_search
%   vgg_kdtree2_demo

% author: Aeron Buchanan <amb@robots.ox.ac.uk>
% date: 14th April 2005
% revised: 2nd December 2005

if nargin>3
	if max_d<0, max_d = inf; end
	[i,d] = vgg_kdtree2_closest_point_mex(kd,D,double(q),max_d);	
else
	[i,d] = vgg_kdtree2_closest_point_mex(kd,D,double(q));
end
