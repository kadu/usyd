/*  
 * see vgg_kdtree2_N_closest_points.m
 *
 */

// author: Aeron Buchanan <amb@cghq.net>
// date: 17 January 2006

#include <mex.h>
#include <stdlib.h>
#include <math.h>

typedef unsigned int uint;

/*
 * The 'position' in binary tree is the node number as when labelled
 * breadth-first: on each level, the nodes, from left to right, as numbered
 * from 2^(n-1) to 2^n-1 where n is the number of the level (root is level 
 * 1). When expressed in binary, the leading bit denotes the level number
 * and the successive bits (read from most to least significant) specify 
 * the route from the top of the tree to that node; zeros being left and 
 * ones being right.
 *
 */

// NOTE: sizeof(T) is always 8 with my compiler (MSVS), hence casting D_data prior to function call
template <class T>
double vgg_kdtree2_N_closest_points_templated_distance(T * D_data, int M, uint index, double * q_data, double max_sqrd_dist){
	--index; // convert from matlab to C-style indexing
	double sqrd_distance = 0;
	uint start = index*M;
	int i;

	// The actual dimension number depends on the dataset, but generally (for real data)
	// faster not to check against distance for less than 10 dimensional data
	if (M<10) {
		for (i=0; i<M; ++i){
			double dim_dist = q_data[i]-(double)D_data[start+i];
			sqrd_distance += dim_dist*dim_dist;
		}
	}
	else {
		i = 0;
		while (i<M && sqrd_distance<=max_sqrd_dist) {
			double dim_dist = q_data[i]-(double)D_data[start+i];
			sqrd_distance += dim_dist*dim_dist;
			++i;
		}
	}

	return sqrd_distance;
}

template <class T>
void vgg_kdtree2_N_closest_points_recurse(T * D_data,
                                          uint M,
                                          double * thresholds_data,
                                          uint * axis_dims_data,
                                          uint * indices,
                                          uint * leaves,
                                          uint * leaf_counts,
                                          uint position,
                                          uint max_position,
                                          double * q_data,
                                          uint * is,
                                          double * sqrd_distances,
                                          uint * nn_count,
                                          uint N,
                                          double * idc_vector,
                                          double idc_sqrd_dist,
                                          double * flag
){

	if (position>=max_position) { // LEAF

		uint leaf_index_base = leaves[position-max_position];

		uint i = 0;
		if (*nn_count==0 && leaf_counts[position-max_position]>0) {
			// check here once rather than for every datum at this leaf
			double d_sqrd = vgg_kdtree2_N_closest_points_templated_distance<T>(D_data,M,indices[leaf_index_base],q_data,sqrd_distances[N-1]);
			sqrd_distances[0] = d_sqrd;
			is[0] = indices[leaf_index_base];
			++*nn_count;
			i = 1;
		}
		for ( ; i<leaf_counts[position-max_position]; ++i){
			double d_sqrd = vgg_kdtree2_N_closest_points_templated_distance<T>(D_data,M,indices[leaf_index_base+i],q_data,sqrd_distances[N-1]);

			// insert sort and shift
			if (d_sqrd<sqrd_distances[N-1]) {
				*flag = 0;

				int k = *nn_count-1;
				double dist_considered = sqrd_distances[k];

				if (*nn_count<N) ++*nn_count;
				else {
					if (sqrd_distances[N-2]==sqrd_distances[N-1]) *flag = 1;
					dist_considered = sqrd_distances[--k];
				}
				while (k>0 && d_sqrd<dist_considered) {
					sqrd_distances[k+1] = dist_considered;
					is[k+1] = is[k];
					dist_considered = sqrd_distances[--k];
				}
				if (k==0 && d_sqrd<dist_considered) {
					sqrd_distances[k+1] = dist_considered;
					is[k+1] = is[k];
					--k;
				}
				sqrd_distances[k+1] = d_sqrd;
				is[k+1] = indices[leaf_index_base+i];

			}
			else {
				if (d_sqrd==sqrd_distances[N-1]) *flag = 1;
			}
			// end insert sort

		}

	}
	else { // BRANCH NODE
		int axis_dimension = axis_dims_data[position-1]-1;
		double axis_dist = q_data[axis_dimension] - thresholds_data[position-1];
		double prev_axis_sqrd_dist = idc_vector[axis_dimension];

		// go down branch with possible closest point first
		if (axis_dist<0) {
			// try left first 
			uint left_position = (position<<1);
			vgg_kdtree2_N_closest_points_recurse(D_data,M,thresholds_data,axis_dims_data,indices,leaves,leaf_counts,left_position,max_position,q_data,is,sqrd_distances,nn_count,N,idc_vector,idc_sqrd_dist,flag);
	
			// distance may have been updated, so there may be no need to investigate the right hand subtree...
			idc_vector[axis_dimension] = axis_dist*axis_dist;
			idc_sqrd_dist += idc_vector[axis_dimension] - prev_axis_sqrd_dist;
			if (idc_sqrd_dist<=sqrd_distances[N-1]){	
				uint right_position = (position<<1)+1;
				vgg_kdtree2_N_closest_points_recurse(D_data,M,thresholds_data,axis_dims_data,indices,leaves,leaf_counts,right_position,max_position,q_data,is,sqrd_distances,nn_count,N,idc_vector,idc_sqrd_dist,flag);
			}
			idc_vector[axis_dimension] = prev_axis_sqrd_dist;
		}
		else{
			// try right first
			uint right_position = (position<<1)+1;
			vgg_kdtree2_N_closest_points_recurse(D_data,M,thresholds_data,axis_dims_data,indices,leaves,leaf_counts,right_position,max_position,q_data,is,sqrd_distances,nn_count,N,idc_vector,idc_sqrd_dist,flag);

			// distance may have been updated, so there may be no need to investigate the left hand subtree...
			idc_vector[axis_dimension] = axis_dist*axis_dist;
			idc_sqrd_dist += idc_vector[axis_dimension] - prev_axis_sqrd_dist;
			if (idc_sqrd_dist<sqrd_distances[N-1]){
				uint left_position = (position<<1);
				vgg_kdtree2_N_closest_points_recurse(D_data,M,thresholds_data,axis_dims_data,indices,leaves,leaf_counts,left_position,max_position,q_data,is,sqrd_distances,nn_count,N,idc_vector,idc_sqrd_dist,flag);
			}
			idc_vector[axis_dimension] = prev_axis_sqrd_dist;
		}
	}
}


void mexFunction(int nlhs, mxArray * plhs[],
                 int nrhs, mxArray const * prhs[])
{
	// function [is,ds,flag] = vgg_kdtree2_N_closest_points_mex(kd,D,q,N,max_d)
	// is are the indices of the N closest points into D
	// ds are the distances bewtween D(:,is) and q
	// flag is 1 if more than N indices should have been returned
	
	if (nrhs!=4 && nrhs!=5) mexErrMsgTxt("vgg_kdtree2_N_closest_points_mex: four or five input variables must be provided.");
	if (nlhs>3) mexErrMsgTxt("vgg_kdtree2_N_closest_points_mex: no more than three output variables are given.");
	if (nlhs<1) mexErrMsgTxt("vgg_kdtree2_N_closest_points_mex: at least one output variable must be taken.");
		
	mxArray const * D_ptr = prhs[1];
	if (mxGetNumberOfDimensions(D_ptr)>2) mexErrMsgTxt("vgg_kdtree2_N_closest_points_mex: the data should be a 2D matrix.");
	int m = mxGetM(D_ptr); // dimension of points
	int n = mxGetN(D_ptr); // number of points
	
	// extract tree data from kd structure
	mxArray const * kd_ptr = prhs[0];
	if (!mxIsStruct(kd_ptr) ) mexErrMsgTxt("vgg_kdtree2_N_closest_points_mex: first argument must be bona fide kd structure.");
	if (mxGetNumberOfFields(kd_ptr) < 4) mexErrMsgTxt("vgg_kdtree2_N_closest_points_mex: kd structure must contain at least four fields.");
	if (mxGetNumberOfElements(kd_ptr) != 1) mexErrMsgTxt("vgg_kdtree2_N_closest_points_mex: wrong number of elements in kd structure (should be 1).");
	// get thresholds
	int kd_breadth_first_thresholds_index = mxGetFieldNumber(kd_ptr,"breadth_first_thresholds");
	if (kd_breadth_first_thresholds_index<0) mexErrMsgTxt("vgg_kdtree2_N_closest_points_mex: kd structure must have a 'breadth_first_thresholds' field.");
	mxArray const * breadth_first_thresholds_ptr = mxGetFieldByNumber(kd_ptr,0,kd_breadth_first_thresholds_index);
	if (!mxIsDouble(breadth_first_thresholds_ptr)) mexErrMsgTxt("vgg_kdtree2_N_closest_points_mex: breadth_first_thresholds (arg #2) must be of type double.");
	if (mxGetM(breadth_first_thresholds_ptr)!=1) mexErrMsgTxt("vgg_kdtree2_N_closest_points_mex: breadth_first_thresholds (arg #2) must be a 1D array.");
	uint num_nodes = mxGetN(breadth_first_thresholds_ptr);
	uint num_leaves = num_nodes + 1;
	uint power_of_two = 1;
	int bit_count = 0; while(power_of_two>0 && bit_count<2){ if ( num_leaves&power_of_two) ++bit_count; power_of_two*=2; }
	if (bit_count!=1) mexErrMsgTxt("vgg_kdtree2_N_closest_points_mex: kd.breadth_first_thresholds must have 2^n-1 elements.");
	// get axis dimensions
	int kd_axis_dims_index = mxGetFieldNumber(kd_ptr,"axis_dimensions");
	if (kd_axis_dims_index<0) mexErrMsgTxt("vgg_kdtree2_ballsearch_mex: kd structure must have a 'axis_dimensions' field.");
	mxArray const * axis_dims_ptr = mxGetFieldByNumber(kd_ptr,0,kd_axis_dims_index);
	if (!mxIsUint32(axis_dims_ptr)) mexErrMsgTxt("vgg_kdtree2_ballsearch_mex: kd.axis_dimensions must be of class uint32.");
	if (mxGetM(axis_dims_ptr)!=1) mexErrMsgTxt("vgg_kdtree2_ballsearch_mex: kd.axis_dimensions must be a 1D array.");
	if (mxGetN(axis_dims_ptr)!=(int)num_nodes) mexErrMsgTxt("vgg_kdtree2_mex: kd.axis_dimensions must have same number of elements as breadth_first_thresholds.");
	// get leaves
	int kd_leaves_index = mxGetFieldNumber(kd_ptr,"leaves");
	if (kd_leaves_index<0) mexErrMsgTxt("vgg_kdtree2_N_closest_points_mex: kd structure must have a 'leaves' field.");
	mxArray const * leaves_ptr = mxGetFieldByNumber(kd_ptr,0,kd_leaves_index);
	if (!mxIsUint32(leaves_ptr)) mexErrMsgTxt("vgg_kdtree2_N_closest_points_mex: kd.leaves must be of class uint32.");
	if (mxGetM(leaves_ptr)!=1) mexErrMsgTxt("vgg_kdtree2_N_closest_points_mex: kd.leaves must be a 1D array.");
	if (mxGetN(leaves_ptr)!=(int)num_leaves) mexErrMsgTxt("vgg_kdtree2_mex: kd.leaves must have one more element than breadth_first_thresholds.");
	// get indices
	int kd_indices_index = mxGetFieldNumber(kd_ptr,"indices");
	if (kd_indices_index<0) mexErrMsgTxt("vgg_kdtree2_N_closest_points_mex: kd structure must have an 'indices' field.");
	mxArray const * indices_ptr = mxGetFieldByNumber(kd_ptr,0,kd_indices_index);
	if (!mxIsUint32(indices_ptr)) mexErrMsgTxt("vgg_kdtree2_N_closest_points_mex: kd.indices must be of class uint32.");
	if (mxGetM(indices_ptr)!=1) mexErrMsgTxt("vgg_kdtree2_N_closest_points_mex: kd.indices must be a 1D array.");
	if (mxGetN(indices_ptr)!=n) mexErrMsgTxt("vgg_kdtree2_N_closest_points_mex: kd.indices must have the same number of elements as data.");

	// check query point
	mxArray const * q_ptr = prhs[2];
	if (mxGetClassID(q_ptr)!=mxDOUBLE_CLASS) mexErrMsgTxt("vgg_kdtree2_N_closest_points_mex: query must be of class double."); // saves another switch statement on class
	if (mxGetM(q_ptr)!=m) mexErrMsgTxt("vgg_kdtree2_N_closest_points_mex: query point must have the same dimensionality as the data.");
	if (mxGetN(q_ptr)!=1) mexErrMsgTxt("vgg_kdtree2_N_closest_points_mex: query point must be a column vector.");
	double * q_data = mxGetPr(q_ptr);

	// obtain data handles
	double * thresholds_data = mxGetPr(breadth_first_thresholds_ptr);
	uint * axis_dims_data = (uint*)mxGetPr(axis_dims_ptr);
	uint * leaves_data = (uint*)mxGetPr(leaves_ptr);
	uint * indices_data = (uint*)mxGetPr(indices_ptr);

	// N, number of closest points
	mxArray const * N_ptr = prhs[3];
	if (mxGetClassID(N_ptr)!=mxDOUBLE_CLASS) mexErrMsgTxt("vgg_kdtree2_N_closest_points_mex: N must be of class double.");
	if (mxGetM(N_ptr)!=1) mexErrMsgTxt("vgg_kdtree2_N_closest_points_mex: N must be a single integer.");
	if (mxGetN(N_ptr)!=1) mexErrMsgTxt("vgg_kdtree2_N_closest_points_mex: N must be a single integer.");
	double N_dbl = mxGetPr(N_ptr)[0];
	if (N_dbl != floor(N_dbl)) mexErrMsgTxt("vgg_kdtree2_N_closest_points_mex: N must be an integer.");
	uint N = (uint) N_dbl;

	// organize minimum distance
	double initial_distance = mxGetInf();
	if (nrhs>4) {
		mxArray const * initial_distance_ptr = prhs[4];
		if (!mxIsDouble(initial_distance_ptr)) mexErrMsgTxt("vgg_kdtree2_N_closest_points_mex: maximum distance must be of class double.");
		if (mxGetM(initial_distance_ptr)!=1) mexErrMsgTxt("vgg_kdtree2_N_closest_points_mex: maximum distance must be a single value.");
		if (mxGetN(initial_distance_ptr)!=1) mexErrMsgTxt("vgg_kdtree2_N_closest_points_mex: maximum distance must be a single value.");
		initial_distance = *mxGetPr(initial_distance_ptr);
		initial_distance *= initial_distance;
	}

	// declare loop variables (for greater compiler compatibility)
	uint i;

	// calculate the number of data at each leaf
	uint * leaf_counts = (uint*)mxMalloc(num_leaves*sizeof(uint));
	for (i=0; i<num_leaves-1; ++i) leaf_counts[i] = leaves_data[i+1]-leaves_data[i];
	leaf_counts[num_leaves-1] = n - leaves_data[num_leaves-1];

	// create output variables
	mxArray * is_ptr = mxCreateNumericMatrix(N,1,mxUINT32_CLASS,mxREAL);
	uint * is = (uint*)mxGetPr(is_ptr);
	for (i=0; i<N; ++i) is[i] = 0;

	mxArray * sqrd_distances_ptr;
	double * sqrd_distances;
	if (nlhs>1) {
		sqrd_distances_ptr = mxCreateDoubleMatrix(N,1,mxREAL);
		sqrd_distances = mxGetPr(sqrd_distances_ptr);
	}
	else {
		sqrd_distances = (double*)mxMalloc(sizeof(double)*N);
	}
	for (i=0; i<N; ++i) sqrd_distances[i] = initial_distance;

	mxArray * flag_ptr;
	double * flag;
	if (nlhs>2) {
		flag_ptr = mxCreateDoubleMatrix(1,1,mxREAL);
		flag = mxGetPr(flag_ptr);
	}
	else {
		flag = (double*)mxMalloc(sizeof(double));
		*flag = 0;
	}
	
	double * idc_vector = (double*)mxCalloc(m,sizeof(double));
	double idc_sqrd_dist = 0;
	uint nn_count = 0; // number of nearest neighbours found so far

	// go
	switch (mxGetClassID(D_ptr)) {
		case (mxDOUBLE_CLASS):
			vgg_kdtree2_N_closest_points_recurse((double *)mxGetPr(D_ptr),m,thresholds_data,axis_dims_data,indices_data,leaves_data,leaf_counts,1,num_leaves,q_data,is,sqrd_distances,&nn_count,N,idc_vector,idc_sqrd_dist,flag);
			break;
		case (mxUINT8_CLASS):
			vgg_kdtree2_N_closest_points_recurse((unsigned char *)mxGetPr(D_ptr),m,thresholds_data,axis_dims_data,indices_data,leaves_data,leaf_counts,1,num_leaves,q_data,is,sqrd_distances,&nn_count,N,idc_vector,idc_sqrd_dist,flag);
			break;
		case (mxINT8_CLASS):
			vgg_kdtree2_N_closest_points_recurse((char *)mxGetPr(D_ptr),m,thresholds_data,axis_dims_data,indices_data,leaves_data,leaf_counts,1,num_leaves,q_data,is,sqrd_distances,&nn_count,N,idc_vector,idc_sqrd_dist,flag);
			break;
		case (mxUINT16_CLASS):
			vgg_kdtree2_N_closest_points_recurse((unsigned short *)mxGetPr(D_ptr),m,thresholds_data,axis_dims_data,indices_data,leaves_data,leaf_counts,1,num_leaves,q_data,is,sqrd_distances,&nn_count,N,idc_vector,idc_sqrd_dist,flag);
			break;
		case (mxINT16_CLASS):
			vgg_kdtree2_N_closest_points_recurse((short *)mxGetPr(D_ptr),m,thresholds_data,axis_dims_data,indices_data,leaves_data,leaf_counts,1,num_leaves,q_data,is,sqrd_distances,&nn_count,N,idc_vector,idc_sqrd_dist,flag);
			break;
		case (mxUINT32_CLASS):
			vgg_kdtree2_N_closest_points_recurse((unsigned int *)mxGetPr(D_ptr),m,thresholds_data,axis_dims_data,indices_data,leaves_data,leaf_counts,1,num_leaves,q_data,is,sqrd_distances,&nn_count,N,idc_vector,idc_sqrd_dist,flag);
			break;
		case (mxINT32_CLASS):
			vgg_kdtree2_N_closest_points_recurse((int *)mxGetPr(D_ptr),m,thresholds_data,axis_dims_data,indices_data,leaves_data,leaf_counts,1,num_leaves,q_data,is,sqrd_distances,&nn_count,N,idc_vector,idc_sqrd_dist,flag);
			break;
		case (mxSINGLE_CLASS):
			vgg_kdtree2_N_closest_points_recurse((float *)mxGetPr(D_ptr),m,thresholds_data,axis_dims_data,indices_data,leaves_data,leaf_counts,1,num_leaves,q_data,is,sqrd_distances,&nn_count,N,idc_vector,idc_sqrd_dist,flag);
			break;
		default:
			mexErrMsgTxt("vgg_kdtree2_N_closest_points_mex: unexpected failure in attempt to assertain data type.");
	}
	
	// transform distances
	if (nlhs>1) for (i=0; i<N; ++i) sqrd_distances[i] = sqrt(sqrd_distances[i]);

	// assign outputs
	plhs[0] = is_ptr;
	if (nlhs>1) plhs[1] = sqrd_distances_ptr; else mxFree(sqrd_distances);
	if (nlhs>2) plhs[2] = flag_ptr; else mxFree(flag);
	
	mxFree(leaf_counts);
	
	// All done
}



