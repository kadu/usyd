load ColourOUtdoorSparseLibraryOfTrainingData


addpath ../../../General_GP_Files/kdtree



%Build the KDtree

[kd,Xsor] = vgg_kdtree2_build(X1,1);

%Necessary according to the KDTree author for efficiency

indXsor = kd.indices;

kd.indices = uint32(1:N1);

ib = 1;