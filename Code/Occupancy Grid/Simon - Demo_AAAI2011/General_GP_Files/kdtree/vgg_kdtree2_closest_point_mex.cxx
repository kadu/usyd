/*  
 * see vgg_kdtree2_closest_point.m
 *
 */

// author: Aeron Buchanan <amb@cghq.net>
// date: 17 January 2006

#include <mex.h>
#include <stdlib.h>
#include <math.h>

typedef unsigned int uint;

/*
 * The 'position' in binary tree is the node number as when labelled
 * breadth-first: on each level, the nodes, from left to right, as numbered
 * from 2^(n-1) to 2^n-1 where n is the number of the level (root is level 
 * 1). When expressed in binary, the leading bit denotes the level number
 * and the successive bits (read from most to least significant) specify 
 * the route from the top of the tree to that node; zeros being left and 
 * ones being right.
 *
 */

// NOTE: sizeof(T) is always 8 with my compiler (MSVS), hence casting D_data prior to function call
template <class T>
double vgg_kdtree2_closest_point_templated_distance(T * D_data, int M, uint index, double * q_data, double max_sqrd_dist){
	--index; // convert from matlab to C-style indexing
	double sqrd_distance = 0;
	uint start = index*M;
	int i;

	// faster not to check against distance for less than 12 dimensional data
	if (M<12) {
		for (i=0; i<M; ++i){
			double dim_dist = q_data[i]-(double)D_data[start+i];
			sqrd_distance += dim_dist*dim_dist;
		}
	}
	else {
		i = 0;
		while (i<M && sqrd_distance<=max_sqrd_dist) {
			double dim_dist = q_data[i]-(double)D_data[start+i];
			sqrd_distance += dim_dist*dim_dist;
			++i;
		}
	}

	return sqrd_distance;
}

template <class T>
uint vgg_kdtree2_closest_point_recurse(T * D_data,
                                       int M,
                                       double * thresholds_data,
                                       uint * axis_dims_data,
                                       uint * indices,
                                       uint * leaves,
                                       uint * leaf_counts,
                                       uint position,
                                       uint max_position,
                                       double * q_data,
                                       double * sqrd_distance,
                                       double * idc_vector,
                                       double idc_sqrd_dist
){
	uint index = 0;

	if (position>=max_position) { // LEAF

		uint leaf_index_base = leaves[position-max_position];
		for (uint i=0; i<leaf_counts[position-max_position]; ++i){
			double d_sqrd = vgg_kdtree2_closest_point_templated_distance<T>(D_data,M,indices[leaf_index_base+i],q_data,*sqrd_distance);

			if (d_sqrd<*sqrd_distance) { *sqrd_distance = d_sqrd; index = indices[leaf_index_base+i]; }
		}

	}
	else { // BRANCH NODE
		int axis_dimension = axis_dims_data[position-1]-1;
		double axis_dist = q_data[axis_dimension] - thresholds_data[position-1];
		double prev_axis_sqrd_dist = idc_vector[axis_dimension];

		if (axis_dist<0) {
			// try left first
			uint left_position = (position<<1);
			uint left_index = vgg_kdtree2_closest_point_recurse(D_data,M,thresholds_data,axis_dims_data,indices,leaves,leaf_counts,left_position,max_position,q_data,sqrd_distance,idc_vector,idc_sqrd_dist);
			if(left_index!=0) index = left_index;

			// distance may have been updated, so there may be no need to investigate the right hand subtree...
			idc_vector[axis_dimension] = axis_dist*axis_dist;
			idc_sqrd_dist += idc_vector[axis_dimension] - prev_axis_sqrd_dist;
			if (idc_sqrd_dist<=*sqrd_distance){
				uint right_position = (position<<1)+1;
				uint right_index = vgg_kdtree2_closest_point_recurse(D_data,M,thresholds_data,axis_dims_data,indices,leaves,leaf_counts,right_position,max_position,q_data,sqrd_distance,idc_vector,idc_sqrd_dist);
				if(right_index!=0) index = right_index;
			}
			idc_vector[axis_dimension] = prev_axis_sqrd_dist;
		}
		else{
			// try right first
			uint right_position = (position<<1)+1;
			uint right_index = vgg_kdtree2_closest_point_recurse(D_data,M,thresholds_data,axis_dims_data,indices,leaves,leaf_counts,right_position,max_position,q_data,sqrd_distance,idc_vector,idc_sqrd_dist);
			if(right_index!=0) index = right_index;

			// distance may have been updated, so there may be no need to investigate the left hand subtree...
			idc_vector[axis_dimension] = axis_dist*axis_dist;
			idc_sqrd_dist += idc_vector[axis_dimension] - prev_axis_sqrd_dist;
			if (idc_sqrd_dist<*sqrd_distance){
				uint left_position = (position<<1);
				uint left_index = vgg_kdtree2_closest_point_recurse(D_data,M,thresholds_data,axis_dims_data,indices,leaves,leaf_counts,left_position,max_position,q_data,sqrd_distance,idc_vector,idc_sqrd_dist);
				if(left_index!=0) index = left_index;
			}
			idc_vector[axis_dimension] = prev_axis_sqrd_dist;
		}
	}

	return index;
}


void mexFunction(int nlhs, mxArray * plhs[],
                 int nrhs, mxArray const * prhs[])
{
	// function [i,d] = vgg_kdtree2_closest_point_mex(kd,D,q,max_d)
	// i is the index of closest point into D
	// d is the distance bewtween p and q
	
	if (nrhs!=3 && nrhs!=4) mexErrMsgTxt("vgg_kdtree2_closest_point_mex: three or four input variables must be provided.");
	if (nlhs>2) mexErrMsgTxt("vgg_kdtree2_closest_point_mex: no more than two output variables are given.");
	if (nlhs<1) mexErrMsgTxt("vgg_kdtree2_closest_point_mex: at least one output variable must be taken.");		
	
	mxArray const * D_ptr = prhs[1];
	if (mxGetNumberOfDimensions(D_ptr)>2) mexErrMsgTxt("vgg_kdtree2_closest_point_mex: the data should be a 2D matrix.");
	int m = mxGetM(D_ptr); // dimension of points
	int n = (uint)mxGetN(D_ptr); // number of points
	
	// extract tree data from kd structure
	mxArray const * kd_ptr = prhs[0];
	if (!mxIsStruct(kd_ptr) ) mexErrMsgTxt("vgg_kdtree2_closest_point_mex: first argument must be bona fide kd structure.");
	if (mxGetNumberOfFields(kd_ptr) < 4) mexErrMsgTxt("vgg_kdtree2_closest_point_mex: kd structure must contain at least four fields.");
	if (mxGetNumberOfElements(kd_ptr) != 1) mexErrMsgTxt("vgg_kdtree2_closest_point_mex: wrong number of elements in kd structure (should be 1).");
	// get thresholds
	int kd_breadth_first_thresholds_index = mxGetFieldNumber(kd_ptr,"breadth_first_thresholds");
	if (kd_breadth_first_thresholds_index<0) mexErrMsgTxt("vgg_kdtree2_closest_point_mex: kd structure must have a 'breadth_first_thresholds' field.");
	mxArray const * breadth_first_thresholds_ptr = mxGetFieldByNumber(kd_ptr,0,kd_breadth_first_thresholds_index);
	if (!mxIsDouble(breadth_first_thresholds_ptr)) mexErrMsgTxt("vgg_kdtree2_closest_point_mex: breadth_first_thresholds (arg #2) must be of type double.");
	if (mxGetM(breadth_first_thresholds_ptr)!=1) mexErrMsgTxt("vgg_kdtree2_closest_point_mex: breadth_first_thresholds (arg #2) must be a 1D array.");
	uint num_nodes = mxGetN(breadth_first_thresholds_ptr);
	uint num_leaves = num_nodes + 1;
	uint power_of_two = 1;
	int bit_count = 0; while(power_of_two>0 && bit_count<2){ if ( num_leaves&power_of_two) ++bit_count; power_of_two*=2; }
	if (bit_count!=1) mexErrMsgTxt("vgg_kdtree2_closest_point_mex: kd.breadth_first_thresholds must have 2^n-1 elements.");
	// get axis dimensions
	int kd_axis_dims_index = mxGetFieldNumber(kd_ptr,"axis_dimensions");
	if (kd_axis_dims_index<0) mexErrMsgTxt("vgg_kdtree2_ballsearch_mex: kd structure must have a 'axis_dimensions' field.");
	mxArray const * axis_dims_ptr = mxGetFieldByNumber(kd_ptr,0,kd_axis_dims_index);
	if (!mxIsUint32(axis_dims_ptr)) mexErrMsgTxt("vgg_kdtree2_ballsearch_mex: kd.axis_dimensions must be of class uint32.");
	if (mxGetM(axis_dims_ptr)!=1) mexErrMsgTxt("vgg_kdtree2_ballsearch_mex: kd.axis_dimensions must be a 1D array.");
	if (mxGetN(axis_dims_ptr)!=(int)num_nodes) mexErrMsgTxt("vgg_kdtree2_mex: kd.axis_dimensions must have same number of elements as breadth_first_thresholds.");
	// get leaves
	int kd_leaves_index = mxGetFieldNumber(kd_ptr,"leaves");
	if (kd_leaves_index<0) mexErrMsgTxt("vgg_kdtree2_closest_point_mex: kd structure must have a 'leaves' field.");
	mxArray const * leaves_ptr = mxGetFieldByNumber(kd_ptr,0,kd_leaves_index);
	if (!mxIsUint32(leaves_ptr)) mexErrMsgTxt("vgg_kdtree2_closest_point_mex: kd.leaves must be of class uint32.");
	if (mxGetM(leaves_ptr)!=1) mexErrMsgTxt("vgg_kdtree2_closest_point_mex: kd.leaves must be a 1D array.");
	if (mxGetN(leaves_ptr)!=(int)num_leaves) mexErrMsgTxt("vgg_kdtree2_mex: kd.leaves must have one more element than breadth_first_thresholds.");
	// get indices
	int kd_indices_index = mxGetFieldNumber(kd_ptr,"indices");
	if (kd_indices_index<0) mexErrMsgTxt("vgg_kdtree2_closest_point_mex: kd structure must have an 'indices' field.");
	mxArray const * indices_ptr = mxGetFieldByNumber(kd_ptr,0,kd_indices_index);
	if (!mxIsUint32(indices_ptr)) mexErrMsgTxt("vgg_kdtree2_closest_point_mex: kd.indices must be of class uint32.");
	if (mxGetM(indices_ptr)!=1) mexErrMsgTxt("vgg_kdtree2_closest_point_mex: kd.indices must be a 1D array.");
	if (mxGetN(indices_ptr)!=n) mexErrMsgTxt("vgg_kdtree2_closest_point_mex: kd.indices must have the same number of elements as data.");

	// check query point
	mxArray const * q_ptr = prhs[2];
	if (mxGetClassID(q_ptr)!=mxDOUBLE_CLASS) mexErrMsgTxt("vgg_kdtree2_closest_point_mex: query must be of class double."); // saves another switch statement on class
	if (mxGetM(q_ptr)!=m) mexErrMsgTxt("vgg_kdtree2_closest_point_mex: query point must have the same dimensionality as the data.");
	if (mxGetN(q_ptr)!=1) mexErrMsgTxt("vgg_kdtree2_closest_point_mex: query point must be a column vector.");
	double * q_data = mxGetPr(q_ptr);

	// obtain data handles
	double * thresholds_data = mxGetPr(breadth_first_thresholds_ptr);
	uint * axis_dims_data = (uint*)mxGetPr(axis_dims_ptr);
	uint * leaves_data = (uint*)mxGetPr(leaves_ptr);
	uint * indices_data = (uint*)mxGetPr(indices_ptr);

	// organize minimum distance
	double initial_distance = mxGetInf();
	if (nrhs>3) {
		mxArray const * initial_distance_ptr = prhs[3];
		if (!mxIsDouble(initial_distance_ptr)) mexErrMsgTxt("vgg_kdtree2_closest_point_mex: maximum distance must be of class double.");
		if (mxGetM(initial_distance_ptr)!=1) mexErrMsgTxt("vgg_kdtree2_closest_point_mex: maximum distance must be a single value.");
		if (mxGetN(initial_distance_ptr)!=1) mexErrMsgTxt("vgg_kdtree2_closest_point_mex: maximum distance must be a single value.");
		initial_distance = *mxGetPr(initial_distance_ptr);
		initial_distance *= initial_distance;
	}

	// calculate the number of data at each leaf
	uint * leaf_counts = (uint*)mxMalloc(num_leaves*sizeof(uint));
	for (uint i=0; i<num_leaves-1; ++i) leaf_counts[i] = leaves_data[i+1]-leaves_data[i];
	leaf_counts[num_leaves-1] = n - leaves_data[num_leaves-1];

	// create output variables
	mxArray * index_ptr = mxCreateNumericMatrix(1,1,mxUINT32_CLASS,mxREAL);
	uint * index = (uint*)mxGetPr(index_ptr);

	mxArray * sqrd_distance_ptr;
	double * sqrd_distance;
	if (nlhs>1) {
		sqrd_distance_ptr = mxCreateDoubleMatrix(1,1,mxREAL);
		sqrd_distance = mxGetPr(sqrd_distance_ptr);
		*sqrd_distance = initial_distance;
	}
	else {
		sqrd_distance = &initial_distance;
	}

	double * idc_vector = (double*)mxCalloc(m,sizeof(double));
	double idc_sqrd_dist = 0;

	// go get 'em
	switch (mxGetClassID(D_ptr)) {
		case (mxDOUBLE_CLASS):
			*index = vgg_kdtree2_closest_point_recurse((double *)mxGetPr(D_ptr),m,thresholds_data,axis_dims_data,indices_data,leaves_data,leaf_counts,1,num_leaves,q_data,sqrd_distance,idc_vector,idc_sqrd_dist);
			break;
		case (mxUINT8_CLASS):
			*index = vgg_kdtree2_closest_point_recurse((unsigned char *)mxGetPr(D_ptr),m,thresholds_data,axis_dims_data,indices_data,leaves_data,leaf_counts,1,num_leaves,q_data,sqrd_distance,idc_vector,idc_sqrd_dist);
			break;
		case (mxINT8_CLASS):
			*index = vgg_kdtree2_closest_point_recurse((char *)mxGetPr(D_ptr),m,thresholds_data,axis_dims_data,indices_data,leaves_data,leaf_counts,1,num_leaves,q_data,sqrd_distance,idc_vector,idc_sqrd_dist);
			break;
		case (mxUINT16_CLASS):
			*index = vgg_kdtree2_closest_point_recurse((unsigned short *)mxGetPr(D_ptr),m,thresholds_data,axis_dims_data,indices_data,leaves_data,leaf_counts,1,num_leaves,q_data,sqrd_distance,idc_vector,idc_sqrd_dist);
			break;
		case (mxINT16_CLASS):
			*index = vgg_kdtree2_closest_point_recurse((short *)mxGetPr(D_ptr),m,thresholds_data,axis_dims_data,indices_data,leaves_data,leaf_counts,1,num_leaves,q_data,sqrd_distance,idc_vector,idc_sqrd_dist);
			break;
		case (mxUINT32_CLASS):
			*index = vgg_kdtree2_closest_point_recurse((unsigned int *)mxGetPr(D_ptr),m,thresholds_data,axis_dims_data,indices_data,leaves_data,leaf_counts,1,num_leaves,q_data,sqrd_distance,idc_vector,idc_sqrd_dist);
			break;
		case (mxINT32_CLASS):
			*index = vgg_kdtree2_closest_point_recurse((int *)mxGetPr(D_ptr),m,thresholds_data,axis_dims_data,indices_data,leaves_data,leaf_counts,1,num_leaves,q_data,sqrd_distance,idc_vector,idc_sqrd_dist);
			break;
		case (mxSINGLE_CLASS):
			*index = vgg_kdtree2_closest_point_recurse((float *)mxGetPr(D_ptr),m,thresholds_data,axis_dims_data,indices_data,leaves_data,leaf_counts,1,num_leaves,q_data,sqrd_distance,idc_vector,idc_sqrd_dist);
			break;
		default:
			mexErrMsgTxt("vgg_kdtree2_N_closest_points_mex: unexpected failure in attempt to assertain data type.");
	}

	// assign outputs
	plhs[0] = index_ptr;
	if (nlhs>1) {
		*sqrd_distance = sqrt(*sqrd_distance);
		plhs[1] = sqrd_distance_ptr;
	}

	mxFree(leaf_counts);
	mxFree(idc_vector);

	// All done
}



