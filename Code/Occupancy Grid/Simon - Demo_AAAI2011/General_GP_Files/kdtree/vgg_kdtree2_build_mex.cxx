/*  
 * see vgg_kdtree2_build.m
 *
 */

// author: Aeron Buchanan <amb@robots.ox.ac.uk>
// date: 14th April 2005
// revised: 2nd December 2005

#include <mex.h>
#include <stdlib.h>
#include <math.h>

typedef unsigned int uint;

/*
 * The 'position' in binary tree is the node number as when labelled
 * breadth-first: on each level, the nodes, from left to right, as numbered
 * from 2^(n-1) to 2^n-1 where n is the number of the level (root is level 
 * 1). When expressed in binary, the leading bit denotes the level number
 * and the successive bits (read from most to least significant) specify 
 * the route from the top of the tree to that node; zeros being left and 
 * ones being right.
 *
 */

// NOTE: sizeof(T) is always 8 with my compiler (MSVS), hence casting D_data prior to function call

// find dimension with largest variance
template <class T>
uint vgg_kdtree2_build_axis(T * D_data, int M, uint * indices, uint n){
	uint axis = 1;
	
	if (n>1) {
		uint i,j;

		double * sum_linear = (double*)mxCalloc(M,sizeof(double)); // initialize with zeros
		double * sum_squares = (double*)mxCalloc(M,sizeof(double)); // initialize with zeros
		for(i=0; i<n; ++i){
			for(j=0; j<M; ++j){
				uint index = indices[i]*M + j;
				sum_linear[j] += D_data[index];
				sum_squares[j] += D_data[index]*D_data[index];
			}
		}

		double max_var = 0;
		double inv_n = (double)1/(double)n; // treat data as a population, hence denominator is N
		for(j=0; j<M; ++j){
			double this_var = inv_n*(sum_squares[j] - inv_n*sum_linear[j]*sum_linear[j]);
			if (this_var>max_var) {
				max_var = this_var;
				axis = j+1; // convert to matlab indexing
			}
		}

		mxFree(sum_linear);
		mxFree(sum_squares);

	}

	return axis;
}

// comparison function for qsort()
struct vgg_kdtree2_build_compare_class {
  static int compare_doubles(void const * c1, void const * c2){ int rv=0; if(*(double*)c1<*(double*)c2){rv=-1;}else if(*(double*)c1>*(double*)c2){rv=1;} return rv; }
};

// find the median
template <class T> double vgg_kdtree2_build_median(T * D_data, int M, int axis, uint * indices, uint n){
	double median = 0;

	if (n==0) { }
	else if (n==1) { 
		median = D_data[indices[0]*M + axis-1];
	}
	else if (n==2) {
		median  = D_data[indices[0]*M + axis-1];
		median += D_data[indices[1]*M + axis-1];
		median /= 2;
	}
	else {
		// convert data to double an make local copy
		double * sort_data = (double*)mxMalloc(n*sizeof(double));
		for(uint i=0; i<n; ++i){ sort_data[i] = static_cast<double>(D_data[indices[i]*M + axis-1]); }

		qsort(sort_data,n,sizeof(double),vgg_kdtree2_build_compare_class::compare_doubles);
	
		// find the median
		if (n%2) { // n is odd
			median = sort_data[(n-1)/2];
		}
		else{ // n is even
			median = ( sort_data[n/2] + sort_data[(n/2)-1] )/2;
		}

		mxFree(sort_data);
	}

	return median;
}

// find the median and split the data
template <class T>
double vgg_kdtree2_build_median_split(T * D_data,
                                      int M,
                                      int axis, 
                                      uint * indices,
                                      uint n,
                                      uint * left_indices,
                                      uint * right_indices,
                                      uint * left_n,
                                      uint * right_n)
{
	double median = vgg_kdtree2_build_median<T>(D_data,M,axis,indices,n);

	for(uint i=0; i<n; ++i){
		uint index = indices[i];

		if(double(D_data[M*index + axis-1])<median){
			// add to left
			left_indices[*left_n] = index;
			++*left_n;
		}
		else{
			// add to right
			right_indices[*right_n] = index;
			++*right_n;
		}
	}

	/* 
	// could use only one extra array rather than 2n as in left_ and right_:
	uint * indices_copy = malloc(n);
	uint * left_ptr = indices_copy; // start of the array
	uint * right_ptr = indices_copy + n*sizeof(uint); // end of the array
	for(i=0; i<n; ++i) D_data[i]<median ? *(left_ptr++)=i : *(right_ptr--)=i;
	assert(right_ptr-left_ptr==sizeof(uint));
	return indices_copy, right_ptr;
	mxFree(indices_copy);
	*/

	if(*left_n+*right_n!=n){
		mexErrMsgTxt("vgg_kdtree2_build_mex: unexpected failure to assign all data while splitting.");
	}

	return median;
}

template <class T>
void vgg_kdtree2_build_recurse(T * D_data,
                               int data_dim,
                               double * medians_data, 
                               uint * axis_dims_data,
                               uint * leaves_data,
                               uint * data_indices,
                               uint n,
                               uint position,
                               uint max_position,
                               uint * I_data,
                               uint I_start)
{
	if (position<max_position) { // create node
		// comparison dimension
		uint axis = vgg_kdtree2_build_axis(D_data,data_dim,data_indices,n);
		axis_dims_data[position-1] = axis;
		// find median and split into left and right based on median
		uint * left_indices = (uint*) mxMalloc(n*sizeof(uint)); // new seems to crash matlab
		uint * right_indices = (uint*) mxMalloc(n*sizeof(uint)); // new seems to crash matlab
		uint left_n = 0; uint right_n = 0;
		medians_data[position-1] = vgg_kdtree2_build_median_split<T>(D_data,data_dim,axis,data_indices,n,left_indices,right_indices,&left_n,&right_n);
		uint left_position = (position<<1);
		uint right_position = (position<<1)+1;
		vgg_kdtree2_build_recurse<T>(D_data,data_dim,medians_data,axis_dims_data,leaves_data,left_indices,left_n,left_position,max_position,I_data,I_start);
		vgg_kdtree2_build_recurse<T>(D_data,data_dim,medians_data,axis_dims_data,leaves_data,right_indices,right_n,right_position,max_position,I_data,I_start+left_n);

		mxFree(left_indices);
		mxFree(right_indices);

	}
	else{
		// create leaf
		leaves_data[position-max_position] = I_start;
		// record indices
		for (uint i=0; i<n; ++i){
			I_data[i+I_start] = data_indices[i];
		}

	}
}

void mexFunction(int nlhs, mxArray * plhs[],
                 int nrhs, mxArray const * prhs[])
{
	// function vgg_kdtree2_build_mex(D,breadth_first_thresholds,axis_dimensions,leaves,indices)
	
	if (nrhs!=5) mexErrMsgTxt("vgg_kdtree2_build_mex: five input variables must be given.");
	if (nlhs!=0) mexErrMsgTxt("vgg_kdtree2_build_mex: no output variables are returned: input variables are [dodgily] updated.");
		
	mxArray const * D_ptr = prhs[0];
	if (mxGetNumberOfDimensions(D_ptr)!=2) mexErrMsgTxt("vgg_kdtree2_build_mex: the data (arg #1) should be a 2D matrix.");
	int m = mxGetM(D_ptr); // dimension of points
	int n = mxGetN(D_ptr); // number of points
	
	mxArray const * breadth_first_thresholds_ptr = prhs[1];
	if (!mxIsDouble(breadth_first_thresholds_ptr)) mexErrMsgTxt("vgg_kdtree2_build_mex: breadth_first_thresholds (arg #2) must be of type double.");
	if (mxGetM(breadth_first_thresholds_ptr)!=1) mexErrMsgTxt("vgg_kdtree2_build_mex: breadth_first_thresholds (arg #2) must be a 1D array.");
	uint num_nodes = mxGetN(breadth_first_thresholds_ptr);

	// check number of leaves is a power of two
	uint num_leaves = num_nodes + 1;
	uint power_of_two = 1;
	int bit_count = 0; while(power_of_two>0 && bit_count<2){ if ( num_leaves&power_of_two) ++bit_count; power_of_two*=2; }

	if (bit_count!=1) mexErrMsgTxt("vgg_kdtree2_build_mex: breadth_first_thresholds (arg #2) must have 2^n-1 elements.");

	mxArray const * axis_dims_ptr = prhs[2];
	if (!mxIsUint32(axis_dims_ptr)) mexErrMsgTxt("vgg_kdtree2_build_mex: axis dimensions (arg #3) must be of class uint32.");
	if (mxGetM(axis_dims_ptr)!=1) mexErrMsgTxt("vgg_kdtree2_build_mex: axis dimensions (arg #3) must be a 1D array.");
	if (mxGetN(axis_dims_ptr)!=num_nodes) mexErrMsgTxt("vgg_kdtree2_mex: axis dimensions (arg #3) must have the same number of elements as breadth_first_thresholds.");

	mxArray const * leaves_ptr = prhs[3];
	if (!mxIsUint32(leaves_ptr)) mexErrMsgTxt("vgg_kdtree2_build_mex: leaves (arg #4) must be of class uint32.");
	if (mxGetM(leaves_ptr)!=1) mexErrMsgTxt("vgg_kdtree2_build_mex: leaves (arg #4) must be a 1D array.");
	if (mxGetN(leaves_ptr)!=num_leaves) mexErrMsgTxt("vgg_kdtree2_mex: leaves (arg #4) must have one more element than breadth_first_thresholds.");

	mxArray const * I_ptr = prhs[4];
	if (!mxIsUint32(I_ptr)) mexErrMsgTxt("vgg_kdtree2_build_mex: indices (arg #5) must be of class uint32.");
	if (mxGetM(I_ptr)!=1) mexErrMsgTxt("vgg_kdtree2_build_mex: indices (arg #5) must be a 1D array.");
	if (mxGetN(I_ptr)!=n) mexErrMsgTxt("vgg_kdtree2_build_mex: indices (arg #5) must have the same number of elements as data.");

	// Get data
	double * breadth_first_thresholds_data = mxGetPr(breadth_first_thresholds_ptr);
	uint * axis_dims_data = (uint*) mxGetPr(axis_dims_ptr);
	uint * leaves_data = (uint*) mxGetPr(leaves_ptr);
	uint * I_data = (uint*) mxGetPr(I_ptr);

	// declare loop variables (for version compatibility)
	uint i;

	// Build tree
	uint first_position = 1;
	uint I_start = 0;
	uint * all_indices = (uint*) mxMalloc(n*sizeof(uint)); 
	for (i=0; i<(uint)n; ++i) { all_indices[i] = i; }

	switch (mxGetClassID(D_ptr)) {
		case (mxINT8_CLASS):
			vgg_kdtree2_build_recurse<char>((char*)mxGetPr(D_ptr),m,breadth_first_thresholds_data,axis_dims_data,leaves_data,all_indices,n,first_position,num_nodes+1,I_data,I_start);
			break;
		case (mxUINT8_CLASS):
			vgg_kdtree2_build_recurse<unsigned char>((unsigned char*)mxGetPr(D_ptr),m,breadth_first_thresholds_data,axis_dims_data,leaves_data,all_indices,n,first_position,num_nodes+1,I_data,I_start);
			break;
		case (mxINT16_CLASS):
			vgg_kdtree2_build_recurse<short>((short*)mxGetPr(D_ptr),m,breadth_first_thresholds_data,axis_dims_data,leaves_data,all_indices,n,first_position,num_nodes+1,I_data,I_start);
			break;
		case (mxUINT16_CLASS):
			vgg_kdtree2_build_recurse<unsigned short>((unsigned short*)mxGetPr(D_ptr),m,breadth_first_thresholds_data,axis_dims_data,leaves_data,all_indices,n,first_position,num_nodes+1,I_data,I_start);
			break;
		case (mxINT32_CLASS):
			vgg_kdtree2_build_recurse<int>((int*)mxGetPr(D_ptr),m,breadth_first_thresholds_data,axis_dims_data,leaves_data,all_indices,n,first_position,num_nodes+1,I_data,I_start);
			break;
		case (mxUINT32_CLASS):
			vgg_kdtree2_build_recurse<uint>((uint*)mxGetPr(D_ptr),m,breadth_first_thresholds_data,axis_dims_data,leaves_data,all_indices,n,first_position,num_nodes+1,I_data,I_start);
			break;
		case (mxSINGLE_CLASS):
			vgg_kdtree2_build_recurse<float>((float*)mxGetPr(D_ptr),m,breadth_first_thresholds_data,axis_dims_data,leaves_data,all_indices,n,first_position,num_nodes+1,I_data,I_start);
			break;
		case (mxDOUBLE_CLASS):
			vgg_kdtree2_build_recurse<double>((double*)mxGetPr(D_ptr),m,breadth_first_thresholds_data,axis_dims_data,leaves_data,all_indices,n,first_position,num_nodes+1,I_data,I_start);
			break;
		default:
			mexErrMsgTxt("vgg_kdtree2_build_mex: unexpected failure to sort data due to incompatible data type.");
	}

	// convert indices to MATLAB counting
	for (i=0; i<(uint)n; ++i) { ++I_data[i]; }

	mxFree(all_indices);
	
	// All done
}



