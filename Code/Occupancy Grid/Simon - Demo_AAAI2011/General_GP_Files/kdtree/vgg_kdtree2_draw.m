function vgg_kdtree2_draw(kd, D, bbox, position)

% vgg_kdtree2_draw(kd, D)
%
% Draw 2d projection of k-d tree generated using vgg_kdtree2_build.

% Author: Andrew Fitzgibbon <awf@robots.ox.ac.uk>
% Date: 15 May 01

% Revised: Aeron Buchanan <amb@robots.ox.ac.uk>
% Date: 15th April 2005
% revised: 17th January 2006

DRAW_POINTS = 0;
MAX_LEVEL = 10;

if nargin < 4
  position = 1; % breadth first position in tree (start with root node)
end

if nargin < 3
	% generate bounding boxes
	bboxlo = min(D,[],2); % min value in each dimension
	bboxhi = max(D,[],2); % max value in each dimension
	bbox = [bboxlo bboxhi]';
end

if position == 1
  w = bbox(2,1) - bbox(1,1);
  h = bbox(2,2) - bbox(1,2);
  lo = bbox(1,1:2);

  axis([-.1 1.1 -.1 1.1] .* [ w w h h ] + lo([1 1 2 2]));
  
  hold on
end

style = '-';

if position>min(length(kd.breadth_first_thresholds),2^MAX_LEVEL);
  if DRAW_POINTS
    h = scatter(D(1:2,:),'.');
    set(h, 'markersize', 0.1);
  end
else
 
  % BBOX is [x0 y0 z0 ....; x1 y1 z1 ....]
  lo = bbox(1,:);
  hi = bbox(2,:);
  
	axis_dim = kd.axis_dimensions(position);
	
  if axis_dim <= 2
    line_start= lo(1:2);
    line_end = hi(1:2);
    line_start(axis_dim) = kd.breadth_first_thresholds(position);
    line_end(axis_dim) = kd.breadth_first_thresholds(position);
    
    draw_line(line_start, line_end, style);
  end
  
  bbox_lt_lo = lo;
  bbox_lt_hi = hi;
  bbox_lt_hi(axis_dim) = kd.breadth_first_thresholds(position);
  
  bbox_gt_lo = lo;
  bbox_gt_hi = hi;
  bbox_gt_lo(axis_dim) = kd.breadth_first_thresholds(position);
  
  vgg_kdtree2_draw(kd, D, [bbox_lt_lo; bbox_lt_hi], position*2);
  vgg_kdtree2_draw(kd, D, [bbox_gt_lo; bbox_gt_hi], position*2+1);
  
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function h = draw_line(p0, p1, style)
% DRAW_LINE     Draw lines from p0 to p1


% Author: Andrew Fitzgibbon <awf@robots.ox.ac.uk>
% Date: 23 Nov 96

if size(p0, 2) == 4
  x0 = p0(:,1);
  y0 = p0(:,2);
  
  x1 = p0(:,3);
  y1 = p0(:,4);
else
  x0 = p0(:,1);
  y0 = p0(:,2);

  x1 = p1(:,1);
  y1 = p1(:,2);
end

if nargin < 3
  H = plot([x0 x1]', [y0 y1]');
else
  H = plot([x0 x1]', [y0 y1]', style);
end

if nargout > 0
  h = H;
end
