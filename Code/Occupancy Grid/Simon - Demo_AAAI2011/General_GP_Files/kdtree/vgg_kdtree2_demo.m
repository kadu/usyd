function vgg_kdtree2_demo(N)

% vgg_kdtree2_demo(N)
%
% Demonstrate k-d tree functions. N is number of points [1000].
%
% See also
%   vgg_kdtree2_build
%   vgg_kdtree2_closest_point
%   vgg_kdtree2_N_closest_points
%   vgg_kdtree2_ball_search

% Author: Andrew Fitzgibbon <awf@robots.ox.ac.uk>
% Date: 15 May 01

% Revision: Aeron Buchanan <amb@robots.ox.ac.uk>
% Date: 15th April 05
% revised: 2nd December 2005

LARGE_NUMBER_OF_POINTS = 10000;

if nargin < 1
	N = 1000;
end

switch 2
	case 1
		X = rand(2,N);
	case 2
		t = rand(1,N) * 2 * pi;
		X = [cos(2*t); sin(t)]/3 + .5;
		X = X + randn(size(X)) * .1;
	case 3
		[m,n] = abm_squarish_factors(N);
		N = m*n;
		X = zeros(2,N);
		for i=0:m-1
			for j=0:n-1
				X(:,i*n+j+1) = [i/(m-1);j/(n-1)];
			end
		end
end

MAX_VALUE = 200;
X = uint8(X*MAX_VALUE);
disp(['m-file: data is of class ''' class(X) '''']) % DEBUG

disp building
MAX_PER_LEAF = 10;
tic
[kd,Xmapped] = vgg_kdtree2_build(X, MAX_PER_LEAF);
Xindices = kd.indices;
kd.indices = uint32(1:N);
%%% DEBUG
kd
disp(['m-file: Xmapped is of class ''' class(Xmapped) ''''])
%%% DEBUG END
toc

fig_h = gcf;
hold off;
if size(X,2)<LARGE_NUMBER_OF_POINTS
	plot(X(1,:), X(2,:), 'r.')
else
	points_to_show = abm_random_permutation(LARGE_NUMBER_OF_POINTS,size(X,2));
	plot(X(1,points_to_show), X(2,points_to_show), 'r.');
end
hold on
middle_point = 0.5*MAX_VALUE;
h2 = plot(middle_point,middle_point,'b+', 'markersize', 10);
h = plot(middle_point,middle_point,'g.', 'markersize', .1);
h1 = plot(middle_point,middle_point,'ko', 'markersize', 10, 'linewidth', 2);
h3 = plot(middle_point,middle_point,'rp', 'markersize', 15);
h4 = plot(middle_point,middle_point,'mo', 'markersize', 7);
vgg_kdtree2_draw(kd, X, [0 0; 1 1]*MAX_VALUE);
axis square
set(fig_h,'doublebuffer','on')

drawnow

title(['VGG\_KDTREE2\_DEMO (' num2str(N) ' points)'])
legend('data','query point','ballsearch','closest point','ballsearch closest','ballsearch furthest','kdtree partition',-1)
xlabel('double click to exit')

set(h, 'erasemode', 'xor');
set(h1, 'erasemode', 'xor');

nruns = 10;
fprintf('benchmark:');
while 1
	tic;
	for k=1:nruns
		[index,dists] = vgg_kdtree2_ballsearch(kd, Xmapped, rand(2,1)*MAX_VALUE, 0.1*MAX_VALUE);
	end
	t = toc;
	fprintf('[%g]', t);
	if t > 1
		fprintf(' per ballsearch query = %f ms\n', t/nruns*1000);
		break
	end
	if t==0, t = 1e-6; end
	nruns=nruns*min(1/t,10);
end

% return

%vgg_event_wait clear
while 1
	%[x, y, button, type] = vgg_event_wait;

	% button:
	% 0 not down,
	% 1 2 3 left-to-right,
	% 4 doubleclick

	% event type:
	% -1 button up
	%  0 mouse motion
	%  1 button down
	%  2 keypress

% 	if button == 4
% 		disp('demo_event_wait: returning');
% 		close(fig_h)
% 		return
% 	end

	if type == 0
		% motion
		if button ~= 0
			[indices,dists] = vgg_kdtree2_ballsearch(kd, Xmapped, [x y]', .1*MAX_VALUE);
			if length(indices)>0
				[unused,iii] = sort(dists);
				closest_index = indices(iii(1));
				furthest_index = indices(iii(end));
			else
				closest_index = [];
				furthest_index = [];
			end

			cutoff = 1000;
			if length(indices) > cutoff
				% Don't display more than 1000 as it will be too slow.
				indices = indices(abm_random_permutation(1000,length(indices)));
			end
			set(h, 'xdata', X(1,Xindices(indices)), 'ydata', X(2,Xindices(indices)));

			index = vgg_kdtree2_closest_point(kd, Xmapped, [x y]');
			set(h1, 'xdata', X(1,Xindices(index)), 'ydata', X(2,Xindices(index)));

			set(h2, 'xdata', x, 'ydata', y);


			set(h3, 'xdata', X(1,Xindices(closest_index)), 'ydata', X(2,Xindices(closest_index)));
			set(h4, 'xdata', X(1,Xindices(furthest_index)), 'ydata', X(2,Xindices(furthest_index)));
		end
	elseif type == 1
		% down
	elseif type == -1
		% up
	elseif type == 2
		% key
		text(x, y, setstr(button));
	else
		type
	end

end

