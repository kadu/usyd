function vgg_kdtree2_closest_point_demo(N,MAX_LEAF_COUNT)

% vgg_kdtree2_closest_point_demo(N,MAX_LEAF_COUNT)
%
% Demonstrate k-d tree functions. N is number of points [1000].

% Author: Andrew Fitzgibbon <awf@robots.ox.ac.uk>
% Date: 15 May 01

% Revision: Aeron Buchanan <amb@robots.ox.ac.uk>
% Date: 15th April 05
% revised: 2nd December 2005

LARGE_NUMBER_OF_POINTS = 10000;

if nargin < 2, MAX_LEAF_COUNT = 10;	
	if nargin < 1, N = 1000;
	end
end

switch 1
case 1
  X = rand(2,N);
case 2
  t = rand(1,N) * 2 * pi;
  X = [cos(2*t); sin(t)]/3 + .5;
  X = X + randn(size(X)) * .1;
case 3
	[m,n] = abm_squarish_factors(N);
	N = m*n;
	X = zeros(2,N);
	for i=0:m-1
		for j=0:n-1
			X(:,i*n+j+1) = [i/(m-1);j/(n-1)];
		end
	end
end

DATA_MAX = 250;
X = uint8(X*DATA_MAX);

disp building
vgg_tic
[kd,Xmapped] = vgg_kdtree2_build(X,MAX_LEAF_COUNT);
% DEBUG
kd
disp(['m-file: Xmapped is of class ''' class(Xmapped) '''']);
% DEBUG END
indices = kd.indices;
kd.indices = uint32(1:N);
vgg_toc

fig_h = gcf;

hold off;
if size(X,2)<LARGE_NUMBER_OF_POINTS
  plot(X(1,:), X(2,:), '.', 'color', [0 .5 0])
else
	% points_to_show = abm_random_permutation(LARGE_NUMBER_OF_POINTS,size(X,2));
  plot(X(1,:), X(2,:), '.', 'color', [0 .5 0],'markersize',.1);
end
hold on
vgg_kdtree2_draw(kd, X, [0 0; 1 1]*DATA_MAX);
axis square

h = plot(.5,.5,'ko');
set(h, 'erasemode', 'xor', 'markersize', 10, 'linewidth', 2);

while 0
	x = ginput(1)
	y = x(2);
	x = x(1);
	
  index = vgg_kdtree2_closest_point(kd, Xmapped, [x; y]);
	% disp(['returned index = ' num2str(index)]); if index==0, index = 1; end % DEBUG
	pts = X(:,indices(index));
  set(h, 'xdata', pts(1,:), 'ydata', pts(2,:));
end

while 1
  [x, y, button, type] = vgg_event_wait;

  % button:
  % 0 not down,
  % 1 2 3 left-to-right,
  % 4 doubleclick

  % event type:
  % -1 button up
  %  0 mouse motion
  %  1 button down
  %  2 keypress
  
  if button == 4
    disp('demo_event_wait: returning');
		close(fig_h)
    return
  end

  if type == 0
    % motion
    if button ~= 0
      index = vgg_kdtree2_closest_point(kd, Xmapped, int32(round([x; y])) );
			% disp(['returned index = ' num2str(index)]); if index==0, index = 1; end % DEBUG
		  set(h, 'xdata', X(1,indices(index)), 'ydata', X(2,indices(index)));
    end
  elseif type == 1
    % down
  elseif type == -1
    % up
  elseif type == 2
    % key
    text(x, y, setstr(button));
  else
		index = vgg_kdtree2_closest_point(kd, X, [x; y]);
		% disp(['returned index' num2str(index)]); if index==0, index = 1; end % DEBUG
		if index~=0
			pts = X(:,index);
			set(h, 'xdata', pts(1,:), 'ydata', pts(2,:));
		end
  end
  
end

