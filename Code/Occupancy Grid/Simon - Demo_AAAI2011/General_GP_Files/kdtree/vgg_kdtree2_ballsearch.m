function [indices,dists] = vgg_kdtree2_ballsearch(kd,D,q,max_d)

% [indices,dists] = vgg_kdtree2_ballsearch(kd,D,q,max_d)
%
% kd is the output of vgg_kdtree2_build.
% D is columnwise data.
% q is a query point of the same dimension as the data in D.
% max_d is the distance within which points have their indices returned.
%
% indices is the index list into D of the closest points to q.
% dists are the corresponding distances.
%
% See also
%   vgg_kdtree2_build
%   vgg_kdtree2_closest_point
%   vgg_kdtree2_N_closest_points
%   vgg_kdtree2_demo

% author: Aeron Buchanan <amb@robots.ox.ac.uk>
% date: 15th April 2005
% revised: 2nd December 2005

if nargout==1
	indices = vgg_kdtree2_ballsearch_mex(kd,D,q,max_d);	
else
	[indices,dists] = vgg_kdtree2_ballsearch_mex(kd,D,q,max_d);
end