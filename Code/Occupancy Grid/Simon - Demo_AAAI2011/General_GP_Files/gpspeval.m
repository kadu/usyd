% Copyright The University of Sydney, 2001 - 2008.  ABN 15 211 513 464
% herafter 'The University'.
%
% The copyright of the computer program(s) herein is the property of
% The University of Sydney, Australia.  The program(s) may be used
% and/or copied only with the written permission of The University of
% Sydney, or in accordance with the terms and conditions stipulated
% in the contract under which the program(s) have been supplied.
% 
% Disclaimer:
% The Universitiy makes no representation or warranty
% that these materials, including any software, are free from errors;
% about the quality or performance of these materials;
% that these materials are fit for any particular purpose.
% These materials are made available on the strict basis that The University
% and its employees or agents have no liability for any direct or indirect
% loss or damage (including for negligence) suffered by any person as a
%consequence of the use of this  material.
%
% Evaluate a Gaussian Process 
% assuming the covariance function is sparse.
% C. Rasmussen and C. Williams)
%
%INPUTS
% X - D x N inputs
% y - 1 x N outputs (targets)
% kfun - covariance function
% kpar - covariance parameters (hyperparameters)
% K - N x N computed covariance 
% noise - scalar noise level
% x - D x M test inputs
% invK - N x N inverted K if available
% 
%OUTPUTS
% mf - predictive mean
% vf - predictive variance
% lml - log marginal likelihood
%
% Fabio Tozeto Ramos 03/11/08
% [lml,mf,vf] = gpspeval(X,y,kfun,kpar,K,noise,x,invK)

function [lml,mf,vf] = gpspeval(X,y,kfun,kpar,K,noise,x,invK)
[D,N] = size(X); %number of inputs
Ns = size(x,2);
mf = zeros(Ns,1);
vf = zeros(Ns,1);
if isempty(K)
    K = feval(kfun,X,X,kpar);
end

A = K+(noise^2)*speye(N);
p = symrcm(A);
L = chol(A(p,p))';
alpha = L'\(L\y(p)');
%alpha = pcg(A,y');        
for i=1:Ns
    ks = feval(kfun,X(:,p),x(:,i),kpar);

    mf(i,1) = ks'*alpha;
    v = L\ks;
    vf(i,1) = feval(kfun,x(:,i),x(:,i),kpar)-sum(v.*v)'+noise^2;

end

%Compute the log marginal likelihood
lml = -0.5*y*alpha-sum(log(diag(L)))-0.5*N*log(2*pi);
% for i=1:Ns
%     % K(X,x)
%     ks = feval(kfun,X,x(:,i),kpar);
%     nnzks = nnz(ks);
%     innz = find(ks~=0);
%     invK = zeros(N,nnzks);
%     for j=1:nnzks
%         I = zeros(N,1);
%         I(innz(j),1) = 1;
%         %invK(:,j) = A\I;
%         invK(:,j) = pcg(A,I);
%     end
%     %Compute the mean
%     %alpha = invK'*y';
%     mf(i,1) = ks(innz)'*alpha;
% 
%     %Compute the variance
%     %v = L\ks;
%     %vf(i) = feval(kfun,x,x,kpar)-sum(v.*v)'+noise^2;
%     vf(i,1) = feval(kfun,x(:,i),x(:,i),kpar)-ks'*invK*ks(innz)+noise^2;
% end
%lml = 0;
% if nargin<8
%     %Cholesky factorisation for covariance inversion
%     if issparse(K)
%         A = K+(noise^2)*speye(N);
%         p = symamd(A);
%         L(p,:) = chol(A(p,p))';
%         alpha = A\y';
%     else
%         L = chol(K+(noise^2)*eye(N))'; 
%         alpha = solve_chol(L',y');
%     end
%     
% else
%     %If the inverted covariance is given use it to 
%     %compute alpha
%     alpha = invK*y;
% end



