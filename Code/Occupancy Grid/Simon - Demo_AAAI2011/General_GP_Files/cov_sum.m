% Copyright The University of Sydney, 2001 - 2008.  ABN 15 211 513 464
% herafter 'The University'.
%
% The copyright of the computer program(s) herein is the property of
% The University of Sydney, Australia.  The program(s) may be used
% and/or copied only with the written permission of The University of
% Sydney, or in accordance with the terms and conditions stipulated
% in the contract under which the program(s) have been supplied.
% 
% Disclaimer:
% The Universitiy makes no representation or warranty
% that these materials, including any software, are free from errors;
% about the quality or performance of these materials;
% that these materials are fit for any particular purpose.
% These materials are made available on the strict basis that The University
% and its employees or agents have no liability for any direct or indirect
% loss or damage (including for negligence) suffered by any person as a
%consequence of the use of this  material.
%
% Sum covariance functions 
%
%INPUTS
% X1 - D x N input points 1
% X2 - D x M input points 2
% cov_funs - cells with pointers to covariance functions
% npar - number of parameters per covariance function
% par - parameters
%OUTPUT
% K - covariance matrix after summing covariance functions
%
% Fabio Tozeto Ramos 06/05/08
% K = cov_sum(X1,X2,cov_funs,npar,par)

function K = cov_sum(X1,X2,cov_funs,npar,par)
[D,N1] = size(X1); %number of points in X1
N2 = size(X2,2); %number of points in X2
nfuns = length(cov_funs);

if size(X1,1)~=size(X2,1)
    error('Dimensionality of X1 and X2 must be the same');
end

K = zeros(N1,N2);
p1 = 1;
for i=1:nfuns 
    p2 = p1 + npar(i)-1;
    %Eval each covariance function
    K = K + feval(cov_funs{i},X1,X2,par(p1:p2));
    p1 = p2 + 1;
end


