% Copyright The University of Sydney, 2001 - 2008.  ABN 15 211 513 464
% herafter 'The University'.
%
% The copyright of the computer program(s) herein is the property of
% The University of Sydney, Australia.  The program(s) may be used
% and/or copied only with the written permission of The University of
% Sydney, or in accordance with the terms and conditions stipulated
% in the contract under which the program(s) have been supplied.
% 
% Disclaimer:
% The Universitiy makes no representation or warranty
% that these materials, including any software, are free from errors;
% about the quality or performance of these materials;
% that these materials are fit for any particular purpose.
% These materials are made available on the strict basis that The University
% and its employees or agents have no liability for any direct or indirect
% loss or damage (including for negligence) suffered by any person as a
%consequence of the use of this  material.
%
% Demo for learning GP hyperparameters
clear global;
global X y;
clear; close all
%Use the same data as Rasmussen in Figure 2.5
%X = [-2.1775;-0.9235;0.7502;-5.8868;-2.7995;4.2504;2.4582;6.1426;...
%   -4.0911;-6.3481;1.0004;-4.7591;0.4715;4.8933;4.3248;-3.7461;...
%   -7.3005;5.8177;2.3851;-6.3772]';
%y = [1.4121;1.6936;-0.7444;0.2493;0.3978;-1.2755;-2.221;-0.8452;...
%   -1.2232;0.0105;-1.0258;-0.8207;-0.1462;-1.5637;-1.098;-1.1721;...
%   -1.7554;-1.0712;-2.6937;-0.0329]';
X = 10+[-7.3005;-6.3772;-6.3481;-5.8868;-4.7591;-4.0911;-3.7461;-2.7995;...
    -2.1775;-0.9235;0.4715;0.7502;1.0004;2.3851;2.4582;4.2504;4.3248;...
    4.8933;5.8177;6.1426]';
y = 1*[-ones(1,10) ones(1,10)]+0.0001*randn(1,20);
xstar = 10 + linspace(-7.5, 7.5, 201);
%xstar = linspace(-7.5, 7.5, 201);

%X=-2:0.1:7;
%y=exp(-(X).^2)-3*exp(-(X-4).^2)+0.1*randn(1,length(X));
%xstar = linspace(-3, 10, 251);



wf = @nntanh;
wdf = @dnntanh;
params0{1} = 0.01*rand(1,3);
params0{1} = [1   90   2 ];
params0{2} = 1*rand(1,3);
noise = rand;
parmap{1} = [1 2 3];
parmap{2} = 1+[3 4 5]; %6 7 8];% 9 10 11];
options = zeros(1,18);
options(1)=1; options(9)=1; options(14)=40;


tic
[params] = wgplearn(X,y,@cov_nn2,[],wf,wdf,params0,parmap,noise);
disp(params);
toc
noise  = params(end);
params = utranspar(params,parmap);
%Evaluates
tic
[lml,mf,vf] = wgpeval(X,y,@cov_nn2,params,[],noise,xstar);
toc

mf2 = zeros(length(mf),1);
mf3 = zeros(length(mf),1);
vf1 = zeros(length(vf),1);
vf2 = zeros(length(vf),1);
x0 = 1;
for i = 1:size(xstar,2)
    if i == 1
        mf2(i) = invnewton(wf,wdf,params{2},mf(i),x0);
        vf1(i) = invnewton(wf,wdf,params{2},mf(i)+2*sqrt(vf(i)),x0);
        vf2(i) = invnewton(wf,wdf,params{2},mf(i)-2*sqrt(vf(i)),x0);
 
    else
        mf2(i) = invnewton(wf,wdf,params{2},mf(i),mf2(i-1));
        vf1(i) = invnewton(wf,wdf,params{2},mf(i)+2*sqrt(vf(i)),vf2(i-1));
        vf2(i) = invnewton(wf,wdf,params{2},mf(i)-2*sqrt(vf(i)),x0); 
    end
end

%Mean from Gauss-Hermite quadrature
for i = 1:size(xstar,2)
    mf3(i) = meanwgp(wf,wdf,params{2},mf(i),vf(i));
end

sigmoid = inline('(1+erf(x/sqrt(2)))/2');
mf4 = sigmoid(mf2);
%S2 = vf;
ty = nntanh(params{2},y);

%f = [mf+2*sqrt(S2);flipdim(mf-2*sqrt(S2),1)];
f = [vf1;flipdim(vf2,1)];
figure(1)
hold on
fill([xstar'; flipdim(xstar',1)], f, [7 7 7]/8, 'EdgeColor', [7 7 7]/8);
figure(1);title('Warped Gaussian Process');
figure(1);plot(xstar,mf2,'b-','LineWidth',2);
figure(1);plot(xstar,mf3,'r-','LineWidth',2);
figure(1);plot(xstar,mf4,'g-','LineWidth',2);
figure(1);plot(X, y, 'k+', 'MarkerSize', 17);
plot(X, ty, 'go', 'MarkerSize', 5);
hold off


%Normal GP
%Check the gradients


params0 = [1 1 1 1];
params0 = rand(1,4);
options = zeros(1,18);
options(1)=1; options(9)=1; options(14)=40;
%Xt = X;
%yt = y;
%X = X(:,1:4:20);
%y = y(1:4:20);

tic
[params] = gplearn(@cov_nn2,@grad_nn2,params0(1:end-1),params0(end),options);
%[params] = gpkdtreelearn(@cov_nn2,@grad_nn2,params0(1:end-1),params0(end),options);

%params = [-13.0941    0.5656    1.7611    10.8639    4.1421    0.9710   13.6508    3.5630    1.2947    1.9408  0.1449];
disp(params);
toc
%X = Xt;
%y = yt;
%Evaluates
tic
[lml,mf,vf] = gpeval(X,y,@cov_nn2,params(1:end-1),[],params(end),xstar);
toc
%[lml,mf,vf] = gpkdtreeeval(X,y,@cov_sqexpfa,params(1:2),params(3),xstar,50);
%[lml,mf,vf] = gpjointeval(X,y,@cov_sqexpfa,params(1:2),params(3),xstar);


%disp('  S2 = S2 - exp(2*loghyper(3));')
S2 = vf - params(end)^2;

f = [mf+2*sqrt(S2);flipdim(mf-2*sqrt(S2),1)];
figure(2)
fill([xstar'; flipdim(xstar',1)], f, [7 7 7]/8, 'EdgeColor', [7 7 7]/8);

hold on
figure(2);plot(xstar,mf,'b-','LineWidth',2);
%plot(xstar,mf2,'b-','LineWidth',2);
figure(2);plot(X, y, 'k+', 'MarkerSize', 17);
figure(2);title('Normal GP');
hold off
