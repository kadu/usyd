% Copyright The University of Sydney, 2001 - 2008.  ABN 15 211 513 464
% herafter 'The University'.
%
% The copyright of the computer program(s) herein is the property of
% The University of Sydney, Australia.  The program(s) may be used
% and/or copied only with the written permission of The University of
% Sydney, or in accordance with the terms and conditions stipulated
% in the contract under which the program(s) have been supplied.
% 
% Disclaimer:
% The Universitiy makes no representation or warranty
% that these materials, including any software, are free from errors;
% about the quality or performance of these materials;
% that these materials are fit for any particular purpose.
% These materials are made available on the strict basis that The University
% and its employees or agents have no liability for any direct or indirect
% loss or damage (including for negligence) suffered by any person as a
%consequence of the use of this  material.
%
% Compute the Neural Network Matrix covariance function as it
% appears in Rasmussen's Book
%X = [x1,x2,x3...;y1,y2,y3...]
%Need to augment it.
%INPUTS
% X1 - D x N input points 1
% X2 - D x M input points 2
% par - cell with the following fields 
%   par(1:end-1) - characteristic length-scales
%   par(end) - sigma f square
%OUTPUT
% K - covariance function
%
% Simon O'Callaghan 19/03/2008
% K = cov_nn2(X1,X2,par)

function K = cov_nn2(X1,X2,par)



N1 = size(X1,2); %number of points in X1
N2 = size(X2,2); %number of points in X2
D = size(X1,1);

if size(X1,1)~=size(X2,1)
    error('Dimensionality of X1 and X2 must be the same');
end



Sig = reshape(par(1:end-2),[D 1]).^(-2);

X1X2 = 2*X1'*diag(Sig)*X2 + par(end-1);
XX1 = sum(X1.*Sig(:,ones(1,N1)).*X1,1);
XX2 = sum(X2.*Sig(:,ones(1,N2)).*X2,1);

z = X1X2./sqrt((1+par(end-1)+2*XX1)'*(1+par(end-1)+2*XX2));
%z = X1X2./sqrt((1+2*XX1)'*(1+2*XX2));


K = (par(end).^2)*asin(z); 
