% Copyright The University of Sydney, 2001 - 2008.  ABN 15 211 513 464
% herafter 'The University'.
%
% The copyright of the computer program(s) herein is the property of
% The University of Sydney, Australia.  The program(s) may be used
% and/or copied only with the written permission of The University of
% Sydney, or in accordance with the terms and conditions stipulated
% in the contract under which the program(s) have been supplied.
% 
% Disclaimer:
% The Universitiy makes no representation or warranty
% that these materials, including any software, are free from errors;
% about the quality or performance of these materials;
% that these materials are fit for any particular purpose.
% These materials are made available on the strict basis that The University
% and its employees or agents have no liability for any direct or indirect
% loss or damage (including for negligence) suffered by any person as a
%consequence of the use of this  material.
%
% Evaluate a Gaussian Process (see page 19 of
% "Gaussian Processes for Machine Learning" by
% C. Rasmussen and C. Williams)
%
%INPUTS
% X - D x N inputs
% y - 1 x N outputs (targets)
% kfun - covariance function
% kpar - covariance parameters (hyperparameters)
% noise - scalar noise level
% x - D x M test inputs
% 
%OUTPUTS
% mf - predictive mean
% vf - predictive variance
% lml - log marginal likelihood
%
% Fabio Tozeto Ramos 04/06/07
% [lml,mf,vf] = gpjointeval(X,y,kfun,kpar,K,noise,x)

function [lml,mf,vf] = gpjointeval(X,y,kfun,kpar,noise,x)
N = size(X,2); %number of inputs
Nt = size(x,2); %number of test points
niter = 50;

%Initial solution as normal GP
K = feval(kfun,X,X,kpar);

%Cholesky factorisation for covariance inversion
L = chol(K+(noise^2)*eye(N))'; 
alpha = L'\(L\y');


% K(X,x)
ks = feval(kfun,X,x,kpar);

%Compute the mean
mf = ks'*alpha;

%Compute the variance
v = L\ks;
vf = diag(feval(kfun,x,x,kpar))-sum(v.*v)'+noise^2;
%vf = feval(kfun,x,x,kpar) - v'*v + noise^2;

%Joint Evaluation
K = zeros(N+Nt-1,N+Nt-1,Nt);
L = zeros(N+Nt-1,N+Nt-1,Nt);
ks = zeros(N+Nt-1,Nt);

if niter~=0
indperm = randperm(Nt);
x = x(:,indperm);
%Compute the covariance for each test point
for i=1:Nt
    ind = setxor(1:Nt,i);
    K(:,:,i) = feval(kfun,[X x(:,ind)],[X x(:,ind)],kpar);
    L(:,:,i) = chol(K(:,:,i)+diag([noise^2*ones(1,N) vf(ind)']))';    
    ks(:,i) = feval(kfun,[X x(:,ind)],x(:,i),kpar);
end
%Iterates
for i=1:niter
    for j=1:Nt
        ind = setxor(1:Nt,j);
        alpha = L(:,:,j)'\(L(:,:,j)\[y mf(ind)']');
        mf(j) = ks(:,j)'*alpha;
        v = L(:,:,j)\ks(:,j);
        vf(j) = diag(feval(kfun,x(:,j),x(:,j),kpar))-sum(v.*v)'+noise^2;
    end
    %test(indperm) = mf;
    %figure;imagesc(reshape(test,[10,4]));
end
mf(indperm) = mf;
vf(indperm) = vf;
end
lml = 0;
%Compute the log marginal likelihood
%lml = -0.5*y*alpha-sum(log(diag(L)))-0.5*N*log(2*pi);