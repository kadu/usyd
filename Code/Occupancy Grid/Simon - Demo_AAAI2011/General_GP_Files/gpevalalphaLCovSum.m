% Copyright The University of Sydney, 2001 - 2008.  ABN 15 211 513 464
% herafter 'The University'.
%
% The copyright of the computer program(s) herein is the property of
% The University of Sydney, Australia.  The program(s) may be used
% and/or copied only with the written permission of The University of
% Sydney, or in accordance with the terms and conditions stipulated
% in the contract under which the program(s) have been supplied.
% 
% Disclaimer:
% The Universitiy makes no representation or warranty
% that these materials, including any software, are free from errors;
% about the quality or performance of these materials;
% that these materials are fit for any particular purpose.
% These materials are made available on the strict basis that The University
% and its employees or agents have no liability for any direct or indirect
% loss or damage (including for negligence) suffered by any person as a
%consequence of the use of this  material.
%
% Evaluate a Gaussian Process (see page 19 of
% "Gaussian Processes for Machine Learning" by
% C. Rasmussen and C. Williams)
%
%INPUTS
% X - D x N inputs
% y - 1 x N outputs (targets)
% kfun - covariance function
% kpar - covariance parameters (hyperparameters)
% K - N x N computed covariance 
% noise - scalar noise level
% x - D x M test inputs
% invK - N x N inverted K if available
% 
%OUTPUTS
% mf - predictive mean
% vf - predictive variance
% lml - log marginal likelihood
%
% Fabio Tozeto Ramos 04/06/07
% [lml,mf,vf] = gpeval(X,y,kfun,kpar,K,noise,x,invK)

function [mf,vf] = gpevalalphaLCovSum(X,y,kfuns,npar,kpar,noise,x,alpha,L)
[d,N] = size(X); %number of inputs
[nx]  = size(x,2); %number of testing points 
myflag = false;
% check is mean is zero
% my = mean(y);
my=0
if abs(my)>1e-3
    y      = y - my;
    myflag = true;
end



if ~isempty(x)    %Evaluate the marginal log-likelihood only
    % K(X,x)
%     ks          = feval(kfun,X,x,kpar);
    ks = cov_sum(X,x,kfuns,npar,kpar);

    %Compute the mean
    mf = ks'*alpha;

    %Compute the variance
    opts.TRANSA = true;
    opts.UT     = true;
    v           = linsolve(L,ks,opts);
    xc          = mat2cell(x,d,ones(1,nx));
    nfuns = length(kfuns);
    p1 = 1;
    a = zeros(size(xc'));
    for i=1:nfuns 
        p2 = p1 + npar(i)-1;
        %Eval each covariance function
        cellsolution =  cellfun(kfuns{i},xc,xc,repmat({kpar(p1:p2)},1,nx))';
        a = a +cellsolution;
        p1 = p2 + 1;
    end
    
    
%     a           = cellfun(kfun, xc, xc, repmat({kpar},1,nx))';
    vf          = a - sum(v.*v)' + noise^2;
    %vf = feval(kfun,x,x,kpar) - v'*v + noise^2;
else
    mf = [];
    vf = [];
end

if myflag
    mf = mf + my;
end
% lml = -0.5*y*alpha-sum(log(diag(L)))-0.5*N*log(2*pi);
