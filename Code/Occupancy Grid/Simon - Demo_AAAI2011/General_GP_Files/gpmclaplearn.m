% Train a GP classification model with scaled conjugade gradient

function [params, options] = gpmclaplearn(kfun,kgfun,kpar0,kmap)

params = cell2mat(kpar0);
%[params, options] = scg(@(params) gplmleval(params, kfun), ...
%    params,options,@(params) gpgradeval(params, kgfun, kfun));
%[params, options] = quasinew(@(params) gplmleval(params, kfun), ...
%    params,options,@(params) gpgradeval(params, kgfun, kfun));
 
%Matlab optimisation toolbox 
opts = optimset('LargeScale','off','MaxIter',500,'Diagnostics', 'on',...
'GradObj', 'off', 'Display', 'iter','TolFun',  1e-10, 'MaxFunEvals', 5000);


%KNITRO options
opts_knitro = optimset('Display', 'iter','MaxIter',20, 'GradObj','off', 'TolFun', 1e-20,...
    'MaxFunEvals', 5000,'Algorithm','active-set');

%lb=0.001*ones(1,size(X,1));
%ub=inf*ones(1,size(X,1));
 
%Simulated Annealing
%[params, fval] = anneal(@(params) gpmclaplmlgrad(params, kfun, kgfun), params);
%tic;[params, options]=fminunc(@(params) gpmclaplmlgrad(params, kfun, kgfun), params,opts);toc;

[params, options]=ktrlink(@(params) gpmclaplmlgrad(params, kfun, kgfun, kmap), params,...
    [],[],[],[],[],[],[],opts_knitro);



