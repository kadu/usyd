

function [costhet sinthet xstart ystart r]= ...
    determLineParameters(Start, End)

%   Start = [1 0.5;4 0.25]
%   End = [-4 -3; 10 -2]
%   

  r = sqrt((End(1,:)-Start(1,:)).^2+(End(2,:)-Start(2,:)).^2)';
  
  costhet = (End(1,:)-Start(1,:))'./r;
  sinthet = (End(2,:)-Start(2,:))'./r;
  
  xstart = Start(1,:)';
  ystart = Start(2,:)';

end

