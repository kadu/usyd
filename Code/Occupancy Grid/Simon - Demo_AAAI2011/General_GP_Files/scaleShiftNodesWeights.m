
function [NodesScaleShift Weights] = ... 
    scaleShiftNodesWeights(r, Nodes, Weights)

%  xmin = min([Start(1,:);End(1,:)],1)
%  xmax = max([Start(1,:);End(1,:)],1)
%  ymin = min([Start(2,:);End(2,:)],1)
%  ymax = max([Start(2,:);End(2,:)],1)
 
 num_nodes = size(Nodes,2);
 num_intervals = size(r,1);
%     xnodes = Nodes(1,:);
%     ynodes = Nodes(2,:);
    
    xnodes = Nodes(ones(1,num_intervals),:);
%     ynodes = ynodes(ones(1,num_intervals),:);

    %Scale nodes
    scalefactor = (r'/2)';
    scalefactor= scalefactor(:,ones(1,num_nodes));
    
%     scalefactory = (( ymax - ymin)/2)';
%     scalefactory = scalefactory(:,ones(1,num_nodes),:);

    xnodes = xnodes.*scalefactor;
%     ynodes = ynodes.*scalefactor;
    
    % Shift Nodes
    Shiftfactor = - xnodes(:,1);
    Shiftfactor = Shiftfactor(:,ones(1,num_nodes));
    
%     Shiftfactory = (ymin' - ynodes(:,1));
%     Shiftfactory = Shiftfactory(:,ones(1,num_nodes));

    NodesScaleShift = xnodes+Shiftfactor;
%     ynodes = ynodes+Shiftfactor;
    
    weightscaling = scalefactor(:,1);%.*scalefactor(:,1);
    Weights = Weights(ones(num_intervals,1),:).*weightscaling(:,ones(1,num_nodes));
    

end

