% Copyright The University of Sydney, 2001 - 2008.  ABN 15 211 513 464
% herafter 'The University'.
%
% The copyright of the computer program(s) herein is the property of
% The University of Sydney, Australia.  The program(s) may be used
% and/or copied only with the written permission of The University of
% Sydney, or in accordance with the terms and conditions stipulated
% in the contract under which the program(s) have been supplied.
% 
% Disclaimer:
% The Universitiy makes no representation or warranty
% that these materials, including any software, are free from errors;
% about the quality or performance of these materials;
% that these materials are fit for any particular purpose.
% These materials are made available on the strict basis that The University
% and its employees or agents have no liability for any direct or indirect
% loss or damage (including for negligence) suffered by any person as a
%consequence of the use of this  material.
%
% Demo for learning GP hyperparameters
global X y;
figure
%Use the same data as Rasmussen in Figure 2.5
X = [-2.1775;-0.9235;0.7502;-5.8868;-2.7995;4.2504;2.4582;6.1426;...
    -4.0911;-6.3481;1.0004;-4.7591;0.4715;4.8933;4.3248;-3.7461;...
    -7.3005;5.8177;2.3851;-6.3772]';
y = [1.4121;1.6936;-0.7444;0.2493;0.3978;-1.2755;-2.221;-0.8452;...
    -1.2232;0.0105;-1.0258;-0.8207;-0.1462;-1.5637;-1.098;-1.1721;...
    -1.7554;-1.0712;-2.6937;-0.0329]';


disp('  plot(x, y, ''k+'')')
plot(X, y, 'k+', 'MarkerSize', 17)
disp('  hold on')
hold on
 
disp(' ')
disp('Press any key to continue.')
pause
xstar = linspace(-7.5, 7.5, 201);
params0 = [0.5 0.5 0.5]; 
disp('Initial solution without optimisation');
si = 1:1:10;
plot(X(si),y(si), 'go', 'MarkerSize', 17);
[mf,vf] = gpsreval(X,y,@cov_sqexp,params0(1:end-1),[],params0(end),xstar,[],si);
S2 = vf - params0(3)^2;

clf
f = [mf+2*sqrt(S2);flipdim(mf-2*sqrt(S2),1)];
fill([xstar'; flipdim(xstar',1)], f, [7 7 7]/8, 'EdgeColor', [7 7 7]/8);
hold on
plot(xstar,mf,'k-','LineWidth',2);
plot(X, y, 'k+', 'MarkerSize', 17);
plot(X(si),y(si), 'go', 'MarkerSize', 17);
disp(' ')
disp('Press any key to continue.')
pause

%Check the gradients


params0 = [1 1 1];
%Xt = X;
%yt = y;
%X = X(:,1:4:20);
%y = y(1:4:20);
tic
[params] = gpsrlearn(@cov_sqexp,@grad_sqexp,params0(1:end-1),params0(end),si);
%[params] = gpkdtreelearn(@cov_nn2,@grad_nn2,params0(1:end-1),params0(end),options);

%params = [-13.0941    0.5656    1.7611    10.8639    4.1421    0.9710   13.6508    3.5630    1.2947    1.9408  0.1449];
disp(params);
toc
%X = Xt;
%y = yt;
%Evaluates
tic
[mf,vf] = gpsreval(X,y,@cov_sqexp,params(1:end-1),[],params(end),xstar,[],si);
% [lml,mf,vf] = gpeval(X,y,@cov_sqexp,params(1:end-1),[],params(end),xstar);
% mf
toc
%[lml,mf,vf] = gpkdtreeeval(X,y,@cov_sqexpfa,params(1:2),params(3),xstar,50);
%[lml,mf,vf] = gpjointeval(X,y,@cov_sqexpfa,params(1:2),params(3),xstar);


%disp('  S2 = S2 - exp(2*loghyper(3));')
S2 = vf% - params(end)^2;

figure
f = [mf+2*sqrt(S2);flipdim(mf-2*sqrt(S2),1)];
fill([xstar'; flipdim(xstar',1)], f, [7 7 7]/8, 'EdgeColor', [7 7 7]/8);
hold on
plot(xstar,mf,'b-','LineWidth',2);
%plot(xstar,mf2,'b-','LineWidth',2);
plot(X, y, 'k+', 'MarkerSize', 17);
plot(X(si),y(si), 'go', 'MarkerSize', 17);
disp(' ')
disp('Press any key to continue.')
pause