% Copyright The University of Sydney, 2001 - 2008.  ABN 15 211 513 464
% herafter 'The University'.
%
% The copyright of the computer program(s) herein is the property of
% The University of Sydney, Australia.  The program(s) may be used
% and/or copied only with the written permission of The University of
% Sydney, or in accordance with the terms and conditions stipulated
% in the contract under which the program(s) have been supplied.
% 
% Disclaimer:
% The Universitiy makes no representation or warranty
% that these materials, including any software, are free from errors;
% about the quality or performance of these materials;
% that these materials are fit for any particular purpose.
% These materials are made available on the strict basis that The University
% and its employees or agents have no liability for any direct or indirect
% loss or damage (including for negligence) suffered by any person as a
%consequence of the use of this  material.
%
% Demonstrate a GP regression

global X y;

%Use the same data as Rasmussen in Figure 2.5
X = [-2.1775;-0.9235;0.7502;-5.8868;-2.7995;4.2504;2.4582;6.1426;...
    -4.0911;-6.3481;1.0004;-4.7591;0.4715;4.8933;4.3248;-3.7461;...
    -7.3005;5.8177;2.3851;-6.3772]';
y = [1.4121;1.6936;-0.7444;0.2493;0.3978;-1.2755;-2.221;-0.8452;...
    -1.2232;0.0105;-1.0258;-0.8207;-0.1462;-1.5637;-1.098;-1.1721;...
    -1.7554;-1.0712;-2.6937;-0.0329]';


disp('  plot(x, y, ''k+'')')
plot(X, y, 'k+', 'MarkerSize', 17)
disp('  hold on')
hold on

disp(' ')
disp('Press any key to continue.')
pause

xstar = linspace(-7.5, 7.5, 201);


kpar = [1,1]; noise = 0.1; 
[lml,mf,vf,K,ks,a] = testgpeval(X,y,@cov_sqexp,kpar,[],noise,xstar);
% [lmlK,mfK,vfK] = testgpevalK(X,y,@cov_sqexp,kpar,[],noise,xstar);

mfK = ks'*inv(K+noise^2*eye(size(K,1)))*y'
% vfK = 
