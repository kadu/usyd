% Copyright The University of Sydney, 2001 - 2008.  ABN 15 211 513 464
% herafter 'The University'.
%
% The copyright of the computer program(s) herein is the property of
% The University of Sydney, Australia.  The program(s) may be used
% and/or copied only with the written permission of The University of
% Sydney, or in accordance with the terms and conditions stipulated
% in the contract under which the program(s) have been supplied.
% 
% Disclaimer:
% The Universitiy makes no representation or warranty
% that these materials, including any software, are free from errors;
% about the quality or performance of these materials;
% that these materials are fit for any particular purpose.
% These materials are made available on the strict basis that The University
% and its employees or agents have no liability for any direct or indirect
% loss or damage (including for negligence) suffered by any person as a
%consequence of the use of this  material.
%
% Compute the gradient of the multi-dimensional finite covariance function
%% constructed by Arman. This implementation uses kd-trees for efficiency
%
% function [g] = grad_sparse(par,K2,X2)
% INPUTS
%   X1 - D x N matrix of D dimensional N points
%   par(1:end-1) = [Length1; Length2; ..., LengthD] - D x 1 vector describing the D dimensional ellipsoid with
%                                                centre at the origin where the covariance function is nonzero
%   par(end) - covariance function coefficient Sigma0
%       
% OUTPUT is [K_Lengths, K_Sigma0] where
%   K_Lengths  - D length set of N x N arrays of derivatives K_Lengths{m}[i,j] = dK(X(:,i),X(:,j))/dLength_m
%   K_Sigma0   - N x N matrix with elements K_Sigma0[i,j] = dK(X(:,i),X(:,j))/dSigma0
%
% Fabio Ramos 23/10/2008

function [g] = grad_sparse(par,K2,X2)
global K X

if nargin<3
    Xg = X;
    Kg = K;
else
    Xg = X2;
    Kg = K2;
end

% Computing number and dimensionality of points
[D N] = size(Xg); % dimensionality and number of points in X
g = cell(1,D+1); %output cells

par = reshape(par,D+1,1);
Sigma0 = abs(par(end));
dK_3D_all = zeros(N,1);
dEucl = zeros(N,1);
%dMat = spalloc(N,D*N,40000);
dMat = zeros(N,D);
isize = ceil(0.8*N*N);
ii = zeros(1,isize);
jj = zeros(1,isize);
ib = 1;
v = cell(1,D);
for j=1:D
    v{j} = zeros(1,isize);
end

L = abs(par(1:end-1));

%Build the KDtree
[kd,Xsor] = vgg_kdtree2_build(Xg,1);
%Necessary according to the KDTree author for efficiency
indXsor = kd.indices;
kd.indices = uint32(1:N);


for i=1:N
    [is] = vgg_kdtree2_ballsearch(kd,Xsor,Xg(:,i),max(L));
    is = is(is~=0);
    ind = indXsor(is);
    X1t = Xsor(:,is);
    X2t = Xg(:,i);
    temp = abs(X1t - X2t(:,ones(1,length(is))))./L(:,ones(1,length(is)));
     
    dMat(:) = 0;
    dMat(ind,:) = temp';
    
    temp = sqrt(sum(temp.^2,1));
    dEuclOut = temp-1<0;
    dEucl(:) = 0;
    dEucl(ind,1) = temp.*dEuclOut;
    %ind = ind(dEuclOut);
    dK_3D_all(:) = 0;
    dK_3D_all(ind,1) = Sigma0*(2/3)*(1-cos(2*pi*dEucl(ind,1)) + ...
        pi*(1-dEucl(ind,1)).*sin(2*pi*dEucl(ind,1)))./(dEucl(ind,1)+(dEucl(ind,1)==0));
    %dK_3D_all(ind,i) = dK_3D_all.*dEuclOut.*(dEucl~=0);
    %dK_3D_all = sparse(double(ind),ones(1,length(ind)),v,N,1);

    ind = ind(dEuclOut);
    ie = ib + length(ind) - 1;
    if ie>isize
        disp('Be careful. Size of non-zero elements larger than pre-allocated');
    end
    ii(ib:ie) = ind;
    jj(ib:ie) = i*ones(1,length(ind));
    for m=1:D
        v{m}(ib:ie) = dK_3D_all(ind,1).*(dMat(ind,m).^2)/L(m);
    end
    ib = ie + 1; 
end

for m=1:D
    %dMat_m = dMat(:,N*(m-1)+1:N*m);
    %g{m} = dK_3D_all.*(dMat_m.^2)/L(m);
    %g{m}(:,i) = dK_3D_all(:,1).*(dMat(:,m).^2)/L(m);
    g{m} = sparse(ii(1:ie),jj(1:ie),v{m}(1:ie),N,N);
end

g{D+1} = Kg/Sigma0;
