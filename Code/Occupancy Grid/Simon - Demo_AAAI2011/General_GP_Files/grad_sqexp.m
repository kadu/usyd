% Copyright The University of Sydney, 2001 - 2008.  ABN 15 211 513 464
% herafter 'The University'.
%
% The copyright of the computer program(s) herein is the property of
% The University of Sydney, Australia.  The program(s) may be used
% and/or copied only with the written permission of The University of
% Sydney, or in accordance with the terms and conditions stipulated
% in the contract under which the program(s) have been supplied.
% 
% Disclaimer:
% The Universitiy makes no representation or warranty
% that these materials, including any software, are free from errors;
% about the quality or performance of these materials;
% that these materials are fit for any particular purpose.
% These materials are made available on the strict basis that The University
% and its employees or agents have no liability for any direct or indirect
% loss or damage (including for negligence) suffered by any person as a
%consequence of the use of this  material.
%
% Compute the gradient of the squared exponential 
% covariance function.
%
%INPUT
%  kpar - hyperparameters 
%OUTPUT
%  g - gradient matrix
%
%
% Fabio Tozeto Ramos 07/06/07
% g = grad_sqexp(kpar,K,X2,X1)

function g = grad_sqexp(kpar,K2,X2,X1)
global K X;

if nargin<4
    if nargin<3
        Xg = X;
        Kg = K;
    else
        Xg = X2;
        Kg = K2;
    end
    [d,n] = size(Xg);
    g = cell(1,d+1); %weights plus sigma f

    for i=1:d
        %Compute weighted squared distance 
        w = kpar(i)^(-3);
        XX = Xg(i,:).*Xg(i,:);
        XTX = Xg(i,:)'*Xg(i,:);
        XXT = XX';
        z = XXT(:,ones(1,n)) + XX(ones(1,n),:) - 2*XTX;
        g{i} = w*Kg.*z;
    end

else
    Kg = K2;
    N1 = size(X1,2);
    [d,N2] = size(X2);
    g = cell(1,d+1); %weights plus sigma f

    for i=1:d
        %Compute weighted squared distance 
        w = kpar(i)^(-3);
        
        XX1 = sum(w*X1(i,:).*X1(i,:),1);
        XX2 = sum(w*X2(i,:).*X2(i,:),1);
        X1X2 = (w*X1(i,:))'*X2(i,:);
        XX1T = XX1';
        z = XX1T(:,ones(1,N2)) + XX2(ones(1,N1),:) - 2*X1X2;
        
        g{i} = w*Kg.*z';
    end    
end
g{d+1} = Kg*(2/kpar(end));