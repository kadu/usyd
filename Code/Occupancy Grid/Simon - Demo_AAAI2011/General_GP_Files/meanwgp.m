%Uses the Gauss-Hermite quadrature to calculate the mean of WGP
%function [mf] = meanwgp(wf,wdf,par,mu,sig2)
function [mf] = meanwgp(wf,wdf,par,mu,sig2)
N       = 5; % 5th order, the results will be exact if f(x) is a polynomial of order less than or equal to 2N-1
sig     = sqrt(sig2);
[N,x,w] = gqzero(N);
y       = sqrt(2)*x*sig+mu;
invy    = zeros(length(y),1);
for i = 1:length(y)
    invy(i) = invnewton(wf,wdf,par,y(i),rand);
end
mf = sum(invy.*w)/sqrt(pi);



% ref: http://www.aims.ac.za/resources/courses/na/gauss_quad.pdf
% n: order of the hermite polynomila
% x : nodes
% w : weights
function [n,x,w] = gqzero(n)
d = sqrt((1:n)/2);
T = diag(d,1)+diag(d,-1);
[V,D] = eig(T);
w = V(1,:).^2;
w = w*sqrt(pi);
x = diag(D);
[x,ind] = sort(x);
w = w(ind);
w=w';