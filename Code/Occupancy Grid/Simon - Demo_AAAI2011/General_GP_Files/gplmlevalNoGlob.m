% Copyright The University of Sydney, 2001 - 2008.  ABN 15 211 513 464
% herafter 'The University'.
%
% The copyright of the computer program(s) herein is the property of
% The University of Sydney, Australia.  The program(s) may be used
% and/or copied only with the written permission of The University of
% Sydney, or in accordance with the terms and conditions stipulated
% in the contract under which the program(s) have been supplied.
% 
% Disclaimer:
% The Universitiy makes no representation or warranty
% that these materials, including any software, are free from errors;
% about the quality or performance of these materials;
% that these materials are fit for any particular purpose.
% These materials are made available on the strict basis that The University
% and its employees or agents have no liability for any direct or indirect
% loss or damage (including for negligence) suffered by any person as a
%consequence of the use of this  material.
%
%Compute the negate log marginal likelihood of a GP

function nlml = gplmlevalNoGlob(X,y,params,kfun,npar)

noise = params(end);
kpar = params(1:end-1);
n = size(X,2);
% K = feval(kfun,X,X,kpar);
K=cov_sum(X,X,kfun,npar,kpar);
L = chol(K+(noise^2)*eye(n))'; 
invK = L'\(L\eye(n));
alpha = invK*y';
%alpha = (K+(noise^2)*eye(n))\y';

%Compute the log marginal likelihood

%nlml = 0.5*y*alpha + sum(diag(L)) + 0.5*n*log(2*pi);
%nlml = 0.5*y*alpha + det(K) + 0.5*n*log(2*pi);=======
nlml = 0.5*y*alpha + sum(log(diag(L))) + 0.5*n*log(2*pi);

