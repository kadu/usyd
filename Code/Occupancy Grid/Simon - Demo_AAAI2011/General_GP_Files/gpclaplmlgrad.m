%Compute the negate log marginal likelihood of a GP classifier
%and its gradient using the Lapalce approximation. The algorithm is described
%in "Gaussian Processes for Machine Learning" by Rasmussen and
%Williams. Most of the code was adapted from Rasmussen's gpml toolbox. 
%It is useful for optimisers like
%matlab which requires a single function passing lml and
%the gradient.
%
%INPUT
% params
% kfun
% kgradfun
%
%OUTPUT
% nlml - negative log marginal likelihood
% ng - gradient of the negative log marginal likelihood
% sw -  
% 
%
% Fabio Tozeto Ramos 11/07/09
% [nlml,ng,sW] = gpclaplmlgrad(params,kfun,kgradfun)
function [nlml,ng,sW] = gpclaplmlgrad(params,kfun,kgradfun)
global X y K;%L invK K alpha;
kpar  = params;
tol   = 1e-6;             % tolerance for when to stop the Newton iterations
[D,n] = size(X);
K     = feval(kfun,X,X,kpar);

persistent best_a best_value;   % keep a copy of the best "a" and its obj value


if any(size(best_a) ~= [n 1])      % find a good starting point for "a" and "f"
  f = zeros(n,1); a = f; [lp dlp W] = cumGauss( f, y' );         % start at zero
  Psi_new = -n*log(2); best_value = inf;
else
  a = best_a; f = K*a; [lp dlp W] = cumGauss( f, y' ); % try the best "a" so far
  Psi_new = -a'*f/2 + lp;         
  if Psi_new < -n*log(2)                                  % if zero is better..
    f = zeros(n,1); a = f; [lp dlp W] = cumGauss( f, y' );  % ..then switch back
    Psi_new = -a'*f/2 + lp;
  end
end
Psi_old = -inf;                                   % make sure while loop starts

while Psi_new - Psi_old > tol                       % begin Newton's iterations
  Psi_old = Psi_new; a_old = a; 
  sW = sqrt(W);                     
  L = chol(eye(n)+sW*sW'.*K);  
  b = W.*f+dlp;
  a = b - sW.*solve_chol(L,sW.*(K*b));
  f = K*a;
  [lp dlp W d3lp] = cumGauss( f, y' );

  Psi_new = -a'*f/2 + lp;
  i = 0;
  while i < 10 && Psi_new < Psi_old               % if objective didn't increase
    a = (a_old+a)/2;                                 % reduce step size by half
    f = K*a;
    [lp dlp W d3lp] = cumGauss( f, y' );
    Psi_new = -a'*f/2 + lp;
    i = i + 1;
  end
end                                                   % end Newton's iterations

%sW = sqrt(W);                     
%L  = chol(eye(n)+sW*sW'.*K);  

nlml = a'*f/2 - lp + sum(log(diag(L)));      % approx neg log marginal lik
if nlml < best_value                                   % if best so far...
  best_a = a; best_value = nlml;          % ...then remember for next call
end


ng = 0*kpar;                         % allocate space for derivatives
Z = repmat(sW, 1, n).*solve_chol(L, diag(sW));
C = L'\(repmat(sW,1,n).*K);                 % FIX: use that L is triangular
s2 = -0.5*(diag(K)-sum(C.^2,1)').*d3lp;
C  = feval(kgradfun, kpar );
    
for j=1:length(kpar)
    s1 = a'*C{j}*a/2-sum(sum(Z.*C{j}))/2;
    b = C{j}*dlp;
    s3 = b-K*(Z*b);
    ng(j) = -s1-s2'*s3;
end






function [lp, dlp, d2lp, d3lp] = cumGauss(f, y )

if nargin == 2
  p = (1+erf(f.*y/sqrt(2)))/2 + 1e-10;
  n = exp(-f.^2/2)/sqrt(2*pi);
  lp = sum(log(p));
  dlp = y.*n./p;
  d2lp = +n.^2./p.^2+y.*f.*n./p;
  d3lp = 2*y.*n.^3./p.^3+3*f.*n.^2./p.^2+y.*(f.^2-1).*n./p; 
  d3lp = -d3lp;
else
  lp = (1+erf(f./sqrt(1+y)/sqrt(2)))/2;
end


