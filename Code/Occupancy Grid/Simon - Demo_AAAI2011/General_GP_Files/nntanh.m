%Transformation function for the Warped Gaussian process based on
%neural nets tanh(x) type.
%f(x) = x + sum_i a_i*tanh(b_i*x+c_i)
function [fy] = nntanh(params,x)
I = size(params,1);
fy = zeros(1,length(x));
for i = 1 : I
    fy = fy + abs(params(i,1))*tanh(abs(params(i,2))*x+params(i,3));
end
fy = fy + x;