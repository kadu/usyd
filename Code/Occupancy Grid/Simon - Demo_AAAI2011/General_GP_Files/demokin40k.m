%Demonstrates GP for the kin40k dataset.
global X y


direc = '~/sshfs/Datasets/Matlab/';
load([direc 'kin40k.mat']);

Xt = KIN40K_X';
yt = KIN40K_Y';

%Learn the model
%params = [0.1*std(X,0,2)' rand 0.01];
%params = [ones(1,8) 2.3 0.01];
%params = [0.0114 0.0373 0.0760 0.1086 0.1254 0.1694 0.2184 0.0755 0.4839 0.0186];

params=[0.0528 0.0356 0.2917 0.3050 0.4017 0.5112 0.1383 0.2127 0.0737 0.00001];
options = zeros(1,18);
options(1)=1; options(9)=1; options(14)=40;
%Subsample
ind = randperm(size(Xt,2));
npts = 10000;
X = Xt(:,ind(1:npts));
y = yt(:,ind(1:npts))-mean(yt(:,ind(1:npts)));
[params]=gplearn(@cov_sparse,@grad_sparse,params(1:end-1),params(end),options)

%X = Xt;
%y = yt - mean(yt(:,ind(1:npts)));

tic;
[lml,mf,vf] = gpevalmulti(X,y,...
    @cov_sparse,params(1:end-1),[],params(end),Xt(:,ind(npts+1:end)));
toc
S2 = vf';

v = mf' + mean(yt(:,ind(1:npts)));
mse = sum(((v'-yt(ind(npts+1:end))).^2))/length(ind(npts+1:end));
disp(mse);

% clf
% f = [v+2*sqrt(S2);flipdim(v-2*sqrt(S2),1)];
% fill([[1:length(v)]; flipdim([1:length(v)],1)], f, [7 7 7]/8, 'EdgeColor', [7 7 7]/8);
% hold on
% plot(1:length(v),v,'k-','LineWidth',2);
% plot(1:length(v), ytest, 'g+');