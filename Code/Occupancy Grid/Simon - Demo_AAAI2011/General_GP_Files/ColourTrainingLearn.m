

% Copyright The University of Sydney, 2001 - 2008.  ABN 15 211 513 464
% herafter 'The University'.
%
% The copyright of the computer program(s) herein is the property of
% The University of Sydney, Australia.  The program(s) may be used
% and/or copied only with the written permission of The University of
% Sydney, or in accordance with the terms and conditions stipulated
% in the contract under which the program(s) have been supplied.
% 
% Disclaimer:
% The Universitiy makes no representation or warranty
% that these materials, including any software, are free from errors;
% about the quality or performance of these materials;
% that these materials are fit for any particular purpose.
% These materials are made available on the strict basis that The University
% and its employees or agents have no liability for any direct or indirect
% loss or damage (including for negligence) suffered by any person as a
%consequence of the use of this  material.
%
% Demo for learning GP hyperparameters

clear all
close all
clc

global X y;
load ColourTraining

X = ColourTraining(1:3,:);

Red = ColourTraining(4,:);    %Training Red First
Green = ColourTraining(5,:);  
Blue = ColourTraining(6,:);  


%%Subsample to reduce size
fractiontoelim=1.8;
elimindex = unique(ceil(size(X,2)*rand(1,fractiontoelim*size(X,2))));
X(:,elimindex)=[];
Red(:,elimindex)=[];
Green(:,elimindex)=[];
Blue(:,elimindex)=[];



disp('  plot(x, y, ''k+'')')
figure
scatter3(X(1,:),X(2,:),X(3,:), 5,Red)

figure
scatter3(X(1,:),X(2,:),X(3,:), 5,Green)

pause

load ParamsR2
params0=ParamsR

disp('Initial solution without optimisation');

options = zeros(1,18);
options(1)=1; options(9)=1; options(14)=40;

tic
 size(X)
 y=Blue-mean(Blue);
 [params]=gplearn(@cov_sqexp,@grad_sqexp,params0(1:end-1),params0(end),options)
 ParamsB = params
 save ParamsB2 ParamsB
toc
