%Compute the negate log marginal likelihood of a multi-class GP classifier
%and its gradient using the Laplace approximation. The algorithm is described
%in "Gaussian Processes for Machine Learning" by Rasmussen and
%Williams. Most of the code was adapted from Rasmussen's gpml toolbox. 
%It is useful for optimisers like
%matlab which requires a single function passing lml and
%the gradient.
%
%INPUT
% params
% kfun
% kgradfun
%
%OUTPUT
% nlml - negative log marginal likelihood
% ng - gradient of the negative log marginal likelihood
% f  -  
% 
%
% Fabio Tozeto Ramos 11/07/09
% [nlml,ng,sW] = gpmclaplmlgrad(params,kfun,kgradfun)
function [nlml,ng,f] = gpmclaplmlgrad(params,kfun,kgradfun,kmap)
global X y 
persistent best_a best_value;   % keep a copy of the best "a" and its obj value

tol   = 1e-6;             % tolerance for when to stop the Newton iterations
[dim,n] = size(X);
C     = length(y)/n;      % number of classes
R     = repmat(eye(n),C,1);  % stacked identity matrices
E     = cell(1,C);
K     = cell(1,C);
sumE  = zeros(n,n);
kpar  = cell(1,C);



%Build K
for i = 1:C
    %ind  = (y((i-1)*n+1:i*n) == 1);
    kpar{i} = params(kmap{i});
    K{i} = feval(kfun{i},X(:,:),X(:,:),kpar{i});
    %invK{i} = inv(K{i}+0.01*eye(n));
end
Kb    = blkdiag(K{:});
%invKb = blkdiag(invK{:});    

% if 0
if any(size(best_a) ~= [C*n 1])      % find a good starting point for "a" and "f"
    f = [y(1:n) 2*y(n+1:end)]';                  % start at zero      
    a = f; 
    Psi_new = -C*n*log(2); 
    best_value = inf;
else
    a = best_a; 
    f = Kb*a; 
    Psi_new = -a'*f/2 + y*f - ...
      sum(log(sum(exp(reshape(f,n,C)'))));         
    if Psi_new < -C*n*log(2)                                  % if zero is better..
        f = zeros(C*n,1); 
        a = f; 
        Psi_new = -a'*f/2 + y*f - ...
            sum(log(sum(exp(reshape(f,n,C)'))));
    end
end

Psi_old = -inf;                                   % make sure while loop starts
bPi     = zeros(C*n,n);
z       = zeros(1,C);
while Psi_new - Psi_old > tol                       % begin Newton's iterations
    Psi_old = Psi_new; 
    a_old   = a;
    temp    = logistic(f, C);
    bpi     = reshape(temp',C*n,1);  %bold pi
    for i = 1 : C
        bPi((i-1)*n+1:i*n,:) = diag(temp(i,:));  %bold Pi
    end
    D       =  diag(bpi);
    sumE(:) =  0;
    for i = 1:C
        ii   = (i-1)*n+1;
        ff   = i*n;
        sDc  = sqrt(D(ii:ff,ii:ff));
        L    = jitChol(eye(n)+sDc*K{i}*sDc,50);  
        E{i} = sDc*(L'\(L\sDc)); 
        %E{i} = sDc*solve_chol(L,sDc);         
        z(i) = sum(log(diag(L)));
        sumE = sumE + E{i};
    end
    Eb = blkdiag(E{:});
    M  = jitChol(sumE);
    b  = (D - bPi*bPi')*f + y' - bpi;
    c  = Eb*(Kb*b);
    a  = b - c + Eb*R*(M'\(M\R'*c));
    f  = Kb*a;
    
    Psi_new = -a'*f/2 + y*f - ...
        sum(logsum(reshape(f,C,n)));
    i = 0;
    while i < 10 && Psi_new < Psi_old               % if objective didn't increase
        a = (a_old+a)/2;                                 % reduce step size by half
        f = Kb*a;
        Psi_new = -a'*f/2 + y*f - ...
            sum(logsum(reshape(f,n,C)'));
        i = i + 1;
    end
end                                                   % end Newton's iterations
% end

%Uses the Matlab optimiser

%opts = optimset('LargeScale','on','MaxIter',500,'Diagnostics', 'off',...
%'GradObj', 'on', 'Hessian','on','Display', 'off','TolFun',  1e-5, 'MaxFunEvals', 5000);
%f = rand(C*n,1);
%f = fminunc(@(f) fobjective(f,C,n,y,invKb), f,opts);
%%%%%%%%%%%%%%%%SIMPLE IMPLEMENTATION%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% temp    = logistic(f, C);
% bpi     = reshape(temp',C*n,1);  %bold pi
% bPi     = zeros(C*n,n);
% 
% for i = 1 : C
%    bPi((i-1)*n+1:i*n,:) = diag(temp(i,:));  %bold Pi
% end
% W  = diag(bpi) - bPi*bPi';
% sW = sqrtm(W);
% 
% 
% nlml = 0.5*f'*invKb*f - y*f + sum(logsum(reshape(f,n,C)',1)) + ...
%   0.5*logdet(eye(C*n) + sW*Kb*sW');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%bPi     = zeros(C*n,n);
%z       = zeros(1,C);
%temp    = logistic(f, C);
%bpi     = reshape(temp',C*n,1);  %bold pi
%for i = 1 : C
%    bPi((i-1)*n+1:i*n,:) = diag(temp(i,:));  %bold Pi
%end
%D       =  diag(bpi);
%sumE(:) =  0;
%for i = 1:C
%    ii   = (i-1)*n+1;
%    ff   = i*n;
%    sDc  = sqrt(D(ii:ff,ii:ff));                     
%    L    = chol(eye(n)+sDc*K{i}*sDc');  
    %E{i} = sDc*(L'\(L\sDc)); 
    %E{i} = sDc*solve_chol(L,sDc);         
%    z(i) = sum(log(diag(L)));
    %sumE = sumE + E{i};
%end
%Eb = blkdiag(E{:});
%M  = chol(sumE);
%b  = (D - bPi*bPi')*f + y' - bpi;
%c  = Eb*Kb*b;
%a  = b - c + Eb*R*M'\(M\R'*c);
%f  = Kb*a;
%sW = sqrt(W);                     
%L  = chol(eye(n)+sW*sW'.*K);  
%a = Kb\f;
nlml = a'*f/2 - y*f + 0.5*sum(z) + ...
    sum(logsum(reshape(f,n,C)'));      % approx neg log marginal lik
if nlml < best_value                                   % if best so far...
    best_a = a; best_value = nlml;          % ...then remember for next call
end

%No gradients at the moment
ng = 0*params;                         % allocate space for derivatives
% Z = repmat(sW, 1, n).*solve_chol(L, diag(sW));
% C = L'\(repmat(sW,1,n).*K);                 % FIX: use that L is triangular
% s2 = -0.5*(diag(K)-sum(C.^2,1)').*d3lp;
% C  = feval(kgradfun, kpar );
%     
% for j=1:length(kpar)
%     s1 = a'*C{j}*a/2-sum(sum(Z.*C{j}))/2;
%     b = C{j}*dlp;
%     s3 = b-K*(Z*b);
%     ng(j) = -s1-s2'*s3;
% end
% 


%Compute the logit function
function [p] = logistic(f, C)
n  = length(f)/C; %number of points
ft = reshape(f,n,C)';
%yt = reshape(y,C,n);  
%lp = exp(ft(yt==1)')./sum(exp(ft));
lp = ft - repmat(logsum(ft,1),C,1);
p  = exp(lp);


%Objective for f
% function [v,dv,ddv] = fobjective(f,C,n,y,invKb)
% 
% v    = 0.5*f'*invKb*f - y*f + sum(logsum(reshape(f,n,C)',1));
% 
% temp    = logistic(f, C);
% bpi     = reshape(temp',C*n,1);  %bold pi
% 
% 
% dv      = invKb*f - y' + bpi;
% bPi     = zeros(C*n,n);
% 
% for i = 1 : C
%      bPi((i-1)*n+1:i*n,:) = diag(temp(i,:));  %bold Pi
% end
% ddv  = invKb + diag(bpi) - bPi*bPi';


