% Copyright The University of Sydney, 2001 - 2008.  ABN 15 211 513 464
% herafter 'The University'.
%
% The copyright of the computer program(s) herein is the property of
% The University of Sydney, Australia.  The program(s) may be used
% and/or copied only with the written permission of The University of
% Sydney, or in accordance with the terms and conditions stipulated
% in the contract under which the program(s) have been supplied.
% 
% Disclaimer:
% The Universitiy makes no representation or warranty
% that these materials, including any software, are free from errors;
% about the quality or performance of these materials;
% that these materials are fit for any particular purpose.
% These materials are made available on the strict basis that The University
% and its employees or agents have no liability for any direct or indirect
% loss or damage (including for negligence) suffered by any person as a
%consequence of the use of this  material.
%
% Evaluate a Gaussian Process (see page 19 of
% "Gaussian Processes for Machine Learning" by
% C. Rasmussen and C. Williams)
% Much faster than gpeval when there are many points
%INPUTS
% X - D x N inputs
% y - 1 x N outputs (targets)
% kfun - covariance function
% kpar - covariance parameters (hyperparameters)
% K - N x N computed covariance 
% noise - scalar noise level
% x - D x M test inputs
% invK - N x N inverted K if available
% step - how many points to perform inference at the time
% 
%OUTPUTS
% mf - predictive mean
% vf - predictive variance
% lml - log marginal likelihood
%
% Fabio Tozeto Ramos 18/03/08
% [mf,vf,lml] = gpevalmulti(X,y,kfun,kpar,K,noise,x,invK,step)

function [lml,mf,vf] = gpevalmulti(X,y,kfun,kpar,K,noise,x,invK,step)
N = size(X,2); %number of inputs
Nt = size(x,2); %number of testing points
mf = zeros(1,Nt);
vf = zeros(1,Nt);

if isempty(K)
    K = feval(kfun,X,X,kpar);
end

if nargin<8
    %Cholesky factorisation for covariance inversion
    A = K+(noise^2)*eye(N);
    p = symrcm(A);
    L = chol(A(p,p))'; 
    alpha = L'\(L\y(p)');
else
    %If the inverted covariance is given use it to 
    %compute alpha
    alpha = invK*y;
end

if nargin<9
    step = 500;
end

if ~isempty(x)    %Evaluate the marginal log-likelihood only
    for i=1:ceil(Nt/step)
        ist = (i-1)*step+1;
        if i*step<Nt
            ien = i*step;
        else
            ien = Nt;
        end
        % K(X,x)
        ks = feval(kfun,X(:,p),x(:,ist:ien),kpar);

        %Compute the mean
        mf(ist:ien) = ks'*alpha;

        %Compute the variance
        v = L\ks;
        vf(ist:ien) = diag(feval(kfun,x(:,ist:ien),x(:,ist:ien),kpar))-...
            sum(v.*v)'+noise^2;
        %vf = feval(kfun,x,x,kpar) - v'*v + noise^2;
    end
else
    mf = [];
    vf = [];
end
%Compute the log marginal likelihood
lml = -0.5*y*alpha-sum(log(diag(L)))-0.5*N*log(2*pi);