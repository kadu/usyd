% Compute the negative log marginal likelihood of a multi-task GP
% Shrihari Vasudevan 11 Nov 2009

function nlml = mogpblocklmleval(X,y,params,kfun,mappar)

% number of tasks
nt = length(kfun);

% separate hyperparams (include task sim params) and noise
kpar  = params(1:end-nt);
noise = params(end-nt+1:end);

% Kf = task sim matrix
% M = symmetric matrix used to compute Kf such that it is always PSD
M = zeros(nt);

% Builds M % bug fix SV20091026
ii = 0; ff = 0;
for i = 1 : nt-1
   ii = ff+1;
   ff = ii+nt-i;
   M(i , i:end) = kpar(ii:ff);
   M(i+1:end , i) = M(i , i+1:end);   % Added by Arman - 02 June 2009
end
M(nt,nt) = kpar(ff+1);

% compute Kf
Kf   = M*M';

% compute K
nlml = 0;
npptask = size(X{1},2);
ntrdata = nt*npptask;
nblock = 2000;
niter = round(ntrdata/nblock);
dbeg  = 0; dend = 0;

for q = 1: niter
    dbeg = (q-1)*(nblock/nt) + 1;
    if (q ~= niter)
        dend = q*(nblock/nt);
    else
        dend = npptask;
    end;

    Xt = cell(1,nt); yt = cell(1,nt);    
    for i = 1:nt
        Xt{i} = X{i}(:,dbeg:dend);  
        yt{i} = y{i}(dbeg:dend);
    end;

    % K = task sim matrix * covariance matrix
    K = cell(nt,nt);

    % compute K, nlml for each block separately
    for i = 1 : nt
        for j = 1 : nt
            if i==j
                % auto-covariance
                K{i,j} = Kf(i,j)*feval(kfun{i,j},Xt{i},Xt{j},kpar(mappar{i,j})) ...
                    + noise(i)^2*eye(size(Xt{i},2));
            else
                %cross terms
                K{i,j} = Kf(i,j)*feval(kfun{i,j},Xt{i},Xt{j},kpar(mappar{i,j}));
            end
        end
    end
    K = cell2mat(K);

    % collect y-data from all tasks into 1 array and get length
    yt = cell2mat(yt);
    n  = length(yt);

    % compute inverse of K
    L     = jitChol(K);
    %invK  = L'\(L\eye(size(K)));
    invK  = solve_chol(L,eye(size(K))); % faster than the previous line

    % compute negative log marginal likelihood
    alpha = invK*yt';
    nlml  = nlml + 0.5*yt*alpha + sum(log(diag(L))) + 0.5 * size(Xt{i},2) * log(2*pi);
end;