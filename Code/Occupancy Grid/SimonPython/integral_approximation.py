# -*- coding: utf-8 -*-
"""
Created on Tue Sep  2 22:37:18 2014

@author: kadu
"""
import numpy as np
import common_functions as cf

res = 0.5
active_sel_switch = False
use_latest_GP = False      # UNIMPLEMENTED!
show_redundant_set = False # setting to True _really_ slows down the algorithm

# Active Threshold: lowering increases the number of points accepted
AT_point = 0.05    # Threshold for active set selection for occ points
AT_line = 0.1      # Threshold for active set selection for occ lines
AT_sample_no = 10  # No. of samples on the line to decide if it's accepted

order = 5 # quadrature order

# Load and initialise training data 
X, y = cf.init_training_data("TrainingData.mat", True)

# Generate test points
min_x = min(X.start[0].min(), X.end[0].min()) - 1.
min_y = min(X.start[1].min(), X.end[1].min()) - 1.
max_x = max(X.start[0].max(), X.end[0].max()) + 1.
max_y = max(X.start[1].max(), X.end[1].max()) + 1.

# In the unlikely event that 'max_x' or 'max_y' are exact multiples of 'res',
# these values must be adjusted by adding 'res' to make up for the fact that
# matlab's '[a:b]' generates the mathematical set [a,b], while in python the
# analogous commands like 'range(a,b)' generatee the set [a,b).
# This was done just for consistency with the original code.

if not(max_x % res):
    max_x += res
if not(max_y % res):
    max_y += res

mesh = np.mgrid[min_x:max_x:res, min_y:max_y:res]
test = (np.reshape(mesh[0], (1,-1)), np.reshape(mesh[1], (1,-1)))

# Initialise GP
#if use_latest_GP:
#    # CURRENTLY UNIMPLEMENTED!
#    cf.load_latest_gp(data_file)
#    # CURRENTLY UNIMPLEMENTED!
#else:
#    '''
#    ?
#    '''


