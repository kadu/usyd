# -*- coding: utf-8 -*-
"""
Created on Tue Sep  2 22:52:26 2014

@author: kadu
"""
from scipy.io import loadmat
import numpy as np
#import matplotlib.pyplot as plt
import pylab as pl
from matplotlib import collections as mc

class struct(dict):
    '''
    dict extension that allows to treat keys as class attributes.
    Behaves like a struct in MATLAB.
    Example:
        >>> d = dict()
        >>> d['i'] = 3
        >>> d['c'] = 'a'
        >>> s = struct()
        >>> s.i = 3
        >>> s.c = 'a'
        >>> d == s
        True
        
    '''
    def __getattr__(self, name):
        return self.__getitem__(name)
    def __setattr__(self, name, value):
        self.__setitem__(name, value)
        
        
# Checked and equal to matlab code
def init_training_data(data_file, plot=False):
    '''
    Reads TrainingData.mat output from LaserDataGenerator
    '''
    training_data = loadmat(data_file)['TrainingData']
    
    X = struct()
    X.start   = training_data[0][0][3]
    X.end     = training_data[0][0][4]
    X.point   = training_data[0][0][2]
    X.pose_no = training_data[0][0][0]
    # All check against matlab code
    
    y = struct()
    y.segment = -1 * np.ones((1,len(X.start[0])))
    y.point   = np.ones((1,len(X.point[0])))
    y.int_seg = y.segment * np.sqrt(sum((X.start-X.end)**2))
    # All check against matlab code

    if plot:
        fig, ax = pl.subplots()
        
        lines = [[(sx, sy), (ex, ey)] for sx, sy, ex, ey 
        in zip(X.start[0], X.start[1], X.end[0], X.end[1])]
        lc = mc.LineCollection(lines)
        ax.add_collection(lc)
        '''
        TODO: Add points to graph
        '''
        ax.autoscale()
        ax.margins(0.1)    

        fig.show()
    return X, y


def load_latest_gp(data_file):
    '''
    TODO
    '''
    raise "Method unimplemented! Sorry for the inconvenience"