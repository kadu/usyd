# -*- coding: utf-8 -*-
"""
Created on Wed Sep  3 23:36:06 2014

@author: kadu
"""
from cf import struct

def merge_scan2set(scan_set, scan, gp, order, node_weight_array):
    if len(scan_set.X[0]) > 0:
        scan_set.start = scan_set.X[:,scan_set.obs_id==1,0]
        scan_set.end   = scan_set.X[:,scan_set.obs_id==1,1]

def build_active_set(X, y, gp, scan_no, act_thresh, squashparams, act_switch, node_weight_array, show_redundant_set):
    # divide observations between old and new
    new_scan = struct()
    new_scan.start   = X.start[:,X.pose_no == scan_no]
    new_scan.end     = X.end[:,X.pose_no == scan_no]
    new_scan.x_point = X.point[:,X.pose_no == scan_no]
    new_scan.y_point = y.point[:,X.pose_no == scan_no]
    new_scan.int_seg = y.int_seg[:,X.pose_no == scan_no]

    if act_switch:
        '''
        TODO
        '''
        
    else:
        if scan_no == 1.:
            X.active_set = struct()
            X.active_set.X      = []
            X.active_set.y      = []
            X.active_set.obs_id = []
            X.active_set.L      = []
            X.active_set.alpha  = []
        X.active_set = merge_scan2set(X.active_set, new_scan, gp, gp.order, node_weight_array)
