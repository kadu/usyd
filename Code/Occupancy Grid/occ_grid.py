# -*- coding: utf-8 -*-
"""
Occupancy grid implementation
"""

import numpy as np
import PIL

def side_of(a, b, c):
    '''
    Given three points, a, b and c, returns if c is to the left (returns -1), to
    the right (returns 1) or collinear (returns 0) with the a->b vector
    '''
    ab = np.asarray([a[0] - b[0], a[1] - b[1]])
    ac = np.asarray([c[0] - a[0], c[1] - a[1]])
    
    return cmp(np.cross(ab, ac), 0)
    
def in_list(A, b):
    try:
        A.index(b)
    except ValueError:
        return False
    return True
    
class Robot():
    def __init__(self, filename):
        '''
        Reads a file from SimDataGenerator. Works for a single robot.
        '''
        f = open(filename)
        data = f.read()
        data = data.split('\n')
        
        n_rob = int(data[4].split()[1]) # number of robots in the file 
        for i in range(len(data)):
            if "Pose" in data[i]:
            # list : all the poses from which data collection was made in
            # reverse order, so popping will give a chronological order
                if n_rob == 1:
                    self.pose_list = [map(np.double, data[i + 4].split()),
                                      map(np.double, data[i + 5].split()),
                                      map(np.double, data[i + 6].split())]
                    self.pose_list = np.asarray(self.pose_list).T.tolist()
                    self.pose_list.reverse()
                else:
                    self.pose_list = []
                    for rob in range(n_rob):
                        while "cell-element" not in data[i]:
                            i += 1
                        pose_list = [map(np.double, data[i + 4].split()),
                                     map(np.double, data[i + 5].split()),
                                     map(np.double, data[i + 6].split())]
                        pose_list = np.asarray(pose_list).T.tolist()
                        if rob == 0:
                            self.pose_list += pose_list
                        i += 6
                    self.pose_list.reverse()
                    self.pose_list_2 = self.pose_list.copy()
                    
        
            if "RangeFinderData" in data[i]:
            # list of lists : all the measured beam lengths in reverse
            # order, so popping will give a chronological order
                self.beam_hits = []
                if n_rob == 1:
                    i += 2
                    n_poses = int(data[i].split()[-1])
                    i += 2
                    for n in range(n_poses):
                        self.beam_hits += [map(np.double, data[i + n].split())]
                    self.beam_hits.reverse()
                else:
                    n_n_poses = []
                    for rob in range(n_rob):
                        while "cell-element" not in data[i]:
                            i += 1
                        i += 2
                        n_poses = int(data[i].split()[-1])
                        n_n_poses += [n_poses]
                        i += 2
                        for n in range(n_poses):
                            self.beam_hits += [map(np.double, data[i + n].split())]
                        i += n
                    self.beam_hits.reverse()
                        
        
            if "LaserOrientations" in data[i]:
            # list of lists : all the measured beam angles in reverse
            # order, so popping will give a chronological order
                self.beam_angles = []
                if n_rob == 1:
                    i += 2
                    n_poses = int(data[i].split()[-1])
                    i += 2
                    for n in range(n_poses):
                        self.beam_angles += [map(np.double, data[i + n].split())]
                    self.beam_angles.reverse()
                else:
                    for rob in range(n_rob):
                        while "cell-element" not in data[i]:
                            i += 1
                        i += 2
                        n_poses = int(data[i].split()[-1])
                        i += 2
                        for n in range(n_poses):
                            self.beam_angles += [map(np.double, data[i + n].split())]
                        i += n
                    self.beam_angles.reverse()
            
                
            if "MaxRange" in data[i]:
            # list of doubles: max range of the sensors in meters
                self.range_list = []
                if n_rob == 1:
                    self.range_list += [np.double(data[i + 2])] * n_poses
                else:
                    for rob in range(n_rob):
                        while "cell-element" not in data[i]:
                            i += 1
                        i += 2
                        self.range_list += [np.double(data[i])] * n_n_poses[0]
                    self.range_list.reverse()


        self.beams = []
        # list of arrays : collection of observations 
        
    def update(self):
        '''
        Moves the robot to the next pose and takes the new collection of
        observations
        '''
        if self.pose_list != []:
            self.pose = tuple(self.pose_list.pop())
            self.beams = zip(self.beam_hits.pop(), self.beam_angles.pop())
            self.sensor_range = self.range_list.pop()
            return True
        else:
            return False
            
            
            
    def next_beam(self):
        '''
        Returns one of the range-angle pairs in the self.beams list
        '''
        if self.beams != []:
            return self.beams.pop()
        else:
            return None



class OccGrid():    
    def __init__(self, robot, mapH, mapW, cellH, cellW):
        self.robot = robot        
        # Robot object : carries information about the robot


        self.mapH = int(mapH)
        self.mapW = int(mapW)
        # integers : map height and map width in number of cells
        

        self.m = np.ndarray((mapH, mapW), buffer=np.ones((mapH, mapW)) * 0.5)
        self.l = np.log(self.m) - np.log(1 - self.m)
        # mapH·mapW ndarrays : map hypothesis and log odds


        if cellH == None or cellH <= 0:
            print "WARNING! Cell height must be positive nonzero. Assuming 0.1m"
            cellH = 0.1
        self.cellH = np.double(cellH)
        # float : cell height in meters, defaults to 0.1
        
        
        if cellW == None or cellW <= 0:
            print "WARNING! Cell width must be positive nonzero. Assuming 0.1m"
            cellW = 0.1
        self.cellW = np.double(cellW)
        # float : cell width in meters, defaults to 0.1
        

        self.confidence = 2.
        # double : how likely a cell marked as occupied or free by a single 
        # reading is to actually be so. Translates to ~88%. Two readings would 
        # give ~98% and three would result in ~99%. As comparison, a cell marked
        # as free due to the robot's position is marked as free with 100% confidence.
        

        self.origin = [mapH//2, mapW//2]
        # array : indices of the cell that contains the [0,0] coordinates. It's
        # initially placed in the center of the map.



    def update(self):
        '''
        Updates the map using all values from the robot
        '''
        if self.robot.update():
            while True:
                beam = self.robot.next_beam()
                if beam == None:
                    break
                self.inverse_sensor_model(beam)
            return True        
        return False


    def cell_center(self, h, w):
        '''
        Returns the real-world coordinates of the center of cell m[h][w]
        '''
        y = -(h - self.origin[0]) * self.cellH
        x = (w - self.origin[1]) * self.cellW
        return x, y
        
        
        
    def corner(self, h, w, i):
        '''
        Returns the i-th corner of cell m[h][w]
        '''
        if i == 0: # top left corner
            x, y = self.cell_center(h, w)
            x -= self.cellW / 2.
            y += self.cellH / 2.
            return x, y
        if i == 1: # top right corner
            x, y = self.cell_center(h, w)
            x += self.cellW / 2.
            y += self.cellH / 2.
            return x, y
        if i == 2: # bottom right corner
            x, y = self.cell_center(h, w)
            x += self.cellW / 2.
            y -= self.cellH / 2.
            return x, y
        if i == 3: # bottom left corner
            x, y = self.cell_center(h, w)
            x -= self.cellW / 2.
            y -= self.cellH / 2.
            return x, y


    
    def cell(self, x, y):
        '''
        Returns the indices h and w of the cell containing the real-world
        coordinates x, y
        '''
        h = int(round(-(y / self.cellH) + self.origin[0]))
        w = int(round((x / self.cellW) + self.origin[1]))
        return h, w
        
    def beam_intersects(self, hit, robot, cell):
        '''
        Given the coordinates x, y of a beam hit, the robot and the coordinates
        h, w of a cell, returns True if all the corners of the cell are to the
        same side of the hit->robot vector and False otherwise.                
        '''
        hx, hy = hit
        rx, ry = robot
        h, w = cell
        
        c0 = side_of(hit, robot, self.corner(h, w, 0))
        c1 = side_of(hit, robot, self.corner(h, w, 1))        
        
        if not c0 and not c1: # beam is tangent to cell's top edge
            return False
        if c0 and c1 and c0 != c1:
            return True

        c2 = side_of(hit, robot, self.corner(h, w, 1))
        if not c1 and not c2: # beam is tangent to cell's right edge
            return False
        if (c0 and c2 and c0 != c2) or (c1 and c2 and c1 != c2):
            return True
        
        c3 = side_of(hit, robot, self.corner(h, w, 3))
        if not c2 and not c3: # beam is tangent to cell's bottom edge
            return False
        if not c3 and not c0: # beam is tangent to cell's left edge
            return False
        if (c0 and c3 and c0 != c3) or (c1 and c3 and c1 != c3) or (c2 and c3 and c2 != c3):
            return True
            
        return False

    def inverse_sensor_model(self, beam):
        '''
        Simple inverse sensor model better suited for lasers
        '''
        x, y, th = self.robot.pose
        z_max = self.robot.sensor_range
        
        z, psi = beam # beam length and angle relative to robot
        
        # • go through the intersected cells:
        #   + check which direction robot is relative to beam_hit (eg: SW)
        #   + check for occupancy:
        #     - if at the hit cell and z < z_max, occupied
        #     - if not at the hit cell, free
        #   + check each cell around the current cell for intersection
        #     (can be done using left-of test on each corner of each cell)
        #   + update coordinates list and move to the next intersected cell
        #   + stop when it arrives to the coordinates where the robot is
        
        hit = (x + (z * np.sin(psi + th)), y + (z * np.cos(psi + th)))
        # x and y coordinates of the beam hit
        rob = (x, y)
        #  x and y coordinates of the robot
        hit_h, hit_w = self.cell(hit[0], hit[1])
        # coordinates of the cell where the beam hit landed
        rob_h, rob_w = self.cell(x, y)
        # coordinates of the cell where the robot is
        inc_h, inc_w = (cmp(rob_h, hit_h), cmp(rob_w, hit_w))
        # grid increments to go from the hit to the robot
                
        h, w = hit_h, hit_w
        
        dist = abs(rob_h - hit_h) + abs(rob_w - hit_w)
        run = 0
        
#        print "*" * 65
#        print "hit = m[" + str(hit_h) + "][" + str(hit_w) + "]"
#        print "rob = m[" + str(rob_h) + "][" + str(rob_w) + "]"
        while(run < dist):
            run += 1
            if h == hit_h and w == hit_w and z < z_max:
#                print "m[" + str(h) + "][" + str(w) + "] : hit, occupied"
#                print "x",
                self.l[h][w] += self.confidence
            else:
#                print "m[" + str(h) + "][" + str(w) + "] : free"
#                print ".",
                self.l[h][w] -= self.confidence
                
            if (inc_h and self.beam_intersects(hit, rob, (h + inc_h, w))):
                h += inc_h # beam exits the cell horizontally
                continue
            if (inc_w and self.beam_intersects(hit, rob, (h, w + inc_w))):
                w += inc_w # beam exits the cell vertically
                continue
            if (self.beam_intersects(hit, rob, (h + inc_h, w + inc_w))):
                h += inc_h # beam exits the cell diagonally
                w += inc_w
                continue
            #break
        self.l[rob_h][rob_w] -= self.confidence
#        print "!"
#        print "m[" + str(rob_h) + "][" + str(rob_w) + "] : free (robot)"
#        print "*" * 65

    def recover_map(self):
        self.m = (1 / (np.exp(self.l) + 1))
        return self.m

    def trim_map(self):
        clean = self.m.copy()

        xmax = xmin = x = int(self.mapW / 2)
        while x > 0:
            x /= 2
            if (clean[xmax] == 0.5).all():
                xmax -= x
            else:
                xmax += x
            if (clean[xmin] == 0.5).all():
                xmin += x
            else:
                xmin -= x
                
        clean = clean[xmin:xmax + self.mapW / 100]
        
        ymax = ymin = y = int(self.mapH / 2)
        while y > 0:
            y /= 2
            if (clean[:,ymax] == 0.5).all():
                ymax -= y
            else:
                ymax += y
            if (clean[:,ymin] == 0.5).all():
                ymin += y
            else:
                ymin -= y
        
        clean = clean[:,ymin:ymax + (self.mapH / 100)]
        
        return clean
    
        

def simulation(filename, mw=100, mh=100, sw=0.5, sh=0.5):
    rob = Robot(filename)
    occ = OccGrid(rob, mw, mh, sw, sh)
    
    t = 0
    print "Running simulation. Timesteps elapsed:"
    while occ.update():
        t += 1
        print t

    im = PIL.Image.fromarray(occ.recover_map() * 255)
    im.show()
    
    return rob, occ