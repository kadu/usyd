function Element = find_elements2(Cell, thresh)

if (length(thresh) == 1)
    thresh = [thresh, 0];
end

O = Cell.OccuPoints; % Occupied points within Cell
F = [Cell.Robot, Cell.FreePoints]; % Free points within Cell
d = diff(Cell.Boundaries, 1, 2);

% Cells without datapoints get discarded
if isempty([O,F])
    Element = [];
    return
end



% Only cells with datapoints from this line onwards.



% Cells with only free points don't get divided any further unless they're
% too big
if isempty(O) && (thresh(2) == 0 || all(d < thresh(2)))
    Cell.Area = prod(diff(Cell.Boundaries, 1, 2));
    Cell.Occ = -numel(F) / Cell.Area;
    Cell.Data = F;
    Element = Cell;
    return
end

% Cells with only occupied points don't get divided any further unless
% they're too big.
if isempty(F) && (thresh(2) == 0 || all(d < thresh(2)))
    Cell.Area = prod(diff(Cell.Boundaries, 1, 2));
    Cell.Occ = numel(O)^2 / Cell.Area;
    Cell.Data = O;
    Element = Cell;
    return
end



% Cells smaller than the size threshold don't get divided
if all(d < thresh(1))
    Cell.Area = prod(diff(Cell.Boundaries, 1, 2));
    Cell.Occ = (numel(O) - numel(F))^2 / Cell.Area;
    Cell.Data = [O,F];
    Element = Cell;
    return
end



% These are all the base cases. All cells that arrive to this line will get
% sliced and go into the next level of recursion.



D = size(Cell.Boundaries,1); % number of dimensions
i = find(d == max(d));  % find longest edge
mid = mean(Cell.Boundaries(i,:)); % find its half point

% make 2 copies of Cell
CellA = Cell;
CellB = Cell;

% Cut them through the middle of the largest dimension
CellA.Boundaries(i,:) = [Cell.Boundaries(i,1), mid];
CellB.Boundaries(i,:) = [mid, Cell.Boundaries(i,2)];

% Divide the occupied points in Cell accordingly
if ~isempty(Cell.OccuPoints)
    CellA.OccuPoints = Cell.OccuPoints(:,Cell.OccuPoints(i,:) <= mid);
    CellB.OccuPoints = Cell.OccuPoints(:,Cell.OccuPoints(i,:)  > mid);
else
    % Everything must have the right dimensions!
    CellA.OccuPoints = zeros(D,0);
    CellB.OccuPoints = zeros(D,0);
end

% Divide the free points in Cell accordingly
if ~isempty(Cell.FreePoints)
    CellA.FreePoints = Cell.FreePoints(:,Cell.FreePoints(i,:) <= mid);
    CellB.FreePoints = Cell.FreePoints(:,Cell.FreePoints(i,:)  > mid);
else
    % Everything must have the right dimensions!
    CellA.FreePoints = zeros(D,0);
    CellB.FreePoints = zeros(D,0);
end

% Place the robot in the adequate cell
if ~isempty(Cell.Robot)
    CellA.Robot = Cell.Robot(:,Cell.Robot(i,:) <= mid);
    CellB.Robot = Cell.Robot(:,Cell.Robot(i,:)  > mid);
else
    CellA.Robot = zeros(D,0);
    CellB.Robot = zeros(D,0);
end

% dive further into recursion!
Element = [find_elements2(CellA, thresh), find_elements2(CellB, thresh)];