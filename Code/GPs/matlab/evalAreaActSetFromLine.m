




function [ActiveSet, RedundantSet] = evalAreaActSetFromLine(ActiveSet, RedundantSet,...
    Candidate, gp, Thresh, NumSamps,squashparams,ShowRedundantSet)

    TestPoints = [linspace(Candidate.Start(1),Candidate.End(1),NumSamps);...
        linspace(Candidate.Start(2),Candidate.End(2),NumSamps)];
    %Evaluate mf and vf before candidate is added
    [mfBefore,vfBefore] = gpeval(ActiveSet, gp,TestPoints);
    SquashedBefore= mfBefore;
    SquashedBefore((find(SquashedBefore >1))) = 1;
    SquashedBefore((find(SquashedBefore<-1))) = -1;
    ProbOccBefore= sigmoid(squashparams(1),squashparams(2),1,SquashedBefore,vfBefore);
    
    
    ActivePlusCandSet = mergeAreaScan2Set(ActiveSet,Candidate,gp);
    
    
    %Evaluate mf and vf after candidate is added
    [mfAfter,vfAfter] = gpeval(ActivePlusCandSet,gp,TestPoints);   
    SquashedAfter= mfAfter;
    SquashedAfter((find(SquashedAfter >1))) = 1;
    SquashedAfter((find(SquashedAfter<-1))) = -1;
    ProbOccAfter= sigmoid(squashparams(1),squashparams(2),1,SquashedAfter,vfAfter);
    
%     %Check KL Divergence
%     D = max(0.5*(log(vfAfter./vfBefore)+...
%         (mfAfter.^2+mfBefore.^2-2*mfBefore.*mfAfter+vfBefore)./vfAfter-1));
    
    if sum(abs(ProbOccAfter-ProbOccBefore)>Thresh ) 
        %Add candidate to active set
        ActiveSet = ActivePlusCandSet;
    end
    if ShowRedundantSet == 1
        RedundantSet=mergeAreaScan2Set(RedundantSet,Candidate,gp);
    end
    
    