function[X, y] = InitAreawiseData(TrainingData, thresh, plotfig)

res = thresh(1); % Resolution to split the beams

if nargin < 3
    plotfig=0;
end

D = size(TrainingData.OccupiedPoints,1);

bound = zeros(D,2);
points = [TrainingData.LibraryOfEndPoints TrainingData.LibraryOfStartPoints];
for dim=1:D
    min_i = min(points(dim,:)) - res;
    max_i = max(points(dim,:)) + res;
    bound(dim,:) = [min_i, max_i];
end
Element0.Boundaries = bound;

Element0.OccuPoints = TrainingData.OccupiedPoints;

% Element0.Robot = zeros(D,max(TrainingData.PoseNumber));

Element0.FreePoints = ProbeBeams(TrainingData, res);


X.Element = [];

X.Element = [X.Element, find_elements(Element0, thresh)];
y.Element = [X.Element.Occ];
y.IntEl = y.Element .* [X.Element.Area];
X.M = numel(y.Element);

X.Point.Coord = TrainingData.OccupiedPoints;
X.Point.PoseNumber = TrainingData.PoseNumber;
y.Point = ones(1,numel(TrainingData.PoseNumber));

X.N = numel(y.Point);

if plotfig
    figure(plotfig+1000)
    plot(TrainingData.OccupiedPoints(1,:),TrainingData.OccupiedPoints(2,:),'+')
    hold on
    plot(TrainingData.LibraryOfStartPoints(1,:),TrainingData.LibraryOfStartPoints(2,:),'go')
    plot([TrainingData.LibraryOfStartPoints(1,:);TrainingData.LibraryOfEndPoints(1,:)],[TrainingData.LibraryOfStartPoints(2,:);TrainingData.LibraryOfEndPoints(2,:)],'r')
    axis equal
    title('Rangefinder Data')
    figure(plotfig);
    hold on;
    plot_element(X.Element, plotfig);
    axis equal
    title('Elements')
end

