load('TestPoints')
load('Test1');
load('Test2');
load('Test3');
load('Test4');
% load('Test5');
% load('Test6');
% load('Test7');
% load('Test8');
% load('./Demo/roombox/Old/Test9');
% load('./Demo/roombox/Old/Test10');
% load('Testx');
% load('Testy');

% x=tx;
% 
% x.Element = struct();
% x.Point.Coord = tx;
% x.Element.Area = [];
% x.Element.Data =  zeros(2,0);
% x.M = 0;
% x.N = size(x.Point.Coord, 2);
% 
% par1 = Test1.par;
% par2 = Test2.par;
% par3 = Test3.par;
% par4 = Test4.par;
% par5 = Test5.par;
% par6 = Test6.par;
% par7 = Test7.par;
% par8 = Test8.par;
% par9 = Test9.par;
% par10 = Test10.par;

% X1 = Test1.X;
% X2 = Test2.X;
% X3 = Test3.X;
% X4 = Test4.X;
% X5 = Test5.X;
% X6 = Test6.X;
% X7 = Test7.X;
% X8 = Test8.X;
% X9 = Test9.X;
% X10 = Test10.X;

% y1 = Test1.y;
% y2 = Test2.y;
% y3 = Test3.y;
% y4 = Test4.y;
% y5 = Test5.y;
% y6 = Test6.y;
% y7 = Test7.y;
% y8 = Test8.y;
% y9 = Test9.y;
% y10 = Test10.y;
% 
% tic; [~, mf1, vf1] = areagpeval(X1,y1, @cov_nDmat3, par1, [], x, []); t1=toc
% tic; [~, mf2, vf2] = areagpeval(X2,y2, @cov_nDsqExp, par2, [], x, []); t2=toc
% tic; [~, mf3, vf3] = areagpeval(X3,y3, @cov_nDmat3, par3, [], x, []); t3=toc
% tic; [~, mf4, vf4] = areagpeval(X4,y4, @cov_nDsqExp, par4, [], x, []); t4=toc
% tic; [~, mf5, vf5] = areagpeval(X5,y5, @cov_nDsqExp, par5, [], x, []); t5=toc
% tic; [~, mf6, vf6] = areagpeval(X6,y6, @cov_nDsqExp, par6, [], x, []); t6=toc
% tic; [~, mf7, vf7] = areagpeval(X7,y7, @cov_nDmat3, par7, [], x, []); t7=toc
% tic; [~, mf8, vf8] = areagpeval(X8,y8, @cov_nDmat3, par8, [], x, []); t8=toc
% tic; [~, mf9, vf9] = areagpeval(X9,y9, @cov_nDsqExp, par9, [], x, []); t9=toc
% tic; [~, mf10, vf10] = areagpeval(X10,y10, @cov_nDsqExp, par10, [], x, []); t10=toc

% Mesh = x.Mesh;

mf1 = Test1.mf;
mf2 = Test2.mf;
mf3 = Test3.mf;
mf4 = Test4.mf;
% mf5 = Test5.mf;
% mf6 = Test6.mf;
% mf7 = Test7.mf;
% mf8 = Test8.mf;
% mf9 = Test9.mf;
% mf10 = Test10.mf;

vf1 = Test1.vf;
vf2 = Test2.vf;
vf3 = Test3.vf;
vf4 = Test4.vf;
% vf5 = Test5.vf;
% vf6 = Test6.vf;
% vf7 = Test7.vf;
% vf8 = Test8.vf;
% vf9 = Test9.vf;
% vf10 = Test10.vf;


% lmf1 = (1 + exp(-mf1)).^(-1);
% lmf2 = (1 + exp(-mf2)).^(-1);
% lmf3 = (1 + exp(-mf3)).^(-1);
% lmf4 = (1 + exp(-mf4)).^(-1);
% lmf5 = (1 + exp(-mf5)).^(-1);
% lmf6 = (1 + exp(-mf6)).^(-1);
% lmf7 = (1 + exp(-mf7)).^(-1);
% lmf8 = (1 + exp(-mf8)).^(-1);
% lmf9 = (1 + exp(-mf9)).^(-1);
% lmf10 = (1 + exp(-mf10)).^(-1);

a = 5;
b = -0.25;
% 
% vf1 = vf1 - min(vf1);
vf2 = vf2 - min(vf2);
% vf3 = vf3 - min(vf3);
vf4 = vf4 - min(vf4);
% vf5 = vf5 - min(vf5);
% vf6 = vf6 - min(vf6);
% vf7 = vf7 - min(vf7);
% vf8 = vf8 - min(vf8);
% vf9 = vf9 - min(vf9);
% vf10 = vf10 - min(vf10);

lmf1 = sigmoid(a, b, 1, mf1, vf1);
lmf2 = sigmoid(a, b, 1, mf2, vf2);
lmf3 = sigmoid(a, b, 1, mf3, vf3);
lmf4 = sigmoid(a, b, 1, mf4, vf4);
% lmf5 = sigmoid(a, b, 1, mf5, vf5);
% lmf6 = sigmoid(a, b, 1, mf6, vf6);
% lmf7 = sigmoid(a, b, 1, mf7, vf7);
% lmf8 = sigmoid(a, b, 1, mf8, vf8);
% lmf9 = sigmoid(a, b, 1, mf9, vf9);
% lmf10 = sigmoid(a, b, 1, mf10, vf10);


% ogm1 = (lmf1 > 0.33)/2 + (lmf1 > 0.66)/2;
% ogm2 = (lmf2 > 0.33)/2 + (lmf2 > 0.66)/2;
% ogm3 = (lmf3 > 0.33)/2 + (lmf3 > 0.66)/2;
% ogm4 = (lmf4 > 0.33)/2 + (lmf4 > 0.66)/2;
% ogm5 = (lmf5 > 0.33)/2 + (lmf5 > 0.66)/2;
% ogm6 = (lmf6 > 0.33)/2 + (lmf6 > 0.66)/2;
% ogm7 = (lmf7 > 0.33)/2 + (lmf7 > 0.66)/2;
% ogm8 = (lmf8 > 0.33)/2 + (lmf8 > 0.66)/2;
% ogm9 = (lmf9 > 0.33)/2 + (lmf9 > 0.66)/2;
% ogm10 = (lmf10 > 0.33)/2 + (lmf10 > 0.66)/2;

[acc1, r951] = Accuracy(lmf1',ty)
[acc2, r952] = Accuracy(lmf2',ty)
[acc3, r953] = Accuracy(lmf3,ty')
[acc4, r954] = Accuracy(lmf4,ty')
% [acc5, r955] = Accuracy(lmf5',ty)
% [acc6, r956] = Accuracy(lmf6',ty)
% [acc7, r957] = Accuracy(lmf7',ty)
% [acc8, r958] = Accuracy(lmf8',ty)
% [acc9, r959] = Accuracy(lmf9',ty)
% [acc0, r950] = Accuracy(lmf10',ty)
% 
% plotroc(ty',lmf1');
% plotroc(ty',lmf2');
% % figure(10003); plotroc(ty,lmf3');
% % figure(10004); plotroc(ty,lmf4');
% % figure(10005); plotroc(ty,lmf5');
% % figure(10006); plotroc(ty,lmf6');
% % figure(10007); plotroc(ty,lmf7');
% % figure(10008); plotroc(ty,lmf8');
% % % figure(10009); plotroc(ty,lmf9');
% % % figure(10010); plotroc(ty,lmf10');
% % 
% % 
% figure(10); surf(Mesh.x, Mesh.y, reshape(lmf1, size(Mesh.x)), reshape(lmf1, size(Mesh.x))); view(0,90); colorbar; axis equal; title('Probability of Occupancy');
% figure(20); surf(Mesh.x, Mesh.y, reshape(lmf2, size(Mesh.x)), reshape(lmf2, size(Mesh.x))); view(0,90); colorbar; axis equal; title('Probability of Occupancy');
% % figure(30); surf(Mesh.x, Mesh.y, reshape(lmf3, size(Mesh.x)), reshape(lmf3, size(Mesh.x))); view(0,90); colorbar; axis equal; title('Probability of Occupancy');
% % figure(40); surf(Mesh.x, Mesh.y, reshape(lmf4, size(Mesh.x)), reshape(lmf4, size(Mesh.x))); view(0,90); colorbar; axis equal; title('Probability of Occupancy');
% % figure(60); surf(Mesh.x, Mesh.y, reshape(lmf6, size(Mesh.x)), reshape(lmf6, size(Mesh.x))); view(0,90); colorbar; axis equal; title('Probability of Occupancy');
% % figure(70); surf(Mesh.x, Mesh.y, reshape(lmf7, size(Mesh.x)), reshape(lmf7, size(Mesh.x))); view(0,90); colorbar; axis equal; title('Probability of Occupancy');
% % figure(80); surf(Mesh.x, Mesh.y, reshape(lmf8, size(Mesh.x)), reshape(lmf8, size(Mesh.x))); view(0,90); colorbar; axis equal; title('Probability of Occupancy');
% % % figure(90); surf(Mesh.x, Mesh.y, reshape(lmf9, size(Mesh.x)), reshape(lmf9, size(Mesh.x))); view(0,90); colorbar; axis equal; title('Probability of Occupancy');
% % % figure(100); surf(Mesh.x, Mesh.y, reshape(lmf10, size(Mesh.x)), reshape(lmf10, size(Mesh.x))); view(0,90); colorbar; axis equal; title('Probability of Occupancy');
% % % 
% % % 
% figure(11); surf(Mesh.x, Mesh.y, reshape(vf1, size(Mesh.x)), reshape(vf1, size(Mesh.x))); view(0,90); colorbar; axis equal; title('Predictive Variance');
% figure(21); surf(Mesh.x, Mesh.y, reshape(vf2, size(Mesh.x)), reshape(vf2, size(Mesh.x))); view(0,90); colorbar; axis equal; title('Predictive Variance');
% % figure(31); surf(Mesh.x, Mesh.y, reshape(vf3, size(Mesh.x)), reshape(vf3, size(Mesh.x))); view(0,90); colorbar; axis equal; title('Predictive Variance');
% % figure(41); surf(Mesh.x, Mesh.y, reshape(vf4, size(Mesh.x)), reshape(vf4, size(Mesh.x))); view(0,90); colorbar; axis equal; title('Predictive Variance');
% % figure(51); surf(Mesh.x, Mesh.y, reshape(vf5, size(Mesh.x)), reshape(vf5, size(Mesh.x))); view(0,90); colorbar; axis equal; title('Predictive Variance');
% % figure(61); surf(Mesh.x, Mesh.y, reshape(vf6, size(Mesh.x)), reshape(vf6, size(Mesh.x))); view(0,90); colorbar; axis equal; title('Predictive Variance');
% % figure(71); surf(Mesh.x, Mesh.y, reshape(vf7, size(Mesh.x)), reshape(vf7, size(Mesh.x))); view(0,90); colorbar; axis equal; title('Predictive Variance');
% % figure(81); surf(Mesh.x, Mesh.y, reshape(vf8, size(Mesh.x)), reshape(vf8, size(Mesh.x))); view(0,90); colorbar; axis equal; title('Predictive Variance');
% % % figure(91); surf(Mesh.x, Mesh.y, reshape(vf7, size(Mesh.x)), reshape(vf7, size(Mesh.x))); view(0,90); colorbar; axis equal; title('Predictive Variance');
% % % figure(101); surf(Mesh.x, Mesh.y, reshape(vf8, size(Mesh.x)), reshape(vf8, size(Mesh.x))); view(0,90); colorbar; axis equal; title('Predictive Variance');
% % % 
% % % 
% figure(12); surf(Mesh.x, Mesh.y, reshape(ogm1, size(Mesh.x)), reshape(ogm1, size(Mesh.x))); view(0,90); colormap(flipud(gray)); axis equal; title('Occupancy Map');
% figure(22); surf(Mesh.x, Mesh.y, reshape(ogm2, size(Mesh.x)), reshape(ogm2, size(Mesh.x))); view(0,90); colormap(flipud(gray)); axis equal; title('Occupancy Map');
% % figure(32); surf(Mesh.x, Mesh.y, reshape(ogm3, size(Mesh.x)), reshape(ogm3, size(Mesh.x))); view(0,90); colormap(flipud(gray)); axis equal; title('Occupancy Map');
% % figure(42); surf(Mesh.x, Mesh.y, reshape(ogm4, size(Mesh.x)), reshape(ogm4, size(Mesh.x))); view(0,90); colormap(flipud(gray)); axis equal; title('Occupancy Map');
% % figure(52); surf(Mesh.x, Mesh.y, reshape(ogm5, size(Mesh.x)), reshape(ogm5, size(Mesh.x))); view(0,90); colormap(flipud(gray)); axis equal; title('Occupancy Map');
% % figure(62); surf(Mesh.x, Mesh.y, reshape(ogm6, size(Mesh.x)), reshape(ogm6, size(Mesh.x))); view(0,90); colormap(flipud(gray)); axis equal; title('Occupancy Map');
% % figure(72); surf(Mesh.x, Mesh.y, reshape(ogm7, size(Mesh.x)), reshape(ogm7, size(Mesh.x))); view(0,90); colormap(flipud(gray)); axis equal; title('Occupancy Map');
% % figure(82); surf(Mesh.x, Mesh.y, reshape(ogm8, size(Mesh.x)), reshape(ogm8, size(Mesh.x))); view(0,90); colormap(flipud(gray)); axis equal; title('Occupancy Map');
% % figure(92); surf(Mesh.x, Mesh.y, reshape(ogm9, size(Mesh.x)), reshape(ogm9, size(Mesh.x))); view(0,90); colormap(flipud(gray)); axis equal; title('Occupancy Map');
% % figure(102); surf(Mesh.x, Mesh.y, reshape(ogm10, size(Mesh.x)), reshape(ogm10, size(Mesh.x))); view(0,90); colormap(flipud(gray)); axis equal; title('Occupancy Map');
% 



