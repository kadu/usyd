function K = cov_element2element(E1, E2, cov_fun, par)

N = numel([E1.Area]);
M = numel([E2.Area]);
K = zeros(N,M);


% meen1 = zeros(N,1);
% meen2 = zeros(M,1);
% 
% for i = 1:N
%     meen1 = mean(E1(i).Data,2);
% end
% 
% for j = 1:M
%     meen2 = mean(E2(i).Data,2);
% end
% 
% K = cov_fun(meen1, meen2, par);

for i = 1:N
    for j = 1:M
        K(i,j)= mean(mean(cov_fun(E1(i).Data, E2(j).Data, par)));
    end
end