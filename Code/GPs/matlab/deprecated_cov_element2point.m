% Calculates an approximation to the covariance between an n-dimensional
% element of the data and a point.
% 
% This uses the approximation in Muandet and Schoelkopf (UAI, 2013) that
% the integral of a kernel over a probability distribution can be
% approximated by the simple average between the kernels of all elements
% in the distribution.
%
% This function takes as input a set of points X and a set of elements E
% defined by two opposing boundary coordinates and the input data points
% contained inside of it.
%
%INPUTS
% X - D x N test points
% E - M input elements containing:
%  XE - D x M' points inside element E
% cov_fun - the covariance function to be used
% par - cell with the parameters for the covariance function
%
%OUTPUT
% K - (approx) integral covariance between point X and element E
%
% Carlos Vido - 


function K = cov_element2point(E, X, par)

if class(E) == 'struct'
    K = cov_point2element(X, E, @cov_nn, par)';
else
    K = cov_nn(E, X, par);
end