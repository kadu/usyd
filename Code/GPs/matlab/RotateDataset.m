% Takes a 2D dataset and rotates it by an arbitrary angle

function[NewDataset] = RotateDataset(Dataset, angle, plotfig)

if nargin < 3
    plotfig=0;
end

NewDataset.PoseNumber = Dataset.PoseNumber;
NewDataset.LibraryOfStartPoints = Dataset.LibraryOfStartPoints;
NewDataset.LibraryOfEndPoints = Dataset.LibraryOfEndPoints;

for i = 1:length(Dataset.PoseNumber)
    NewDataset.LibraryOfStartPoints(:,i) = [(NewDataset.LibraryOfStartPoints(1,i) * cos(angle) - NewDataset.LibraryOfStartPoints(2,i) * sin(angle));(NewDataset.LibraryOfStartPoints(1,i) * sin(angle) + NewDataset.LibraryOfStartPoints(2,i) * cos(angle))];
    NewDataset.LibraryOfEndPoints(:,i) = [(NewDataset.LibraryOfEndPoints(1,i) * cos(angle) - NewDataset.LibraryOfEndPoints(2,i) * sin(angle));(NewDataset.LibraryOfEndPoints(1,i) * sin(angle) + NewDataset.LibraryOfEndPoints(2,i) * cos(angle))];
end

NewDataset.OccupiedPoints = NewDataset.LibraryOfEndPoints;


if plotfig
    figure(plotfig)
    clf(plotfig)
    subplot(2,1,1)
    plot(Dataset.OccupiedPoints(1,:),Dataset.OccupiedPoints(2,:),'+')
    hold on
    plot(Dataset.LibraryOfStartPoints(1,:),Dataset.LibraryOfStartPoints(2,:),'go')
    plot([Dataset.LibraryOfStartPoints(1,:);Dataset.LibraryOfEndPoints(1,:)],[Dataset.LibraryOfStartPoints(2,:);Dataset.LibraryOfEndPoints(2,:)],'r')
    axis equal
    title('Original Dataset')
    subplot(2,1,2)
    plot(NewDataset.OccupiedPoints(1,:),NewDataset.OccupiedPoints(2,:),'+')
    hold on
    plot(NewDataset.LibraryOfStartPoints(1,:),NewDataset.LibraryOfStartPoints(2,:),'go')
    plot([NewDataset.LibraryOfStartPoints(1,:);NewDataset.LibraryOfEndPoints(1,:)],[NewDataset.LibraryOfStartPoints(2,:);NewDataset.LibraryOfEndPoints(2,:)],'r')
    axis equal
    title('Rotated Dataset')
end
