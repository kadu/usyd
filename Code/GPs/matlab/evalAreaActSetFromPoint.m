


function [ActiveSet, RedundantSet] = evalAreaActSetFromPoint(ActiveSet, RedundantSet,...
    Candidate, gp, Thresh, squashparams,ShowRedundantSet)

    TestPoint = Candidate.Point;
    %Evaluate mf and vf before candidate is added
    [mfBefore,vfBefore] = gpeval(ActiveSet, gp,TestPoint);
    SquashedBefore= mfBefore;
    SquashedBefore((find(SquashedBefore >1))) = 1;
    SquashedBefore((find(SquashedBefore<-1))) = -1;
    ProbOccBefore= sigmoid(squashparams(1),squashparams(2),1,SquashedBefore,vfBefore);
    
    ActivePlusCandSet = mergeAreaScan2Set(ActiveSet,Candidate,gp);
    
    %Evaluate mf and vf after candidate is added
    [mfAfter,vfAfter] = gpeval(ActivePlusCandSet,gp,TestPoint);   
    SquashedAfter= mfAfter;
    SquashedAfter((find(SquashedAfter >1))) = 1;
    SquashedAfter((find(SquashedAfter<-1))) = -1;
    ProbOccAfter= sigmoid(squashparams(1),squashparams(2),1,SquashedAfter,vfAfter);
    
%     %Check KL Divergence
%     D = 0.5*(log(vfAfter/vfBefore)+...
%         (mfAfter^2+mfBefore^2-2*mfBefore*mfAfter+vfBefore)/vfAfter-1);
    
    if abs(ProbOccAfter-ProbOccBefore)>Thresh 
        %Add candidate to active set
        ActiveSet = ActivePlusCandSet;
    end
    if ShowRedundantSet ==1  
        RedundantSet=mergeAreaScan2Set(RedundantSet,Candidate,gp);
    end