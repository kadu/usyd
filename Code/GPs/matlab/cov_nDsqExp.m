function K = cov_nDsqExp(X1, X2, par, diag)

if nargin < 4
    diag = false;
end

K = cov_nD(X1, X2, @cov_sqexp, par, diag);