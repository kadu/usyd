function X = buildActiveSet(X,y,gp,ScanNum,ActThresh,squashparams,ActSwitch,NodeWeightArray,ShowRedundantSet)

% disp('*Function: buildActiveSet')
% pause;

%Divide observations into old observations and new observations
NewScan.Element = X.Element([X.Element.PoseNum] == ScanNum);
NewScan.Point = X.Point;

NewScan.yPoint = y.Point;
NewScan.yIntEl = y.IntEl([X.Element.PoseNum] == ScanNum);
NewScan.yElement = y.Element([X.Element.PoseNum] == ScanNum);

if ActSwitch == 1 %Switch for active selection
    %     disp('*ActSwitch == 1');
    %     pause;
    for Obs = 1:size(NewScan.Start,2)
        size(NewScan.Start,2)-Obs
        if Obs==1 && ScanNum == 1 %Initial observation
            
            InitialObs.Element = NewScan.Element(:,Obs);
            InitialObs.Point = NewScan.Point(:,Obs);
            InitialObs.yPoint = NewScan.yPoint(Obs);
            InitialObs.yIntEl = NewScan.yIntEl(Obs);
            InitialObs.yElement = NewScan.yElement(:,Obs);
            
            [X.ActiveSet]= initialiseAreaActSet(InitialObs, gp);
            X.RedundantSet = X.ActiveSet; %Bit of a hack to just initialise
            %             disp('*InitialObs ActiveSet initialised');
            %             pause;
            continue
        end
        
        
        %First check a point
        CandidatePoint.Element = struct();
        CandidatePoint.Point = NewScan.Point(:,Obs);
        CandidatePoint.yPoint = NewScan.yPoint(Obs) ;
        CandidatePoint.yIntEl = [];
        
        
        [X.ActiveSet, X.RedundantSet] = evalAreaActSetFromPoint(...
            X.ActiveSet, X.RedundantSet,...
            CandidatePoint, gp, ActThresh.Point,...
            squashparams,ShowRedundantSet);
        
        %Then check a line
        CandidateLine.Element = NewScan.Element(:,Obs);
        CandidateLine.Point = [] ;
        CandidateLine.yPoint = [];
        CandidateLine.yIntSeg = NewScan.yIntEl(Obs);
        
        [X.ActiveSet, X.RedundantSet] = evalAreaActSetFromLine(X.ActiveSet, X.RedundantSet,...
            CandidateLine, gp, ActThresh.Line, ActThresh.NumSamples,squashparams,...
            ShowRedundantSet);
        
        %         X.ActiveSet =X ActiveSet;
        %         X.RedundantSet = RedundantSet;
        %         disp(['*Obs ' num2str(Obs) ' initialised']);
        %         pause;
        
    end
else
    %     disp('*ActSwitch != 1');
    %     pause;
    
    if ScanNum ==1
        X.ActiveSet.X= [];
        X.ActiveSet.y = [];
        X.ActiveSet.ObsId = [];
        X.ActiveSet.L = [];
        X.ActiveSet.alpha = [];
    end
    X.ActiveSet = mergeAreaScan2Set(X.ActiveSet,NewScan,gp)
    %     disp('*ActiveSet created');
    %     pause;
    
    %     disp('*Function buildActiveSet: ok');
    %     pause;
end