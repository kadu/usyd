# -*- coding: utf-8 -*-
"""
Created on Wed Mar 18 16:33:47 2015

@author: kadu
"""

import numpy as np
import scipy.io as sio
import matplotlib.pyplot as plt

#FIELD_OF_VIEW = 180 # deg
FIELD_OF_VIEW = np.pi # deg
MAX_RANGE     = 80

#SCAN_ANGLE_STEP = 10 # deg
SCAN_ANGLE_STEP = np.pi / 36 # rad
SCAN_COUNT_STEP = 10
MIN_STEP_DIST   = 0.0
#MIN_STEP_ANGLE  = 30 # deg
MIN_STEP_ANGLE  = np.pi / 6. # rad
SCANS_PER_POSE  = 36

class struct(dict):
    def __getattr__(self, name):
        return self.__getitem__(name)
    def __setattr__(self, name, value):
        self.__setitem__(name, value)


def dist(a, b):
    return np.linalg.norm(a-b)

readings = []
#for line in open("intel_lab_data.wtf"):
#    li = line.strip()
#    if li.startswith("#LASER") or li.startswith("#ROBOT"):
#        readings += [li]

for line in open("belgioioso.gfs.log"):
    li = line.strip()
    if li.startswith("FLASER"):
        readings += [li]

pose = np.zeros((3, 0))
hits = np.zeros((SCANS_PER_POSE, 0))

#r = len(readings) - 1
##print (r+1)/2
#i = 0
#while(i < r):
#    while readings[i].startswith("#ROBOT") and readings[i+1].startswith("#ROBOT"):
#        readings.pop(i)
#        r -= 1
#    if readings[i].startswith("#ROBOT"):
#        pose = np.append(pose, np.array([map(float, readings[i].split(' ')[1:])]).T, 1)
#    elif readings[i].startswith("#LASER"):
#        hits = np.append(hits, np.array([map(float, readings[i].split(' ')[3::SCAN_ANGLE_STEP])]).T, 1)
#    i += 1

for rd in readings:
    rd = map(float, rd.split(' ')[1:-3])
    pose = np.append(pose, np.array([rd[-3:]]).T, 1)
    hits = np.append(hits, np.array([rd[2:-6:SCAN_COUNT_STEP]]).T, 1)

i = 0
r = pose.shape[-1]
keep = np.array([True] * r)
while(i < r - 1):
    j = i+1
    while j < r and (dist(pose[:-1,i], pose[:-1,j]) < MIN_STEP_DIST and dist(pose[-1,i], pose[-1,j]) < MIN_STEP_ANGLE):
        keep[j] = False
        j += 1
    i = j

#print i

pose = pose[:,keep]
hits = hits[:,keep]

#print pose.shape[-1]

#pose[:-1,:] /= 1.0
#hits /= 10.0
#pose[-1,:] *= (np.pi / 180.0)

l = pose.shape[-1]

s0 = []
sn = []
op = []
pn = []
for i in range(l):
    oc = []
    for j in range(len(hits[:,i])):
        if hits[j,i] > MAX_RANGE:
            continue
        s0 += [pose[:-1,i]]
        ang = pose[-1,i] - (FIELD_OF_VIEW / 2.) + (j * SCAN_ANGLE_STEP)
        occ = pose[:-1,i] + [hits[j,i] * x for x in [np.cos(ang), np.sin(ang)]]
        sn += [occ]
        pn += [i + 1]

op += sn

TrainingData = struct()
TrainingData.PoseNumber           = np.array(pn)
TrainingData.OccupiedPoints       = np.array(op).T
TrainingData.LibraryOfEndPoints   = np.array(sn).T
TrainingData.LibraryOfStartPoints = np.array(s0).T

x = []
y = []
for head, tail in zip(s0, sn):
    x += [head[0]]
    x += [tail[0]]
    x += [None]
    y += [head[1]]
    y += [tail[1]]
    y += [None]
plt.plot(x,y)
plt.show()    

sio.savemat('BelTestData.mat', {'TrainingData': TrainingData})