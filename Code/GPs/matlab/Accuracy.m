function [acc, rate95] = Accuracy(prediction, ground_truth)

[tpr, fpr] = roc(ground_truth, prediction);

rate95 = min(fpr(tpr==0.95));

acc = trapz(fpr,tpr);