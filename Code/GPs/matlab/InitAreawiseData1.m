function[X y] = InitAreawiseData(TrainingData, thresh, plotfig)

if nargin == 2
    plotfig=0;
end
if plotfig
    figure(plotfig)
    hold on
end

X.Start = [];
X.End = [];
X.Point = [];
X.PoseNum = [];
X.Element = [];
y.Point = [];
y.Segment = [];
y.Element = [];
for i = 1:size(TrainingData.Cells,2)
    Elements = find_elements(TrainingData.Cells(i), thresh);
    for el = Elements
        S = el.Boundaries(:,1);
        E = el.Boundaries(:,2);
        O = (2 * el.Occ) - 1;
        n = size(el.Hits, 2) + size(el.Robot, 2);
        
        X.Point   = [X.Point, el.Hits, el.Robot];
        X.Start   = [X.Start, repmat(S, 1, n)];
        X.End     = [X.End, repmat(E, 1, n)];
        X.PoseNum = [X.PoseNum, repmat(i, 1, n)];
        X.Element = [X.Element, repmat(el,1, n)];
        
        y.Point = [y.Point, ones(1,n)];
        y.Segment = [y.Segment, repmat(O, 1, n)];
        y.Element = [y.Element, repmat(O, 1, n)];
        
        if(plotfig)
            [a, b] = S';
            [c, d] = E';
            if ~isempty(el.Hits)
                p = patch([a, a, c, c], [b, d, d, b], [1, 0.9, 1]);
                scatter(el.Hits(1,:), el.Hits(2,:), [], 'r', 'x');
            else
                p = patch([a, a, c, c], [b, d, d, b], [0.9, 1, 0.9]);
            end
            if ~isempty(el.Robot)
                scatter(el.Robot(1),el.Robot(2), [], 'g');
            end
            set(p, 'FaceAlpha', 0.1, 'LineStyle', ':');
        end
    end
end

% Create target vector for line segments
y.IntSeg = y.Segment .* prod(X.Start - X.End);
