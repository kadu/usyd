addpath('../..')

load('TestPoints');
% load('Test1');
% load('Test2');
load('Old/Test3');
% load('Test4');
% load('Test5');
% load('Test6');
% load('Test7');
% load('Test8');

% par1 = Test1.par;
% par2 = Test2.par;
par3 = Test3.par;
% par4 = Test4.par;
% par5 = Test5.par;
% par6 = Test6.par;
% par7 = Test7.par;
% par8 = Test8.par;

% X1 = Test1.X;
% X2 = Test2.X;
X3 = Test3.X;
% X4 = Test4.X;
% X5 = Test5.X;
% X6 = Test6.X;
% X7 = Test7.X;
% X8 = Test8.X;

% y1 = Test1.y;
% y2 = Test2.y;
y3 = Test3.y;
% y4 = Test4.y;
% y5 = Test5.y;
% y6 = Test6.y;
% y7 = Test7.y;
% y8 = Test8.y;

% tic; [lml, mf1, vf1] = areagpeval(X1,y1, @cov_nDmat3, par1, [], x, []); t1=toc
% tic; [lml, mf2, vf2] = areagpeval(X2,y2, @cov_nDmat3, par2, [], x, []); t2=toc
tic; [lml, mf3, vf3] = areagpeval(X3,y3, @cov_nDsqExp, par3, [], x, []); t3=toc
% tic; [lml, mf4, vf4] = areagpeval(X4,y4, @cov_nDsqExp, par4, [], x, []); t4=toc
% tic; [lml, mf5, vf5] = areagpeval(X5,y5, @cov_nD_nn, par5, [], x, []); t5=toc
% tic; [lml, mf6, vf6] = areagpeval(X6,y6, @cov_nD_nn, par6, [], x, []); t6=toc
% tic; [lml, mf7, vf7] = areagpeval(X7,y7, @cov_nDmat3, par7, [], x, []); t7=toc
% tic; [lml, mf8, vf8] = areagpeval(X8,y8, @cov_nDmat3, par8, [], x, []); t8=toc

Mesh = x.Mesh;

% lmf1 = (1 + exp(-mf1)).^(-1);
% lmf2 = (1 + exp(-mf2)).^(-1);
% lmf3 = (1 + exp(-mf3)).^(-1);
% lmf4 = (1 + exp(-mf4)).^(-1);
% lmf5 = (1 + exp(-mf5)).^(-1);
% lmf6 = (1 + exp(-mf6)).^(-1);
% lmf7 = (1 + exp(-mf7)).^(-1);
% lmf8 = (1 + exp(-mf8)).^(-1);

% lmf1 = sigmoid(1, 1, 1, mf1, vf1);
% lmf2 = sigmoid(1, 1, 1, mf2, vf2);
lmf3 = sigmoid(1, 1, 1, mf3, vf3);
% lmf4 = sigmoid(1, 1, 1, mf4, vf4);
% lmf5 = sigmoid(1, 1, 1, mf5, vf5);
% lmf6 = sigmoid(1, 1, 1, mf6, vf6);
% lmf7 = sigmoid(1, 1, 1, mf7, vf7);
% lmf8 = sigmoid(1, 1, 1, mf8, vf8);


% ogm1 = (lmf1 > 0.33)/2 + (lmf1 > 0.66)/2;
% ogm2 = (lmf2 > 0.33)/2 + (lmf2 > 0.66)/2;
ogm3 = (lmf3 > 0.33)/2 + (lmf3 > 0.66)/2;
% ogm4 = (lmf4 > 0.33)/2 + (lmf4 > 0.66)/2;
% ogm5 = (lmf5 > 0.33)/2 + (lmf5 > 0.66)/2;
% ogm6 = (lmf6 > 0.35)/2 + (lmf6 > 0.65)/2;
% ogm7 = (lmf7 > 0.35)/2 + (lmf7 > 0.65)/2;
% ogm8 = (lmf8 > 0.35)/2 + (lmf8 > 0.65)/2;


% figure(10); surf(Mesh.x, Mesh.y, reshape(mf1, size(Mesh.x)), reshape(mf1, size(Mesh.x))); view(0,90); colorbar; axis equal; title('Probability of Occupancy');
% figure(20); surf(Mesh.x, Mesh.y, reshape(mf2, size(Mesh.x)), reshape(mf2, size(Mesh.x))); view(0,90); colorbar; axis equal; title('Probability of Occupancy');
figure(30); surf(Mesh.x, Mesh.y, reshape(lmf3, size(Mesh.x)), reshape(lmf3, size(Mesh.x))); view(0,90); colorbar; axis equal; title('Probability of Occupancy');
% figure(40); surf(Mesh.x, Mesh.y, reshape(lmf4, size(Mesh.x)), reshape(lmf4, size(Mesh.x))); view(0,90); colorbar; axis equal; title('Probability of Occupancy');
% figure(50); surf(Mesh.x, Mesh.y, reshape(lmf5, size(Mesh.x)), reshape(lmf5, size(Mesh.x))); view(0,90); colorbar; axis equal; title('Probability of Occupancy');
% figure(60); surf(Mesh.x, Mesh.y, reshape(lmf6, size(Mesh.x)), reshape(lmf6, size(Mesh.x))); view(0,90); colorbar; axis equal; title('Probability of Occupancy');
% figure(70); surf(Mesh.x, Mesh.y, reshape(lmf7, size(Mesh.x)), reshape(lmf7, size(Mesh.x))); view(0,90); colorbar; axis equal; title('Probability of Occupancy');
% figure(80); surf(Mesh.x, Mesh.y, reshape(lmf8, size(Mesh.x)), reshape(lmf8, size(Mesh.x))); view(0,90); colorbar; axis equal; title('Probability of Occupancy');


% figure(11); surf(Mesh.x, Mesh.y, reshape(vf1, size(Mesh.x)), reshape(vf1, size(Mesh.x))); view(0,90); colorbar; axis equal; title('Predictive Variance');
% figure(21); surf(Mesh.x, Mesh.y, reshape(vf2, size(Mesh.x)), reshape(vf2, size(Mesh.x))); view(0,90); colorbar; axis equal; title('Predictive Variance');
figure(31); surf(Mesh.x, Mesh.y, reshape(vf3, size(Mesh.x)), reshape(vf3, size(Mesh.x))); view(0,90); colorbar; axis equal; title('Predictive Variance');
% figure(41); surf(Mesh.x, Mesh.y, reshape(vf4, size(Mesh.x)), reshape(vf4, size(Mesh.x))); view(0,90); colorbar; axis equal; title('Predictive Variance');
% figure(51); surf(Mesh.x, Mesh.y, reshape(vf5, size(Mesh.x)), reshape(vf5, size(Mesh.x))); view(0,90); colorbar; axis equal; title('Predictive Variance');
% figure(61); surf(Mesh.x, Mesh.y, reshape(vf6, size(Mesh.x)), reshape(vf6, size(Mesh.x))); view(0,90); colorbar; axis equal; title('Predictive Variance');
% figure(71); surf(Mesh.x, Mesh.y, reshape(vf7, size(Mesh.x)), reshape(vf7, size(Mesh.x))); view(0,90); colorbar; axis equal; title('Predictive Variance');
% figure(81); surf(Mesh.x, Mesh.y, reshape(vf8, size(Mesh.x)), reshape(vf8, size(Mesh.x))); view(0,90); colorbar; axis equal; title('Predictive Variance');


% figure(12); surf(Mesh.x, Mesh.y, reshape(ogm1, size(Mesh.x)), reshape(ogm1, size(Mesh.x))); view(0,90); colormap(flipud(gray)); axis equal; title('Occupancy Map');
% figure(22); surf(Mesh.x, Mesh.y, reshape(ogm2, size(Mesh.x)), reshape(ogm2, size(Mesh.x))); view(0,90); colormap(flipud(gray)); axis equal; title('Occupancy Map');
figure(32); surf(Mesh.x, Mesh.y, reshape(ogm3, size(Mesh.x)), reshape(ogm3, size(Mesh.x))); view(0,90); colormap(flipud(gray)); axis equal; title('Occupancy Map');
% figure(42); surf(Mesh.x, Mesh.y, reshape(ogm4, size(Mesh.x)), reshape(ogm4, size(Mesh.x))); view(0,90); colormap(flipud(gray)); axis equal; title('Occupancy Map');
% figure(52); surf(Mesh.x, Mesh.y, reshape(ogm5, size(Mesh.x)), reshape(ogm5, size(Mesh.x))); view(0,90); colormap(flipud(gray)); axis equal; title('Occupancy Map');
% figure(62); surf(Mesh.x, Mesh.y, reshape(ogm6, size(Mesh.x)), reshape(ogm6, size(Mesh.x))); view(0,90); colormap(flipud(gray)); axis equal; title('Occupancy Map');
% figure(72); surf(Mesh.x, Mesh.y, reshape(ogm7, size(Mesh.x)), reshape(ogm7, size(Mesh.x))); view(0,90); colormap(flipud(gray)); axis equal; title('Occupancy Map');
% figure(82); surf(Mesh.x, Mesh.y, reshape(ogm8, size(Mesh.x)), reshape(ogm8, size(Mesh.x))); view(0,90); colormap(flipud(gray)); axis equal; title('Occupancy Map');
