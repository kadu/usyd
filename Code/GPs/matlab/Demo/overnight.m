function overnight()

clear all;

load('SimonTrainingData');
load('manual_elements');

empty.Coord =  zeros(2,0);
empty.PoseNumber = zeros(2,0);

res = 0.5;

Mins = min([TrainingData.LibraryOfEndPoints, TrainingData.LibraryOfStartPoints]');
xmin = Mins(1)-1;
ymin = Mins(2)-1;
Maxs = max([TrainingData.LibraryOfEndPoints, TrainingData.LibraryOfStartPoints]');
xmax = Maxs(1)+1;
ymax = Maxs(2)+1;

[Mesh.x, Mesh.y] = meshgrid(xmin:res:xmax,ymin:res:ymax);
clear xmin ymin Mins Maxs xmax ymax
Test.x = reshape(Mesh.x,[1 numel(Mesh.x)]);
Test.y = reshape(Mesh.y,[1 numel(Mesh.y)]);

x.Element = struct();
x.Point.Coord = [Test.x; Test.y];
isempty(x.Element);
x.Element.Area = [];
x.Element.Data =  zeros(2,0);
x.M = 0;
x.N = size(x.Point.Coord, 2);

save('TestPoints', 'x')

'Starting...'

% tic;
% [X1, y1] = InitAreawiseData(TrainingData, 100);
% D = [X1.Element.Data];
% for i = manual_elements'
% X5.Element(i(1)).Boundaries = [i(2), i(3); i(4), i(5)];
% X5.Element(i(1)).Area = abs(i(2) - i(3)) * abs(i(4) - i(5));
% X5.Element(i(1)).Data = D(:, D(1,:) > min(i(2), i(3)) & D(1,:) < max(i(2), i(3)) & D(2,:) > min(i(4), i(5)) & D(2,:) < max(i(4),i(5)));
% y5.Element(i(1)) = i(6);
% y5.IntEl(i(1)) = i(6) * abs((i(2) - i(3)) * (i(4) - i(5)));
% end
% X5.Point = X1.Point;
% y5.Point = y1.Point;
% X5.N = X1.N;
% X5.M = numel(y5.IntEl);
% 
% [par5, fv5] = areagplearn(X5, y5, @cov_nDsqExp,[],[0.2,0.3,0.4,0.5]);
% [par5, fv5] = areagplearn2(X5, y5, @cov_nDsqExp,[],par5);
% [lml5, mf5, vf5] = areagpeval(X5,y5, @cov_nDsqExp, par5, [], x, []);
% '5 done, time elapsed:'
% time5 = toc
% Test5.X = X5;
% Test5.y = y5;
% Test5.par = par5;
% Test5.fv = fv5;
% Test5.lml = lml5;
% Test5.mf = mf5;
% Test5.vf = vf5;
% Test5.time = time5;
% save('Test5', 'Test5');

b = [TrainingData.Cells.Boundaries];
b = [min(b(1,:)), max(b(1,:)); min(b(2,:)), max(b(2,:))];
Map.Boundaries = b;
Map.OccuPoints = [TrainingData.Cells.OccuPoints];
f = [TrainingData.Cells.FreePoints];
Map.FreePoints = f;%(:,1:10:end);
Map.Robot = [TrainingData.Cells.Robot];

TD2 = TrainingData;
TD2.Cells = Map;
tic;
[X6, y6] = InitAreawiseData(TD2, 0.5, 6);
% X6.Point = empty;
% y6.Point = [];
% X6.N = 0;
% Test6.X = X6;
% Test6.y = y6;
% 
% [par6, fv6] = areagplearn(X6, y6, @cov_nD_nn,[],[0.2,0.3,0.4]);
% [par6, fv6] = areagplearn2(X6, y6, @cov_nD_nn,[],par6);
% Test6.fv = fv6;
% Test6.par = par6;
% 
% [lml6, mf6, vf6] = areagpeval(X6,y6, @cov_nD_nn, par6, [], x, []);
% '6 done, time elapsed:'
% time6 = toc
% Test6.lml = lml6;
% Test6.mf = mf6;
% Test6.vf = vf6;
% Test6.time = time6;
% save('Test6', 'Test6');
% 
% tic;
% [X1, y1] = InitAreawiseData(TrainingData, 100);
% X1.Point = empty;
% y1.Point = [];
% X1.N = 0;
% [par1, fv1] = areagplearn(X1, y1, @cov_nDsqExp,[],[0.2,0.3,0.4,0.5]);
% [par1, fv1] = areagplearn2(X1, y1, @cov_nDsqExp,[],par1);
% [lml1, mf1, vf1] = areagpeval(X1,y1, @cov_nDsqExp, par1, [], x, []);
% '1 done, time elapsed:'
% time1 = toc
% Test1.X = X1;
% Test1.y = y1;
% Test1.par = par1;
% Test1.fv = fv1;
% Test1.lml = lml1;
% Test1.mf = mf1;
% Test1.vf = vf1;
% Test1.time = time1;
% save('Test1', 'Test1');
% 
% tic;
% [X2, y2] = InitAreawiseData(TrainingData, 3);
% X2.Point = empty;
% y2.Point = [];
% X2.N = 0;
% [par2, fv2] = areagplearn(X2, y2, @cov_nDsqExp,[],[0.2,0.3,0.4,0.5]);
% [par2, fv2] = areagplearn2(X2, y2, @cov_nDsqExp,[],par2);
% [lml2, mf2, vf2] = areagpeval(X2,y2, @cov_nDsqExp, par2, [], x, []);
% '2 done, time elapsed:'
% time2 = toc
% Test2.X = X2;
% Test2.y = y2;
% Test2.par = par2;
% Test2.fv = fv2;
% Test2.lml = lml2;
% Test2.mf = mf2;
% Test2.vf = vf2;
% Test2.time = time2;
% % save('Test2', 'Test2');
% 
% tic;
[X3, y3] = InitAreawiseData(TrainingData, 2, 3);
% X3.Point = empty;
% y3.Point = [];
% X3.N = 0;
% [par3, fv3] = areagplearn(X3, y3, @cov_nD_nn,[],[0.2,0.3,0.4]);
% [par3, fv3] = areagplearn2(X3, y3, @cov_nD_nn,[],par3);
% [lml3, mf3, vf3] = areagpeval(X3,y3, @cov_nD_nn, par3, [], x, []);
% '3 done, time elapsed:'
% time3 = toc
% Test3.X = X3;
% Test3.y = y3;
% Test3.par = par3;
% Test3.fv = fv3;
% Test3.lml = lml3;
% Test3.mf = mf3;
% Test3.vf = vf3;
% Test3.time = time3;
% save('Test3', 'Test3');
% 
% tic;
% [X4, y4] = InitAreawiseData(TrainingData, 1);
% X4.Point = empty;
% y4.Point = [];
% X4.N = 0;
% [par4, fv4] = areagplearn(X4, y4, @cov_nDsqExp,[],[0.2,0.3,0.4,0.5]);
% [par4, fv4] = areagplearn2(X4, y4, @cov_nDsqExp,[],par4);
% [lml4, mf4, vf4] = areagpeval(X4,y4, @cov_nDsqExp, par4, [], x, []);
% '4 done, time elapsed:'
% time4 = toc
% Test4.X = X4;
% Test4.y = y4;
% Test4.par = par4;
% Test4.fv = fv4;
% Test4.lml = lml4;
% Test4.mf = mf4;
% Test4.vf = vf4;
% Test4.time = time4;
% save('Test4', 'Test4');
% 
% 
% tic;
% [X42, y42] = InitAreawiseData(TrainingData, 0.1);
% X42.Point = empty;
% y42.Point = [];
% X42.N = 0; 
% [par42, fv42] = areagplearn(X42, y42, @cov_nDsqExp,[],[0.2,0.3,0.4,0.5]);
% [lml42, mf42, vf42] = areagpeval(X42,y42, @cov_nDsqExp, par42, [], x, []);
% '42 done, time elapsed:'
% time42 = toc
% Test42.X = X42;
% Test42.y = y42;
% Test42.par = par42;
% Test42.fv = fv42;
% Test42.lml = lml42;
% Test42.mf = mf42;
% Test42.vf = vf42;
% Test42.time = time42;
% save('Test42', 'Test42');
% 
% b = [TrainingData.Cells.Boundaries];
% b = [min(b(1,:)), max(b(1,:)); min(b(2,:)), max(b(2,:))];
% Map.Boundaries = b;
% Map.OccuPoints = [TrainingData.Cells.OccuPoints];
% f = [TrainingData.Cells.FreePoints];
% Map.FreePoints = f(:,1:10:end);
% Map.Robot = [TrainingData.Cells.Robot];
% 
% TD2 = TrainingData;
% TD2.Cells = Map;
% tic;
% [X17, y17] = InitAreawiseData(TD2, 0.1);
% X17.Point = empty;
% y17.Point = [];
% X17.N = 0;
% Test17.X = X17;
% Test17.y = y17;
% 
% [par17, fv17] = areagplearn(X17, y17, @cov_nDsqExp,[],[0.2,0.3,0.4, 0.5]);
% [par17, fv17] = areagplearn2(X17, y17, @cov_nDsqExp,[],par17);
% Test17.fv = fv17;
% Test17.par = par17;
% 
% 
% [lml17, mf17, vf17] = areagpeval(X17,y17, @cov_nDsqExp, par17, [], x, []);
% '6 done, time elapsed:'
% time17 = toc
% Test17.lml = lml17;
% Test17.mf = mf17;
% Test17.vf = vf17;
% Test17.time = time17;
% save('Test17', 'Test17');
% 
% 
load('TestSet10***/Test6')
% load('Test5')
% load('Test4')
% load('Test42')
load('TestSet10***/Test3')
% load('Test2')
% load('Test1')
% load('Test17')
% load('TestPoints')
% 
% mf1 = Test1.mf;
% mf17 = Test17.mf;
% mf2 = Test2.mf;
mf3 = Test3.mf;
% mf4 = Test4.mf;
% mf42 = Test42.mf;
% mf5 = Test5.mf;
mf6 = Test6.mf;
% 
% vf1 = Test1.vf;
% vf17 = Test17.vf;
% vf2 = Test2.vf;
vf3 = diag(Test3.vf);
% vf4 = Test4.vf;
% vf42 = Test42.vf;
% vf5 = Test5.vf;
vf6 = diag(Test6.vf);
% 
% lmf1 = (1 + exp(-mf1)).^(-1);
% lmf17 = (1 + exp(-mf17)).^(-1);
% lmf2 = (1 + exp(-mf2)).^(-1);
% lmf3 = (1 + exp(-mf3)).^(-1);
lmf3 = sigmoid(1, 1, 1, mf3, vf3);

% lmf4 = (1 + exp(-mf4)).^(-1);
% lmf42 = (1 + exp(-mf42*5)).^(-1);
% lmf5 = (1 + exp(-mf5)).^(-1);
% lmf6 = (1 + exp(-mf6*5)).^(-1);
lmf6 = sigmoid(1, 1, 1, mf6, vf6);

% 
% figure(1); scatter3(x.Point.Coord(1,:), x.Point.Coord(2,:), mf1, 100, mf1, 'fill')
% figure(17); scatter3(x.Point.Coord(1,:), x.Point.Coord(2,:), mf17, 100, mf17, 'fill')
% figure(2); scatter3(x.Point.Coord(1,:), x.Point.Coord(2,:), mf2, 100, mf2, 'fill')
figure(30); scatter3(x.Point.Coord(1,:), x.Point.Coord(2,:), mf3, 100, mf3, 'fill')
% figure(4); scatter3(x.Point.Coord(1,:), x.Point.Coord(2,:), mf4, 100, mf4, 'fill')
% figure(42); scatter3(x.Point.Coord(1,:), x.Point.Coord(2,:), mf42, 100, mf42, 'fill')
% figure(5); scatter3(x.Point.Coord(1,:), x.Point.Coord(2,:), mf5, 100, mf5, 'fill')
figure(60); scatter3(x.Point.Coord(1,:), x.Point.Coord(2,:), mf6, 100, mf6, 'fill')
% 
% figure(1); scatter3(x.Point.Coord(1,:), x.Point.Coord(2,:), lmf1, 100, lmf1, 'fill')
% figure(2); scatter3(x.Point.Coord(1,:), x.Point.Coord(2,:), lmf2, 100, lmf2, 'fill')
figure(31); scatter3(x.Point.Coord(1,:), x.Point.Coord(2,:), lmf3, 100, lmf3, 'fill')
% figure(42); scatter3(x.Point.Coord(1,:), x.Point.Coord(2,:), lmf42, 100, lmf42, 'fill')
% figure(4); scatter3(x.Point.Coord(1,:), x.Point.Coord(2,:), lmf4, 100, lmf4, 'fill')
% figure(5); scatter3(x.Point.Coord(1,:), x.Point.Coord(2,:), lmf5, 100, lmf5, 'fill')
figure(61); scatter3(x.Point.Coord(1,:), x.Point.Coord(2,:), lmf6, 100, lmf6, 'fill')
% 
% 
figure(32);  scatter3(x.Point.Coord(1,:), x.Point.Coord(2,:), vf3, 100, vf3, 'fill')
figure(62);  scatter3(x.Point.Coord(1,:), x.Point.Coord(2,:), vf6, 100, vf6, 'fill')
