function Element = find_elements(Element, thresh)

O = Element.OccuPoints; % Occupied points within Cell
F = Element.FreePoints; % Free points within Cell

% Cells without datapoints get discarded
if isempty([O,F])
    Element = [];
    return
end



% Only cells with datapoints from this line onwards.

d = diff(Element.Boundaries, 1, 2);

% Cells with only free points don't get divided any further
if isempty(O)% && all(d < thresh(2))
    Element.Occ = -1;
    Element.Area = prod(diff(Element.Boundaries, 1, 2));
    Element.Data = F;
    Element = Element;
    return
end

% Cells with only occupied points don't get divided any further.
if isempty(F) && all(d < thresh(2))
    Element.Occ = 1;
    Element.Area = prod(diff(Element.Boundaries, 1, 2));
    Element.Data = O;
    Element = Element;
    return
end



% Only cells with both free and occupied points from this line onwards.



% Cells smaller than the size thresh(1)old don't get divided
if all(d < thresh(1))
    nO = numel(O);
    nF = numel(F);
    Element.Occ = (nO - nF) / (nO + nF);
    Element.Area = prod(diff(Element.Boundaries, 1, 2));
    Element.Data = [O,F];
    Element = Element;
    return
end



% These are all the base cases. All cells that arrive to this line will get
% sliced and go into the next level of recursion.



D = size(Element.Boundaries,1); % number of dimensions
i = find(d == max(d));  % find longest edge
i = i(1); % if more than one, choose one arbitrarily
mid = mean(Element.Boundaries(i,:)); % find its half point

% make 2 copies of Cell
ElementA = Element;
ElementB = Element;

% Cut them through the middle of the largest dimension
ElementA.Boundaries(i,:) = [Element.Boundaries(i,1), mid];
ElementB.Boundaries(i,:) = [mid, Element.Boundaries(i,2)];

% Divide the occupied points in Cell accordingly
if ~isempty(Element.OccuPoints)
    ElementA.OccuPoints = Element.OccuPoints(:,Element.OccuPoints(i,:) <= mid);
    ElementB.OccuPoints = Element.OccuPoints(:,Element.OccuPoints(i,:)  > mid);
else
    % Everything must have the right dimensions!
    ElementA.OccuPoints = zeros(D,0);
    ElementB.OccuPoints = zeros(D,0);
end

% Divide the free points in Cell accordingly
if ~isempty(Element.FreePoints)
    ElementA.FreePoints = Element.FreePoints(:,Element.FreePoints(i,:) <= mid);
    ElementB.FreePoints = Element.FreePoints(:,Element.FreePoints(i,:)  > mid);
else
    % Everything must have the right dimensions!
    ElementA.FreePoints = zeros(D,0);
    ElementB.FreePoints = zeros(D,0);
end

% Place the robot in the adequate cell
% if ~isempty(Element.Robot)
%     CellA.Robot = Element.Robot(:,Element.Robot(i,:) <= mid);
%     CellB.Robot = Element.Robot(:,Element.Robot(i,:)  > mid);
% else
%     CellA.Robot = zeros(D,0);
%     CellB.Robot = zeros(D,0);
% end

% dive further into recursion!
Element = [find_elements(ElementA, thresh), find_elements(ElementB, thresh)];