function K = cov_nD(X1, X2, cov_fun, par, diag)

if nargin < 5
    diag = false;
end

P1 = X1.Point.Coord;
E1 = X1.Element;
P2 = X2.Point.Coord;
E2 = X2.Element;

N1 = X1.N;
M1 = X1.M;
N2 = X2.N;
M2 = X2.M;

if diag
    if ~isequal(X1, X2)
        error('Diagonal can only be calculated for X1 == X2');
    end
    
    K = zeros(M1+N1,1);
    
    for i = 1:N1
       K(i) = cov_fun(P1(:,i), P1(:,i), par); 
    end
    
    for i = 1:M1
        K(N1+i) = cov_element2element(E1(i), E1(i), cov_fun, par);
    end
else
    if N1 == 0 || N2 == 0
        Kxx = zeros(N1, N2);
    else
        Kxx = cov_fun(P1, P2, par);
    end


    if N1 == 0 || M2 == 0
        Kxe = zeros(N1, M2);
    else
        Kxe = cov_point2element(P1, E2, cov_fun, par);
    end

    if M1 == 0 || N2 == 0
        Kex = zeros(M1, N2);
    else
        Kex = cov_point2element(P2, E1, cov_fun, par)';
    end

    if M1 == 0 || M2 == 0
        Kee = zeros(M1, M2);
    else
        Kee = cov_element2element(E1, E2, cov_fun, par);
    end

    K = [Kxx, Kxe; Kex, Kee];
end