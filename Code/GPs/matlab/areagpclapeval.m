%Evaluate a Gaussian Process Classifier using the Laplace Approximation
%(see page 59 of "Gaussian Processes for Machine Learning" by
% C. Rasmussen and C. Williams)
%
%INPUTS
% X - D x N inputs
% y - 1 x N outputs (targets)
% kfun - covariance function
% kpar - covariance parameters (hyperparameters)
% K - N x N computed covariance 
% noise - scalar noise level
% x - D x M test inputs
% invK - N x N inverted K if available
% 
%OUTPUTS
% p - predictive probability
% mf - predictive mean
% vf - predictive variance
% lml - log marginal likelihood
%
% Fabio Tozeto Ramos 11/07/09
% [p,mf,vf,nlml] = gpclapeval(X,y,kfun,kpar,K,noise,x,sW)

function [p,mf,vf,nlml] = areagpclapeval(X,y,kfun,kpar,K,x,sW)
persistent best_a best_value;   % keep a copy of the best "a" and its obj value

D = size(X.Point.Coord,1); %number of inputs
Np = X.N;
Nel = X.M;
if (Nel > 0 && D ~= size(X.Element(1).Boundaries, 1))
    error('Dimensions do not match!')
end
N = Np+Nel;

Dx = size(x.Point.Coord,1); %number of inputs
if (Dx ~= D)
    error('Dimensions do not match!')
end
Nx = x.N;
if (x.M ~= 0)
    warning('Unable to use test elements, these will be discarded.');
end
x = x.Point.Coord;

tol   = 1e-6;
if isempty(K)
    K = feval(kfun,X,X,kpar);
end

y = [y.Point, y.Element];

if nargin<7
    if any(size(best_a) ~= [N 1])      % find a good starting point for "a" and "f"
      f = zeros(N,1); a = f; [lp, dlp, W] = cumGauss( f, y' );         % start at zero
      Psi_new = -N*log(2); best_value = inf;
    else
      a = best_a; f = K*a; [lp, dlp, W] = cumGauss( f, y' ); % try the best "a" so far
      Psi_new = -a'*f/2 + lp;         
      if Psi_new < -N*log(2)                                  % if zero is better..
        f = zeros(N,1); a = f; [lp, dlp, W] = cumGauss( f, y' );  % ..then switch back
        Psi_new = -a'*f/2 + lp;
      end
    end
    Psi_old = -inf;                                   % make sure while loop starts

    while Psi_new - Psi_old > tol                       % begin Newton's iterations
        Psi_old = Psi_new; a_old = a; 
        sW = sqrt(W);                     
        L  = chol(eye(N)+sW*sW'.*K);  
        b  = W.*f+dlp;
        a  = b - sW.*solve_chol(L,sW.*(K*b));
        f  = K*a;
        [lp, dlp, W, d3lp] = cumGauss( f, y' );

        Psi_new = -a'*f/2 + lp;
        i = 0;
        while i < 10 && Psi_new < Psi_old               % if objective didn't increase
            a = (a_old+a)/2;                                 % reduce step size by half
            f = K*a;
            [lp, dlp, W, d3lp] = cumGauss( f, y' );
            Psi_new = -a'*f/2 + lp;
            i = i + 1;
        end
    end                                                   % end Newton's iterations
    sW = sqrt(W);
end

                     
L   = chol(eye(N)+sW*sW'.*K);  
xc  = mat2cell(x,D,ones(1,Nx));
a   = cellfun(kfun, xc, xc, repmat({kpar},1,Nx))';    
b   = feval(kfun, X, x, kpar);
v   = L'\(repmat(sW,1,Nx).*b);       % FIX: use that L is triangular
mu  = b'*dlp;
s2  = a - sum(v.*v,1)';
p   = (1+erf(mu./sqrt(1+s2)/sqrt(2)))/2;         % the real probabilities
                       
if nargout > 1
    mf = mu; vf = s2;
    if nargout == 4
        nlml = a'*f/2 - lp + sum(log(diag(L)));      % approx neg log marginal lik
        if nlml < best_value                                   % if best so far...
            best_a = a; best_value = nlml;          % ...then remember for next call
        end
    end
end
 
% Computes the cumulative Gaussian with f and y
function [lp, dlp, d2lp, d3lp] = cumGauss(f, y)

if nargin == 2
  p = (1+erf(f.*y/sqrt(2)))/2 + 1e-10;
  n = exp(-f.^2/2)/sqrt(2*pi);
  lp = sum(log(p));
  dlp = y.*n./p;
  d2lp = +n.^2./p.^2+y.*f.*n./p;
  d3lp = 2*y.*n.^3./p.^3+3*f.*n.^2./p.^2+y.*(f.^2-1).*n./p; 
  d3lp = -d3lp;
else
  lp = (1+erf(f./sqrt(1+y)/sqrt(2)))/2;
end
