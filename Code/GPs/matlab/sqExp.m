% Square exponential covariance function.
% Implemented according to Rasmussen's GPs for Machine LEarning,
% equation 2.31 (page 19, 1st ed), minus the noise term
%
% Inputs:
%       A  = D * N input points
%       B  = D * M test points (defaults to a)
%       hp = hyperparameters:
%           hp[1:end-1] = D characteristic lengthscales
%           hp[end]     = sig_f




function K = sqExp(A, B, hp)

D = size(A,1);

if isempty(B) % if B = [] calculates cov(A,A)
    B = A;
else
    if size(B,1) ~= D;
        error('SQEXP DIMENSIONALITY ERROR: Both vectors must of be the same dimension!')
    end
end

if isempty(hp) % if hp = [], arbitrarily sets hp=[0,5, 0.5, ..., 0.5]
    hp = ones(1,D+1)./2;
end

ls = hp(1:end-1)'; % characteristic lengthscales
sf = hp(end);      % signal variance

C = (ls.^-2)./-2; % This is the constant inside the exponential, -1/(2*ls^2)

N = size(A,2); % number of input points
M = size(B,2); % number of test points

AA = repmat(A(:), 1, M);
BB = repmat(B, N, 1);
RR = AA - BB;
CC = repmat(C, N, M);
VV = CC .* RR .* RR;
% This is the vectorised way to do V=C*R^2 where R=A-B
% As such, it should work for any dimension D

V = reshape(sum(reshape(VV, D, [])), [], size(VV,2));
% This sums accross all dimensions

K = exp(log(sf^2) + V);