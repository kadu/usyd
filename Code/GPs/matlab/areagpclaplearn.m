% Train a GP classification model with scaled conjugade gradient

function [params, options] = areagpclaplearn(kfun,kgfun,kpar0)

params = [kpar0];
%[params, options] = scg(@(params) gplmleval(params, kfun), ...
%    params,options,@(params) gpgradeval(params, kgfun, kfun));
%[params, options] = quasinew(@(params) gplmleval(params, kfun), ...
%    params,options,@(params) gpgradeval(params, kgfun, kfun));
 
%Matlab optimisation toolbox 
opts = optimset('LargeScale','off','MaxIter',500,'Diagnostics', 'on',...
'GradObj', 'on', 'Display', 'iter','TolFun',  1e-5, 'MaxFunEvals', 5000);

%lb=0.001*ones(1,size(X,1));
%ub=inf*ones(1,size(X,1));
%tic;[params, options]=fminunc(@(params) gplmlgrad(params, kfun, kgfun), params,opts);toc;
%tic;[params, options]=fmincon(@(params) gplmlgrad(params, kfun, kgfun), params,...
 %   [],[],[],[],lb,ub,[],opts);toc
 
%Simulated Annealing
%[params, fval] = anneal(@(params) gpsrlmlgrad(params, kfun, kgfun, 1:2:20), params);
tic;[params, options]=fminunc(@(params) gpclaplmlgrad(params, kfun, kgfun), params,opts);toc;
%Genetic Algorithm Optimization
%[params, fval] = ga(@(params) gplmlgrad(params, kfun, kgfun), length(params));
%disp(fval);

%Simulated Annealing in Matlab
%[params, fval] = simulannealbnd(@(params) gplmlgrad(params, kfun, kgfun), params);
%disp(fval);