% This function is meant to break down the data in areawise elements that
% are significant to the computation of the map. In the 2D case, this means
% straight(ish) lines (which possibly indicate obstacles like walls).
%
% For any pair of points P1, P2 in the data:
%
% 1. check if they're within a cityblock of a given size. If so, assign
%    both the same neighbourhood number.
% 2. otherwise, store the slope between P1 and P2.


function [col] = find2Delements(P, obstacle_size, robot_size, tolerance)

if nargin < 4
    tolerance = 1e-5;
end
if nargin < 3 || isempty(robot_size)
    robot_size = obstacle_size;
end

% A - find collinear points
N = size(P, 2);
col = zeros(N,1);
n_col = 0;

% P = sortrows(P')';

slope = -inf(N,1);

for i = 1:N
%     if neigh(i) == 0
%         n_neigh = n_neigh + 1;
%         neigh(i) = n_neigh;
%     end
    for j = (i+1):N
        % 1. P2 is within one block of P1, mark neighbourhood.
%         if pdist([P(:,i), P(:,j)]', 'cityblock') < obstacle_size
%             neigh(j) = neigh(i);
%         end
         
        % 2. Store the slope in the matrix.
        slope(i,j) = (P(2,i) - P(2,j)) / (P(1,i) - P(1,j));
        slope(j,i) = (P(2,j) - P(2,i)) / (P(1,j) - P(1,i));

%         slower:        
%         slope(i,j) = (P(:,i)' * P(:,j)) / (length(P(:,i)) * length(P(1,j)));
%         slope(j,i) = slope(i,j);
    end
    % Now that we know the slopes between all points and P(i), let's group
    % the ones that have similar slopes
    s = slope(:,i);
    [s, ix] = sort(s);
    
    for k = 2:numel(s)
       if abs(s(k) - s(k-1)) < tolerance 
           if pdist([P(:,ix(k)), P(:,ix(k-1))]', 'cityblock') > obstacle_size
               continue
           end
           if col(ix(k-1)) == 0 && col(ix(k)) == 0
               n_col = n_col + 1;
               col(ix(k-1)) = n_col;
               col(ix(k))   = n_col;
           end
           if col(ix(k-1)) ~= 0 && col(ix(k)) == 0
               col(ix(k)) = col(ix(k-1));
           end
           if col(ix(k-1)) == 0 && col(ix(k)) ~= 0
               col(ix(k-1)) = col(ix(k));
           end
       end
    end
end
