function [FreePoints] = ProbeBeams(TrainingData, resolution)

D = size(TrainingData.OccupiedPoints,1);

free = [];
for step = 1:max(TrainingData.PoseNumber)
    robot = TrainingData.LibraryOfStartPoints(:,TrainingData.PoseNumber==step);
    robot = robot (:,1);
%     Element0.Robot(:,step) = robot;
    hits = TrainingData.LibraryOfEndPoints(:,TrainingData.PoseNumber==step);
    for h=hits
        d = pdist2(h', robot');          % distance between points
        if (d < 2 * resolution)
            continue;
        end
%         s = floor(d/res);                % number of samples from beam
        dres = resolution * (diag(pdist2(robot,h)) / d);
        sample = [];
        for dim=1:D
            if robot(dim) < h(dim)
                s = [robot(dim)+dres(dim):dres(dim):h(dim)];
            else
                s = [robot(dim)-dres(dim):-dres(dim):h(dim)];
            end
            if dim == 1 || length(sample(1,:)) == length(s)
                sample(dim,:) = s;
            else
                m = min(length(sample(1,:)), length(s));
                sample = sample(:,1:m);
                s = s(1:m);
                sample(dim,:) = s;
            end
        end
        free = [free, robot, sample(:,1:end-1)];
    end
end

FreePoints = free;