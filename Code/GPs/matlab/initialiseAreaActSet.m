

function [ActiveSet]=initialiseAreaActSet(InitalObs,gp)
% load NodeWeightArray
ActiveSet.X.Element = InitalObs.Element;
ActiveSet.X.Point = InitalObs.Point;
ActiveSet.y.IntEl = InitalObs.yIntEl;
ActiveSet.y.Point = InitalObs.yPoint;
ActiveSet.ObsId = [ones(1,size(InitalObs.Element,2)) zeros(1,size(InitalObs.Point,2))];


%Evaluate K
p1 = 1;
for j =  1:numel(gp.npar)
    p2 = p1 + gp.npar(j)-1;
    KSum = gp.cov_funs{j}(ActiveSet.X, ActiveSet.X, gp.par(p1:p2));
    
    if j==1
        K = KSum;
    else
        K = K + KSum;
    end
    
    p1 = p2 + 1;
end
if isreal(K)~=1
    K=real(K);
end
K = triu(K,1)'+triu(K);

ActiveSet.L = chol(K+(gp.noise^2)*eye(length(K)));
ActiveSet.alpha = solve_chol(ActiveSet.L,ActiveSet.y');





