function K = cov_nDmat3(X1, X2, par, diag)

if nargin < 4
    diag = false;
end

K = cov_nD(X1, X2, @covMat3, par, diag);