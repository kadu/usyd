function [NewDataset] = CutDataset(Dataset, boundaries, plotfig)

if nargin < 3
    plotfig=0;
end

NewDataset.LibraryOfEndPoints = [];
NewDataset.LibraryOfStartPoints = [];
NewDataset.PoseNumber = [];

newpose = 1;
for pose = 1:max(Dataset.PoseNumber)
    robot = Dataset.LibraryOfStartPoints(:,Dataset.PoseNumber == pose);
    robot = robot(:,1);
    if all(robot > boundaries(:,1)) && all(robot < boundaries(:,2))
        flag = 0;
        for hit = Dataset.LibraryOfEndPoints(:,Dataset.PoseNumber == pose)
            if all(hit > boundaries(:,1)) && all(hit < boundaries(:,2))
                NewDataset.LibraryOfEndPoints = [NewDataset.LibraryOfEndPoints,hit];
                NewDataset.LibraryOfStartPoints = [NewDataset.LibraryOfStartPoints,robot];
                NewDataset.PoseNumber = [NewDataset.PoseNumber,newpose];
                flag = 1;
            end
        end
        if flag
            newpose = newpose + 1;
        end
    end
end

NewDataset.OccupiedPoints = NewDataset.LibraryOfEndPoints;

if plotfig
    figure(plotfig)
    clf(plotfig)
    subplot(2,1,1)
    plot(Dataset.OccupiedPoints(1,:),Dataset.OccupiedPoints(2,:),'+')
    hold on
    plot(Dataset.LibraryOfStartPoints(1,:),Dataset.LibraryOfStartPoints(2,:),'go')
    plot([Dataset.LibraryOfStartPoints(1,:);Dataset.LibraryOfEndPoints(1,:)],[Dataset.LibraryOfStartPoints(2,:);Dataset.LibraryOfEndPoints(2,:)],'r')
    axis equal
    title('Original Dataset')
    subplot(2,1,2)
    plot(NewDataset.OccupiedPoints(1,:),NewDataset.OccupiedPoints(2,:),'+')
    hold on
    plot(NewDataset.LibraryOfStartPoints(1,:),NewDataset.LibraryOfStartPoints(2,:),'go')
    plot([NewDataset.LibraryOfStartPoints(1,:);NewDataset.LibraryOfEndPoints(1,:)],[NewDataset.LibraryOfStartPoints(2,:);NewDataset.LibraryOfEndPoints(2,:)],'r')
    axis equal
    title('Cut Dataset')
end
