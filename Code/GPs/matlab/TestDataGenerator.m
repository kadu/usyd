function [tx, ty] = TestDataGenerator(InputData, TestData, sample_size)
i = 1;
while(i)
    i
    try
        X = InputData.X;
        y = InputData.y;
        par = InputData.par;

        if nargin < 3
            sample_size = size(TestData.OccupiedPoints,2)
        end
        
        free = datasample(ProbeBeams(TestData, 0.5), sample_size, 2, 'Replace', false);
        occu = datasample(TestData.OccupiedPoints,   sample_size, 2, 'Replace', false);

        x = struct();
        x.Element = struct();
        x.Point.Coord = [free, occu];
        x.Element.Area = [];
        x.Element.Data =  zeros(2,0);
        x.M = 0;
        x.N = size(x.Point.Coord, 2);

        ty = [zeros(1, sample_size), ones(1, sample_size)]';

        [~, mf, vf] = areagpeval(X,y, @cov_nDmat3, par, [], x, []);


        lmf = sigmoid(5, -0.25, 1, mf, vf);
%         ogm = (lmf > 0.33)/2 + (lmf > 0.66)/2;
        i = -1;
    end
    i = i + 1;
end

tx = x;