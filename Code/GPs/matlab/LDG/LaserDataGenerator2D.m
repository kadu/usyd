
% Script that generates the laser data obtained by a robot as it navigates
% a user-created environment moving along a predetermined path. It also
% outputs the pose of the robot during each of the scans so successive
% scans can be merged together precisely.

%Set up to handle multiple robots

clc
close all
disp(' ')
disp('*********LaserDataGenerator***********')
disp(' ')
clear all
addpath Scenarios
addpath Functions

%% Load Scenario File (World Map and Laser settings and Initial Conditions)

roombox
disp('Environment Loaded')
disp('Robot Path Loaded')
disp(' ')



%% Initialise Settings
SimData = [];

%% Generate the map
MapXs = [WorldMap.WallStartPointsX;WorldMap.WallEndPointsX];
MapYs = [WorldMap.WallStartPointsY;WorldMap.WallEndPointsY];

screen_size = get(0, 'ScreenSize');
f1 = figure(7)
% set(f1, 'Position', [0 0 screen_size(3)/2 screen_size(4) ] );
% subplot(3,1,1)
plot(MapXs, MapYs, 'b','LineWidth',2);
axis equal
title('Ground Truth');
pause(1)


%% Begin simulating data
disp('Running Simulation...')
disp(' ')

NumberOfRobots = size(SpeedsOfRobots,2);

% Generate data for each robot separately
for Robot = 1:NumberOfRobots
    
    RangeFinderData = [];
    Pose = [];
    LaserOrientations = [];
    PoseNumber = [];
    
    %load in environment layout
    MapXs = [WorldMap.WallStartPointsX;WorldMap.WallEndPointsX];
    MapYs = [WorldMap.WallStartPointsY;WorldMap.WallEndPointsY];
    
    %load in robot parameters
    InitialLocation = RobotFixedParameters.InitialLocation(:,Robot);
    InitialOrientation = RobotFixedParameters.InitialOrientation(Robot);
    DegreesOfSweep = RobotFixedParameters.DegreesOfSweep(Robot);
    NumberOfBeams = RobotFixedParameters.NumberOfBeams(Robot);
    MaxRange = RobotFixedParameters.MaxRange(Robot);
    LaserAngVar = RobotFixedParameters.LaserAngVar(Robot)*pi/180;
    LaserRangVar = RobotFixedParameters.LaserRangVar(Robot);
    
    Direction = DirectionsOfRobots{Robot};
    RobotSpeed = SpeedsOfRobots{Robot};
    
    SpeedVar = 0;
    DirectionVar = 0;
    
    for Time = 0:size(RobotSpeed,2)
        
        if Time == 0
            RobotLocation = InitialLocation;
            RobotOrientation = InitialOrientation;
            PoseInfo = [RobotLocation;RobotOrientation];
        else
            RobotLocation = [RobotSpeed(Time)*cosd(Direction(Time)+90);RobotSpeed(Time)*sind(Direction(Time)+90)];
            RobotOrientation = Direction(Time)+90;
            PoseInfo = [PoseInfo, PoseInfo(:,end)+[RobotSpeed(Time)*cosd(Direction(Time)+PoseInfo(3,end));RobotSpeed(Time)*sind(Direction(Time)+PoseInfo(3,end));Direction(Time)]];
            
        end
        
        %=== Orient Environment To Robot Starting Point========
        %Translate
        if Time == 0
            CurGloAng=0;     %Current Global Angles
            MapXs = MapXs - RobotLocation(1,:);
            MapYs = MapYs - RobotLocation(2,:);
        else
            CurGloAng =  (sum(Direction(1:Time-1))+90)*pi/180;
            RotMat = [cos(CurGloAng) -sin(CurGloAng);sin(CurGloAng) cos(CurGloAng)];
            RotVarOfVeh = inv(RotMat)*[0 0;0 0]*RotMat;
            MapXs = MapXs - RobotLocation(1,:);
            MapYs = MapYs - RobotLocation(2,:);
        end
        
        %Rotate the map
        if Time == 0
            Angle = (90-RobotOrientation)*pi/180;
        else
            Angle = (90-RobotOrientation)*pi/180+sqrt(0*pi/180)*randn(1);
        end
        
        %Convert to map to polar
        [StartPointsTheta, StartPointsR] = cart2pol(MapXs(1,:),MapYs(1,:));
        [EndPointsTheta, EndPointsR] = cart2pol(MapXs(2,:),MapYs(2,:));
        
        %Rotate
        StartPointsTheta = StartPointsTheta + Angle;
        EndPointsTheta = EndPointsTheta + Angle;
        
        %Convert back to cartesian
        [StartX, StartY] = pol2cart(StartPointsTheta,StartPointsR);
        [EndX, EndY] = pol2cart(EndPointsTheta,EndPointsR);
        
        MapXs = [StartX;EndX];
        MapYs = [StartY;EndY];
        %===================================================
        
        figure(2) %Generate figure of current robot pose
        plot(MapXs, MapYs, 'b');
        hold on
        plot(0,0,'ro')              %Include Robot in plot
        plot([0;0],[0;0.5],'r')
        title('New Map')
        %  hold off
        axis equal
        
        
        %% Create Laser Beam Segments
        BeamAngles = linspace(0,DegreesOfSweep,NumberOfBeams)*pi/180 + sqrt(LaserAngVar)*randn(1,NumberOfBeams);
        BeamRange = MaxRange*ones(1,NumberOfBeams);
        [MaxBeamEndPointsX,MaxBeamEndPointsY]  = pol2cart(BeamAngles,BeamRange);
        MaxBeamEndPoints = [MaxBeamEndPointsX;MaxBeamEndPointsY];
        BeamXs = [zeros(1,NumberOfBeams);MaxBeamEndPointsX];
        BeamYs = [zeros(1,NumberOfBeams);MaxBeamEndPointsY];
        
        plot(BeamXs,BeamYs,'g')
        plot(0,0,'ro')              %Include Robot in plot
        plot([0;0],[0;0.5],'r')
        
        
        %% Find Points of intersection
        Hits = findLaserHits(MapXs,MapYs,NumberOfBeams,BeamAngles,MaxRange);
                
        plot(Hits(1,:),Hits(2,:),'r+')
        hold off
        pause(0.2)
        
        [PolarHitsTheta,PolarHitsR] = cart2pol(Hits(1,:),Hits(2,:));
        PolarHits = [PolarHitsTheta;PolarHitsR];
        
        RangeFinderData = [RangeFinderData; PolarHitsR];
        LaserOrientations = [LaserOrientations; linspace(0,DegreesOfSweep,NumberOfBeams)*pi/180-pi/2];
        PoseNumber = [PoseNumber; (Time+1)*ones(1,size(RangeFinderData,2))];
        
        
        
    end
    
    %%============Noise in laser===========
    
    
    %Generate Range Noise
    RangeNoise = sqrt(LaserRangVar)*randn(size(RangeFinderData));
    I = find(RangeFinderData>MaxRange-0.1); %Makes sure no range is greater than max
    RangeNoise(I) = 0;
    RangeFinderData = RangeFinderData + RangeNoise;
    
    %Check for negative distances
    I = find(RangeFinderData<0);
    RangeFinderData(I) = 0;
    
    %==================================
    
    
    %=====================================================================
    
    %Creates stucture containing only laser info
    %--------Evaluates Pose Relative to Vehicles Origin for each scan
    [TempThet,TempR]= cart2pol(PoseInfo(1,:)-PoseInfo(1,1),PoseInfo(2,:)-PoseInfo(2,1));
    TempThet= TempThet - PoseInfo(3,1)*pi/180;%relative to vehicle intial orientation - first move was on the robot main axis.
    [Pose(1,:),Pose(2,:)] = pol2cart(TempThet,TempR);
    
    % Pose = [PoseInfo(1,:)-PoseInfo(1,1);PoseInfo(2,:)-PoseInfo(2,1);PoseInfo(3,:)-PoseInfo(3,1)];
    Pose(3,:) = (PoseInfo(3,:)-PoseInfo(3,1))*pi/180;
    
    %---------------------------------------------------------
    %% Structure of ranges orientations and robot pose
    
    TrainingData.PoseNumber = reshape(PoseNumber',1,NumberOfBeams*(Time+1));
    TrainingData.Pose = zeros(3,numel(PoseNumber));
    for i = 1:max(PoseNumber)
        TrainingData.Pose(:,TrainingData.PoseNumber==i) =  repmat(Pose(:,i),1,size(PoseNumber,2));
    end
    RangeFinderData = reshape(RangeFinderData',1,NumberOfBeams*(Time+1));
    LaserOrientations = reshape(LaserOrientations',1,NumberOfBeams*(Time+1));
    
    %  Add all ranges
    FreePoints = Sensor2World(TrainingData.Pose(1:2,:),TrainingData.Pose(3,:),RangeFinderData,LaserOrientations);
    fpn = TrainingData.PoseNumber;
%     MaxRangInd = find(RangeFinderData > MaxRange-0.1);
%     RangeFinderData(MaxRangInd)=[];
%     LaserOrientations(MaxRangInd)=[];
%     TrainingData.Pose(:,MaxRangInd)=[];
%     TrainingData.PoseNumber(:,MaxRangInd)=[];
    Temp.OccupiedPoints = Sensor2World(TrainingData.Pose(1:2,:),TrainingData.Pose(3,:),RangeFinderData,LaserOrientations);
    Temp.LibraryOfStartPoints = TrainingData.Pose(1:2,:);
    Temp.LibraryOfEndPoints = Sensor2World(TrainingData.Pose(1:2,:),TrainingData.Pose(3,:),RangeFinderData-0.1,LaserOrientations);
    
    figure(8)
%     subplot(3,1,3)
    title('Free Points and Hits');
    hold on
%     plot([Temp.LibraryOfStartPoints(1,:);Temp.LibraryOfEndPoints(1,:)],[Temp.LibraryOfStartPoints(2,:);Temp.LibraryOfEndPoints(2,:)],'r')

    % Add free points
    res = 1;
    for i = 1:max(TrainingData.PoseNumber)
%         pause
        hits = Temp.LibraryOfEndPoints(:,TrainingData.PoseNumber==i);
        robot = Temp.LibraryOfStartPoints(:,TrainingData.PoseNumber==i);
        robot = robot(:,1);
        free = [];
        for el = FreePoints(:,fpn == i)
            d = pdist([el, robot]);
            if d < res
                continue
            else
                ds = (d - mod(d,res)) / res;
                dx = (el(1) - robot(1)) / ds;
                dy = (el(2) - robot(2)) / ds;
                xs = robot(1):dx:el(1);
                ys = robot(2):dy:el(2);
                if(isempty(xs) || isempty(ys))
                    continue;
                else
                    free = [free, [xs(1:end-1);ys(1:end-1)]];
                end
            end
        end
        TrainingData.Cells(i).FreePoints = free;
    end
    free = [TrainingData.Cells(:).FreePoints];
    plot(free(1,:),free(2,:),'go')
    axis equal

    
    % Remove max ranges
    FreePoints = Sensor2World(TrainingData.Pose(1:2,:),TrainingData.Pose(3,:),RangeFinderData,LaserOrientations);
    fpn = TrainingData.PoseNumber;
    MaxRangInd = find(RangeFinderData > MaxRange-0.1);
    RangeFinderData(MaxRangInd)=[];
    LaserOrientations(MaxRangInd)=[];
    TrainingData.Pose(:,MaxRangInd)=[];
    TrainingData.PoseNumber(:,MaxRangInd)=[];
    TrainingData.OccupiedPoints = Sensor2World(TrainingData.Pose(1:2,:),TrainingData.Pose(3,:),RangeFinderData,LaserOrientations);
    TrainingData.LibraryOfStartPoints = TrainingData.Pose(1:2,:);
    TrainingData.LibraryOfEndPoints = Sensor2World(TrainingData.Pose(1:2,:),TrainingData.Pose(3,:),RangeFinderData-0.1,LaserOrientations);
    
    
    plot(TrainingData.OccupiedPoints(1,:),TrainingData.OccupiedPoints(2,:),'r+')

    figure(9)
%     subplot(3,1,2)
    plot(Temp.OccupiedPoints(1,:),Temp.OccupiedPoints(2,:),'+')
    hold on
    plot([Temp.LibraryOfStartPoints(1,:);Temp.LibraryOfEndPoints(1,:)],[Temp.LibraryOfStartPoints(2,:);Temp.LibraryOfEndPoints(2,:)],'r')
    axis equal
    title('Rangefinder Data')
    
    for i = 1:max(TrainingData.PoseNumber)
%         pause
        hits = TrainingData.LibraryOfEndPoints(:,TrainingData.PoseNumber==i);
        x1 = min(hits(1,:)) - 1;
        y1 = min(hits(2,:)) - 1;
        x2 = max(hits(1,:)) + 1;
        y2 = max(hits(2,:)) + 1;
        TrainingData.Cells(i).Boundaries = [x1 x2; y1 y2];
        TrainingData.Cells(i).OccuPoints = hits;
        TrainingData.Cells(i).Robot = robot;
    end
end

save('TrainingData', 'TrainingData')