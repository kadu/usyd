% Environment file.
% Creates the world and the robots actions
% Set up to handle multiple robots.

%% Create World Map

%Initialise
WorldMap = struct('WallStartPointsX',[],'WallStartPointsY',[],'WallEndPointsX',[],'WallEndPointsY',[])


%% Create Object Boundaries
% Initialise Corner = [] at the start of each object and
% Always end each object with "WorldMap = IncludeObjectInWorld(Corner,
% WorldMap);"

%Object 1 --- Outer boundary of area
Corner = [];
Corner(1,:) = [-2 -2];
Corner(2,:) = [8 -2];
Corner(3,:) = [8 14];
Corner(4,:) = [-2 14];
WorldMap = IncludeObjectInWorld(Corner, WorldMap);

%Object 2 -- wall on one side of the room
Corner = [];
Corner(1,:) = [2 2.5];
Corner(2,:) = [5 2.5];
Corner(3,:) = [5 2];
Corner(4,:) = [2 2];
WorldMap = IncludeObjectInWorld(Corner, WorldMap);

%Object two -- wall on the other side
Corner = [];
Corner(1,:) = [2 8.5];
Corner(2,:) = [5 8.5];
Corner(3,:) = [5 8];
Corner(4,:) = [2 8];
WorldMap = IncludeObjectInWorld(Corner, WorldMap);

%% Initialise the robot parameters
RobotFixedParameters = struct('InitialLocation', [],...
                  'InitialOrientation',[],...
                  'NumberOfBeams',[],...
                  'DegreesOfSweep',[],...
                  'MaxRange',[],...
                  'LaserAngVar',[],...
                  'LaserRangVar',[],...
                  'SpeedVar',[],...
                  'DirectionVar',[]);


%% -----------Robot 1-------------------------------------
RobotNumber = 1;

InitialLocation = [0;0];
InitialOrientation = 0;     %Relative to X axis in degrees
NumberOfBeams = 25;         %37;
DegreesOfSweep = 180;
MaxRange = 10;
LaserAngVar = 0;            %Measured in degrees
LaserRangVar = 0;           %Measured in metres
SpeedVar = 0;    %Variance of vehicle's speed. Measured as a fraction of vehicles speed. i.e. 0.1 m var for 1 m/s, 0.2 m var for 2 m/s
DirectionVar = 0; %Variance of vehicles direction. Measured as a fraction of vehicles turn angle

RobotSpeed = [1,1,1,1, 1, 1, 1, 1, 1, 1, 1, 1, 1,  1,  1,  1,  1,  1,  1,  1,  1,  1];
Direction =  [0,0,0,0,20,20,20,20,20,20,20,20,20,-20,-20,-20,-20,-20,-20,-20,-20,-20];        %0 is straight ahead. +degrees to left, -degrees to right

%-------------------------------------------------------

%Update RobotFixedParameters

RobotFixedParameters = struct('InitialLocation', [RobotFixedParameters.InitialLocation InitialLocation],...
                  'InitialOrientation',[RobotFixedParameters.InitialOrientation InitialOrientation],...
                  'NumberOfBeams',[RobotFixedParameters.NumberOfBeams NumberOfBeams],...
                  'DegreesOfSweep',[RobotFixedParameters.DegreesOfSweep DegreesOfSweep],...
                  'MaxRange',[RobotFixedParameters.MaxRange MaxRange],...
                  'LaserAngVar',[RobotFixedParameters.LaserAngVar LaserAngVar],...
                  'LaserRangVar',[RobotFixedParameters.LaserRangVar LaserRangVar],...
                  'SpeedVar',[RobotFixedParameters.SpeedVar SpeedVar],...
                  'DirectionVar',[RobotFixedParameters.DirectionVar DirectionVar]);
              
SpeedsOfRobots(RobotNumber) = {RobotSpeed};       
DirectionsOfRobots(RobotNumber) = {Direction};
              


%% Clean Up
clear DegreesOfSweep
clear Direction
clear InitialLocation
clear InitialOrientation
clear MaxRange
clear NumberOfBeams
clear RobotNumber
clear RobotSpeed
clear LaserAngVar
clear LaserRangVar
clear SpeedVar
clear DirectionVar
clear Corner




