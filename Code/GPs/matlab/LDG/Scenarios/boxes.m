% Environment file.
% Creates the world and the robots actions
% Set up to handle multiple robots.

%% Create World Map

%Initialise
WorldMap = struct('WallStartPointsX',[],'WallStartPointsY',[],'WallEndPointsX',[],'WallEndPointsY',[])


%% Create Object Boundaries
% Initialise Corner = [] at the start of each object and
% Always end each object with "WorldMap = IncludeObjectInWorld(Corner,
% WorldMap);"

%Object 1 --- Outer boundary of area
Corner = [];
Corner(1,:) = [-2 -2];
Corner(2,:) = [-2 16];
Corner(3,:) = [14 16];
Corner(4,:) = [14 -2];
WorldMap = IncludeObjectInWorld(Corner, WorldMap);

%Object 2 --- walls
Corner = [];
Corner(1,:) = [-2 2];
Corner(2,:) = [-2 12];
Corner(3,:) = [4 12];
Corner(4,:) = [4 2];
WorldMap = IncludeObjectInWorld(Corner, WorldMap);

%Object 2 --- box straight
Corner = [];
Corner(1,:) = [6  2];
Corner(2,:) = [6  6];
Corner(3,:) = [10 6];
Corner(4,:) = [10 2];
WorldMap = IncludeObjectInWorld(Corner, WorldMap);

%Object 2 --- box twisted
Corner = [];
Corner(1,:) = [8 8];
Corner(2,:) = [10 10];
Corner(3,:) = [8 12];
Corner(4,:) = [6 10];
WorldMap = IncludeObjectInWorld(Corner, WorldMap);



%% Initialise the robot parameters
RobotFixedParameters = struct('InitialLocation', [],...
                  'InitialOrientation',[],...
                  'NumberOfBeams',[],...
                  'DegreesOfSweep',[],...
                  'MaxRange',[],...
                  'LaserAngVar',[],...
                  'LaserRangVar',[],...
                  'SpeedVar',[],...
                  'DirectionVar',[]);


%% -----------Robot 1-------------------------------------
RobotNumber = 1;

InitialLocation = [0;0];
InitialOrientation = 00;     %Relative to X axis in degrees
NumberOfBeams = 19;         %37;
DegreesOfSweep = 360;
MaxRange = 8;
LaserAngVar = 0.01;            %Measured in degrees
LaserRangVar = 0.05;           %Measured in metres
SpeedVar = 0.01;    %Variance of vehicle's speed. Measured as a fraction of vehicles speed. i.e. 0.1 m var for 1 m/s, 0.2 m var for 2 m/s
DirectionVar = 0.05; %Variance of vehicles direction. Measured as a fraction of vehicles turn angle

RobotSpeed = [2 2 2 2 2 2  0 2 2 2 2 2.4 2.4 2.4 2.4 2.4];
Direction =  [0 0 0 0 0 0 90 0 0 0 0  18  18  18  18  18];        %0 is straight ahead. +degrees to left, -degrees to right

%-------------------------------------------------------

%Update RobotFixedParameters

RobotFixedParameters = struct('InitialLocation', [RobotFixedParameters.InitialLocation InitialLocation],...
                  'InitialOrientation',[RobotFixedParameters.InitialOrientation InitialOrientation],...
                  'NumberOfBeams',[RobotFixedParameters.NumberOfBeams NumberOfBeams],...
                  'DegreesOfSweep',[RobotFixedParameters.DegreesOfSweep DegreesOfSweep],...
                  'MaxRange',[RobotFixedParameters.MaxRange MaxRange],...
                  'LaserAngVar',[RobotFixedParameters.LaserAngVar LaserAngVar],...
                  'LaserRangVar',[RobotFixedParameters.LaserRangVar LaserRangVar],...
                  'SpeedVar',[RobotFixedParameters.SpeedVar SpeedVar],...
                  'DirectionVar',[RobotFixedParameters.DirectionVar DirectionVar]);
              
SpeedsOfRobots(RobotNumber) = {RobotSpeed};       
DirectionsOfRobots(RobotNumber) = {Direction};
              
% %-------------------------------------------------------------

%Update RobotFixedParameters

RobotFixedParameters = struct('InitialLocation', [RobotFixedParameters.InitialLocation InitialLocation],...
                  'InitialOrientation',[RobotFixedParameters.InitialOrientation InitialOrientation],...
                  'NumberOfBeams',[RobotFixedParameters.NumberOfBeams NumberOfBeams],...
                  'DegreesOfSweep',[RobotFixedParameters.DegreesOfSweep DegreesOfSweep],...
                  'MaxRange',[RobotFixedParameters.MaxRange MaxRange],...
                  'LaserAngVar',[RobotFixedParameters.LaserAngVar LaserAngVar],...
                  'LaserRangVar',[RobotFixedParameters.LaserRangVar LaserRangVar],...
                  'SpeedVar',[RobotFixedParameters.SpeedVar SpeedVar],...
                  'DirectionVar',[RobotFixedParameters.DirectionVar DirectionVar]);
              
SpeedsOfRobots(RobotNumber) = { RobotSpeed};
DirectionsOfRobots(RobotNumber) = { Direction};


% VehicleNoiseSwitch = 0;             %This switch should not be in this file

%% Clean Up
clear DegreesOfSweep
clear Direction
clear InitialLocation
clear InitialOrientation
clear MaxRange
clear NumberOfBeams
clear RobotNumber
clear RobotSpeed
clear LaserAngVar
clear LaserRangVar
clear SpeedVar
clear DirectionVar
clear Corner




