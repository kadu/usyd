

function [WorldMap] = IncludeObjectInWorld(Corner, WorldMap)

WorldMap.WallStartPointsX = [WorldMap.WallStartPointsX Corner(:,1)'];
WorldMap.WallStartPointsY = [WorldMap.WallStartPointsY Corner(:,2)'];
WorldMap.WallEndPointsX = [WorldMap.WallEndPointsX  [Corner(2:end,1)' Corner(1,1)']];
WorldMap.WallEndPointsY = [WorldMap.WallEndPointsY  [Corner(2:end,2)' Corner(1,2)']];
