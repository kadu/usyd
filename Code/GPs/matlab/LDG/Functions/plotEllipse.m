% Receives the means [x;y] and covariances (3-d matrix) of the Gaussian and
% plots the ellipses
% Means=[0;0]
% Cov=[1 0;1 0.01]

function  plotEllipse(Means,Cov)

figure
plot(Means(1,:),Means(2,:),'b+')
hold on
axis equal
[V,D] = eig(Cov)
for i = 1:size(Means,2)
   
    ellipse = ellipse_sigma(Means(:,i),Cov(:,:,i),1);
    plot(ellipse(1,:),ellipse(2,:),'r')
    
end

plot([0; V(2,1)], [0;V(1,1)])
plot([0; V(2,2)], [0;V(1,2)])
hold off
