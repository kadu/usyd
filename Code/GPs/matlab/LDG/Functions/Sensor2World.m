
function Projection = Sensor2World(VehLoc,VehOrient,SensorRange,SensorBearing)

%Inputs
%VehLoc = [x,y] - Global location of vehicle
%VehOrient = Orientation of vehicle in radians relative to global frame
%SensorRange = range reading of sensor
%SensorBearing = bearing of sensor reading relative to robot

%Outputs
%Projection is an (x,y) location in the world.


% %Test data
% VehLoc = [-6;4];
% VehOrient = 0;%pi/4;
% SensorRange = 5.7;
% SensorBearing = -1.4;



% SensorBearing = SensorBearing
% VehLoc
% VehOrient
% SensorRange
% SensorBearing
% % 




% [tempX,tempY] = pol2cart(SensorBearing,SensorRange);
% 
% Projection = VehLoc + [tempX;tempY];







GlobalBearing = SensorBearing+VehOrient;

[tempX,tempY] = pol2cart(GlobalBearing,SensorRange);

Projection = VehLoc + [tempX;tempY];
% pause
