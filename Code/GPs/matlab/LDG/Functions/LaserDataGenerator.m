
% Script that generates the laser data obtained by a robot as it navigates 
% a user-created environment moving along a predetermined path. It also
% outputs the pose of the robot during each of the scans so successive
% scans can be merged together precisely.

%Set up to handle multiple robots

clc
close all
disp(' ')
disp('*********LaserDataGenerator***********')
disp(' ')
clear all
addpath Scenarios

%% Load Scenario File (World Map and Laser settings and Initial Conditions)
RoomTwoRobots

%% Switches
SensorNoiseSwitch = 1;
PositionalUncertSwitch = 1;  %Include uncertainty in the robot's position?
                            %This feature is currently not finished


%% Initialise Settings
SimData = [];

disp('Environment Loaded')
disp('Robot Path Loaded')
disp(' ')


%% Generate the map
MapXs = [WorldMap.WallStartPointsX;WorldMap.WallEndPointsX];
MapYs = [WorldMap.WallStartPointsY;WorldMap.WallEndPointsY];
figure(7)
plot(MapXs, MapYs, 'b');
axis equal
title('Ground Truth');
% hold off
pause


%% Begin simulating data
disp('Running Simulation...')
disp(' ')

NumberOfRobots = size(SpeedsOfRobots,2);
%Could include error check here to make sure that every robot has the right
%number of parameters

% Generate data for each robot separately
for Robot = 1:NumberOfRobots
    
   
    RangeFinderData = [];
    Pose = [];
    LaserOrientations = [];
    
    %load in environment layout
    MapXs = [WorldMap.WallStartPointsX;WorldMap.WallEndPointsX];
    MapYs = [WorldMap.WallStartPointsY;WorldMap.WallEndPointsY];
    
    %load in robot parameters
    InitialLocation = RobotFixedParameters.InitialLocation(:,Robot);
    InitialOrientation = RobotFixedParameters.InitialOrientation(Robot);
    DegreesOfSweep = RobotFixedParameters.DegreesOfSweep(Robot);
    NumberOfBeams = RobotFixedParameters.NumberOfBeams(Robot);
    MaxRange = RobotFixedParameters.MaxRange(Robot);
    LaserAngVar = RobotFixedParameters.LaserAngVar(Robot)*pi/180;
    LaserRangVar = RobotFixedParameters.LaserRangVar(Robot);
    SpeedVar = RobotFixedParameters.SpeedVar(Robot);
    DirectionVar = RobotFixedParameters.SpeedVar(Robot);
    
    Direction = DirectionsOfRobots{Robot};
    RobotSpeed = SpeedsOfRobots{Robot};
    
    if PositionalUncertSwitch ~= 1
        SpeedVar = 0;
        DirectionVar = 0;
    end
    
    
    %Generate Noise In Speed
    RobotSpeed = RobotSpeed + RobotSpeed.*(sqrt(SpeedVar)*randn(1,size(RobotSpeed,2)));
    Direction = Direction + Direction.*(sqrt(DirectionVar)*randn(1,size(Direction,2)));
    

    for Time = 0:size(RobotSpeed,2)

        if Time == 0
            RobotLocation = InitialLocation;
            RobotOrientation = InitialOrientation;
            PoseInfo = [RobotLocation;RobotOrientation];
        else
            RobotLocation = [RobotSpeed(Time)*cosd(Direction(Time)+90);RobotSpeed(Time)*sind(Direction(Time)+90)];
            RobotOrientation = Direction(Time)+90;
            PoseInfo = [PoseInfo, PoseInfo(:,end)+[RobotSpeed(Time)*cosd(Direction(Time)+PoseInfo(3,end));RobotSpeed(Time)*sind(Direction(Time)+PoseInfo(3,end));Direction(Time)]];

        end

        %=== Orient Environment To Robot Starting Point========
        %Translate
        if Time == 0
           CurGloAng=0;
           MapXs = MapXs - RobotLocation(1,:);
           MapYs = MapYs - RobotLocation(2,:);
        else
           CurGloAng =  (sum(Direction(1:Time-1))+90)*pi/180;
           RotMat = [cos(CurGloAng) -sin(CurGloAng);sin(CurGloAng) cos(CurGloAng)];
           RotVarOfVeh = inv(RotMat)*[0 0;0 0]*RotMat;
           MapXs = MapXs - RobotLocation(1,:);
           MapYs = MapYs - RobotLocation(2,:);
        end

        %Rotate the map
        if Time == 0
           Angle = (90-RobotOrientation)*pi/180;
        else
            Angle = (90-RobotOrientation)*pi/180+sqrt(0*pi/180)*randn(1);   
        end

        %Convert to map to polar
        [StartPointsTheta, StartPointsR] = cart2pol(MapXs(1,:),MapYs(1,:));
        [EndPointsTheta, EndPointsR] = cart2pol(MapXs(2,:),MapYs(2,:));

        %Rotate
        StartPointsTheta = StartPointsTheta + Angle;
        EndPointsTheta = EndPointsTheta + Angle;

        %Convert back to cartesian
        [StartX, StartY] = pol2cart(StartPointsTheta,StartPointsR);
        [EndX, EndY] = pol2cart(EndPointsTheta,EndPointsR);

        MapXs = [StartX;EndX];
        MapYs = [StartY;EndY]; 
        %===================================================

        figure(2) %Generate figure of current robot pose
            plot(MapXs, MapYs, 'b');
            hold on
            plot(0,0,'ro')              %Include Robot in plot
            plot([0;0],[0;0.5],'r')
            title('New Map')
            %  hold off
            axis equal


        %% Create Laser Beam Segments
        BeamAngles = linspace(0,DegreesOfSweep,NumberOfBeams)*pi/180 + sqrt(LaserAngVar)*randn(1,NumberOfBeams);
        BeamRange = MaxRange*ones(1,NumberOfBeams);
        [MaxBeamEndPointsX,MaxBeamEndPointsY]  = pol2cart(BeamAngles,BeamRange);
        MaxBeamEndPoints = [MaxBeamEndPointsX;MaxBeamEndPointsY];
        BeamXs = [zeros(1,NumberOfBeams);MaxBeamEndPointsX];
        BeamYs = [zeros(1,NumberOfBeams);MaxBeamEndPointsY];

        plot(BeamXs,BeamYs,'g')
        plot(0,0,'ro')              %Include Robot in plot
        plot([0;0],[0;0.5],'r')


        %% Find Points of intersection
        Hits = findLaserHits(MapXs,MapYs,NumberOfBeams,BeamAngles,MaxRange);

        plot(Hits(1,:),Hits(2,:),'r+')
        hold off
        pause(0.2)

        [PolarHitsTheta,PolarHitsR] = cart2pol(Hits(1,:),Hits(2,:));
        PolarHits = [PolarHitsTheta;PolarHitsR];

        RangeFinderData = [RangeFinderData; PolarHitsR];
        LaserOrientations = [LaserOrientations; linspace(0,DegreesOfSweep,NumberOfBeams)*pi/180-pi/2];


    end

    %%============Noise in laser===========
    if SensorNoiseSwitch == 1

         %Generate Range Noise
         RangeNoise = sqrt(LaserRangVar)*randn(size(RangeFinderData));
         I = find(RangeFinderData>MaxRange-0.1); %Makes sure no range is greater than max
         RangeNoise(I) = 0;
         RangeFinderData = RangeFinderData + RangeNoise;

         %Check for negative distances
         I = find(RangeFinderData<0);
         RangeFinderData(I) = 0;

    end

    %==================================
    
    %====Estimates Vehicle Position Uncertainty============
    [UKFRelLocOrien UKFVar] = CalculateRelPoseVar(RobotSpeed, Direction, SpeedVar, DirectionVar);
    VehicleUncert = UKFVar;
    
  %=====================================================================

    %Creates stucture containing only laser info
    %--------Evaluates Pose Relative to Vehicles Origin for each scan
    [TempThet,TempR]= cart2pol(PoseInfo(1,:)-PoseInfo(1,1),PoseInfo(2,:)-PoseInfo(2,1));
    TempThet= TempThet - PoseInfo(3,1)*pi/180;
    [Pose(1,:),Pose(2,:)] = pol2cart(TempThet,TempR);

    % Pose = [PoseInfo(1,:)-PoseInfo(1,1);PoseInfo(2,:)-PoseInfo(2,1);PoseInfo(3,:)-PoseInfo(3,1)];
    Pose(3,:) = (PoseInfo(3,:)-PoseInfo(3,1))*pi/180;

    %---------------------------------------------------------
    %% Structure of ranges orientations and robot pose

    RobotDataKnown = struct('RangeFinderData',RangeFinderData,'Pose',Pose,'LaserOrientations',LaserOrientations,...
                     'BeamsPerScan',NumberOfBeams,'InitialLocation',InitialLocation,'InitialOrientation',InitialOrientation,...
                     'MaxRange', MaxRange,'VehicleUncert',VehicleUncert,...
                     'LaserAngVar',LaserAngVar,'LaserRangVar',LaserRangVar);

    SimData = [SimData RobotDataKnown];

end


disp('Simulation Complete.')
disp(' ')


save SimData SimData
disp('--Laser Data and Pose Data saved as SimData')
disp(' ')
pause(0.5)


%% Evaluate Pose To Allow Truthful Merging of Scans

clear all
load SimData
%-------------Rebuilds the map based on known pose and scan data-----------
disp('Merging scans using known pose... ')
disp(' ')
pause(0.5)
figure(99)
for Robot=1:size(SimData,2)
    clear GlobalPose
    clear GlobalLaserHits
   
    RobotData = SimData(Robot);
    Pose = RobotData.Pose;
    RangeFinderData = RobotData.RangeFinderData;
    InitialLocation = RobotData.InitialLocation;
    InitialOrientation = RobotData.InitialOrientation;
    LaserOrientations = RobotData.LaserOrientations;
    
    %Determine Global Pose
    %Rotate
    [TempThet1 TempR1] = cart2pol(Pose(1,:),Pose(2,:));
    TempThet1 = TempThet1 + InitialOrientation*pi/180;
    [GlobalPose(1,:),GlobalPose(2,:)] = pol2cart(TempThet1, TempR1);
    %Translate
    GlobalPose = [GlobalPose(1:2,:)+repmat(InitialLocation,1,size(Pose,2));Pose(3,:)+InitialOrientation*pi/180];

    
   for i = 1:size(Pose,2)
    
        LaserHitsRelativeToRobot = [RangeFinderData(i,:).*-cos(LaserOrientations(i,:)-pi/2);RangeFinderData(i,:).*-sin(LaserOrientations(i,:)-pi/2)];
        
        %In GlobalCoords

        [TempThet TempR] = cart2pol(LaserHitsRelativeToRobot(1,:),LaserHitsRelativeToRobot(2,:));
        TempThet = TempThet+GlobalPose(3,i)-pi/2;
        [GlobalLaserHits(1,:),GlobalLaserHits(2,:)] = pol2cart(TempThet,TempR);
        
        
       GlobalLaserHits = GlobalLaserHits+repmat(GlobalPose(1:2,i),1,size(LaserHitsRelativeToRobot,2));
    
     if Robot == 1
        plot(GlobalLaserHits(1,:),GlobalLaserHits(2,:),'b+')
        hold on
     else
        plot(GlobalLaserHits(1,:),GlobalLaserHits(2,:),'r+')
        hold on
     end
     
    end
end
title('Reconstructed Map including Non-returns')
hold off

disp('Merge Complete.')
disp(' ')



%% Create Laser Struct
clear all
load SimData


    StructRange = [];
    StructPoseNumber = [];
    StructRobotPose = [];
    StructLaserOrien = [];
    StructBearingVar = [];
    StructRangeVar = [];
    StructRelPoseVar = [];



for Robot=1:size(SimData,2)
    
    LasersPerScan = size(SimData(Robot).RangeFinderData,2);
    NumberOfPoses = size(SimData(Robot).Pose,2);
    NumberOfLasers = LasersPerScan*NumberOfPoses;
    
    clear GlobalPose
    clear GlobalLaserHits
   
    RobotData = SimData(Robot);
    Pose = RobotData.Pose;
    RangeFinderData = RobotData.RangeFinderData;
    InitialLocation = RobotData.InitialLocation;
    InitialOrientation = RobotData.InitialOrientation;
    LaserOrientations = RobotData.LaserOrientations;
    
    %Determine Global Pose
    %Rotate
    [TempThet1 TempR1] = cart2pol(Pose(1,:),Pose(2,:));
    TempThet1 = TempThet1 + InitialOrientation*pi/180;
    [GlobalPose(1,:),GlobalPose(2,:)] = pol2cart(TempThet1, TempR1);
    %Translate
    GlobalPose = [GlobalPose(1:2,:)+repmat(InitialLocation,1,size(Pose,2));Pose(3,:)+InitialOrientation*pi/180];

    
    Range = reshape(SimData(Robot).RangeFinderData',1,numel(SimData(Robot).RangeFinderData));
    PoseNumber = reshape(repmat(linspace(1,NumberOfPoses,NumberOfPoses)',1,LasersPerScan)',1,NumberOfLasers);
    RobotPose = GlobalPose(:,PoseNumber);
    LaserOrien = reshape(LaserOrientations',1,NumberOfLasers);
    BearingVar = RobotData.LaserAngVar*ones(1,NumberOfLasers);
    RangeVar = RobotData.LaserAngVar*ones(1,NumberOfLasers);
    RelPoseVar = RobotData.VehicleUncert(:,:,PoseNumber);
    
    
    StructRange = [StructRange Range];
    StructPoseNumber = [StructPoseNumber PoseNumber];
    StructRobotPose = [StructRobotPose, RobotPose];
    StructLaserOrien = [StructLaserOrien LaserOrien];
    StructBearingVar = [StructBearingVar BearingVar];
    StructRangeVar = [StructRangeVar RangeVar];
    StructRelPoseVar = cat(3,StructRelPoseVar,RelPoseVar);
   
end
    
    LaserStruct = struct('Range',StructRange,'Pose',StructRobotPose,'PoseNumber',StructPoseNumber,'LaserOrient',StructLaserOrien,'BearingCov',StructBearingVar,...
        'RangeCov',StructRangeVar,'PoseCov',StructRelPoseVar);
    
    save LaserStruct LaserStruct
%     
%        LaserStruct.Range =  LaserStruct.Range(LocationOfBeamsOfInterest);
%    LaserStruct.Pose =  LaserStruct.Pose(:,LocationOfBeamsOfInterest);
%    LaserStruct.PoseNumber =  LaserStruct.PoseNumber(LocationOfBeamsOfInterest);
%    LaserStruct.LaserOrient =  LaserStruct.LaserOrient(LocationOfBeamsOfInterest);
%    LaserStruct.BearingCov = LaserStruct.BearingCov(LocationOfBeamsOfInterest);
%    LaserStruct.RangeCov = LaserStruct.RangeCov(LocationOfBeamsOfInterest);
%    LaserStruct.PoseCov =
%    LaserStruct.PoseCov(:,:,LocationOfBeamsOfInterest);
%     
    