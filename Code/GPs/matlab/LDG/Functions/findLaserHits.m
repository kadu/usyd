


% Function returns laser hits given environment and beam line parameters

%Robot is assumed to be at the origin and facing in the positive direction
%of the y-axis. MapXs and MapYs represent the environment relative to the
%robot's pose.
%BeamAngles - Number of beams per scan
%Angle of each beam relative to direction which robot is facing
%Maximum range of the beam


function Hits = findLaserHits(MapXs,MapYs,NumberOfBeams,BeamAngles,MaxRange)


% Represent Each Line Segment in terms of a,b,c
% ax+by+c = 0


%Map Line Segments          % This can be vectorised quite easily
MapLineEquations = [];

BeamRange = MaxRange*ones(1,NumberOfBeams);
[MaxBeamEndPointsX,MaxBeamEndPointsY]  = pol2cart(BeamAngles,BeamRange);
MaxBeamEndPoints = [MaxBeamEndPointsX;MaxBeamEndPointsY];

 for i = 1:size(MapXs,2)    
     
     LineStartPoint = [MapXs(1,i); MapYs(1,i)];
     LineEndPoint = [MapXs(2,i); MapYs(2,i)];
     
     %Slope
     if abs(LineEndPoint(1)-LineStartPoint(1)) >0.0001 %Odd Rounding Error
     Slope = (LineEndPoint(2)-LineStartPoint(2))/ (LineEndPoint(1)-LineStartPoint(1));
     
        if abs(Slope)>0.0001 %Odd Rounding Error
            a = -Slope;
            b = 1;
            c = -LineStartPoint(2)+ LineStartPoint(1)*Slope;
        else
            a = 0;
            b = 1;
            c = -LineEndPoint(2);
        end
     
     else
         a = 1;
         b = 0;
         c = -LineEndPoint(1)  ;
     end
     
     LineEquation = [a;b;c];
     MapLineEquations = [MapLineEquations,LineEquation];
 end

 Hits = []; 
for i = 1:NumberOfBeams
    
    %Equation of Beam
    
    if abs(MaxBeamEndPoints(1,i)) > 0.0001
        Slope = MaxBeamEndPoints(2,i)/MaxBeamEndPoints(1,i);
        a = -Slope;
        b = 1;
        c = 0;
        
    else
        a = 1;
        b = 0;
        c = 0;
    end
    RayEquation = [a;b;c];
    
    %---------------Find Intersects with Map
    HitsOnThisBeam = [];
    for Counter = 1:size(MapLineEquations,2)
        
%         disp('Map Equation');
%         disp(MapLineEquations(:,Counter))
    
        aMap = MapLineEquations(1,Counter);
        bMap = MapLineEquations(2,Counter);
        cMap = MapLineEquations(3,Counter);

         x = [];
         y = [];
         IntersectFound = 0;
        if bMap == 1 && b==1 && aMap~=a
            
            x = -(cMap-c)/(aMap-a);
            y = -c -x*a;
            IntersectFound = 1;
        elseif bMap == 1 && b==0 
            x = -c/a;
            y = -cMap-aMap*x;

            IntersectFound = 1;
        elseif bMap == 0 && b==1 
            x = -cMap/aMap;
            y = -c-a*x;

            IntersectFound = 1;
        end
        
     %Find if Intersect is on the line segment
     
       if IntersectFound ==1
           
        PointOfIntersect = [x;y];
     
        LineStartPoint = [MapXs(1,Counter); MapYs(1,Counter)];
        LineEndPoint = [MapXs(2,Counter); MapYs(2,Counter)];
     
        dist = pdist([PointOfIntersect,LineStartPoint,LineEndPoint]');
        if dist(1) > dist(3) || dist(2) > dist(3);
         x = [];
         y = [];
         IntersectFound = 0;
%          disp ('Ray Erased!')
        end
     
        RayStart = [0;0];
        RayEnd = MaxBeamEndPoints(:,i);
     
        dist = pdist([PointOfIntersect,RayStart,RayEnd]');
        if dist(1) > dist(3) || dist(2) > dist(3);
         x = [];
         y = [];
         IntersectFound = 0;
%          disp ('Ray Erased!')
        end   

       end
     
       if IntersectFound == 1;
        HitsOnThisBeam = [HitsOnThisBeam,PointOfIntersect];
       end
      
    end
    

    
    if size(HitsOnThisBeam,2) ==1
        NearestHit = HitsOnThisBeam;
    elseif size(HitsOnThisBeam,2) ==0       %No Return
        
        [NearestHitCoordX, NearestHitCoordY] = pol2cart(BeamAngles(i),MaxRange);
        NearestHit = [NearestHitCoordX; NearestHitCoordY];
    else
        
        HitsOnThisBeam = [[0;0],HitsOnThisBeam];
        Distances = pdist(HitsOnThisBeam');
        Distances = Distances(1:size(HitsOnThisBeam,2)-1);
        Pointer = 1;
        for j = 1:(size(Distances,2)-1);             %Had to do this coz min() wasn't working. :-/
            if Distances(j+1) < Distances(Pointer);
                Pointer = j+1;
            end
        end
        
        NearestHit = HitsOnThisBeam(:,(Pointer+1));
       
    end
    
    Hits = [Hits, NearestHit];
    
end 