function [xPts, wPts, nPts] = scaledSymmetricSigmaPoints(x,P,alpha,beta,kappa)



% This function returns the scaled symmetric sigma point distribution.
%
%  [xPts, wPts, nPts] = scaledSymmetricSigmaPoints(x,P,alpha,beta,kappa)  
%
% Inputs:
%	 x	      mean
%	 P	      covariance
%    alpha        scaling parameter 1
%    beta         extra weight on zero'th point
%	 kappa	      scaling parameter 2 (usually set to default 0)
%
% Outputs:
%    xPts	 The sigma points
%    wPts	 The weights on the points
%	 nPts	 The number of points
%
%
%
% (C) 2000      Rudolph van der Merwe  
% (C) 1998-2000 S. J. Julier.

% clear all
% x = [5;5;45]; 
% P = [1 0 0; 0 1 0;0 0 5];
% alpha = 1;
% beta = 2;
% kappa = 0;


% Number of sigma points and scaling terms
n    = size(x(:),1); % 19
nPts = 2*n+1;            % we're using the symmetric SUT 39

% Recalculate kappa according to scaling parameters
kappa = alpha^2*(n+kappa)-n;

% Allocate space

wPts=zeros(1,nPts); % 1*39
xPts=zeros(n,nPts); % 19*39

% Calculate matrix square root of weighted covariance matrix
Psqrtm=(chol((n+kappa)*P))';  %19*19

% Array of the sigma points
xPts=[zeros(size(P,1),1) -Psqrtm Psqrtm]; %[19*1 19*19 19*19]=19*39

% Add mean back in
xPts = xPts + repmat(x,1,nPts);  % 19*39+19*39=19*39

% Array of the weights for each sigma point
wPts=[kappa 0.5*ones(1,nPts-1) 0]/(n+kappa); %[1*1 1*38]=1*39

% Now calculate the zero'th covariance term weight
wPts(nPts+1) = wPts(1) + (1-alpha^2) + beta; % Э����ݵ�Ȩ�صĳ�ʼ����Ȩ���������һ���ֵ��Э�����һ��Ȩ�ؾ���

