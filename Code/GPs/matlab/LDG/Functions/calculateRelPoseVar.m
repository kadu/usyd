
function [UKFPose UKFVar]  = calculateRelPoseVar(RobotSpeed, Direction, SpeedVar, DirectionVar)

%Append Starting Positions

RobotSpeed = [0 RobotSpeed];
Direction = [0 Direction];

SpeedVariance = SpeedVar * RobotSpeed;
DirectionVariance = DirectionVar * Direction;


NumberOfPoses = size(RobotSpeed,2);


%Create 3d Matrix of variance

for i = 1:NumberOfPoses
    Var(:,:,i) = [SpeedVariance(i) 0;0 DirectionVariance(i)];
end

Mean = [RobotSpeed;Direction];

[UKFPose UKFVar] = UKF(Mean,Var,1,0,0)


