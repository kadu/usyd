function p = train_sig(p, mf, vf, ty)

function ia = inv_acc(mf, vf, ty, p)
    lmf = sigmoid(p(1), p(2), 1, mf, vf);
    ogm = (lmf > 0.33)/2 + (lmf > 0.66)/2;
    
    [~, accu] = Accuracy(ogm,ty);
    ia = 1 - accu;
end

opts = optimset('LargeScale','off','MaxIter',100,'Diagnostics', 'on',...
'GradObj', 'off', 'Display', 'iter','TolFun',  1e-10, 'MaxFunEvals', 5000);

[p, ~] = anneal(@(p) inv_acc(mf, vf, ty, p), p);


end