function plot_element(ElementList, fig)

if nargin == 1
    figure()
else
    figure(fig)
end

hold on

for el = ElementList
    b = el.Boundaries;
    x1 = b(1);
    y1 = b(2);
    x2 = b(3);
    y2 = b(4);
    
    O = el.OccuPoints;
    F = el.FreePoints;
%     
    if ~isempty(O)
        p = patch([x1, x1, x2, x2], [y1, y2, y2, y1], [1, 0.9, 1]);
%         scatter(O(1,:), O(2,:), [], 'r+');
    else
        p = patch([x1, x1, x2, x2], [y1, y2, y2, y1], [0.9, 1, 0.9]);
    end
%     if ~isempty(F)
%         scatter(F(1,:),F(2,:), [], 'go');
%     end
    set(p, 'LineStyle', ':');
end

hold off