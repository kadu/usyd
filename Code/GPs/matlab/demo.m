function demo()

clear all;

load('Demo2TrainingData');
% TrainingData = RotateDataset(TrainingData, -12*pi/27, 17);
% b = [[45, 52];[-10, 6]];
% TrainingData = CutDataset(TrainingData, b, 42);
% save('CastleTrainingData', 'TrainingData');

empty.Coord =  zeros(2,0);
empty.PoseNumber = zeros(2,0);

res = 0.1;

Mins = min([TrainingData.LibraryOfEndPoints, TrainingData.LibraryOfStartPoints]');
xmin = Mins(1)-1;
ymin = Mins(2)-1;
Maxs = max([TrainingData.LibraryOfEndPoints, TrainingData.LibraryOfStartPoints]');
xmax = Maxs(1)+1;
ymax = Maxs(2)+1;

[Mesh.x, Mesh.y] = meshgrid(xmin:res:xmax,ymin:res:ymax);
clear xmin ymin Mins Maxs xmax ymax
Test.x = reshape(Mesh.x,[1 numel(Mesh.x)]);
Test.y = reshape(Mesh.y,[1 numel(Mesh.y)]);

x.Element = struct();
x.Point.Coord = [Test.x; Test.y];
isempty(x.Element);
x.Element.Area = [];
x.Element.Data =  zeros(2,0);
x.M = 0;
x.N = size(x.Point.Coord, 2);
x.Mesh = Mesh;

save('TestPoints', 'x')

'Starting...'
'First, changing the smallest element size threshold:'
'Matern3, Thresh = 2, QN'
try
    tic;
    thresh = [1,100];
    [X1, y1] = InitAreawiseData(TrainingData, thresh, 1);
    X1.Point = empty;
    y1.Point = [];
    X1.N = 0;
    [par1, fv1] = areagplearn2(X1, y1, @cov_nDmat3,[],[1,1,1,1]);
    [lml1, mf1, vf1] = areagpeval(X1,y1, @cov_nDmat3, par1, [], x, []);
    '1 done, time elapsed:'
    time1 = toc
    Test1.X = X1;
    Test1.y = y1;
    Test1.par = par1;
    Test1.fv = fv1;
    Test1.lml = lml1;
    Test1.mf = mf1;
    Test1.vf = vf1;
    Test1.time = time1;
    save('Test1', 'Test1');
%     lmf1 = sigmoid(1, 1, 1, mf1, vf1);
% %     lmf1 = (1 + exp(-mf1)).^(-1);
% 
%     figure(12); surf(Mesh.x, Mesh.y, reshape(lmf1, size(Mesh.x)), reshape(lmf1, size(Mesh.x))); view(0,90); colorbar;
%     figure(13); surf(Mesh.x, Mesh.y, reshape(vf1, size(Mesh.x)), reshape(vf1, size(Mesh.x))); view(0,90); colorbar;
%     ogm1 = (lmf1 > 0.35)/2 + (lmf1 > 0.65)/2;
%     figure(14); surf(Mesh.x, Mesh.y, reshape(ogm1, size(Mesh.x)), reshape(ogm1, size(Mesh.x))); view(0,90); colorbar;
end

'mat3, Thresh = 4, QN'
try
    tic;
    thresh = [1,100];
    [X2, y2] = InitAreawiseData(TrainingData, thresh, 2);
    X2.Point = empty;
    y2.Point = [];
    X2.N = 0;
    [par2, fv2] = areagplearn2(X2, y2, @cov_nDsqExp,[],[1,1,1,1]);
    [lml2, mf2, vf2] = areagpeval(X2,y2, @cov_nDsqExp, par2, [], x, []);
    '2 done, time elapsed:'
    time2 = toc
    Test2.X = X2;
    Test2.y = y2;
    Test2.par = par2;
    Test2.fv = fv2;
    Test2.lml = lml2;
    Test2.mf = mf2;
    Test2.vf = vf2;
    Test2.time = time2;
    save('Test2', 'Test2');
% %     lmf2 = sigmoid(1, 1, 1, mf2, vf2);
%     lmf2 = (1 + exp(-mf2)).^(-1);
% 
%     figure(22); surf(Mesh.x, Mesh.y, reshape(lmf2, size(Mesh.x)), reshape(lmf2, size(Mesh.x))); view(0,90), colorbar;
%     figure(23); surf(Mesh.x, Mesh.y, reshape(vf2, size(Mesh.x)), reshape(vf2, size(Mesh.x))); view(0,90), colorbar;
%     ogm2 = (lmf2 > 0.35)/2 + (lmf2 > 0.65)/2;
%     figure(24); surf(Mesh.x, Mesh.y, reshape(ogm2, size(Mesh.x)), reshape(ogm2, size(Mesh.x))); view(0,90); colorbar;
end

'Matern3, Thresh = 2, ann'
try
    tic;
    thresh = [1,100];
    [X3, y3] = InitAreawiseData(TrainingData, thresh, 3);
    X3.Point = empty;
    y3.Point = [];
    X3.N = 0;
    [par3, fv3] = areagplearn(X3, y3, @cov_nDmat3,[],[1,1,1,1]);
    [lml3, mf3, vf3] = areagpeval(X3,y3, @cov_nDmat3, par3, [], x, []);
    '3 done, time elapsed:'
    time3 = toc
    Test3.X = X3;
    Test3.y = y3;
    Test3.par = par3;
    Test3.fv = fv3;
    Test3.lml = lml3;
    Test3.mf = mf3;
    Test3.vf = vf3;
    Test3.time = time3;
    save('Test3', 'Test3');
%     lmf3 = sigmoid(1, 1, 1, mf3, vf3);
% %     lmf3 = (1 + exp(-mf3)).^(-1);
% 
%     figure(30); scatter3(x.Point.Coord(1,:), x.Point.Coord(2,:), lmf3, 100, lmf3, 'fill'); view(0,90), colorbar;
%     figure(31);  scatter3(x.Point.Coord(1,:), x.Point.Coord(2,:), vf3, 100, vf3, 'fill'); view(0,90), colorbar;
%     figure(32); surf(Mesh.x, Mesh.y, reshape(lmf3, size(Mesh.x)), reshape(lmf3, size(Mesh.x))); view(0,90), colorbar;
%     figure(33); surf(Mesh.x, Mesh.y, reshape(vf3, size(Mesh.x)), reshape(vf3, size(Mesh.x))); view(0,90), colorbar;
%     ogm3 = (lmf3 > 0.35)/2 + (lmf3 > 0.65)/2;
%     figure(34); surf(Mesh.x, Mesh.y, reshape(ogm3, size(Mesh.x)), reshape(ogm3, size(Mesh.x))); view(0,90); colorbar;
end



'Now testing different covariance functions'
'sqexp, 4, ann'
try
    tic;
    thresh = [1,100];
    [X4, y4] = InitAreawiseData(TrainingData, thresh, 4);
    X4.Point = empty;
    y4.Point = [];
    X4.N = 0;
    [par4, fv4] = areagplearn(X4, y4, @cov_nDsqExp,[],[1,1,1,1]);
    [lml4, mf4, vf4] = areagpeval(X4,y4, @cov_nDsqExp, par4, [], x, []);
    '4 done, time elapsed:'
    time4 = toc
    Test4.X = X4;
    Test4.y = y4;
    Test4.par = par4;
    Test4.fv = fv4;
    Test4.lml = lml4;
    Test4.mf = mf4;
    Test4.vf = vf4;
    Test4.time = time4;
    save('Test4', 'Test4');
%     lmf4 = sigmoid(1, 1, 1, mf4, vf4);
% %     lmf4 = (1 + exp(-mf4)).^(-1);
%     figure(40); scatter3(x.Point.Coord(1,:), x.Point.Coord(2,:), lmf4, 100, lmf4, 'fill'); view(0,90), colorbar;
%     figure(41);  scatter3(x.Point.Coord(1,:), x.Point.Coord(2,:), vf4, 100, vf4, 'fill'); view(0,90), colorbar;
%     figure(42); surf(Mesh.x, Mesh.y, reshape(lmf4, size(Mesh.x)), reshape(lmf4, size(Mesh.x))); view(0,90), colorbar;
%     figure(43); surf(Mesh.x, Mesh.y, reshape(vf4, size(Mesh.x)), reshape(vf4, size(Mesh.x))); view(0,90), colorbar;
%     ogm4 = (lmf4 > 0.35)/2 + (lmf4 > 0.65)/2;
%     figure(44); surf(Mesh.x, Mesh.y, reshape(ogm4, size(Mesh.x)), reshape(ogm4, size(Mesh.x))); view(0,90); colorbar;
end
% 
% 'sqexp, thresh=2, QN'
% try
%     tic;
%     thresh = [2,100];
%     [X5, y5] = InitAreawiseData(TrainingData, thresh, 5);
%     X5.Point = empty;
%     y5.Point = [];
%     X5.N = 0; 
%     [par5, fv5] = areagplearn2(X5, y5, @cov_nDsqExp,[],[1,1,1,1]);
%     [lml5, mf5, vf5] = areagpeval(X5,y5, @cov_nDsqExp, par5, [], x, []);
%     '5 done, time elapsed:'
%     time5 = toc
%     Test5.X = X5;
%     Test5.y = y5;
%     Test5.par = par5;
%     Test5.fv = fv5;
%     Test5.lml = lml5;
%     Test5.mf = mf5;
%     Test5.vf = vf5;,
%     Test5.time = time5;
%     save('Test5', 'Test5');
%     lmf5 = sigmoid(1, 1, 1, mf5, vf5);
% %     lmf5 = (1 + exp(-mf5)).^(-1);
%     figure(50); scatter3(x.Point.Coord(1,:), x.Point.Coord(2,:), lmf5, 100, lmf5, 'fill'); view(0,90), colorbar;
%     figure(51);  scatter3(x.Point.Coord(1,:), x.Point.Coord(2,:), vf5, 100, vf5, 'fill'); view(0,90), colorbar;
%     figure(52); surf(Mesh.x, Mesh.y, reshape(lmf5, size(Mesh.x)), reshape(lmf5, size(Mesh.x))); view(0,90), colorbar;
%     figure(53); surf(Mesh.x, Mesh.y, reshape(vf5, size(Mesh.x)), reshape(vf5, size(Mesh.x))); view(0,90), colorbar;
%     ogm5 = (lmf5 > 0.35)/2 + (lmf5 > 0.65)/2;
%     figure(54); surf(Mesh.x, Mesh.y, reshape(ogm5, size(Mesh.x)), reshape(ogm5, size(Mesh.x))); view(0,90); colorbar;
% end
% 
% 'sqexp, thresh=4, QN'
% try
%     tic;
%     thresh = [4,100];
%     [X6, y6] = InitAreawiseData(TrainingData, thresh, 6);
%     X6.Point = empty;
%     y6.Point = [];
%     X6.N = 0;
%     [par6, fv6] = areagplearn2(X6, y6, @cov_nDsqExp,[],[1,1,1,1]);
%     [lml6, mf6, vf6] = areagpeval(X6,y6, @cov_nDsqExp, par6, [], x, []);
%     '6 done, time elapsed:'
%     time6 = toc
%     Test6.X = X6;
%     Test6.y = y6;
%     Test6.par = par6;
%     Test6.fv = fv6;
%     Test6.lml = lml6;
%     Test6.mf = mf6;
%     Test6.vf = vf6;
%     Test6.time = time6;
%     save('Test6', 'Test6');
%     lmf6 = sigmoid(1, 1, 1, mf6, vf6);
%     figure(62); surf(Mesh.x, Mesh.y, reshape(lmf6, size(Mesh.x)), reshape(lmf6, size(Mesh.x))); view(0,90), colorbar;
%     figure(63); surf(Mesh.x, Mesh.y, reshape(vf6, size(Mesh.x)), reshape(vf6, size(Mesh.x))); view(0,90), colorbar;
% end
% 
% 'sqexp, thresh=2, ann'
% try
%     tic;
%     thresh = [2,100];
%     [X7, y7] = InitAreawiseData(TrainingData, thresh, 7);
%     X7.Point = empty;
%     y7.Point = [];
%     X7.N = 0;
%     [par7, fv7] = areagplearn(X7, y7, @cov_nDsqExp,[],[1,1,1,1]);
%     [lml7, mf7, vf7] = areagpeval(X7,y7, @cov_nDsqExp, par7, [], x, []);
%     '7 done, time elapsed:'
%     time7 = toc
%     Test7.X = X7;
%     Test7.y = y7;
%     Test7.par = par7;
%     Test7.fv = fv7;
%     Test7.lml = lml7;
%     Test7.mf = mf7;
%     Test7.vf = vf7;
%     Test7.time = time7;
%     save('Test7', 'Test7');
% %     lmf7 = sigmoid(1, 1, 1, mf7, vf7);
%     lmf7 = sigmoid(1, 1, 1, mf7, vf7);
% %     figure(70); scatter3(x.Point.Coord(1,:), x.Point.Coord(2,:), lmf7, 100, lmf7, 'fill'); view(0,90), colorbar;
% %     figure(71);  scatter3(x.Point.Coord(1,:), x.Point.Coord(2,:), vf7, 100, vf7, 'fill'); view(0,90), colorbar;
%     figure(72); surf(Mesh.x, Mesh.y, reshape(lmf7, size(Mesh.x)), reshape(lmf7, size(Mesh.x))); view(0,90), colorbar;
%     figure(73); surf(Mesh.x, Mesh.y, reshape(vf7, size(Mesh.x)), reshape(vf7, size(Mesh.x))); view(0,90), colorbar;
%     ogm7 = (lmf7 > 0.35)/2 + (lmf7 > 0.65)/2;
%     figure(74); surf(Mesh.x, Mesh.y, reshape(ogm7, size(Mesh.x)), reshape(ogm7, size(Mesh.x))); view(0,90); colorbar;
% end
% 
% 'sqexp, thresh=4, ann'
% try
%     tic;
%     thresh = [4,100];
%     [X8, y8] = InitAreawiseData(TrainingData, thresh, 8);
%     X8.Point = empty;
%     y8.Point = [];
%     X8.N = 0;
%     [par8, fv8] = areagplearn(X8, y8, @cov_nDsqExp,[],[1,1,1,1]);
%     [lml8, mf8, vf8] = areagpeval(X8,y8, @cov_nDsqExp, par8, [], x, []);
%     '8 done, time elapsed:'
%     time8 = toc
%     Test8.X = X8;
%     Test8.y = y8;
%     Test8.par = par8;
%     Test8.fv = fv8;
%     Test8.lml = lml8;
%     Test8.mf = mf8;
%     Test8.vf = vf8;
%     Test8.time = time8;
%     save('Test8', 'Test8');
%     lmf8 = sigmoid(1, 1, 1, mf8, vf8);
%     figure(82); surf(Mesh.x, Mesh.y, reshape(lmf8, size(Mesh.x)), reshape(lmf8, size(Mesh.x))); view(0,90), colorbar;
%     figure(83); surf(Mesh.x, Mesh.y, reshape(vf8, size(Mesh.x)), reshape(vf8, size(Mesh.x))); view(0,90), colorbar;
%     ogm8 = (lmf8 > 0.35)/2 + (lmf8 > 0.65)/2;
%     figure(84); surf(Mesh.x, Mesh.y, reshape(ogm8, size(Mesh.x)), reshape(ogm8, size(Mesh.x))); view(0,90); colorbar;
% end
% 
% % 'NN, thresh=1, annealing'
% % try
% %     tic;
% %     thresh = [1,1]; thresh = thresh(1);
% %     [X9, y9] = InitAreawiseData(TrainingData, thresh, 9);
% %     X9.Point = empty;
% %     y9.Point = [];
% %     X9.N = 0;
% %     [par9, fv9] = areagplearn(X9, y9, @cov_nD_nn,[],[1,1,1]);
% %     [lml9, mf9, vf9] = areagpeval(X9,y9, @cov_nD_nn, par9, [], x, []);
% %     '9 done, time elapsed:'
% %     time9 = toc
% %     Test9.X = X9;
% %     Test9.y = y9;
% %     Test9.par = par9;
% %     Test9.fv = fv9;
% %     Test9.lml = lml9;
% %     Test9.mf = mf9;
% %     Test9.vf = vf9;
% %     Test9.time = time9;
% %     save('Test9', 'Test9');
% %     lmf9 = sigmoid(1, 1, 1, mf9, vf9);
% %     figure(92); surf(Mesh.x, Mesh.y, reshape(lmf9, size(Mesh.x)), reshape(lmf9, size(Mesh.x))); view(0,90), colorbar;
% %     figure(93); surf(Mesh.x, Mesh.y, reshape(vf9, size(Mesh.x)), reshape(vf9, size(Mesh.x))); view(0,90), colorbar;
% %     ogm9 = (lmf9 > 0.35)/2 + (lmf9 > 0.65)/2;
% %     figure(94); surf(Mesh.x, Mesh.y, reshape(ogm9, size(Mesh.x)), reshape(ogm9, size(Mesh.x))); view(0,90); colorbar;
% % end