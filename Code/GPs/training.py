# -*- coding: utf-8 -*-
"""
Created on Fri Jan 17 13:14:25 2014

@author: kadu
"""
from scipy import optimize as op
import nlopt


import gp2 as gp
import cf2 as cf
#from numpy import linalg as la
import numpy as np

def nsqexp_training(inputs, hyperparameters=[]):
    if hyperparameters == []:
        np.random.seed(123456789)
        hyperparameters = np.random.random(3)

    inputs_vector, targets, test_vector = inputs
#    n = inputs_vector.size
    
    def lml(hyperparameters):
        lml = gp.gaussian_process(inputs, cf.nsqexp, hyperparameters, True)
        print str(hyperparameters) + " : " + str(lml)
        return -lml

    def lml_jac(hyperparameters):
        K, L, alpha, Ki = gp._kernel(inputs, cf.nsqexp, hyperparameters)
        
        dKdTh = cf.nsqexp(inputs_vector, inputs_vector, hyperparameters, jac=True)
        
        aux = alpha.dot(alpha.T) - Ki
        jac = [0.5 * np.trace(aux.dot(d)) for d in dKdTh]

        return np.asarray(jac)

#    theta = op.minimize(lml,
#                   hyperparameters,
#                   method='BFGS',
##                   jac=lml_jac,
#                   options={'disp': True}).x

    def nlopt_f(hyperparameters, grad = []):
        grad[:] = lml_jac(hyperparameters)
        return lml(hyperparameters)

    try:
        opt = nlopt.opt(nlopt.LD_LBFGS, np.size(hyperparameters))
        opt.set_min_objective(nlopt_f)
        opt.set_maxtime(60)
        opt.set_xtol_rel(1e-6)
#        opt.set_lower_bounds([0, 0, 0])
#        opt.set_upper_bounds([500, 1, 1])
        theta = opt.optimize(hyperparameters)
    except RuntimeError:
        print "Warning: algorithm terminated with unknown error"
        return hyperparameters
    
    return theta