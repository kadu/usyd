# -*- coding: utf-8 -*-
"""
Created on Fri Jan 17 13:30:26 2014

@author: kadu
"""
import numpy as np

import sample_functions as sf
import matplotlib.pyplot as plt

def input_generator(tgt_fun=sf.fun1, n=15, m=500, bounds=(-5,5), noise=0.1) :
    ceil = max(bounds)
    floor = min(bounds)
    
    x = np.array([[0.0, 0.2, 0.4, 1.5, 1.7, 1.9, 2.1, 2.3, 3.8, 4.0, 4.2, 4.4, 4.6, 4.8]]).T
#    x = np.reshape(np.linspace(floor + 1, ceil - 1, n + 2), (n + 2 , 1))
    # take n + 2 equally spaced x ...
 #   x = x[1 : (n + 1)] 
    # ... and get rid of boundary elements
    np.random.seed(12345)
    sig = np.random.normal(0, noise, (n,1))
    y = tgt_fun(x) + sig

    x_star = np.reshape(np.linspace(floor - 1, ceil + 1, m + 2), (m + 2,1)) 
    # take m + 2 equally spaced x_star...
    x_star = x_star[1 : m + 1]
    # and get rid of the boundary elements.
    # this avoids x[0] = x_star[0]

    return (x, y, x_star)
    
def reid_input_generator(plot = False, dtype = '', line_res = 0.):
    n = np.reshape(np.linspace(0, 5, 300), (300, 1))
    n2 = np.reshape(np.linspace(-1, 6, 400), (400, 1))
    def f(x):
        '''
        sample function
        '''
        y  = 6.9625352721316893e-4
        y += 6.9749474532828364 * x
        y -= 1.1458999751486417e+1 * x**2
        y -= 2.2166424983907191 * x**3
        y += 1.3639097732713992e+1 * x ** 4
        y -= 9.6193887734765990 * x ** 5
        y += 2.9617889258467613 * x ** 6
        y -= 4.3139424886366623e-1 * x ** 7
        y += 2.4302969018786130e-2 * x ** 8
        return y
    
    ans = f(n)
    ans2 = f(n2)
    step = n[1]
    
    ground_truth = (n2, ans2)
    
    def f2(p1, p2=None):
        '''
        discretised version of f(x)
        '''
        if p2 == None:
            return [ans[i] for i in p1 // step]
        
        a = p1 // step
        b = p2 // step
        return [(sum(ans[a[i]:b[i]]) / (b[i] - a[i])) for i in range(len(a))]
        
    if dtype == 'l':
        if line_res == 0.:
            line_res = 0.5
        lnum = (5. / line_res)
        
        ll2 = np.linspace(0, 5 - line_res, lnum)
        lr2 = ll2 + line_res
        ly2 = f2(ll2, lr2)
        
        ll2 = np.asarray([ll2]).T
        lr2 = np.asarray([lr2]).T
        ly2 = np.asarray(ly2)
        
        if plot:
            l_plot = np.concatenate((ll2, lr2, ly2, ly2), 1)
            
            fig, ax = plt.subplots(1)
            ax.set_xlim([-0.5,5.5])
            ax.set_ylim([-3,3])
            ax.plot(n2, ans2, 'k-')
            for el in l_plot:
                ax.plot(el[0:2], el[2:4], 'r|')
                ax.plot(el[0:2], el[2:4], 'r-')
            plt.draw()
        
        data_points = (np.concatenate((ll2, lr2), 1), ly2)
        
    
    elif dtype == 'p':
        p2 = [0.0, 0.2, 0.4, 1.5, 1.7, 1.9, 2.1, 2.3, 3.8, 4.0, 4.2, 4.4, 4.6, 4.8]
        p2y = f2(p2)
        
        if plot:
            fig, ax = plt.subplots(1)
            ax.set_xlim([-0.5,5.5])
            ax.set_ylim([-3,3])
            ax.plot(n2, ans2, 'k-')
            ax.plot(p2, p2y, 'ro', fillstyle='none')
            plt.draw()
            
        p2 = np.array([p2]).T
 
        data_points = (np.concatenate((p2, p2), 1), np.array(p2y))
        
    else:
        p  = [1.1, 1.7, 2.3, 3.2, 3.9, 4.1, 4.8]
        ll = [0.0, 0.3, 1.4, 1.8, 2.1, 3.0, 3.6, 4.1]
        lr = [0.9, 0.8, 1.5, 2.5, 3.0, 3.4, 4.2, 4.5]
        
        py  = f2(p)
        ly = f2(ll, lr)
        
        p  = np.asarray([p]).T
        ll = np.asarray([ll]).T
        lr = np.asarray([lr]).T
        ly = np.asarray(ly)
        
        if plot:
            l_plot = np.concatenate((ll, lr, ly, ly), 1)
        
            fig, ax = plt.subplots(1)
            ax.set_xlim([-0.5,5.5])
            ax.set_ylim([-3,3])
            ax.plot(n2, ans2, 'k-')
            ax.plot(p, py, 'ro', fillstyle='none')
            for el in l_plot:
                ax.plot(el[0:2], el[2:4], 'r|')
                ax.plot(el[0:2], el[2:4], 'r-')
            plt.draw()
        
        L = np.concatenate((ll, lr), 1)
        P = np.concatenate(( p,  p), 1)
        
        py = np.reshape(py, (len(p), 1))
        ly = np.reshape(ly, (len(L), 1))
        
        data_points = np.concatenate((L, P)), np.concatenate((ly, py))

    return ground_truth, data_points