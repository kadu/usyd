# -*- coding: utf-8 -*-
"""
Sample functions for GP regression testing
"""
import numpy as np

def rndm(x):
    return np.random.rand(x.size,1)
    
def fun1(x):
    return ((x ** 3) / 4) + 2 * ( x**2 ) * np.sin(x * np.pi)
    
def fun2(x):
    return (x / 4) + 2 * ( x**2 ) * np.sin(x * np.pi)
    
def fun3(x):
    C = 2
    b = 0.5
    
    y  = b*x + C + 1 * np.sin(x)
    y -= y.mean()
    
    return y

def fun4(x):
    y  = 6.9625352721316893e-4
    y += 6.9749474532828364 * x
    y -= 1.1458999751486417e+1 * x**2
    y -= 2.2166424983907191 * x**3
    y += 1.3639097732713992e+1 * x ** 4
    y -= 9.6193887734765990 * x ** 5
    y += 2.9617889258467613 * x ** 6
    y -= 4.3139424886366623e-1 * x ** 7
    y += 2.4302969018786130e-2 * x ** 8
    return y
