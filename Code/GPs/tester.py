# -*- coding: utf-8 -*-
"""
Spyder Editor

"""
import numpy as np
import matplotlib.pyplot as plt

import gp2 as gp
import sample_functions as sf
import cf2 as cf
import training as tr
import inputs_generator as ig

def tester(n, m, tgt_fun=sf.fun1, cov_fun=cf.nsqexp, noise=0.0, test_noise=0.0, bounds=(0,1)):
    '''
    Simple GP tester
    '''
    
    c = max(bounds[0], bounds[1])
    f = min(bounds[0], bounds[1])
    w = c - f
    
    x, y, x_star = ig.input_generator(tgt_fun=tgt_fun, n=n, m=m, bounds=bounds, noise=noise)
    theta = tr.nsqexp_training((x, y, x_star)) 
#    theta = [10., 0.01, 0.25]

    # the function we are estimating
    ans = np.reshape(np.linspace(1.2 * f, 1.2 * c, (w * 10)), ((w * 10),1))
    f_ans = tgt_fun(ans)

    # gp estimates function and variance
    mu, var, lml = gp.gaussian_process((x, y, x_star), cov_fun, theta)
    sig = np.diag(var)
    
    sig1 = mu.flatten() + 2 * np.sqrt(sig)
    sig2 = mu.flatten() - 2 * np.sqrt(sig)
    aux, sig1, sig2 = zip(*sorted(zip(x_star, sig1, sig2)))
        
    fig, ax = plt.subplots(1)
    ax.plot(ans, f_ans, 'b-', label="target function")
    ax.plot(x_star, mu, 'r-', label="estimation")
    ax.plot(x, y, 'bo', label="input values")
    ax.fill_between(x_star.flatten(), sig1, sig2, color='grey', alpha=0.5)
    ax.legend(loc='lower right')
    textstr = r'$\sigma^2_f =$ %.2f' % theta[0] + '\n'
    textstr += r'$\sigma^2_n =$ %.2f' % theta[2]    + '\n'
    textstr += r'$l =$   %.2f' % theta[1]
    props = dict(boxstyle='square', facecolor='white', alpha=0.5)
    ax.text(0.42, 0.95, textstr, transform=ax.transAxes, fontsize=14,
    verticalalignment='top', bbox=props)
    plt.title('lml = %.2f' % lml)
    plt.show()
    
    return mu, var, lml

par = tester(14, 500, sf.fun1, bounds=(-0,5), noise=0.1)

def hp_tester(n, m, tgt_fun=sf.fun1, cov_fun=cf.nsqexp, noise=0.0, test_noise=0.0, bounds=(0,1)):
    c = max(bounds[0], bounds[1])
    f = min(bounds[0], bounds[1])
    w = c - f
    
    x, y, x_star = ig.input_generator(tgt_fun=tgt_fun, n=n, m=m, bounds=bounds, noise=noise)
    
    theta_list = [[ 0.46810575, 0.4593732,  0.70950978],
[ 0.46810577, 0.4593732,  0.70950978],
[ 0.46810575, 0.45937322, 0.70950978],
[ 0.46810575, 0.4593732,  0.7095098 ],
[ 0.46810575, 0.4593732,  0.70950978],
[ -2.57445959,  0.4593732,  15.12866585],
[ -2.57445959,  0.4593732,  15.12866585],
[ -2.57445958,  0.4593732,  15.12866585],
[ -2.57445959,  0.45937322, 15.12866585],
[ -2.57445959,  0.4593732,  15.12866587],
[ 0.17649015, 0.4593732,  2.09151819],
[ 0.17649015, 0.4593732,  2.09151819],
[ 0.17649017, 0.4593732,  2.09151819],
[ 0.17649015, 0.45937322, 2.09151819],
[ 0.17649015, 0.4593732,  2.09151821],
[ 0.34782141, 0.4593732,  1.27955461],
[ 0.34782141, 0.4593732,  1.27955461],
[ 0.34782143, 0.4593732,  1.27955461],
[ 0.34782141, 0.45937322, 1.27955461],
[ 0.34782141, 0.4593732,  1.27955463],
[ 2.72586395, 0.4593732,  2.62067862],
[ 2.72586395, 0.4593732,  2.62067862],
[ 2.72586396, 0.4593732,  2.62067862],
[ 2.72586395, 0.45937322, 2.62067862],
[ 2.72586395, 0.4593732,  2.62067863],
[ 0.5169382,  0.4593732,  1.37492994],
[ 0.5169382,  0.4593732,  1.37492994],
[ 0.51693821, 0.4593732,  1.37492994],
[ 0.5169382,  0.45937322, 1.37492994],
[ 0.5169382,  0.4593732,  1.37492995],
[ 0.3825887,  0.4593732,  1.29916202],
[ 0.3825887,  0.4593732,  1.29916202],
[ 0.38258872, 0.4593732,  1.29916202],
[ 0.3825887,  0.45937322, 1.29916202],
[ 0.3825887,  0.4593732,  1.29916204],
[ 0.37929864, 0.4593732,  1.32861794],
[ 0.37929864, 0.4593732,  1.32861794],
[ 0.37929866, 0.4593732,  1.32861794],
[ 0.37929864, 0.45937322, 1.32861794],
[ 0.37929864, 0.4593732,  1.32861795],
[ 0.3661384, 0.4593732, 1.4464416],
[ 0.3661384, 0.4593732, 1.4464416],
[ 0.36613842, 0.4593732,  1.4464416 ],
[ 0.3661384,  0.45937322, 1.4464416 ],
[ 0.3661384,  0.4593732,  1.44644161],
[ 0.33728001, 0.4593732,  1.75989928],
[ 0.33728001, 0.4593732,  1.75989928],
[ 0.33728002, 0.4593732,  1.75989928],
[ 0.33728001, 0.45937322, 1.75989928],
[ 0.33728001, 0.4593732,  1.7598993 ],
[ 0.35155979, 0.4593732,  1.81342415],
[ 0.35155979, 0.4593732,  1.81342415],
[ 0.35155981, 0.4593732,  1.81342415],
[ 0.35155979, 0.45937322, 1.81342415],
[ 0.35155979, 0.4593732,  1.81342416],
[ 0.35282457, 0.4593732,  1.83027337],
[ 0.35282457, 0.4593732,  1.83027337],
[ 0.35282459, 0.4593732,  1.83027337],
[ 0.35282457, 0.45937322, 1.83027337],
[ 0.35282457, 0.4593732,  1.83027338],
[ 0.35306318, 0.4593732,  1.83089846],
[ 0.35306318, 0.4593732,  1.83089846],
[ 0.35306319, 0.4593732,  1.83089846],
[ 0.35306318, 0.45937322, 1.83089846],
[ 0.35306318, 0.4593732,  1.83089848],
[ 0.35307174, 0.4593732,  1.8309033 ],
[ 0.35307174, 0.4593732,  1.8309033 ],
[ 0.35307175, 0.4593732,  1.8309033 ],
[ 0.35307174, 0.45937322, 1.8309033 ],
[ 0.35307174, 0.4593732,  1.83090331]]

    theta_list.reverse()
    for theta in theta_list:
        # gp estimates function and variance
        mu, var, lml = gp.gaussian_process((x, y, x_star), cov_fun, theta)
        sig = np.diag(var)
        aux = x_star.flatten()
        
        # the function we are estimating
        ans = np.reshape(np.linspace(f, c, (w * 10)), ((w * 10),1))
        f_ans = tgt_fun(ans)
        
        sig1 = mu.flatten() + 2 * np.sqrt(sig)
        sig2 = mu.flatten() - 2 * np.sqrt(sig)
        aux, sig1, sig2 = zip(*sorted(zip   (aux, sig1, sig2)))
        
        fig, ax = plt.subplots(1)
        ax.plot(ans, f_ans, 'b-', label="target function")
        ax.plot(x_star, mu, 'r-', label="estimation")
        ax.plot(x, y, 'bo', label="input values")
        ax.fill_between(aux, sig1, sig2, color='grey', alpha=0.5)
        ax.legend(loc='lower right')
        textstr = r'$\sigma^2_f =$ %.2f' % theta[0] + '\n'
        textstr += r'$\sigma^2_n =$ %.2f' % theta[1]    + '\n'
        textstr += r'$l =$   %.2f' % theta[2]
        props = dict(boxstyle='square', facecolor='white', alpha=0.5)
        ax.text(0.42, 0.95, textstr, transform=ax.transAxes, fontsize=14,
        verticalalignment='top', bbox=props)
        plt.title('lml = %.2f' % lml)
        plt.draw()

#hp_tester(31, 500, sf.fun3, bounds=(-5,5), noise=0.01)
