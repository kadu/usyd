# -*- coding: utf-8 -*-
"""
Created on Wed Jul 30 01:45:35 2014

@author: kadu
"""

from numpy import linalg as la
import numpy as np

def _kernel(inputs, covariance_function, hyperparameters=[]):
    inputs_vector, targets, test_vector = inputs
    n = inputs_vector.size
    #inputs_noise, test_noise = noise
    
    K = covariance_function(inputs_vector, inputs_vector, hyperparameters)

    def jitter_cholesky(m, jitter = 1e-7, maxjitter = 1e-3):
        try:
          return la.cholesky(m)
        except la.LinAlgError:
            n = len(m)
            while (jitter <= maxjitter):
                j = jitter * np.eye(n)
                print "WARNING: Adding " + str(jitter) + " jitter"
                try:
                    return la.cholesky(m + j)
                except la.LinAlgError:
                    jitter *= 10
            # if it gets here, it left the loop, meaning adding jitter failed
            error = 'Matrix is not positive definite - '
            error += 'Failed after adding ' + str(jitter / 10) + ' jitter -'
            error += 'Cholesky decomposition cannot be computed -'
            error += 'Try increasing maxjitter.'
            raise la.LinAlgError(error)
                
    L = jitter_cholesky(K)

#    L = la.cholesky(K)

    Ki = la.solve(L.T, la.solve(L, np.eye(n)))
    alpha = Ki.dot(targets)
    
    return K, L, alpha, Ki

def gaussian_process(inputs, covariance_function, hyperparameters, lml_only=False):
    '''
    Gaussian process implemented according to Rasmussen & Williams, Gaussian
    Processes for Machine Learning, algorithm 2.1
    '''

    x, y, x_ = inputs
    n = x.size
    
    K, L, alpha, Ki = _kernel(inputs, covariance_function, hyperparameters)

    lml  = -.5 * y.T.dot(alpha)[0][0]
    lml -= np.log(L.diagonal()).sum()
    lml -= ((n / 2.) * np.log(2. * np.pi))
    
    if lml_only == True:
        return lml

    k_ = covariance_function(x, x_, hyperparameters)
    
    mu = (k_.T).dot(alpha)
    
    v = la.solve(L, k_)
    k__ = covariance_function(x_, x_, hyperparameters)
    var = k__ - (v.T).dot(v)

    var += np.eye(x_.size) * hyperparameters[2]

    return mu, var, lml