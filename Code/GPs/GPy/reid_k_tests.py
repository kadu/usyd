# -*- coding: utf-8 -*-
"""
Created on Sun Jun  1 15:45:52 2014

@author: kadu
"""

import numpy as np
import scipy.special as ss
import sympy as sp

def kLL():
    L1, R1 = (3, 7)
    L2, R2 = (3, 7)

    sig = 0.5
    sig2 = sig ** 2
    lam = 0.3
    lam2 = lam ** 2
    
    vpi2  = np.sqrt(np.pi / 2.)
    v2lam = np.sqrt(2) * lam

###############

    a = vpi2 * sig2 * lam / ((R2 - L2) * (R1 - L1))
    
    b1 = (R1 - L2) * ss.erf((R1 - L2) / v2lam)
    b2 = (L1 - L2) * ss.erf((L1 - L2) / v2lam)
    b3 = (L1 - R2) * ss.erf((L1 - R2) / v2lam)
    b4 = (R1 - R2) * ss.erf((R1 - R2) / v2lam)
    
    part1 = a * (b1 - b2 + b3 - b4)

    c = lam2 / (2 * (R2 - L2) * (R1 - L1))
    
    d1 = np.exp(-((R1 - L2) ** 2) / (2. * lam2))
    d2 = np.exp(-((L1 - L2) ** 2) / (2. * lam2))
    d3 = np.exp(-((L1 - R2) ** 2) / (2. * lam2))
    d4 = np.exp(-((R1 - R2) ** 2) / (2. * lam2))
    
    part2 = c * (d1 - d2 + d3 - d4)
    
    return part1 + part2

def symkLL():
    sig = sp.Symbol('sigf')
    lam = sp.Symbol('ls')
    
    
#    L1, R1 = (0, 7)
#    L2, R2 = (0, 7)
    
    L1 = sp.Symbol('l1l')
    R1 = sp.Symbol('l1r')
    L2 = sp.Symbol('l2l')
    R2 = sp.Symbol('l2r')

    
    ###############
    # dk/dsig
    
    a = (sp.sqrt(sp.pi / 2.)) * (sig ** 2) * lam / ((R2 - L2) * (R1 - L1))
    
    b  = (R1 - L2) * sp.erf((R1 - L2) / (sp.sqrt(2) * lam))
    b -= (L1 - L2) * sp.erf((L1 - L2) / (sp.sqrt(2) * lam))
    b += (L1 - R2) * sp.erf((L1 - R2) / (sp.sqrt(2) * lam))
    b -= (R1 - R2) * sp.erf((R1 - R2) / (sp.sqrt(2) * lam))
    
    
    
    c = (lam ** 2) / (2 * (R2 - L2) * (R1 - L1))
    
    d  = sp.exp(-((R1 - L2) ** 2) / (2. * (lam **2)))
    d -= sp.exp(-((L1 - L2) ** 2) / (2. * (lam ** 2)))
    d += sp.exp(-((L1 - R2) ** 2) / (2. * (lam ** 2)))
    d -= sp.exp(-((R1 - R2) ** 2) / (2. * (lam ** 2)))

    return (a * b) + (c * d)

def kPL():
    L, R = (0, 7.)
    P = 3.

    sig = 0.5
    sig2 = sig ** 2
    lam = 0.3
    
    vpi2  = np.sqrt(np.pi / 2.)
    v2lam = np.sqrt(2) * lam
    
###############

    a = vpi2 * sig2 * lam / (R - L)
    
    b1 = ss.erf((R - P) / v2lam)
    b2 = ss.erf((L - P) / v2lam)
    
    return a * (b1 - b2)
    
def symkPL():
    
    L = sp.Symbol('ll')
    R = sp.Symbol('lr')
    P = sp.Symbol('p')
    
#    L, R = (0, 7.)
#    P = 3.

    sig = sp.Symbol('sigf')
    lam = sp.Symbol('ls')
    
###############

    a = sp.sqrt(sp.pi / 2.) * (sig ** 2) * lam / (R - L)
    
    b  = sp.erf((R - P) / (sp.sqrt(2) * lam))
    b -= sp.erf((L - P) / (sp.sqrt(2) * lam))
    
    return a * b


def symk(input_array):
    from scipy.special import erf
    from numpy import sqrt, exp    

    l, p = input_array
    
    l1l, l1r = l
    ll, lr = l2l, l2r = l1l.T, l1r.T
    

    
    kll   = 0.09399856029866252*(-(l1l - l2l)*erf(1.66666666666667*sqrt(2)*(l1l - l2l)) + (l1l - l2r)*erf(1.66666666666667*sqrt(2)*(l1l - l2r)) + (l1r - l2l)*erf(1.66666666666667*sqrt(2)*(l1r - l2l)) - (l1r - l2r)*erf(1.66666666666667*sqrt(2)*(l1r - l2r)))/((-l1l + l1r)*(-l2l + l2r)) + 0.09*(-exp(-5.55555555555556*(l1l - l2l)**2) + exp(-5.55555555555556*(l1l - l2r)**2) + exp(-5.55555555555556*(l1r - l2l)**2) - exp(-5.55555555555556*(l1r - l2r)**2))/((-l1l + l1r)*(-2.0*l2l + 2.0*l2r))
    dkll0 = 0.3133285343288751*(-(l1l - l2l)*erf(1.66666666666667*sqrt(2)*(l1l - l2l)) + (l1l - l2r)*erf(1.66666666666667*sqrt(2)*(l1l - l2r)) + (l1r - l2l)*erf(1.66666666666667*sqrt(2)*(l1r - l2l)) - (l1r - l2r)*erf(1.66666666666667*sqrt(2)*(l1r - l2r)))/((-l1l + l1r)*(-l2l + l2r)) + 0.09399856029866252*(8.865384008920727*(l1l - l2l)**2*exp(-5.55555555555556*(l1l - l2l)**2) - 8.865384008920727*(l1l - l2r)**2*exp(-5.55555555555556*(l1l - l2r)**2) - 8.865384008920727*(l1r - l2l)**2*exp(-5.55555555555556*(l1r - l2l)**2) + 8.865384008920727*(l1r - l2r)**2*exp(-5.55555555555556*(l1r - l2r)**2))/((-l1l + l1r)*(-l2l + l2r)) + 0.09*(-37.037037037037*(l1l - l2l)**2*exp(-5.55555555555556*(l1l - l2l)**2) + 37.037037037037*(l1l - l2r)**2*exp(-5.55555555555556*(l1l - l2r)**2) + 37.037037037037*(l1r - l2l)**2*exp(-5.55555555555556*(l1r - l2l)**2) - 37.037037037037*(l1r - l2r)**2*exp(-5.55555555555556*(l1r - l2r)**2))/((-l1l + l1r)*(-2.0*l2l + 2.0*l2r)) + 0.6*(-exp(-5.55555555555556*(l1l - l2l)**2) + exp(-5.55555555555556*(l1l - l2r)**2) + exp(-5.55555555555556*(l1r - l2l)**2) - exp(-5.55555555555556*(l1r - l2r)**2))/((-l1l + l1r)*(-2.0*l2l + 2.0*l2r))
    dkll1 = 0.3759942411946501*(-(l1l - l2l)*erf(1.66666666666667*sqrt(2)*(l1l - l2l)) + (l1l - l2r)*erf(1.66666666666667*sqrt(2)*(l1l - l2r)) + (l1r - l2l)*erf(1.66666666666667*sqrt(2)*(l1r - l2l)) - (l1r - l2r)*erf(1.66666666666667*sqrt(2)*(l1r - l2r)))/((-l1l + l1r)*(-l2l + l2r))
    
    kpl   = 0.09399856029866252*(-erf(1.66666666666667*sqrt(2)*(ll - p)) + erf(1.66666666666667*sqrt(2)*(lr - p)))/(-ll + lr)
    dkpl0 = 0.09399856029866252*(8.865384008920727*(ll - p)*exp(-5.55555555555556*(ll - p)**2) - 8.865384008920727*(lr - p)*exp(-5.55555555555556*(lr - p)**2))/(-ll + lr) + 0.3133285343288751*(-erf(1.66666666666667*sqrt(2)*(ll - p)) + erf(1.66666666666667*sqrt(2)*(lr - p)))/(-ll + lr)
    dkpl1 = 0.3759942411946501*(-erf(1.66666666666667*sqrt(2)*(ll - p)) + erf(1.66666666666667*sqrt(2)*(lr - p)))/(-ll + lr)
    
    return kll, dkll0, dkll1, dkpl1, dkpl0, kpl


def test():
    X = np.array([[ 0. ,  0.9],[ 0.3,  0.8],[ 1.4,  1.5],[ 1.8,  2.5],
                  [ 2.1,  3. ],[ 3. ,  3.4],[ 3.6,  4.2],[ 4.1,  4.5], 
                  # /\ 8 lines and 7 points \/
                  [ 1.1,  1.1],[ 1.7,  1.7],[ 2.3,  2.3],[ 3.2,  3.2],
                  [ 3.9,  3.9],[ 4.1,  4.1],[ 4.8,  4.8]])
        
    
    from GPy.kern.parts.Reid import Reid
    t = Reid(1, 0.3, 0.5)
    
    k = t._K(X)
    dkls, dksig = t._dK_dTh(X)
    d = t._K_diag(X)
    dls, dsig = t._dK_diag_dTh(X)
    
    sym = symk(t.input_array)
    thresh = 1e-14
    
    kll    = (np.abs(k[:8, :8]     - sym[0]) < thresh).all()
    lldls  = (np.abs(dkls[:8, :8]  - sym[1]) < thresh).all()
    lldsig = (np.abs(dksig[:8, :8] - sym[2]) < thresh).all()
    
    kpl    = (np.abs(k[8:, :8]     - sym[-1]) < thresh).all()
    pldls  = (np.abs(dkls[8:, :8]  - sym[-2]) < thresh).all()
    pldsig = (np.abs(dksig[8:, :8] - sym[-3]) < thresh).all()
    
    dk    = (np.abs(np.diag(k)     - d)    < thresh).all()
    ddls  = (np.abs(np.diag(dkls)  - dls)  < thresh).all()
    ddsig = (np.abs(np.diag(dksig) - dsig) < thresh).all()
    
    return [kll, lldls, lldsig], [kpl, pldls, pldsig], [dk, ddls, ddsig]
    
