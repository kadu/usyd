# -*- coding: utf-8 -*-
"""
Testing GPy reid kernel
"""

import reid_input_generator as ig
import GPy
import numpy as np
import matplotlib.pyplot as plt

ground_truth, data_points = ig.input_generator(plot=1, dtype='l')

X, Y = data_points

#X = X.T[:1].T
#reidk = GPy.kern.rbf(1)

reidk = GPy.kern.reid(1, 1, 1)

m = GPy.models.GPRegression(X, Y, reidk)
#m._set_params(np.array([0.3795,  0.933 ,  0.0026])) # this program
m._set_params(np.array([0.8352,  0.3339 ,  0.0000])) # GPy

print m

m.ensure_default_constraints()
#m.optimize()

print m

X_ = np.reshape(np.linspace(-0.5, 5.5, 300), (300, 1))

Y += (np.random.randn(Y.shape[0], Y.shape[1]) * 0.0026)

mu, var, ymin, ymax = m.predict(X_)

n, ans = ground_truth

fig, ax = plt.subplots(1)
ax.set_xlim([-0.5, 5.5])
ax.set_ylim([-3,3])
ax.plot(X_, mu, 'r-', label="estimation")
ax.plot(X, Y, 'kx', c='k', marker='o')
ax.fill_between(X_.flatten(), ymin.flatten(), ymax.flatten(), color='grey', alpha=0.5)
ax.plot(n, ans, 'k-')