
# -*- coding: utf-8 -*-
"""
Areawise kernel from reid
"""

from kernpart import Kernpart
import numpy as np
import scipy.special as ss

class Reid(Kernpart):
    """
    Areawise kernel.
    Models a cell as the result of observing the output function f(x)
    over a discrete rectangle R or line L rather than at a point P.
    In 1D, a line is represented as a pair of coordinates [X0, X1]
    A point is  then represented as a line of length zero (X0 == X1)
    In 2D, a rectangle is represented as a pair of diagonally opposed corners [X0, Y0, X1, Y1]
    Like above, a point is a rectangle of area 0.
    
    The test observations (usually X*, here X2) are assumed to be points only.

    :param input_dim: the number of input dimensions
    :type input_dim: int
    :param lengthscale: lengthscale of the underlying square exponential kernel
    :type lengthscale: float
    :param sigvar: signal variance of the underlying square exponential kernel
    :type sigvar: float
    """
    def __init__(self, input_dim, lengthscale=None, sigvar=None):
        self.input_dim = input_dim
        assert self.input_dim==1, "Line kernel works in 1D only"
        self.num_params = 2 # for sqexp covariance function
        self.name = 'Reid'

        if lengthscale is not None:
            lengthscale = np.asarray(lengthscale)
        else:
            lengthscale = 1. # allows for multi dimensional lengthscale
            
        if sigvar is not None:
            sigvar = np.asarray(sigvar)
        else:
            sigvar = 1.
        
        self._set_params(np.hstack((lengthscale, sigvar)))

        self.input_array = None
        self.input_id = None
        self.test_array = None
        self.test_id = None

    def _set_params(self, params):
        assert params.size == (self.num_params)
        self.lengthscale = params[0]
        self.sigvar = params[1]
        
    def _get_params(self):
         return np.hstack((self.lengthscale,self.sigvar))

    def _get_param_names(self):
        return ['lengthscale', 'signal variance']

    def K(self, X, X2, target):
        target += self._K(X, X2)

    def Kdiag(self, X, target):
        target += self._K_diag(X)
        
    def dK_dtheta(self, dL_dK, X, X2, target):
        dks = self._dK_dTh(X, X2)
        
        target[0] += np.sum(dks[0]*dL_dK)
        target[1] += np.sum(dks[1]*dL_dK)
        

    def dKdiag_dtheta(self, dL_dKdiag, X, target):
        dk = self._dK_diag_dTh(X)
        
        target[0] += np.sum(dk[0]*dL_dKdiag)
        target[1] += np.sum(dk[1]*dL_dKdiag)


    def dK_dX(self,dL_dK,X,X2,target):
        # TODO
        raise NotImplementedError, "TODO"

    def dKdiag_dX(self,dL_dKdiag,X,target):
        # TODO
        raise NotImplementedError, "TODO"



    #--------------------------------------#
    #             Computations             #
    #--------------------------------------#

    def _format_input(self, x):
        self.input_id = id(x)
        p = []
        ll = []
        lr = []
        for el in x:
            if el[0] == el[1]:
                p.append(el[0])
            else:
                ll.append(min(el))
                lr.append(max(el))
                
        p  = np.reshape( p, (len( p), 1))
        ll = np.reshape(ll, (len(ll), 1))
        lr = np.reshape(lr, (len(lr), 1))        
        
        l = (ll, lr)
        
        self.input_array = (l, p)

        
    def _format_test(self, x):
        self.test_id = id(x)
        if x.shape[1] != 1:
            x = [a[0] for a in x]
            self.test_array = np.asarray([x]).T
        else:
            self.test_array = x

    def _K(self, x, x2 = None):
        if x2 is x:
            x2 = None
        if self.input_id != id(x): # If array hasn't been reformatted
            self._format_input(x)  # reformat for compliance
        x = self.input_array
        l, p = x
        
        if x2 == None:
            kll = self._l2lSqExp1D(l)
            if len(p) == 0: # no input points
                return kll

            kpp = self._p2pSqExp1D(p)            
            if len(l[0]) == 0: # no input lines
                return kpp
            
            kpl = self._p2lSqExp1D(p, l)
            klp = kpl.T
            
            a = np.concatenate((kll, kpl))
            b = np.concatenate((klp, kpp))
            
            return np.concatenate((a, b), 1)
            
        if self.test_id != id(x2):
            self._format_test(x2)
        x2 = self.test_array

        kpx = self._p2pSqExp1D(p, x2)
        if len(l[0]) == 0: # no input lines
            return kpx
        klx = self._p2lSqExp1D(x2, l).T
        if len(p) == 0: # no input points
            return klx
    
        return np.concatenate((klx, kpx))
        
    def _dK_dTh(self, x, x2 = None):
        if x2 is x:
            x2 = None
        if self.input_id != id(x):
            self._format_input(x)
        x = self.input_array
        l, p = x

        if x2 == None:    
            ll = self._l2lSqExp1D(l, delTh = True)
            if len(p) == 0:
                return ll
            
            pp = self._p2pSqExp1D(p, delTh = True)
            if len(l[0]) == 0:
                return pp

            pl = self._p2lSqExp1D(p, l, delTh = True)            
            lp = map(np.transpose, pl)
            
            a = np.concatenate((ll[0], pl[0]))
            b = np.concatenate((lp[0], pp[0]))
            dkdls = np.concatenate((a, b), 1)
            
            a = np.concatenate((ll[1], pl[1]))
            b = np.concatenate((lp[1], pp[1]))
            dkdsig = np.concatenate((a, b), 1)
            
            return dkdls, dkdsig
            
        if self.test_id != id(x2):
            self._format_test(x2)
        x2 = self.test_array

        px = self._p2pSqExp1D(p, x2, delTh = True)
        if len(l[0]) == 0: # no input lines
            return px
        
        lx = map(np.transpose, self._p2lSqExp1D(x2, l, delTh = True))
        if len(p) == 0: # no input points
            return lx            
        
        dkdls  = np.concatenate((lx[0], px[0]))
        dkdsig = np.concatenate((lx[1], px[1]))
        
        return dkdls, dkdsig

    def _K_diag(self, x):
        if self.input_id != id(x):
            self._format_input(x)
        x = self.input_array
        l, p = x
        
        a = self._l2lSqExp1D(l, diag = True)
        b = self._p2pSqExp1D(p, diag = True)
        
        return np.concatenate((a, b))

    def _dK_diag_dTh(self, x):
        if self.input_id != id(x):
            self._format_input(x)
        x = self.input_array
        l, p = x
        
        a = self._l2lSqExp1D(l, diag = True, delTh = True)
        b = self._p2pSqExp1D(p, diag = True, delTh = True)
        
        dkdls  = np.concatenate((a[0], b[0]))
        dkdsig = np.concatenate((a[1], b[1]))
        
        return dkdls, dkdsig


    #--------------------------------------#
    #         Covariance Functions         #
    #--------------------------------------#

    def _p2pSqExp1D(self, p1, p2 = None, diag = False, delTh = False):
        '''
        Square exponential point-to-point covariance function.
        If diag == True, calculates only the diagonal of k(x,x).
        If delTh == True, calculate dk/dTh, where Th are the hyperparameters
        '''
        if p1 == []:
            return []

        if delTh:
            sigf = self.sigvar    
            
            if diag:
                diag = np.ones(len(p1))
                dsig = 2 * sigf * diag
                dls = np.zeros(len(p1))
            
                return dls.flatten(), dsig.flatten()
                # dKdiag_dTh

            if p2 == None:
                p2 = p1.copy().T
            else:
                p2 = p2.T

            sig2f = self.sigvar ** 2
            ls2 = self.lengthscale ** 2            
            ls3 = self.lengthscale * ls2
            
            d = (p1 - p2) ** 2
                        
            dsig = 2 * sigf * np.exp(-d / (2 * ls2))
            dls = sig2f * d * np.exp(-d / (2 * ls2)) / ls3
            
            return dls, dsig
            # dK_dTh
        

        sig2f = self.sigvar ** 2
        
        if diag:
            diag = np.ones(len(p1))
            return diag * sig2f
            # Kdiag

        if p2 == None:
            p2 = p1.copy().T
        else:
            p2 = p2.T
        
        ls2 = self.lengthscale ** 2
        
        return sig2f * np.exp((- (p1 - p2) ** 2) / (2 * ls2))
        # K
    
    
    def _p2lSqExp1D(self, p, l, delTh = False):
        '''
        Square exponential point-to-line covariance function
        If delTh == True, calculate dk/dTh, where Th are the hyperparameters
        '''
        if p == [] or l == []:
            return []

        sig2f = self.sigvar ** 2
        ls = self.lengthscale
        V2ls = np.sqrt(2.) * ls
        vpi2 = np.sqrt(np.pi / 2.)
    
        ll, lr = l
        
        ll, lr = ll.T, lr.T
        
        r = (lr - p) / V2ls
        erfr = ss.erf(r)
        l = (ll - p) / V2ls
        erfl = ss.erf(l)
        
        if delTh:        
            sigf = self.sigvar
            
            c1 = vpi2 * 2 * sigf * ls / (lr - ll)
            dsig = c1 * (erfr - erfl)
            
            dls = np.sqrt(np.pi / 2.)*ls*sigf**2*(np.sqrt(2)*(ll - p)*np.exp(-(ll - p)**2/(2*ls**2))/(np.sqrt(np.pi)*ls**2) - np.sqrt(2)*(lr - p)*np.exp(-(lr - p)**2/(2*ls**2))/(np.sqrt(np.pi)*ls**2))/(-ll + lr) + np.sqrt(np.pi / 2.)*sigf**2*(-ss.erf(np.sqrt(2)*(ll - p)/(2*ls)) + ss.erf(np.sqrt(2)*(lr - p)/(2*ls)))/(-ll + lr)
            
            return dls, dsig


        c = vpi2 * sig2f * ls / (lr - ll)

        return c * (erfr - erfl)

    
    def _l2lSqExp1D(self, l1, l2 = None, diag = False, delTh = False):
        '''
        Square exponential line-to-line covariance function
        If diag == True, calculates only the diagonal of k(x,x).
        If delTh == True, calculate dk/dTh, where Th are the hyperparameters
        '''
        if l1 == []:
            return []
        
        sigf = self.sigvar
        sig2f = self.sigvar ** 2
        ls = self.lengthscale
        ls2 = self.lengthscale ** 2
        V2ls = np.sqrt(2) * ls
        
        l1l, l1r = l1
        
        if delTh:
            if diag:
                l2l, l2r = l1l, l1r
                
                r = l2r - l2l
                
                d1 = l1r - l2l
                d3 = l1l - l2r
                
                dls = ((d1**2)*np.exp(-0.5*(d1**2)/ls2)/ls + (d3**2)*np.exp(-0.5*(d3**2)/ls2)/ls)/(2*(r**2)) + 2*ls*(np.exp(-0.5*(d1**2)/ls2) + np.exp(-0.5*(d3**2)/ls2) - 2)/(2*(r**2)) +np.sqrt(np.pi/2.)*ls*sig2f*(-np.sqrt(2)*(d1**2)*np.exp(-(d1**2)/(2*ls2))/(np.sqrt(np.pi)*ls2) - np.sqrt(2)*(d3**2)*np.exp(-(d3**2)/(2*ls2))/(np.sqrt(np.pi)*ls2))/r**2 +np.sqrt(np.pi/2.)*sig2f*(d1*ss.erf(np.sqrt(2)*d1/(2*ls)) + d3*ss.erf(np.sqrt(2)*d3/(2*ls)))/r**2

                z1 = d1 * ss.erf(d1 / V2ls)
                z3 = d3 * ss.erf(d3 / V2ls)
                zz = (z1 + z3)

                d = np.sqrt(np.pi / 2.) * (2 * sigf) * ls / (r ** 2)
                dsig = d * zz
                return dls.flatten(), dsig.flatten()
                # dK_diag_dTh
    
            if l2 == None:
                l2l, l2r = l1l.copy().T, l1r.copy().T
            else:
                l2l, l2r = l2[0].T, l2[1].T
                
            r1 = l1r - l1l
            r2 = l2r - l2l
            
            d1 = l1r - l2l
            d2 = l1l - l2l
            d3 = l1l - l2r
            d4 = l1r - l2r
            
            dls = ls2*(d1**2*np.exp(-0.5*d1**2/ls2)/ls**3 - d2**2*np.exp(-0.5*d2**2/ls2)/ls**3 + d3**2*np.exp(-0.5*d3**2/ls2)/ls**3 - d4**2*np.exp(-0.5*d4**2/ls2)/ls**3)/(r1*r2 * 2.) + 2*ls*(np.exp(-0.5*d1**2/ls2) - np.exp(-0.5*d2**2/ls2) + np.exp(-0.5*d3**2/ls2) - np.exp(-0.5*d4**2/ls2))/(r1*r2 * 2.) + np.sqrt(np.pi/2.)*ls*sig2f*(-np.sqrt(2)*d1**2*np.exp(-d1**2/(2*ls2))/(np.sqrt(np.pi)*ls2) + np.sqrt(2)*d2**2*np.exp(-d2**2/(2*ls2))/(np.sqrt(np.pi)*ls2) - np.sqrt(2)*d3**2*np.exp(-d3**2/(2*ls2))/(np.sqrt(np.pi)*ls2) + np.sqrt(2)*d4**2*np.exp(-d4**2/(2*ls2))/(np.sqrt(np.pi)*ls2))/(r1*r2) + np.sqrt(np.pi/2.)*sig2f*(d1*ss.erf(d1/V2ls) - d2*ss.erf(d2/V2ls) + d3*ss.erf(d3/V2ls) - d4*ss.erf(d4/V2ls))/(r1*r2)

            z1 = d1 * ss.erf(d1 / V2ls)
            z2 = d2 * ss.erf(d2 / V2ls)
            z3 = d3 * ss.erf(d3 / V2ls)
            z4 = d4 * ss.erf(d4 / V2ls)
            zz = (z1 - z2 + z3 - z4)

            
            d = np.sqrt(np.pi / 2.) * (2 * sigf) * ls / (r2 * r1)
            dsig = d * zz
            
            return dls, dsig
            # dK_dTh

        if diag:
            l2l, l2r = l1l, l1r
            
            r = l2r - l2l
            
            c1 = np.sqrt(np.pi / 2.) * sig2f * ls / (r ** 2)
            
            d1 = l1r - l2l
            d3 = l1l - l2r
            
            c2 =  d1 * ss.erf(d1 / V2ls)
            c2 += d3 * ss.erf(d3 / V2ls)        
            
            c3 = ls2 / (2 * (r ** 2))
            
            c4 =  np.exp(- (d1 ** 2) / (2 * ls2))
            c4 -= 1.
            c4 += np.exp(- (d3 ** 2) / (2 * ls2))
            c4 -= 1.

            return np.squeeze((c1 * c2) + (c3 * c4))
            # K_diag

        if l2 == None:
            l2l, l2r = l1l.copy().T, l1r.copy().T
        else:
            l2l, l2r = l2[0].T, l2[1].T
            
        r1 = l1r - l1l
        r2 = l2r - l2l
    
        c1 = np.sqrt(np.pi / 2.) * (sig2f) * ls / (r2 * r1 )
        
        d1 = l1r - l2l
        d2 = l1l - l2l
        d3 = l1l - l2r
        d4 = l1r - l2r
        
        c2 =  d1 * ss.erf(d1 / V2ls)
        c2 -= d2 * ss.erf(d2 / V2ls)
        c2 += d3 * ss.erf(d3 / V2ls)        
        c2 -= d4 * ss.erf(d4 / V2ls)        
        
        c3 = ls2 / (2 * r2 * r1)
        
        c4 =  np.exp(- (d1 ** 2) / (2 * ls2))
        c4 -= np.exp(- (d2 ** 2) / (2 * ls2))
        c4 += np.exp(- (d3 ** 2) / (2 * ls2))
        c4 -= np.exp(- (d4 ** 2) / (2 * ls2))
        
        return (c1 * c2) + (c3 * c4)
        # K

    def _p2pPoly1D (self, p1, p2 = None, diag = False, delTh = False):
        ls = self.lengthscale
        
        if p2 == None:
            p2 = p1.T
        else:
            p2 = p2.T
        r = np.abs(p2 - p1)

        if delTh and diag:
            ls2 = self.lengthscale ** 2
            rd = np.diag(r)

            a = (1 - (rd / ls)) ** 3
            b = (3 * rd / ls) + 1.
            da = (3 * rd / ls2) * (1 - (rd / ls))**2
            db = -3 * rd / ls2
            
            ans = (a * db) + (b * da)
            ans[rd > ls] = 0.

            return ans
        
        if delTh and not diag:
            ls2 = self.lengthscale ** 2

            a = (1 - (r / ls)) ** 3
            b = (3 * r / ls) + 1
            da = (3 * r / ls2) * (1 - (r / ls)) ** 2
            db = -3 * r / (ls ** 2)
            
            ans = (a * db) + (b * da)
            ans[r > ls] = 0.
            
            return ans
            
        if not delTh and diag:
            rd = np.ones(r.shape)
            
            a = (1 - (rd / ls)) ** 3
            b = (3 * rd / ls) + 1.
            
            ans = a * b
            ans[rd > ls] = 0.
            
            return ans
            
        if not delTh and not diag:
            a = 1 - (r / ls)
            b = (3 * r / ls) + 1
            
            ans = (a ** 3) * b
            ans[r > ls] = 0.
            
            return ans
            
    def _p2lPoly1D(self, p, l, diag = False, delTh = False):        
        if l == None:
            return np.ndarray((len(p), 0))
        if p == None:
            return np.ndarray((0, len(l)))

        ls = self.lengthscale
        ls2 = ls ** 2
        ls3 = ls ** 3
        ls4 = ls ** 4
        
        ll, lr = l         
        ans = np.zeros((len(p), len(ll)))
        ll, lr = ll.T, lr.T

        case = np.zeros(ans.shape, dtype='int')
        case[p >= lr + ls] = 9
        land = np.logical_and
        
        if (ll + ls <= lr - ls).any():
            cond = ll + ls <= lr - ls
            case[land(land(ll - ls <= p, p <  ll     ), cond)] = 1
            case[land(land(ll      <= p, p <  ll + ls), cond)] = 2
            case[land(land(ll + ls <= p, p <  lr - ls), cond)] = 3
            case[land(land(lr - ls <= p, p <  lr     ), cond)] = 4
            case[land(land(lr      <= p, p <  lr + ls), cond)] = 5
        if (land(ll <= lr - ls, lr - ls < ll + ls)).any():
            cond = land(ll <= lr - ls, lr - ls < ll + ls)
            case[land(land(ll - ls <= p, p <  ll     ), cond)] = 1
            case[land(land(ll      <= p, p <  lr - ls), cond)] = 2
            case[land(land(ll + ls <= p, p <  lr     ), cond)] = 4
            case[land(land(lr      <= p, p <  lr + ls), cond)] = 5
            case[land(land(lr - ls <= p, p <  ll + ls), cond)] = 6
        if (lr - ls < ll).any():
            cond = lr - ls < ll
            case[land(land(ll - ls <= p, p <  lr - ls), cond)] = 1
            case[land(land(ll + ls <= p, p <  lr + ls), cond)] = 5
            case[land(land(ll      <= p, p <  lr     ), cond)] = 6
            case[land(land(lr - ls <= p, p <  ll     ), cond)] = 7
            case[land(land(lr      <= p, p <  ll + ls), cond)] = 8
            
#        # Testing if all intervals are correctly found
#        ans = case
#        return ans
            
        def int_k(r):
            ans  = r
            ans -= 2. * (r ** 3) / ls2
            ans += 2. * (r ** 4) / ls3
            ans -= .6 * (r ** 5) / ls4
            return ans
        
        int_0  = int_k(0.)
        int_ls = int_k(ls)
        pll = p - ll
        plr = p - lr
        llp = ll - p
        lrp = lr - p
        
        ans[case == 1] = int_ls   - int_k(llp[case == 1])
        ans[case == 2] = int_k(pll[case == 2]) - int_0 + int_ls - int_0
        ans[case == 3] = 2. * (int_ls - int_0)
        ans[case == 4] = int_ls - int_0    + int_k(lrp[case == 4]) - int_0
        ans[case == 5] = int_ls - int_k(plr[case == 5])
        ans[case == 6] = int_k(pll[case == 6]) - int_0 + int_k(lrp[case == 6]) - int_0
        ans[case == 7] = int_k(lrp[case == 7]) - int_k(llp[case == 7])
        ans[case == 8] = int_k(pll[case == 8]) - int_k(plr[case == 8])
        
        return ans

    def _l2lPoly1D(self, l1, l2, diag = False, delTh = False):        
        if l2 == None:
            return np.ndarray((len(l1), 0))
        if l1 == None:
            return np.ndarray((0, len(l2)))

        ls = self.lengthscale
        ls2 = ls ** 2
        ls3 = ls ** 3
        ls4 = ls ** 4
        
        l1l, l1r = l1
        l2l, l2r = l2
        ans = np.zeros((len(l1l), len(l2l)))
        l2l, l2r = l2l.T, l2r.T

        land = np.logical_and

        # first determine where l2l is relative to l1:

        case_l = np.zeros(ans.shape, dtype='bool')
        case_l[l2l >= l1r + ls] = 9        

        cond1 = l1l + ls <= l1r - ls
        if (cond1).any():
            case_l[land(land(l1l - ls <= l2l, l2l <  l1l     ), cond1)] = 1
            case_l[land(land(l1l      <= l2l, l2l <  l1l + ls), cond1)] = 2
            case_l[land(land(l1l + ls <= l2l, l2l <  l1r - ls), cond1)] = 3
            case_l[land(land(l1r - ls <= l2l, l2l <  l1r     ), cond1)] = 4
            case_l[land(land(l1r      <= l2l, l2l <  l1r + ls), cond1)] = 5

        cond2 = land(l1l <= l1r - ls, l1r - ls < l1l + ls)
        if (cond2).any():
            case_l[land(land(l1l - ls <= l2l, l2l <  l1l     ), cond2)] = 1
            case_l[land(land(l1l      <= l2l, l2l <  l1r - ls), cond2)] = 2
            case_l[land(land(l1r - ls <= l2l, l2l <  l1l + ls), cond2)] = 3 # case 6
            case_l[land(land(l1l + ls <= l2l, l2l <  l1r     ), cond2)] = 4
            case_l[land(land(l1r      <= l2l, l2l <  l1r + ls), cond2)] = 5

        cond3 = l1r - ls < l1l
        if (cond3).any():
            case_l[land(land(l1l - ls <= l2l, l2l <  l1r - ls), cond3)] = 1
            case_l[land(land(l1r - ls <= l2l, l2l <  l1l     ), cond3)] = 2 # case 7
            case_l[land(land(l1l      <= l2l, l2l <  l1r     ), cond3)] = 3 # case 6
            case_l[land(land(l1r      <= l2l, l2l <  l1l + ls), cond3)] = 4 # case 8
            case_l[land(land(l1l + ls <= l2l, l2l <  l1r + ls), cond3)] = 5
            
        # then determine where l2r is relative to l1:
            
        case_r = np.zeros(ans.shape, dtype='bool')
        case_r[l2r >= l1r + ls] = 9

        # cond1 = l1l + ls <= l1r - ls
        if (cond1).any():
            case_r[land(land(l1l - ls <= l2r, l2r <  l1l     ), cond1)] = 1
            case_r[land(land(l1l      <= l2r, l2r <  l1l + ls), cond1)] = 2
            case_r[land(land(l1l + ls <= l2r, l2r <  l1r - ls), cond1)] = 3
            case_r[land(land(l1r - ls <= l2r, l2r <  l1r     ), cond1)] = 4
            case_r[land(land(l1r      <= l2r, l2r <  l1r + ls), cond1)] = 5
            
        # cond2 = land(l1l <= l1r - ls, l1r - ls < l1l + ls)
        if (cond2).any():
            case_r[land(land(l1l - ls <= l2r, l2r <  l1l     ), cond2)] = 1
            case_r[land(land(l1l      <= l2r, l2r <  l1r - ls), cond2)] = 2
            case_r[land(land(l1r - ls <= l2r, l2r <  l1l + ls), cond2)] = 3 # case 6
            case_r[land(land(l1l + ls <= l2r, l2r <  l1r     ), cond2)] = 4
            case_r[land(land(l1r      <= l2r, l2r <  l1r + ls), cond2)] = 5
            
        # cond3 = l1r - ls < l1l
        if (l1r - ls < l1l).any():
            case_r[land(land(l1l - ls <= l2r, l2r <  l1r - ls), cond3)] = 1
            case_r[land(land(l1r - ls <= l2r, l2r <  l1l     ), cond3)] = 2 # case 7
            case_r[land(land(l1l      <= l2r, l2r <  l1r     ), cond3)] = 3 # case 6
            case_r[land(land(l1r      <= l2r, l2r <  l1l + ls), cond3)] = 4 # case 8
            case_r[land(land(l1l + ls <= l2r, l2r <  l1r + ls), cond3)] = 5

        def int2_k(r):
            ans  = .5 * (r ** 2)
            ans -= .5 * (r ** 4) / ls2
            ans += .4 * (r ** 5) / ls3
            ans -= .1 * (r ** 6) / ls4
            return ans

        int_0  = 0. # int2_k(0.)
        int_ls = int2_k(ls)
        r1 = (l1r - l1l)
        int_r1 = int2_k(r1)
        int_lr = int2_k(ls - r1)
        int_rl = int2_k(r1 - ls)        

        # integrals over whole intervals are combinations between the
        # following:
        l0  = int_ls - int_0
        rl0 = int_rl - int_0
        llr = int_ls - int_lr
        r0  = int_r1 - int_0
        
        # All whole intervals are added to the answer
        # cond1 = l1l + ls <= l1r - ls
        ans[land(cond1, land(case_l < 1, case_r > 1))] += l0
        ans[land(cond1, land(case_l < 2, case_r > 2))] += l0
        ans[land(cond1, land(case_l < 3, case_r > 3))] += 2 * l0
        ans[land(cond1, land(case_l < 4, case_r > 4))] += -l0
        ans[land(cond1, land(case_l < 5, case_r > 5))] += -l0

        # cond2 = land(l1l <= l1r - ls, l1r - ls < l1l + ls)
        ans[land(cond2, land(case_l < 1, case_r > 1))] += l0
        ans[land(cond2, land(case_l < 2, case_r > 2))] += rl0
        ans[land(cond2, land(case_l < 3, case_r > 3))] += 2 * l0
        ans[land(cond2, land(case_l < 4, case_r > 4))] += -rl0
        ans[land(cond2, land(case_l < 5, case_r > 5))] += -l0
        
        # cond3 = l1r - ls < l1l
        ans[land(cond3, land(case_l < 1, case_r > 1))] += llr
        ans[land(cond3, land(case_l < 2, case_r > 2))] += r0 - llr
        ans[land(cond3, land(case_l < 4, case_r > 4))] += llr - r0
        ans[land(cond3, land(case_l < 5, case_r > 5))] += -llr
        
        # Now the intervals where the ends of l2 lay must be added
        r2 = l2l - l2r
        dll = l1l - l2l
        int_dll = int2_k(dll)
        int_lld  = int2_k(-dll)
        drl = l1r - l2l
        int_drl = int2_k(drl)
        int_rld = int2_k(-drl)
        dlr = l1l - l2r
        int_dlr = int2_k(dlr)
        int_lrd = int2_k(-dlr)
        drr = l1r - l2r
        int_drr = int2_k(drr)
        int_rrd = int2_k(-drr)
        
        # left end first
        # cond1 = l1l + ls <= l1r - ls
        ans[land(cond1, land(case_l == 1, case_r > 1))] += int_dll - int_0
        ans[land(cond1, land(case_l == 2, case_r > 2))] += int_ls - int_dll
        ans[land(cond1, land(case_l == 3, case_r > 3))] += 2 * l0 * (drl - ls) / (r1 - 2 * ls)
        ans[land(cond1, land(case_l == 4, case_r > 4))] += int_0 - int_drl
        ans[land(cond1, land(case_l == 5, case_r > 5))] += int_rld - int_ls
        
        # cond2 = land(l1l <= l1r - ls, l1r - ls < l1l + ls)
        ans[land(cond2, land(case_l == 1, case_r > 1))] += int_dll - int_0
        ans[land(cond2, land(case_l == 2, case_r > 2))] += int_rl - int_dll
        ans[land(cond2, land(case_l == 3, case_r > 3))] += 2 * l0 * (dll + ls) / (-r1 + 2 * ls)
        ans[land(cond2, land(case_l == 4, case_r > 4))] += int_0 - int_drl
        ans[land(cond2, land(case_l == 5, case_r > 5))] += int_rld - int_ls

        # cond3 = l1r - ls < l1l        
        ans[land(cond3, land(case_l == 1, case_r > 1))] += int_dll - int_lr
        ans[land(cond3, land(case_l == 2, case_r > 2))] += int_r1 - int_0 - int_drl + int_dll
        ans[land(cond3, land(case_l == 3, case_r > 3))] += int_r1 - int_lld - int_drl + int_0
        ans[land(cond3, land(case_l == 4, case_r > 4))] += int_ls - int_lr - int_lld + int_rld
        ans[land(cond3, land(case_l == 5, case_r > 5))] += int_rld - int_ls


        # Then the right end
        # cond1 = l1l + ls <= l1r - ls
        ans[land(cond1, land(case_l < 1, case_r == 1))] += int_dlr - int_ls
        ans[land(cond1, land(case_l < 2, case_r == 2))] += int_rld - int_0
        ans[land(cond1, land(case_l < 3, case_r == 3))] += 2 * l0 * (-drl - ls) / (r1 - 2 * ls)
        ans[land(cond1, land(case_l < 4, case_r == 4))] += int_drr - int_ls
        ans[land(cond1, land(case_l < 5, case_r == 5))] += int_0 - int_rrd
        
        # cond2 = land(l1l <= l1r - ls, l1r - ls < l1l + ls)
        ans[land(cond2, land(case_l < 1, case_r == 1))] += int_dlr - int_ls
        ans[land(cond2, land(case_l < 2, case_r == 2))] += int_rld - int_0
        ans[land(cond2, land(case_l < 3, case_r == 3))] += 2 * l0 * (-drr + ls) / (-r1 + 2 * ls)
        ans[land(cond2, land(case_l < 4, case_r == 4))] += int_drr - int_rl
        ans[land(cond2, land(case_l < 5, case_r == 5))] += int_0 - int_rrd

        # cond3 = l1r - ls < l1l        
        ans[land(cond2, land(case_l < 1, case_r == 1))] += int_dlr - int_ls
        ans[land(cond2, land(case_l < 2, case_r == 2))] += int_drr - int_dlr + int_lr - int_ls
        ans[land(cond2, land(case_l < 3, case_r == 3))] += int_lrd - int_drr + int_0 - int_r1
        ans[land(cond2, land(case_l < 4, case_r == 4))] += int_lrd - int_rrd + int_0 - int_r1
        ans[land(cond2, land(case_l < 5, case_r == 5))] += int_0 - int_rrd
        
        # Finally, we account for when both ends of l2 are inte same region
        # cond1 = l1l + ls <= l1r - ls
        ans[land(cond1, land(case_l == 1, case_r == 1))] += int_dlr - int_dll
        ans[land(cond1, land(case_l == 2, case_r == 2))] += int_lrd - int_lld
        ans[land(cond1, land(case_l == 3, case_r == 3))] += 2 * l0 * r2 / (r1 - 2 * ls)
        ans[land(cond1, land(case_l == 4, case_r == 4))] += int_drr - int_drl
        ans[land(cond1, land(case_l == 5, case_r == 5))] += int_rld - int_rrd
        
        # cond2 = land(l1l <= l1r - ls, l1r - ls < l1l + ls)
        ans[land(cond2, land(case_l == 1, case_r == 1))] += int_dlr - int_dll
        ans[land(cond2, land(case_l == 2, case_r == 2))] += int_lrd - int_lld
        ans[land(cond2, land(case_l == 3, case_r == 3))] += 2 * l0 * r2 / (-1 + 2 * ls)
        ans[land(cond2, land(case_l == 4, case_r == 4))] += int_drr - int_drl
        ans[land(cond2, land(case_l == 5, case_r == 5))] += int_rld - int_rrd

        # cond3 = l1r - ls < l1l        
        ans[land(cond2, land(case_l == 1, case_r == 1))] += int_dlr - int_dll
        ans[land(cond2, land(case_l == 2, case_r == 2))] += int_drr - int_dlr + int_dll - int_drl
        ans[land(cond2, land(case_l == 3, case_r == 3))] += int_lrd - int_drr + int_drl - int_lld
        ans[land(cond2, land(case_l == 4, case_r == 4))] += int_lrd - int_rrd + int_rld - int_lld
        ans[land(cond2, land(case_l == 5, case_r == 5))] += int_rld - int_rrd
        
        # It wasnt too hard, was it?
        # Good luck debugging.        
        
        return ans