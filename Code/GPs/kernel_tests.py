# -*- coding: utf-8 -*-
"""
Testing covariance functions
"""

import numpy as np
import GPy
import covariance_functions as cf

dim = 11
np.random.seed(1)
X = np.random.random((3,dim))

kernel = GPy.kern.rbf(input_dim = dim, variance = 1., lengthscale = 1.)
A = kernel.K(X)

B = cf.sqexp(X, X, [1.,1.])