# Computes four benchmarks for comparision with matlab
# Fabio Ramos 03/11/2013
import numpy as np
#import scipy as sp
import scipy.linalg as spla
#import numpy.linalg as npla
import matplotlib.pyplot as plt
import time
import pylab

def cholesky_test(A):
    M = A.dot(A.T)
    t = time.time()
    for i in range(0,10):
        spla.cholesky(M)
    elapsed = time.time() - t                
    return elapsed/10
    
def multi_test(A):
    t = time.time()
    for i in range(0,10):
        A.dot(A)
    elapsed = time.time() - t                
    return elapsed/10

def sqexp_test(X):
    t = time.time()
    for i in range(0,10):
        dim = X.shape[0]
        len = X.shape[1]    
        Xp  = X.T.reshape(1,len,dim)
        Yp  = X.T.reshape(len,1,dim)
        tmp = Xp.repeat(len,0) - Yp.repeat(len,1)
        tmp = tmp*tmp
        tmp = tmp.sum(axis=2)
        tmp = np.exp(-0.5*tmp)
    elapsed = time.time() - t    
    return elapsed/10

def linsys_test(A):
    b = np.random.rand(A.shape[0])
    t = time.time()
    for i in range(0,10):
        spla.solve(A,b)
    elapsed = time.time() - t                
    return elapsed/10

#Asizes    = np.concatenate((np.arange(100,600,100), np.arange(500,10500,500)))
Asizes      = np.arange(100,1100,100)
len         = Asizes.shape[0]
choltimes   = np.zeros(len)
multitimes  = np.zeros(len)
sqexptimes  = np.zeros(len)
linsystimes = np.zeros(len)
i = 0
for s in Asizes:
    A              = np.random.rand(s,s)
    X              = np.random.rand(3,s)
    choltimes[i]   = cholesky_test(A)
    multitimes[i]  = multi_test(A)    
    sqexptimes[i]  = sqexp_test(X)
    linsystimes[i] = linsys_test(A)    
    i              = i + 1
print 'Choltimes = ',choltimes  
print 'Multi times = ',multitimes
print 'Cov fun times = ',sqexptimes  
print 'Linear system solving times = ', linsystimes
# Plot Cholesky benchmarks 
fig1 = plt.figure()
l, = plt.plot(Asizes, choltimes, 'r-')
plt.xlabel('Matrix size')
plt.ylabel('Time in seconds')
plt.title('Cholesky benchmarks')
plt.show

# Plot multiplication benchmarks 
fig2 = plt.figure()
l, = plt.plot(Asizes, multitimes, 'r-')
plt.xlabel('Matrix size')
plt.ylabel('Time in seconds')
plt.title('Multiplication benchmarks')
plt.show

# Plot multiplication benchmarks 
fig3 = plt.figure()
l, = plt.plot(Asizes, sqexptimes, 'r-')
plt.xlabel('Matrix size')
plt.ylabel('Time in seconds')
plt.title('Squared exponential cov fun benchmarks')
plt.show

# Plot multiplication benchmarks 
fig4 = plt.figure()
l, = plt.plot(Asizes, linsystimes, 'r-')
plt.xlabel('Matrix size')
plt.ylabel('Time in seconds')
plt.title('Linear system benchmarks')
plt.show

pylab.show()
