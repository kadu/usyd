# -*- coding: utf-8 -*-
"""
Covariance functions for GP regression
"""
import numpy as np
#import numpy.linalg as la
import scipy.spatial.distance as sd
import scipy.special as ss

def sqexp(a, b, theta = [], jac=False):
    '''
    Simple square exponential covariance.
    Hyperparameters: sig_f (signal variance) and l (length-scale)
    '''
    if theta == []:
        theta = [1.0, 1.0]
    sig_f, l = theta
    
    if not jac:
        return (sig_f ** 2) * np.exp(-(sd.cdist(a, b)**2)/(2*(l**2)))
    else:
        del1 = (2 * sig_f) * np.exp(-(sd.cdist(a, b)**2)/(2*(l**2)))
        del2 = del1 * (sig_f * (sd.cdist(a, b)**2)) / (2 * (l ** 3))
        return del1, del2
    
def noise(a, b, sig_n = 1.0, jac=False):
    '''
    Simple noise covariance function.
    If calculating K(x, x) or K(x*, x*), add sig_n**2 noise in the main diagonl.
    '''

    if jac:
        return 1.

    noise = 0.;
    if a is b:
        noise = (sig_n ** 2) * np.eye(len(a))

    return noise
    
def nsqexp(a, b, theta = [], jac = False):
    '''
    Simple square exponential covariance combined with noise covariance.
    nsqexp = sqexp + noise
    '''
    
    if theta == []:
        theta = [1.0, 1.0, 1.0]
        
    if not jac:
        return sqexp(a, b, theta[:2]) + noise(a, b, theta[2])
    else:
        del1, del2 = sqexp(a, b, theta[:2], True)
        del3 = 1.
        return del1, del2, del3



#-----------------------------------------#
#        Reid Covariance Functions        #
#-----------------------------------------#

def p2pSqExp1D(p1, p2, theta, diag = False, delTh = False):
    '''
    Square exponential point-to-point covariance function.
    If diag == True, calculates only the diagonal of k(x,x).
    If delTh == True, calculate dk/dTh, where Th are the hyperparameters
    '''
    ls, sigf = theta[0], theta[1]

    if p1 == []:
        return []

    if delTh:
        if diag:
            diag = np.ones(len(p1))
            dsig = 2 * sigf * diag
            dls = np.zeros(len(p1))
        
            return dls.flatten(), dsig.flatten()
            # dKdiag_dTh

        if p2 == None:
            p2 = p1.copy().T
        else:
            p2 = p2.T

        sig2f = sigf ** 2
        ls2 = ls ** 2            
        ls3 = ls * ls2
        
        d = (p1 - p2) ** 2
                    
        dsig = 2 * sigf * np.exp(-d / (2 * ls2))
        dls = sig2f * d * np.exp(-d / (2 * ls2)) / ls3
        
        return dls, dsig
        # dK_dTh
    

    sig2f = sigf ** 2
    
    if diag:
        diag = np.ones(len(p1))
        return diag * sig2f
        # Kdiag

    if p2 == None:
        p2 = p1.copy().T
    else:
        p2 = p2.T
    
    ls2 = ls ** 2
    
    return sig2f * np.exp((- (p1 - p2) ** 2) / (2 * ls2))
    # K


def p2lSqExp1D(p, l, theta, delTh = False):
    '''
    Square exponential point-to-line covariance function
    If delTh == True, calculate dk/dTh, where Th are the hyperparameters
    '''
    ls, sigf = theta[0], theta[1]    
    
    if p == [] or l == []:
        return []
        
    

    sig2f = sigf ** 2
    V2ls = np.sqrt(2.) * ls
    vpi2 = np.sqrt(np.pi / 2.)

    ll, lr = l
    
    ll, lr = ll.T, lr.T
    
    r = (lr - p) / V2ls
    erfr = ss.erf(r)
    l = (ll - p) / V2ls
    erfl = ss.erf(l)
    
    if delTh:
        c1 = vpi2 * 2 * sigf * ls / (lr - ll)
        dsig = c1 * (erfr - erfl)
        
        dls = np.sqrt(np.pi / 2.)*ls*sigf**2*(np.sqrt(2)*(ll - p)*np.exp(-(ll - p)**2/(2*ls**2))/(np.sqrt(np.pi)*ls**2) - np.sqrt(2)*(lr - p)*np.exp(-(lr - p)**2/(2*ls**2))/(np.sqrt(np.pi)*ls**2))/(-ll + lr) + np.sqrt(np.pi / 2.)*sigf**2*(-ss.erf(np.sqrt(2)*(ll - p)/(2*ls)) + ss.erf(np.sqrt(2)*(lr - p)/(2*ls)))/(-ll + lr)
        
        return dls, dsig


    c = vpi2 * sig2f * ls / (lr - ll)

    return c * (erfr - erfl)


def l2lSqExp1D(l1, l2, theta, diag = False, delTh = False):
    '''
    Square exponential line-to-line covariance function
    If diag == True, calculates only the diagonal of k(x,x).
    If delTh == True, calculate dk/dTh, where Th are the hyperparameters
    '''
    ls, sigf = theta[0], theta[1]    
    
    if l1 == []:
        return []
    
    sig2f = sigf ** 2
    ls2 = ls ** 2
    V2ls = np.sqrt(2) * ls
    
    l1l, l1r = l1
    
    if delTh:
        if diag:
            l2l, l2r = l1l, l1r
            
            r = l2r - l2l
            
            d1 = l1r - l2l
            d3 = l1l - l2r
            
            dls = ((d1**2)*np.exp(-0.5*(d1**2)/ls2)/ls + (d3**2)*np.exp(-0.5*(d3**2)/ls2)/ls)/(2*(r**2)) + 2*ls*(np.exp(-0.5*(d1**2)/ls2) + np.exp(-0.5*(d3**2)/ls2) - 2)/(2*(r**2)) +np.sqrt(np.pi/2.)*ls*sig2f*(-np.sqrt(2)*(d1**2)*np.exp(-(d1**2)/(2*ls2))/(np.sqrt(np.pi)*ls2) - np.sqrt(2)*(d3**2)*np.exp(-(d3**2)/(2*ls2))/(np.sqrt(np.pi)*ls2))/r**2 +np.sqrt(np.pi/2.)*sig2f*(d1*ss.erf(np.sqrt(2)*d1/(2*ls)) + d3*ss.erf(np.sqrt(2)*d3/(2*ls)))/r**2

            z1 = d1 * ss.erf(d1 / V2ls)
            z3 = d3 * ss.erf(d3 / V2ls)
            zz = (z1 + z3)

            d = np.sqrt(np.pi / 2.) * (2 * sigf) * ls / (r ** 2)
            dsig = d * zz
            return dls.flatten(), dsig.flatten()
            # dK_diag_dTh

        if l2 == None:
            l2l, l2r = l1l.copy().T, l1r.copy().T
        else:
            l2l, l2r = l2[0].T, l2[1].T
            
        r1 = l1r - l1l
        r2 = l2r - l2l
        
        d1 = l1r - l2l
        d2 = l1l - l2l
        d3 = l1l - l2r
        d4 = l1r - l2r
        
        dls = ls2*(d1**2*np.exp(-0.5*d1**2/ls2)/ls**3 - d2**2*np.exp(-0.5*d2**2/ls2)/ls**3 + d3**2*np.exp(-0.5*d3**2/ls2)/ls**3 - d4**2*np.exp(-0.5*d4**2/ls2)/ls**3)/(r1*r2 * 2.) + 2*ls*(np.exp(-0.5*d1**2/ls2) - np.exp(-0.5*d2**2/ls2) + np.exp(-0.5*d3**2/ls2) - np.exp(-0.5*d4**2/ls2))/(r1*r2 * 2.) + np.sqrt(np.pi/2.)*ls*sig2f*(-np.sqrt(2)*d1**2*np.exp(-d1**2/(2*ls2))/(np.sqrt(np.pi)*ls2) + np.sqrt(2)*d2**2*np.exp(-d2**2/(2*ls2))/(np.sqrt(np.pi)*ls2) - np.sqrt(2)*d3**2*np.exp(-d3**2/(2*ls2))/(np.sqrt(np.pi)*ls2) + np.sqrt(2)*d4**2*np.exp(-d4**2/(2*ls2))/(np.sqrt(np.pi)*ls2))/(r1*r2) + np.sqrt(np.pi/2.)*sig2f*(d1*ss.erf(d1/V2ls) - d2*ss.erf(d2/V2ls) + d3*ss.erf(d3/V2ls) - d4*ss.erf(d4/V2ls))/(r1*r2)

        z1 = d1 * ss.erf(d1 / V2ls)
        z2 = d2 * ss.erf(d2 / V2ls)
        z3 = d3 * ss.erf(d3 / V2ls)
        z4 = d4 * ss.erf(d4 / V2ls)
        zz = (z1 - z2 + z3 - z4)

        
        d = np.sqrt(np.pi / 2.) * (2 * sigf) * ls / (r2 * r1)
        dsig = d * zz
        
        return dls, dsig
        # dK_dTh

    if diag:
        l2l, l2r = l1l, l1r
        
        r = l2r - l2l
        
        c1 = np.sqrt(np.pi / 2.) * sig2f * ls / (r ** 2)
        
        d1 = l1r - l2l
        d3 = l1l - l2r
        
        c2 =  d1 * ss.erf(d1 / V2ls)
        c2 += d3 * ss.erf(d3 / V2ls)        
        
        c3 = ls2 / (2 * (r ** 2))
        
        c4 =  np.exp(- (d1 ** 2) / (2 * ls2))
        c4 -= 1.
        c4 += np.exp(- (d3 ** 2) / (2 * ls2))
        c4 -= 1.

        return np.squeeze((c1 * c2) + (c3 * c4))
        # K_diag

    if l2 == None:
        l2l, l2r = l1l.copy().T, l1r.copy().T
    else:
        l2l, l2r = l2[0].T, l2[1].T
        
    r1 = l1r - l1l
    r2 = l2r - l2l

    c1 = np.sqrt(np.pi / 2.) * (sig2f) * ls / (r2 * r1 )
    
    d1 = l1r - l2l
    d2 = l1l - l2l
    d3 = l1l - l2r
    d4 = l1r - l2r
    
    c2 =  d1 * ss.erf(d1 / V2ls)
    c2 -= d2 * ss.erf(d2 / V2ls)
    c2 += d3 * ss.erf(d3 / V2ls)        
    c2 -= d4 * ss.erf(d4 / V2ls)        
    
    c3 = ls2 / (2 * r2 * r1)
    
    c4 =  np.exp(- (d1 ** 2) / (2 * ls2))
    c4 -= np.exp(- (d2 ** 2) / (2 * ls2))
    c4 += np.exp(- (d3 ** 2) / (2 * ls2))
    c4 -= np.exp(- (d4 ** 2) / (2 * ls2))
    
    return (c1 * c2) + (c3 * c4)
    # K

#def p2pPoly1D (self, p1, p2 = None, diag = False, delTh = False):
#    ls = self.lengthscale
#    
#    if p2 == None:
#        p2 = p1
#    r = p2 - p1
#
#    if delTh:
#        ls2 =  ** 2
#        if diag:
#            a = (1. - (1. / ls)) ** 3
#            b = (3. / ls) + 1.
#            da = (3. / ls2) * (1. - (1. / ls))**2
#            db = -3.0 / ls2
#
#            return (a * db) + (b * da)
#
#        a = (1 - (r / ls)) ** 3
#        b = (3 * r / ls) + 1
#        da = (3 * r / ls2) * (1 - r / ls) ** 2
#        db = -3 * r / (ls ** 2)
#        return (a * db) + (b * da)
#        
#    if diag:
#        a = (1. - (1. / ls)) ** 3
#        b = (3. / ls) + 1.
#        return a * b
#
#    if p2 == None:
#        p2 = p1
#    r = p2 - p1
#    
#    a = 1 - (r / ls)
#    b = (3 * r / ls) + 1
#    return (a ** 3) * b