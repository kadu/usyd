# -*- coding: utf-8 -*-
"""
Spyder Editor
"""

from numpy import linalg as la
import numpy as np
import covariance_functions as cf

def _reidKernel(inputs, hyperparameters):
    inputs_vector, targets, test_vector = inputs

    if (inputs_vector == None):
        def format_test(x):
            if x.shape[1] != 1:
                x = [a[0] for a in x]
                return np.asarray([x]).T
            else:
                return x
        p = format_test(test_vector)

        return cf.p2pSqExp1D(p, None, hyperparameters)



    def format_input(x):
        p = []
        ll = []
        lr = []
        for el in x:
            if el[0] == el[1]:
                p.append(el[0])
            else:
                ll.append(min(el))
                lr.append(max(el))
                
        p  = np.reshape( p, (len( p), 1))
        ll = np.reshape(ll, (len(ll), 1))
        lr = np.reshape(lr, (len(lr), 1))        
        
        l = (ll, lr)
        
        return (l, p)

    l, p = format_input(inputs_vector)
    
    if test_vector == None:
        if len(l[0]) == 0: # no input lines
            K = cf.p2pSqExp1D(p, None, hyperparameters)
        elif len(p) == 0: # no input points
            K = cf.l2lSqExp1D(l, None, hyperparameters)
        else:
            kll = cf.l2lSqExp1D(l, None, hyperparameters)
            kpp = cf.p2pSqExp1D(p, None, hyperparameters)
            kpl = cf.p2lSqExp1D(p, l, hyperparameters)
            klp = kpl.T
            
            a = np.concatenate((kll, kpl))
            b = np.concatenate((klp, kpp))
            
            K = np.concatenate((a, b), 1)
    
    else:        
        def format_test(x):
            if x.shape[1] != 1:
                x = [a[0] for a in x]
                return np.asarray([x]).T
            else:
                return x
        x2 = format_test(test_vector)
        
        if len(l[0]) == 0: # no input lines
            Kstar = cf.p2pSqExp1D(p, x2, hyperparameters)
        elif len(p) == 0: # no input points
            Kstar = cf.p2lSqExp1D(x2, l, hyperparameters).T
        else:
            kpx = cf.p2pSqExp1D(p, x2, hyperparameters)
            klx = cf.p2lSqExp1D(x2, l, hyperparameters).T
            Kstar = np.concatenate((klx, kpx))
        return Kstar

    def jitter_cholesky(m, jitter = 1e-7, maxjitter = 1e-3):
        try:
          return la.cholesky(m)
        except la.LinAlgError:
            n = len(m)
            while (jitter <= maxjitter):
                j = jitter * np.eye(n)
                print "WARNING: Adding " + str(jitter) + " jitter"
                try:
                    return la.cholesky(m + j)
                except la.LinAlgError:
                    jitter *= 10
            # if it gets here, it left the loop, meaning adding jitter failed
            error = 'Matrix is not positive definite - '
            error += 'Failed after adding ' + str(jitter / 10) + ' jitter -'
            error += 'Cholesky decomposition cannot be computed -'
            error += 'Try increasing maxjitter.'
            raise la.LinAlgError(error)
                
    L = jitter_cholesky(K)
    '''
    L = la.cholesky(K)
    '''
    alpha = la.solve(L.T, la.solve(L, targets))
    
    return K, L, alpha
    
def _reidDiff(inputs, hyperparameters):

    inputs_vector, targets, test_vector = inputs
    
    def format_input(x):
        p = []
        ll = []
        lr = []
        for el in x:
            if el[0] == el[1]:
                p.append(el[0])
            else:
                ll.append(min(el))
                lr.append(max(el))
                
        p  = np.reshape( p, (len( p), 1))
        ll = np.reshape(ll, (len(ll), 1))
        lr = np.reshape(lr, (len(lr), 1))        
        
        l = (ll, lr)
        
        return (l, p)

    x = format_input(inputs_vector)
    l, p = x

    if test_vector == None:    
        if len(p) == 0:
            return cf.l2lSqExp1D(l, None, hyperparameters, delTh = True)
        
        if len(l[0]) == 0:
            return cf.p2pSqExp1D(p, None, hyperparameters, delTh = True)

        ll = cf.l2lSqExp1D(l, None, hyperparameters, delTh = True)
        pp = cf.p2pSqExp1D(p, None, hyperparameters, delTh = True)
        pl = cf.p2lSqExp1D(p, l, hyperparameters, delTh = True)            
        lp = map(np.transpose, pl)
        
        a = np.concatenate((ll[0], pl[0]))
        b = np.concatenate((lp[0], pp[0]))
        dkdls = np.concatenate((a, b), 1)
        
        a = np.concatenate((ll[1], pl[1]))
        b = np.concatenate((lp[1], pp[1]))
        dkdsig = np.concatenate((a, b), 1)
        
        return dkdls, dkdsig

    else:
        def format_test(x):
            if x.shape[1] != 1:
                x = [a[0] for a in x]
                return np.asarray([x]).T
            else:
                return x
        x2 = format_test(test_vector)

        if len(l[0]) == 0: # no input lines
            return cf.p2pSqExp1D(p, x2, hyperparameters, delTh = True)
        if len(p) == 0: # no input points
            return map(np.transpose, cf.p2lSqExp1D(x2, l, hyperparameters, delTh = True))
    
        px = cf.p2pSqExp1D(p, x2, hyperparameters, delTh = True)
        lx = map(np.transpose, cf.p2lSqExp1D(x2, l, hyperparameters, delTh = True))
        dkdls  = np.concatenate((lx[0], px[0]))
        dkdsig = np.concatenate((lx[1], px[1]))
        
        return dkdls, dkdsig


def _kernel(inputs, covariance_function, hyperparameters=[]):
    inputs_vector, targets, test_vector = inputs
    #n = inputs_vector.size
    #inputs_noise, test_noise = noise
    
    K = covariance_function(inputs_vector, inputs_vector, hyperparameters)
    #'''
    def jitter_cholesky(m, jitter = 1e-7, maxjitter = 1e-3):
        try:
          return la.cholesky(m)
        except la.LinAlgError:
            n = len(m)
            while (jitter <= maxjitter):
                j = jitter * np.eye(n)
                print "WARNING: Adding " + str(jitter) + " jitter"
                try:
                    return la.cholesky(m + j)
                except la.LinAlgError:
                    jitter *= 10
            # if it gets here, it left the loop, meaning adding jitter failed
            error = 'Matrix is not positive definite - '
            error += 'Failed after adding ' + str(jitter / 10) + ' jitter -'
            error += 'Cholesky decomposition cannot be computed -'
            error += 'Try increasing maxjitter.'
            raise la.LinAlgError(error)
                
    L = jitter_cholesky(K)
    '''
    L = la.cholesky(K)
    '''
    alpha = la.solve(L.T, la.solve(L, targets))
    
    return K, L, alpha

def gaussian_process(inputs, covariance_function, hyperparameters):
    '''
    Gaussian process implemented according to Rasmussen & Williams, Gaussian
    Processes for Machine Learning, algorithm 2.1
    '''
        
    inputs_vector, targets, test_vector = inputs
    n = inputs_vector.size
#    inputs_noise, test_noise = noise
    
#    input1 = (inputs_vector, targets, None)
#    K, L, alpha = _reidKernel(input1, hyperparameters)
    K, L, alpha = _kernel(inputs, covariance_function, hyperparameters)

    log_marg_likelihood = -.5 * targets.T.dot(alpha)[0][0]
    log_marg_likelihood -= np.log(L.diagonal()).sum()
    log_marg_likelihood -= ((n / 2.) * np.log(2. * np.pi))

    input2 = inputs    
#    covariance_vectors = _reidKernel(input2, hyperparameters)
    covariance_vectors = _kernel(input2, hyperparameters)
    
    predictive_mean = (covariance_vectors.T).dot(alpha)
    
    v = la.solve(L, covariance_vectors)
    input3 = (None, targets, test_vector)
#    variance = _reidKernel(input3, hyperparameters) - (v.T).dot(v)

    if test_noise != 0:
        variance += np.eye(test_vector.size) * test_noise

    return predictive_mean, variance, log_marg_likelihood